<?php
namespace App\Helpers;

use Mail;
use OneSignal;
use App\Module\Model\User;
use App\Module\Model\Notification;

class UserNotification{

    public static function send_push_notification($message,$playerid,$userid,$for) {

        $notification = Notification::where(['user_id' => $userid,'type' => 2, 'notification_for' => $for])->first();

        if(empty($notification))
        {
            OneSignal::addParams([
                'small_icon' => 'icon',
                'headings' =>  array( "en" => "Helajob Notification" ),
            ])->sendNotificationToUser(
                $message,$playerid
            );
        }else{
            if($notification->status == 1){
                OneSignal::addParams([
                    'small_icon' => 'icon',
                    'headings' =>  array( "en" => "Helajob Notification" ),
                ])->sendNotificationToUser(
                    $message,$playerid
                );
            }
        }
    }

    public static function send_resetpwd_mail($first_name,$email,$url) {
        $userData = [
            'first_name' => ucfirst($first_name),
            'email' => $email,
            'url' => $url,
            'title' => 'Reset Password',
            'mail' => env('MAIL_FROM_ADDRESS', 'ravismartdata@gmail.com'),
        ];
        $check = Mail::send('email.admin.resetpassword', $userData , function ($message)  use ($email){
            $message->to("lalit@yopmail.com");
            $message->subject("Admin-Hola Job : Password Reset");
        });
        return 1;
    }

    public static function send_requestPayout_mail($first_name, $email, $amount) {
        $userData = [
            'first_name' => ucfirst($first_name),
            'email' => $email,
            'amount' => $amount,
            'title' => 'Hela Job - Payout Requested',
            'mail' => env('MAIL_FROM_ADDRESS', 'ravismartdata@gmail.com'),
        ];
        $check = Mail::send('email.payoutrequest', $userData , function ($message)  use ($email){
            $message->to($email);
            $message->subject("Hela Job - Payout Requested");
        });
        return 1;
    }

    public static function send_cancelserviceexpert_mail($data) {
        $email = $data->expert->email;
        $userData = [
            'first_name' => ucfirst($data->expert->first_name),
            'email' => $email,
            'data' => $data,
            'title' => 'Hela Job - Cancel Service',
            'mail' => env('MAIL_FROM_ADDRESS', 'ravismartdata@gmail.com'),
        ];
        $check = Mail::send('email.cancelservice', $userData , function ($message)  use ($email){
            $message->to($email);
            $message->subject("Hela Job - Cancel Service");
        });
        return 1;
    }

    public static function send_cancelservicecustomer_mail($data) {
        $email = $data->customer->email;
        $userData = [
            'first_name' => ucfirst($data->customer->first_name),
            'email' => $email,
            'data' => $data,
            'title' => 'Hela Job - Cancel Service',
            'mail' => env('MAIL_FROM_ADDRESS', 'ravismartdata@gmail.com'),
        ];
        $check = Mail::send('email.cancelservice', $userData , function ($message)  use ($email){
            $message->to($email);
            $message->subject("Hela Job - Cancel Service");
        });
        return 1;
    }

}
