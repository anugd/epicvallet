<?php

namespace App\Module\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use App\Module\Contracts\SmsInterface;
use App\Module\Model\SmsLog;

/**
 * Description of SmsService
 *
 * @author Shweta Trivedi
 */
class SmsService implements SmsInterface {

    /** @var SmsLog */
    protected $log;

    /** @var Client */
    protected $client;
    protected $body = [];
    protected $options = [
        'base_uri' => 'https://api.bulksms.com/v1/',
        'json' => [
//            'from' => '',
            'to' => '',
            'body' => '',
            'encoding' => 'TEXT', //"TEXT","UNICODE","BINARY"
            'routingGroup' => 'STANDARD', // "ECONOMY","STANDARD","PREMIUM",
//          'messageClass' => '', // "FLASH_SMS" "ME_SPECIFIC" "SIM_SPECIFIC" "TE_SPECIFIC"
        ],
        'connect_timeout' => 10,
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ];

    public function __construct(Client $client = null, SmsLog $smslog = null) {
        $this->client = $client ?: new Client();
        $auth = base64_encode(config('sms.token_id') . ':' . config('sms.token_secret'));
        $this->options['headers']['Authorization'] = 'Basic ' . $auth;
        config('from') && ($this->options['json']['from'] = config('from'));
        $this->log = $smslog ?: new SmsLog();
    }

    public function sendSms($contactNumber, $message) {
        $this->options['json']['to'] = $contactNumber;
        $this->options['json']['body'] = $message;
        if (config('sms.live_env')) {
            $resp = $this->client->post('messages', $this->options);
            return $this->handleResponse($resp);
        } else {
            $resp = $this->fakeResponse($this->options);
            return $this->fakehandleResponse($resp);
        }
    }

    protected function handleResponse($res) {
        if (strval(($res->getStatusCode()))[0] == '2') {
            $this->log->insert(['mobile_number' => $this->options['json']['to'], 'payload' => $res->getBody()->getContents()]);
            return true;
        }
        $this->log->insert(['mobile_number' => $this->options['json']['to'], 'status' => 'failed', 'payload' => $res->getBody()->getContents()]);
        return false;
    }

    public function sendOTP($contactNumber, $template = 'login_otp_template') {
        $this->options['json']['to'] = $contactNumber;
        $digits = config('sms.otp_length') ?: 4;
        $otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
        $message = str_replace('#code#', $otp, config('sms.' . $template));
        return $this->sendSms($contactNumber, $message) ? $otp : false;
    }

    protected function fakehandleResponse($res) {
        $this->log->insert(['mobile_number' => $this->options['json']['to'], 'payload' => $res]);
        return true;
    }

    protected function fakeResponse($options) {

        return <<<pre
        [{
  "id" : "680736359320657920",
  "type" : "SENT",
  "from" : "StudtAiders",
  "to" : "{$options['json']['to']}",
  "body" : "{$options['json']['body']}",
  "encoding" : "TEXT",
  "protocolId" : 0,
  "messageClass" : 0,
  "submission" : {
    "id" : "2-00000000001426597199",
    "date" : "2019-02-22T11:23:15Z"
  },
  "status" : {
    "id" : "ACCEPTED.null",
    "type" : "ACCEPTED",
    "subtype" : null
  },
  "relatedSentMessageId" : null,
  "userSuppliedId" : null,
  "numberOfParts" : null,
  "creditCost" : null
}]    
pre;
    }

}
