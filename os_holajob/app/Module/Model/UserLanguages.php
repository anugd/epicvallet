<?php
namespace App\Module\Model;

/**
 * Description of Languages speak by expert
 *
 * @author Shweta Trivedi
 */
class UserLanguages extends BaseModel {
    protected $table = 'user_languages';
    protected $guarded = [];


    public function Language() {
        return $this->hasOne('App\Module\Model\Languages', 'id', 'language_id');
    }
}
