<?php

namespace App\Module\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of User Document
 *
 * @author Shweta Trivedi
 */
class UserDocuments extends BaseModel {

    protected $table = 'user_document';
    protected $guarded = [];

}
