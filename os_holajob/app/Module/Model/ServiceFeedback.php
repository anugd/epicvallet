<?php

namespace App\Module\Model;

/**
 * Description of ServiceImages
 *
 * @author ashutoshsharma
 */
class ServiceFeedback extends BaseModel {
    protected $table = 'service_feedback';
    protected $guarded = [];

    public function Service() {
        return $this->hasOne('App\Module\Model\ServiceRequest', 'id','service_request_id');
    }

    public function Sender() {
        return $this->hasOne('App\Module\Model\User', 'id','sender');
    }

    public function Receiver() {
        return $this->hasOne('App\Module\Model\User', 'id','receiver');
    }
}
