<?php

namespace App\Module\Model;

/**
 * Description of EmergencyContacts
 *
 * @author Shweta Trivedi
 */
class EmergencyContacts {
    protected $table = 'emergency_contacts';
    protected $guarded = [];
}
