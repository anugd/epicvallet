<?php

namespace App\Module\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vouchers extends Model
{

	protected $table = 'vouchers';

    protected $fillable = ['code','name','description','discount_amount','is_fixed', 'starts_at', 'expires_at'];

}
