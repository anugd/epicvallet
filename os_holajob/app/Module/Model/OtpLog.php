<?php
namespace App\Module\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtpLog extends BaseModel {

    protected $table = 'otp_log';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'expires_at'];
    protected $fillable = ['otp','user_id','type','expires_at'];

    public function checkOtpLog($user_id, $otp) {

        return $this->
                        where('user_id', $user_id)
                        //->where('expires_at', '>=', Carbon::now())
                        ->where('otp', $otp)
                        ->where('type', 1)
                        ->orderBy('id', 'desc')
                        ->first();
    }

    public function saveMobileVerificationOtp($user_id, $otp) {
        $expiry = Carbon::now()->addMinute(config('sms.expired_in'));
        return $this->create([
            'user_id' => $user_id,
            'otp' => $otp,
            'type' => 1,
            'expires_at' => $expiry
        ]);
    }

}
