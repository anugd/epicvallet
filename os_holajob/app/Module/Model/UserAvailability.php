<?php namespace App\Module\Model;

/**
 * Description : UserAvailability
 * @author Amit Sharma
 */
class UserAvailability extends BaseModel {
    protected $table = 'user_weekly_availability';
    protected $guarded = [];

}
