<?php

namespace App\Module\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Profile
 *
 * @author Lalit Mohan
 */
class UserAddresses extends BaseModel {

    protected $table = 'user_addresses';
    protected $guarded = [];

}
