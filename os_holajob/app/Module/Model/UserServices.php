<?php namespace App\Module\Model;

/**
 * Description : Services provided by expert
 * @author Shweta Trivedi
 */
class UserServices extends BaseModel {
    protected $table = 'user_services';
    protected $guarded = [];

    /*

    */
    public function User() {
        return $this->hasOne('App\Module\Model\User', 'id', 'user_id');
    }
    public function Services() {
        return $this->hasOne('App\Module\Model\Services', 'id', 'service_id');
    }

    public function Service() {
        return $this->hasOne('App\Module\Model\Services', 'id', 'service_id');
    }

}
