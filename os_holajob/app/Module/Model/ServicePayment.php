<?php

namespace App\Module\Model;

use Illuminate\Http\Request;

/**
 * Description of Services
 *
 * @author Amit Sharma
 */
class ServicePayment extends BaseModel {
	protected $table = 'service_payment';
	protected $guarded = [];

	public function expert() {
        return $this->hasOne('App\Module\Model\User', 'id', 'expert_id');
    }


    public function service() {
        return $this->hasOne('App\Module\Model\ServiceRequest', 'id', 'service_id');
    }
}
