<?php

namespace App\Module\Model;

use Illuminate\Http\Request;

/**
 * Description of Services
 *
 * @author Amit Sharma
 */
class AdminSetting extends BaseModel {
	protected $table = 'admin_settings';
	protected $guarded = [];

}