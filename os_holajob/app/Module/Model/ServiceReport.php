<?php

namespace App\Module\Model;

/**
 * Description of ServiceImages
 *
 * @author amitsharma
 */
class ServiceReport extends BaseModel {
    protected $table = 'service_report';
    protected $guarded = [];

    public function Service() {
        return $this->hasOne('App\Module\Model\ServiceRequest', 'id','service_id');
    }

    public function Expert() {
        return $this->hasOne('App\Module\Model\User', 'id','user_id');
    }
}
