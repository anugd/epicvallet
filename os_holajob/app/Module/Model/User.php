<?php

namespace App\Module\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Description of User
 *
 * @author ashutoshsharma
 */
class User extends Authenticatable {

    use Notifiable;

    protected $guarded = [];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname','first_name','last_name','username', 'email', 'password','user_type_id','mobile_number','otp_id','provider','provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


     /**
     * Get the profile of user
     */
    public function profile() {
        return $this->hasOne('App\Module\Model\Profile', 'user_id', 'id');
    }

    /**
     * Get the addresses of user
     */
    public function addresses() {
        return $this->hasMany('App\Module\Model\UserAddresses', 'user_id', 'id');
    }

    public function address() {
        return $this->hasOne('App\Module\Model\UserAddresses', 'user_id', 'id');
    }

    /**
     * Get User's Documents
     */
    public function documents() {
        return $this->hasMany('App\Module\Model\UserDocuments', 'user_id', 'id');
    }

    /**
     * Get User's Services
    */

    public function UserServices() {
        return $this->hasMany('App\Module\Model\UserServices', 'user_id', 'id');
    }

    /**
     * Get User's Languages
    */

    public function UserLanguages() {
        return $this->hasMany('App\Module\Model\UserLanguages', 'user_id', 'id');
    }

    /**
     * Get User's Languages
    */

    public function UserDocuments() {
        return $this->hasOne('App\Module\Model\UserDocuments', 'user_id', 'id');
    }

    /**
     * Get Expert's Address
    */
    public function ExpertAddress() {
        return $this->hasOne('App\Module\Model\UserAddresses', 'user_id', 'id');
    }

    // Get user profile picture
    public function photo()
    {
        if(!empty($this->profile_image))
        {
            if (file_exists( public_path() . '/avatars/'. $this->profile_image )) {
                return '/avatars/' .$this->profile_image;
            } else {
                return '/img/download.jpeg';
            }
        }else{
            return '/img/download.jpeg';
        }
    }
}
