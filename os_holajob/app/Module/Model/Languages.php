<?php
namespace App\Module\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * Description : All Languages
 * @author Shweta Trivedi
 */
class Languages extends BaseModel {
    protected $table = 'languages';
    protected $guarded = [];
}
