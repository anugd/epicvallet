<?php 
namespace App\Module\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description : RelationShip
 * @author Amit Sharma
 */
class RelationShip extends BaseModel {
    protected $table = 'relationship';
    protected $guarded = [];
}
