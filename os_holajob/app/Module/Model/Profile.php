<?php

namespace App\Module\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Profile
 *
 * @author Shweta Trivedi
 */
class Profile extends BaseModel {

    protected $table = 'profiles';

    protected $guarded = [];

    protected $fillable = ['user_id','company_name','company_registration_no','company_no','company_address'];

    /**
     * Get the profile of user
    */

    public function User() {
        return $this->hasOne('App\Module\Model\User', 'id', 'user_id');
    }


}
