<?php namespace App\Module\Model;

/**
 * Description of ExpertAvailablity
 *
 * @author Shweta Trivedi
 */
class ExpertAvailablity extends BaseModel {
    protected $table = 'expert_availablity';
    protected $guarded = [];
}
