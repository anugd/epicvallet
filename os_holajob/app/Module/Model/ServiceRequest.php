<?php

namespace App\Module\Model;

use Illuminate\Http\Request;

/**
 * Description of Services
 *
 * @author Shweta Trivedi
 */
class ServiceRequest extends BaseModel {
	protected $table = 'service_request';
	protected $guarded = [];

	public function User() {
		return $this->hasOne('App\Module\Model\User', 'id', 'user_id');
	}

	public function expert() {
        return $this->hasOne('App\Module\Model\User', 'id', 'expert_id');
    }

    public function customer() {
        return $this->hasOne('App\Module\Model\User', 'id', 'user_id');
    }

    public function service() {
        return $this->hasOne('App\Module\Model\Services', 'id', 'service_id');
    }

    public function ServiceLocation() {
        return $this->hasOne('App\Module\Model\UserAddresses', 'user_id', 'user_id');
    }

    public function ServiceFeedback() {
        return $this->hasOne('App\Module\Model\ServiceFeedback', 'service_request_id','id');
    }

    public function ServicePayment() {
        return $this->hasOne('App\Module\Model\ServicePayment', 'service_id','id');
    }
}
