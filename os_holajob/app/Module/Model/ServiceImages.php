<?php

namespace App\Module\Model;

/**
 * Description of ServiceImages
 *
 * @author ashutoshsharma
 */
class ServiceImages extends BaseModel {
    protected $table = 'service_images';
    protected $guarded = [];
}
