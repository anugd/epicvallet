<?php namespace App\Module\Model;
use Carbon\Carbon;

class Payouts extends BaseModel {

    protected $table = 'payouts';

    public function User() {
        return $this->hasOne('App\Module\Model\User', 'id', 'user_id');
    }
}
