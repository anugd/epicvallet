<?php

namespace App\Module\Model;

use Illuminate\Http\Request;

/**
 * Description of Services
 *
 * @author Shweta Trivedi
 */
class Services extends BaseModel {

    protected $table = 'services';

    protected $fillable = [
        'parent_id',
        'language_id',
        'service',
        'price',
        'service_image',
        'service_icon',
        'status',
    ];

    protected $guarded = [];

    public function find(Request $request) {
        if ($request->has('id')) {
            return $this->where('parent_id', $request->id)->get()->toArray();
        }
        return $this->get()->toArray();
    }

    public function createService($data) {
        return $this->create($data);
    }

    public function updateService($data) {
        $id = $data['id'];
        unset($data['id']);
        return $this->where('id', $id)->update($data);
    }

    public function deleteService($id) {
        $m = $this->find($id);
        return $m ? $m->delete() ? $m : false : false;
    }
     /**
     * Get all main category and sub-categories
     * @author Shweta Trivedi
     */
    public function getParentData(Request $request) {
        if ($request->has('id')) {
            return $this->where('parent_id', $request->id)->get()->toArray();
        }
        return $this->where('parent_id', 0)->get()->toArray();
    }

    /**
     * Get Parent Services
    */

    public function ServiceParent() {
        return $this->hasMany('App\Module\Model\Services', 'id', 'parent_id');
    }

    public function Language() {
        return $this->hasOne('App\Module\Model\Languages', 'id', 'language_id');
    }

    public function Services() {
        return $this->hasMany('App\Module\Model\ServiceRequest', 'service_id', 'id');
    }
}
