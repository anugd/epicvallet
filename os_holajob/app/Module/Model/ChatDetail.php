<?php namespace App\Module\Model;

/**
 * Description : Chat Detail
 * @author Amit Sharma
 */
class ChatDetail extends BaseModel {
    protected $table = 'chat_details';
    protected $guarded = [];

    public function Sender() {
        return $this->hasOne('App\Module\Model\User', 'id', 'sender');
    }

    public function User() {
        return $this->hasOne('App\Module\Model\User', 'id', 'sender');
    }

    public function Receiver() {
        return $this->hasOne('App\Module\Model\User', 'id', 'receiver');
    }
}
