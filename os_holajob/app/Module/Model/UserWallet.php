<?php

namespace App\Module\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Profile
 *
 * @author Shweta
 */
class UserWallet extends BaseModel {
    protected $table = 'user_wallet';
    protected $guarded = [];
}