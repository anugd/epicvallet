<?php namespace App\Module\Model;

/**
 * Description : Chat Module
 * @author Amit Sharma
 */
class Chat extends BaseModel {
    protected $table = 'chat';
    protected $guarded = [];

    /*
		Messagess
    */

    public function Message() {
        return $this->hasOne('App\Module\Model\ChatDetail', 'chat_id', 'id')->orderBy('_id', 'desc')->limit(1);
    }
    public function Sender() {
        return $this->hasOne('App\Module\Model\User', 'id', 'sender');
    }
    public function Receiver() {
        return $this->hasOne('App\Module\Model\User', 'id', 'receiver');
    }
}
