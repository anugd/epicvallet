<?php

namespace App\Module\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Profile
 *
 * @author Shweta
 */
class UserCard extends BaseModel {
    protected $table = 'cards';
    protected $guarded = [];
}