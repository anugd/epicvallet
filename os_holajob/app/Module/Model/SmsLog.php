<?php namespace App\Module\Model;

class SmsLog extends BaseModel {
    protected $table = 'sent_sms_log';
    protected $timestamp = false;
    protected $guarded = [];
}
