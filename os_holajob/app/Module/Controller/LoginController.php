<?php

namespace App\Module\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Module\Model\User;
use \App\Module\Model\Profile;
use Validator;
use JWTFactory;
use JWTAuth;
use OneSignal;

class LoginController extends Controller
{
     /**
     * To validate user
     * @author Shweta
     * @param Request $request
     */
    
    public function login(Request $request)
    {
       $data = json_decode($request->getContent(), true);
       $validator = Validator::make($data, [
            'email' => 'required|string|email|max:255',
            'password'=> 'required',
            'user_type_id' => 'required'
        ]);
        if ($validator->fails()) {
             return response()->json([
                'requestStatus' => 'invalid',
                'message' => implode('<br />', $validator->errors()->all())
            ], 422);
        }
        $detail = User::with(['profile','addresses','documents'])->where(['email'=> $data['email'],'is_deleted' => '0', 'user_type_id' => $data['user_type_id']])->first();
        if($detail){
            try {
                if(!$token = JWTAuth::attempt($data)) {
                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => 'Invalid Credentials'], 401);
                }
            } catch (JWTException $e) {
                return response()->json([
                        'requestStatus' => 'error',
                        'message' => 'Could not create token'], 500);
            }

            if($detail->user_type_id == 2){
                if($detail->admin_verified == 1){
                    // if($detail->is_logged == 1){
                    //     return response()->json([
                    //         'requestStatus' => 'invalid',
                    //         'message' => 'Your account is already logged in on another device.'], 401);
                    // }
                    if($detail->status == 0){
                        return response()->json([
                            'requestStatus' => 'invalid',
                            'message' => trans('messages.account_not_active')], 401);
                    }else{
                        User::where('id', $detail->id)->update(['is_logged' => 1]);
                        return response()->json([
                            'requestStatus' => 'success',
                            'token' => $token,
                            'data' => $detail,
                        ]);
                    }
                }
                elseif ($detail->admin_verified == 2) {
                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => trans('messages.account_rejected')], 401);
                }
                else{
                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => trans('messages.account_not_active')], 401);
                }
            }else{
                User::where('id', $detail->id)->update(['is_logged' => 1]);
                return response()->json([
                    'requestStatus' => 'success',
                    'token' => $token,
                    'data' => $detail,
                ]);
            }

        }else{
            return response()->json([
                'requestStatus' => 'fail',
                'message' => trans('messages.no_user_found'),
            ]);
        }
    }
}
