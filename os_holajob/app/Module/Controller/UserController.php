<?php

namespace App\Module\Controller;

use Illuminate\Http\Request;
use App\Module\Service\SmsService;
use Carbon\Carbon;
use App\Module\BaseResponse;
use \App\Module\Model\User;
use \App\Module\Model\OtpLog;
use \App\Module\Model\Profile;
use App\Module\Model\ServiceRequest;
use App\Module\Model\ServiceFeedback;
use App\Module\Model\ServicePayment;
use App\Module\Model\UserAddresses;
use App\Module\Model\UserLanguages;
use App\Module\Model\UserServices;
use App\Module\Model\UserAvailability;
use App\Module\Model\UserCoupons;
use App\Module\Model\Faq;
use App\Module\Model\FaqCategory;
use App\Module\Model\Cms;
use GuzzleHttp\Client;
use Validator;
use Storage;
use Datatables;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
use Huddle\Zendesk\Services\ZendeskService;
use App\Module\Model\UserDocuments;
use App\Module\Model\Vouchers;
use \App\Module\Model\UserWallet;
use App\Http\Controllers\Auth\ForgotPasswordController;

/**
 * Description of UserController
 *
 * @author Shweta Trivedi
 */

class UserController extends BaseController {

    /**
     * __construct
     *
     * @param $ZendeskService
     * @author Amit Sharma
     * @return 
     */

    public function __construct(ZendeskService $zendesk_service) {
        $this->zendesk_service = $zendesk_service;
        if (config('app.MODE') == 'live') {
            \Stripe\Stripe::setApiKey(config('app.STRIPE_LIVE_KEY'));
            $this->stripeKey = config('app.STRIPE_LIVE_KEY');
        } else {
            \Stripe\Stripe::setApiKey(config('app.STRIPE_TEST_KEY'));
            $this->stripeKey = config('app.STRIPE_TEST_KEY');
        }
    }

    /**
     * registerUser
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function registerUser(Request $request) {
        $validated_data = json_decode($request->getContent(), true);
        $validator = Validator::make($validated_data, [
            'mobile_number' => 'required|min:10|numeric|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'fullname' => '',
            'username' => 'required',
            'password' => 'required',
            'user_type_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => $validator->errors()->first()
                ], 422);
        }
        $model = '';$otp = '';
        try {
            \DB::transaction(function () use (&$model, $validated_data, &$otp) {
                $userData['fullname']=$validated_data['fullname'];
                $userData['username']=$validated_data['username'];
                $userData['email']=$validated_data['email'];
                $userData['mobile_number']= $validated_data['mobile_number'];
                $userData['password'] = bcrypt($validated_data['password']);
                $userData['user_type_id'] = $validated_data['user_type_id'];
                $userData['password']= bcrypt($validated_data['password']);
                $model = User::create($userData);
                if (!empty($validated_data['company_name'])) {
                    $profile = new Profile();
                    $profile->user_id = $model['id'];
                    $profile->company_name = $validated_data['company_name'];
                    $profile->company_registration_no = $validated_data['company_registration_no'];
                    $profile->company_phone = $validated_data['mobile_number'];
                    $profile->company_address1 = $validated_data['company_address1'];
                    $profile->company_address2 = $validated_data['company_address2'];
                    $profile->save();
                }
                //$otp = "123456";//To test API when Error “Insufficient-Credits” in Bulksms
                $otp = $this->sendOTP($model);
                (new OtpLog())->saveMobileVerificationOtp($model['id'], $otp);
                $data =  $model->toArray();
                $data['otp'] = $otp ;
                $model->verification_token = $this->sendMail($data);
                $model->save();
            });
            $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($model);
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => trans('messages.registration_success'),
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => 3600,
                'data' => array('OTP'=>$otp, 'user_id'=>$model['id']),
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * User Logout Logic
    */

    public function logout(Request $request){
        $data = json_decode($request->getContent(), true);
        $request->validate([
            'token' => 'required'
        ]);
        try{
           \Tymon\JWTAuth\Facades\JWTAuth::invalidate($data['token']);
            User::where('id', $data['user_id'])->update(['is_logged' => 0,'device_token' => '','player_id' =>'']);
            return response()->json([
                'requestStatus' => true,
                'message' => trans('messages.success')
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => trans('messages.something_wrong')
            ], 500);
        }
    }

    /**
     * @param Request $request
     * User getAuthUser Logic
    */

    public function getAuthUser(Request $request){
        try {
            $token = $request->header('Authorization');
            $user = \Tymon\JWTAuth\Facades\JWTAuth::authenticate($token);
            return $user;
        } catch (JWTException $exception) {
            return false;
        }
    }

    /**
     * send email to user
     *
     * @param $ZendeskService
     * @author Amit Sharma
     * @return true
     */

    private function sendMail($user) {
        $validated_data = $user ;
        $verification = \Illuminate\Support\Str::random(45);
        $validated_data['verification_token'] = $verification;
        \Mail::to($validated_data['email'])->send(new \App\Module\Mail\EmailVerification($validated_data));
        return $verification;
    }

    /**
     * @param Request $request
     * User sendForgetPassMail Logic
    */

    private function sendForgetPassMail($user) {
        $validated_data = $user ;
        $verification = \Illuminate\Support\Str::random(45);
        $validated_data['remember_token'] = $verification;
        \Mail::to($validated_data['email'])->send(new \App\Module\Mail\ForgetPassEmailVerification($validated_data));
        return $verification;
    }

    /**
     * @param Request $request
     * User sendOTP Logic
    */

    private function sendOTP($user) {
        return (new SmsService())->sendOTP($user->mobile_number);
    }

    /**
     * @param Request $request
     * User validateUser Logic
    */

    protected function validateUser(Request $request) {
        $rule = [
            'email' => 'required|string|email|max:255|unique:users',
            'fullname' => '',
            'mobile_number' => 'required|min:10|numeric',
            'username' => 'required',
            'password' => 'required',
            'user_type_id' => 'required'
        ];
        return $request->validate($rule);
    }

    /**
     * @param Request $request
     * User respondWithToken Logic
    */

    protected function respondWithToken($token) {
        return [
            'requestStatus' => 'success',
            'token' => $token,
            'message' => trans('messages.registration_success'),
            'Authorization' => $token,
            'token_type' => 'bearer',
            'expires_in' => 3600
        ];
    }

    /**
     * @param Request $request
     * User validateOtp Logic
    */

    public function validateOtp(Request $request) {
        $token = $request->header('Authorization');
        if(empty($token)){
           return response()->json([
                'requestStatus' => 'error',
                'message' => 'Access Token is required'
            ], 500);
        }
        $authUser = auth()->user();
        if(!$authUser){
            return response()->json([
                'requestStatus' => 'error',
                'message' => 'Invalid User'
            ], 500);
        }
        $user_id = 0;
        if ($request->email && is_scalar($request->email)) {
            $user = User::where('email', $request->email)->first();
            if (is_null($user)) {
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => trans('messages.invalid_otp')
                ]);
            }
            $user_id = $user->id;
        } else {
            $user_id = $request->user_id;
            $user = User::where('id', $request->user_id)->first();
        }

        if ($user_id == 0) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.invalid_user')
            ]);
        }
        $log = (new OtpLog())->checkOtpLog($user_id, $request->otp);
        if ($log) {
            if (Carbon::now() < $log->expires_at) {
                if($user->user_type_id == 2)
                {
                    $profile_complete_step = 4;
                    $is_profile_complete = 0;
                }
                else
                {
                    $profile_complete_step = 2;
                    $is_profile_complete = 1;
                }
                $user->mobile_verified = 1;
                $user->profile_complete_step = $profile_complete_step;
                $user->is_profile_complete = $is_profile_complete;
                $user->status = 1;
                $user->save();
                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'message' => trans('messages.otp_verified'),
                    'data' =>$user,
                    'authUser'=>$authUser
                ], 201);
            }
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.otp_expired')
            ], 400);
        }
        /**
         * Commented OTP code
         * Error “Insufficient-Credits” in Bulksms API
         */
        return BaseResponse::response([
            'requestStatus' => 'error',
            'message' => trans('messages.otp_invalid')
        ], 406);
    }

    /**
     * @param Request $request
     * User validateEmail Logic
    */

    public function validateEmail($token) {
        $token = $request->header('Authorization');
        if(empty($token)){
           return response()->json([
                'requestStatus' => 'error',
                'message' => trans('messages.access_token_required')
            ], 500);
        }
        $authUser = auth()->user();
        if(!$authUser){
            return response()->json([
                'requestStatus' => 'error',
                'message' => trans('messages.invalid_user')
            ], 500);
        }

        $user = User::where('verification_token', $token)->take(1)->first();
        if ($user && $token) {
            $user->verification_token = '';
            $user->email_verified = 1;
            if ($user->save()) {
                return view('email.common.email-verification', ['user' => $data, 'verified' => 1]);
            }
            return view('email.common.email-verification', ['verified' => 0, 'error' => 'Internal Server Error.']);
        }
        return view('email.common.email-verification', ['verified' => 0, 'error' => 'Token Expired or Not Exist']);
    }

    /**
     * forgot password
     *
     * @param $request
     * @author Amit Sharma
     * @return 
     */

    public function forgotPassword(Request $request){
        $validated_data = json_decode($request->getContent(), true);
        $validator = Validator::make($validated_data, [
            'email' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => implode('<br />', $validator->errors()->all())
            ], 422);
        }

        $user = User::where('email', $validated_data['email'])->first();
        if (empty($user)) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => trans('messages.email_mobile_not_found')
            ], 401);
        }
        $otp =  (new SmsService())->sendOTP($user->mobile_number);
        $otp_expiry_time =  date('Y-m-d H:i:s', strtotime("+3 min"));
        try {
            $forgotPasswordController = new ForgotPasswordController;
            $brokerToken = $forgotPasswordController->generateToken($user);
            (new OtpLog())->saveMobileVerificationOtp($user['id'], $otp);
            $user['otp'] = $otp;
            $user['brokerToken'] = $brokerToken;
            $remember_token = $this->sendForgetPassMail($user);
            User::where('id', $user['id'])->update(array('remember_token' => $remember_token));
            $otpLog = OtpLog::create([
                'otp' =>  $otp,
                'user_id' =>$user['id'],
                'type' =>  3,
                'expires_at' => $otp_expiry_time,
            ]);
            return response()->json([
                'requestStatus' => 'success',
                'message'=> trans('messages.email_reset')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => trans('messages.something_wrong')
            ], 401);
        }
    }

    /**
     * resend OTP
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function resendOTP(Request $request){
        $token = $request->header('Authorization');
        if(empty($token)){
            return response()->json([
                'requestStatus' => 'error',
                'message' => trans('messages.access_token_required')
            ], 500);
        }
        $authUser = auth()->user();
        if(!$authUser){
            return response()->json([
                'requestStatus' => 'error',
                'message' => trans('messages.invalid_user')
            ], 500);
        }
        //validated data
        $validated_data = json_decode($request->getContent(), true);
        $user = User::where('id', $validated_data['user_id'])->first();
        if(!empty($user)){
            //$otp = "123456";//To test API when Error “Insufficient-Credits” in Bulksms
            $otp = (new SmsService())->sendOTP($user['mobile_number']);
            $otp_expiry_time =  date('Y-m-d H:i:s', strtotime("+3 min"));
             try {
                (new OtpLog())->saveMobileVerificationOtp($user['id'], $otp);
                $user['otp'] = $otp ;
                // $remember_token = $this->sendForgetPassMail($user);
                // User::where('id', $user['id'])->update(array('remember_token' => $remember_token));
                $otpLog = OtpLog::create([
                    'otp' =>  $otp,
                    'user_id' =>$user['id'],
                    'type' =>  3,
                    'expires_at' => $otp_expiry_time,
                ]);
                return response()->json([
                    'requestStatus' => 'success',
                    'message'=> trans('messages.otp_sent')
                ]);
            } catch (\Exception $e) {
                $error_message = $e->getMessage();
                return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => trans('messages.something_wrong')
                ], 401);
            }
        }else{
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => trans('messages.not_found')
            ], 401);
        }
    }

    /**
     * social login api
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function authSocial(Request $request){
        $data = json_decode($request->getContent(), true);
        $usert = array(1, 2, 3);
        if(!isset($data['provider']) || empty($data['provider'])){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.provider_required'),
            ],200);
        }else{
            $provider = $data['provider'];
        }

        if(!isset($data['token']) || empty($data['token'])){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.token_required'),
            ],200);
        }else{
            $token = $data['token'];
        }

        if(!isset($data['user_type']) || empty($data['user_type'])){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.user_type_required')
            ],200);
        }else{
            $usertype = $data['user_type'];
            if (!in_array($usertype, $usert)){
                return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.user_type_required')
                ],200);
            }
        }


        if($provider == "google"){
            $user = $token;
        }else if($provider == "facebook"){
            $url = 'https://graph.facebook.com/v2.5/me?fields=email,first_name,last_name,name,picture&access_token='.$token;
            $content = @file_get_contents($url);
            if($content === FALSE) {
              return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.invalid'),
              ],200);
            }
            $user = json_decode($content, true);
        }else{
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.invalid')
            ],200);
        }

        if(!isset($user['email']) || empty($user['email'])){
            return BaseResponse::response([
                'requestStatus' => 'success',
                'email_is' => 0,
                'message' => trans('messages.email_required'),
                'data' => $user
            ],200);
        }

        $dbuser=User::where(['email'=>$user['email'], 'user_type_id'=>$data['user_type']])->first();
        if (!empty($dbuser)) {
            $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($dbuser);
            $data = User::with('profile','addresses','documents')->where('id', $dbuser['id'])->first()->toArray();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'email_is' => 1,
                'message' => trans('messages.email_exist'),
                'token' => $token,
                'data' => $data
            ],200);
        }else{
            if($provider == "google"){
                $result = $this->registerGoogleUser($user,$usertype);
                return $result;
            }else if($provider == "facebook"){
                $result = $this->registerFacebookUser($user,$usertype);
                return $result;
            }else{
                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'email_is' => 2,
                    'message' => trans('messages.invalid'),
                ],200);
            }
        }
    }

    /**
     * get google user detail
     *
     * @param $user, $usertype
     * @author Amit Sharma
     * @return $data
     */

    public function registerGoogleUser($user,$usertype){
        $namearr = explode(" ",  $user['name']);
        $userData['first_name'] = isset($namearr[0]) && !empty($namearr[0]) ? $namearr[0] : "";
        $userData['last_name'] = isset($namearr[1]) && !empty($namearr[1]) ? $namearr[1] : "";
        $userData['fullname']= !empty($user['name'])?$user['name']: "";
        $userData['email'] = !empty($user['email'])?$user['email']:"";
        $userData['user_type_id'] = $usertype;
        $imgname = "";
        if(isset($user['photo']) && !empty($user['photo'])){
            $url = $user['photo'];
            $contents =  file_get_contents($url);
            $basename = basename($url);
            $imgname = time().$basename;
            Storage::disk('user')->put($imgname, $contents);
        }
        $userData['profile_image'] = $imgname;
        $model = User::create($userData);
        if (!empty($model)) {
            $profile = new Profile();
            $profile->user_id = $model['id'];
            $profile->google_id = !empty($user['id']) ? $user['id'] : "";
            $profile->save();
        }
        $tokenData = User::where('id', $model['id'])->first();
        if(!empty($tokenData)){
            $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($tokenData);
        }else{
            $token = "";
        }

        $response = array(
            'user_id' => $model['id'],
            'name' => $userData['fullname'],
            'email' => $userData['email'],
            'picture' => $imgname
        );
        return BaseResponse::response([
            'requestStatus' => 'success',
            'email_is' => 2,
            'message' => trans('messages.user_success'),
            'token' => $token,
            'data' => $response
        ],200);
    }

    /**
     * get facebook user detail
     *
     * @param $user, $usertype
     * @author Amit Sharma
     * @return $data
     */

    public function registerFacebookUser($user,$usertype){
        $namearr = explode(" ",  $user['name']);
        $userData['first_name']= isset($namearr[0]) && !empty($namearr[0]) ? $namearr[0] : "";
        $userData['last_name']= isset($namearr[1]) && !empty($namearr[1]) ? $namearr[1] : "";
        $userData['fullname']= !empty($user['name'])? $user['name'] : "";
        $userData['email']= !empty($user['email'])?$user['email']:"";
        $userData['user_type_id'] = $usertype;
        $basename = "";
        if(isset($user['picture']) && !empty($user['picture'])){
            $url = $user['picture']['data']['url'];
            $contents =  file_get_contents($url);
            $basename = time().$userData['fullname'].".jpg";
            Storage::disk('user')->put($basename, $contents);
        }
        $userData['profile_image'] = $basename;
        $model = User::create($userData);
        if (!empty($model)) {
            $profile = new Profile();
            $profile->user_id = $model['id'];
            $profile->facebook_id = $user['id'];
            $profile->save();
        }
        $tokenData = User::where('id', $model['id'])->first();
        if(!empty($tokenData)){
            $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($tokenData);
        }else{
            $token = "";
        }
        $resposne = array(
            'user_id' => $model['id'],
            'name' => $userData['fullname'],
            'email' => $userData['email'],
            'picture' => $basename
        );
        return BaseResponse::response([
            'requestStatus' => 'success',
            'email_is' => 2,
            'message' => trans('messages.user_success'),
            'token' => $token,
            'data' => $resposne
        ],200);
    }

    /**
     * get user detail
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function getUserDetail(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $result = User::with('UserServices','UserServices.Services','addresses','profile','documents')->where('id', $data['user_id'])->get();
            $requestedServices = ServiceRequest::where('user_id' , $data['user_id'])->count();
            $averageRating = ServiceFeedback::where('receiver' , $data['user_id'])->avg('rate');
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $result,
                'totalServices' => $requestedServices,
                'averageFeedback' => ($averageRating) ? round($averageRating,2) * 100 : '0'
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * get expert detail
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function getExpertDetail(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $result = User::with('UserServices','UserServices.Services','addresses','profile','documents')->where('id', $data['user_id'])->get();
            $workExperience = ServicePayment::where('expert_id',$data['user_id'])->sum('job_duration');;
            $recentFeedbacks = ServiceFeedback::with('Sender')->where('receiver',$data['user_id'])->get();
            $averageFeedback = ServiceFeedback::where('receiver',$data['user_id'])->avg('rate');
            $userServices = UserServices::where('user_id' , $data['user_id'])->get();
            $userLanguages = UserLanguages::where('user_id' , $data['user_id'])->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $result,
                'workExperience' => $workExperience,
                'recentFeedbacks' => $recentFeedbacks,
                'userServices' => $userServices,
                'userLanguages' => $userLanguages,
                'averageFeedback' => ($averageFeedback) ? round($averageFeedback,2) * 100 : '0'
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * User getUserListing Logic
    */

    public function getUserListing(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $type = $data['type'];
            $limit = $data['limit'];
            $page = $data['page'];
            $offset = ($page-1) * $limit;
            $query = User::with('addresses','profile','documents')->where('user_type_id', $type)->where('status', 1);
            $totalRecord = $query->get()->count();
            $data = $query->offset($offset)->limit($limit)->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data,
                'total' => $totalRecord,
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * update device token
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function updateDeviceToken(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $user_id = $data['user_id'];
            $device_type = $data['device_type'];
            $device_token = $data['device_token'];
            $playerId = $data['player_id'];
            User::where('id', $user_id)->update([
                'device_type' => $device_type,
                'device_token' => $device_token,
                'player_id' => $playerId
            ]);
            return BaseResponse::response([
                'requestStatus' => 'success'
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * save user location
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function addLocation(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $address = new UserAddresses;
            $address->user_id =  $data['user_id'];
            $address->location_name =  $data['location_name'];
            $address->postal_code =  $data['postal_code'];
            $address->address_line1 =  $data['street_address'];
            $address->address_line2 =  $data['street_address1'];
            $address->latitude =  $data['latitude'];
            $address->current_latitude =  $data['latitude'];
            $address->longitude =  $data['longitude'];
            $address->current_longitude =  $data['longitude'];
            $address->is_home =  $data['is_home'];
            $address->is_job =  $data['is_job'];
            $address->is_fav =  $data['is_fav'];
            $address->save();
            return BaseResponse::response([
                'requestStatus' => 'success'
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * User listLocations Logic
    */

    public function listLocations(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $result = UserAddresses::where(['user_id'=> $data['user_id'],'is_primary' => '0'])->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $result
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * User deleteLocations Logic
    */

    public function deleteLocations(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $result = UserAddresses::where('id', $data['location_id'])->delete();
            return BaseResponse::response([
                'requestStatus' => 'success'
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * shuftipro kyc
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function shuftiProKYC(Request $request) {
        $data = json_decode($request->getContent(), true);
        $user_id = $data['user_id'];
        $userDetail = User::with(['profile'])->where('id', $user_id)->first();
        if($userDetail){
            $document_front = 'document/'.rand(1,9999999).time().".png";
            $path = public_path().'/'.$document_front;
            Image::make(file_get_contents($data['document_front']))->save($path);
            // $document_back = 'document/'.rand(1,9999999).time().".png";
            // $path = public_path().'/'.$document_back;
            // Image::make(file_get_contents($data['document_back']))->save($path);
            $face = 'document/'.rand(1,9999999).time().".png";
            $path = public_path().'/'.$face;
            Image::make(file_get_contents($data['face']))->save($path);

            $userDocument['user_id'] = $user_id;
            $userDocument['front_image'] = $document_front;
            // $userDocument['back_image'] = $document_back;
            $userDocument['face'] = $face;
            UserDocuments::create($userDocument);
            $url = env("SHUFTIPRO_URL");
            $client_id = env("SHUTRIPRO_CLIENTID");
            $secret_key = env("SHUTRIPRO_KEY");
            $dob = date('Y-m-d', strtotime($userDetail->profile->dob));
            $verification_services = [
                'document_type' => $data['document_type'],
                'document_id_no' => $data['document_id_no'],
                'document_expiry_date' => $data['document_expiry_date'],
                'first_name' => $userDetail->first_name,
                'last_name' => $userDetail->last_name,
                'dob' => $dob,
                'background_checks' => '1'
            ];
            $verification_services = json_encode($verification_services);
            $document_front = $data['document_front'];
            $face_image = $data['face'];
            $verification_data = array(
                "face_image"  => $face_image,
                "document_front_image"  => $document_front
            );
            $verification_data = json_encode($verification_data);
            $ref = "ref-" . rand(1000,100000);
            $post_data = [
                'client_id' => $client_id,
                "verification_mode" => "image_only",
                'reference' => $ref,
                'country' => (!empty($data['country'])) ? $data['country'] : 'GB',
                'redirect_url' => 'http://34.211.31.84:7039',
                'verification_services' => $verification_services,
                'verification_data' => $verification_data
            ];
            ksort($post_data);
            $raw_data = implode("", $post_data) . $secret_key;
            $signature = hash("sha256", $raw_data);
            $post_data["signature"] = $signature;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($response);
            if(isset($response->message)){
                if($response->message == 'Verified'){
                    User::where('id', $user_id)->update(array('shuftipro_verified' => 1));
                    return BaseResponse::response([
                        'requestStatus' => 'success',
                        'message' => $response->message
                    ], 201);
                }else{
                    return BaseResponse::response([
                        'requestStatus' => 'success',
                        'message' => $response->message
                    ], 201);
                }
            }else{
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => $response->error->message
                ], 500);
            }
        }else{
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.not_found')
            ], 500);
        }
    }

    /**
     * shuftipro KYC address
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function shuftiProKYC_Address(Request $request) {
        $data = json_decode($request->getContent(), true);
        $user_id = $data['user_id'];
        $userDetail = User::with(['profile','address'])->where('id', $user_id)->first();
        if($userDetail){
            $url = env('SHUFTIPRO_URL');
            $client_id  = 'kniyk2OkwofcF85xmD5v0tFPe8cpl2FnnxBpBMqKQUXjPzIfqK1556172691';
            $secret_key = '$2y$10$XN1An4GJc0RsNAll/NxCEuDK65Jl1xNkZUZBhiauu.acl/gxYxuUi';
            $dob = date('Y-m-d', strtotime($userDetail->profile->dob));
            $document_front = base64_encode(file_get_contents($data['document_front']));
            $verification_request = [
                'reference'    => 'ref-'.rand(4,444).rand(4,444),
                'country'      => 'GB',
                'language'     => 'EN',
                'email'        => $userDetail->email,
                'callback_url' =>  env('APP_URL'),
                'verification_mode' => 'any',
            ];
            $verification_request['document'] =[
                'proof' => $document_front,
                'additional_proof' => $document_front,
                'name' => [
                    'first_name' => $userDetail->first_name,
                    'last_name'  => $userDetail->last_name,
                    'fuzzy_match' => '1'
                ],
                'dob'             => $dob,
                'document_number' => $data['document_id_no'],
                'expiry_date'     => $data['document_expiry_date'],
                'issue_date'      => $data['document_issue_date'],
                'supported_types' => [$data['document_type']]
            ];
            $verification_request['address'] = [
                'proof' => $document_front,
                'name' => [
                    'first_name' => $userDetail->first_name,
                    'last_name'  => $userDetail->last_name,
                    'fuzzy_match' => '1'
                ],
                'full_address'    => $userDetail->address->address_line1,
                'address_fuzzy_match' => '1',
                'issue_date' => $data['document_issue_date'],
                'supported_types' => [$data['document_type']]
            ];

            $verification_request['background_checks'] = [
                'name' => [
                    'first_name' => $userDetail->first_name,
                    'last_name'  => $userDetail->last_name
                ],
                'dob' => $dob,
            ];
            $auth = $client_id.":".$secret_key;
            $headers = ['Content-Type: application/json'];
            $post_data = json_encode($verification_request);
            $response = $this->send_curl($url, $post_data, $headers, $auth);
            $response_data    = $response['body'];
            if($response_data->event == 'verification.accepted'){
                User::where('id', $user_id)->update(array('shuftipro_verified' => 1));
                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'message' => trans('messages.success')
                ], 201);
            }else{
                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'message' => trans('messages.something_wrong')
                ], 201);
            }
        }else{
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.not_found')
            ], 500);
        }
    }

    /**
     * @param Request $request
     * User phoneEmailVerifiedStatus Logic
    */

    public function phoneEmailVerifiedStatus(Request $request) {
        $data = json_decode($request->getContent(), true);
        try{
            $user_id = $data['user_id'];
            $data = User::select('mobile_verified','email_verified')->where(['id'=> $user_id])->first();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data
            ], 201);
        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * check credit card if added or not, if not check waller amount
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function checkCreditCard(Request $request) {
        $data = json_decode($request->getContent(), true);
        try{
            $user_id = $data['user_id'];
            $amount = $data['amount'];
            $return = true;
            $data = Profile::select('stripe_account_id')->where(['user_id'=> $user_id])->first();
            if(empty($data->stripe_account_id)){
                $userWallet = DB::select( DB::raw("SELECT (SUM(IF(flag = 1, amount, 0)) - SUM(IF(flag = 2, amount, 0))) as balance FROM user_wallet WHERE user_id =".$user_id) );
                if($userWallet['0']->balance < $amount){
                    $return = false;
                }
            }
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $return
            ], 201);
        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * verify coupon ocde
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function verifyCouponCode(Request $request) {
        $data = json_decode($request->getContent(), true);
        try{
            $user_id = $data['user_id'];
            $coupon = $data['coupon'];
            $return = false;
            $data = Vouchers::where(['name'=> $coupon])->first();
            if($data){
                if(strtotime($data->starts_at) < time() && strtotime($data->expires_at) >= time()){
                    $return = $data;
                    UserCoupons::where(['user_id' => $user_id,'coupon_id' => $data->id,'status' => '0'])->update(['status' => '1']);
                    return BaseResponse::response([
                        'requestStatus' => 'true',
                        'data' => $data
                    ], 201);
                }else{
                    return BaseResponse::response([
                        'requestStatus' => 'false',
                    ], 201);
                }
            }else{
                return BaseResponse::response([
                    'requestStatus' => 'false',
                ], 201);
            }
        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * apply coupon code to account
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function applyCouponCode(Request $request) {
        $data = json_decode($request->getContent(), true);
        try{
            $user_id = $data['user_id'];
            $coupon = $data['coupon'];
            $return = false;
            $data = Vouchers::where(['name'=> $coupon])->first();
            if($data){
                if(strtotime($data->starts_at) < time() && strtotime($data->expires_at) >= time()){
                    $return = $data;
                    $userCoupons = new UserCoupons();
                    $userCoupons->user_id = $user_id;
                    $userCoupons->coupon_id = $data->id;
                    $userCoupons->code = $data->code;
                    $userCoupons->discount = $data->discount_amount;
                    $userCoupons->is_fixed = $data->is_fixed;
                    $userCoupons->save();
                    return BaseResponse::response([
                        'requestStatus' => 'true',
                        'data' => $data
                    ], 201);
                }else{
                    return BaseResponse::response([
                        'requestStatus' => 'false',
                    ], 201);
                }
            }else{
                return BaseResponse::response([
                    'requestStatus' => 'false',
                ], 201);
            }
        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * stripe webhook
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function stripeKYCWebhook(Request $request) {
        $requestData = json_decode($request->getContent(), true);
        $body = $requestData['data'];
        if($body){
            $updatedStatus = $body['object']['legal_entity']['verification']['status'];
            $status = ($updatedStatus == 'verified') ? '1' : '0';
            $userDetail = Profile::where('stripe_account_id',$body['object']['id'])->first();
            if($userDetail){
                $data = Profile::where('id',$userDetail->id)->update([
                    'sripe_status' => $status
                ]);
                return BaseResponse::response([
                    'requestStatus' => 'success'
                ], 201);
            }
        }
    }

    /**
     * update customer profile
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function updateCustomerProfile(Request $request) {
        try {
            $data = $request->all();
            $user = User::where('id', $data['user_id'])->first();
            $user->first_name = $data['firstname'];
            $user->last_name  = $data['lastname'];
            $user->fullname = $data['firstname']." ".$data['lastname'];
            if($request->file('profile_image')){
                $path = $request->file('profile_image')->store('',['disk' => 'user']);
                $user->profile_image = 'avatars/'.$path;
            }
            $user->password = bcrypt($data['password']);
            if($user->save()){
                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'message' => trans('messages.profile_updated') ,
                ], 201);
            }else{
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => trans('messages.something_wrong'),
                ], 500);
            }
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * list weekly availability
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function listWeeklyAvailablity(Request $request) {
        try {
            $data = json_decode($request->getContent(), true);
            $availability = UserAvailability::where('user_id', $data['user_id'])->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $availability,
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * save weekly availability
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function saveWeeklyAvailablity(Request $request) {
        try {
            $data = json_decode($request->getContent(), true);
            if(is_array($data['week'])){
                foreach ($data['week'] as $key => $value) {
                    $userAvailability = new UserAvailability();
                    if(!empty($value['id'])){
                        $userAvailability = UserAvailability::where('id',$value['id'])->first();
                    }
                    $userAvailability->user_id = $data['user_id'];
                    $userAvailability->day = $value['day'];
                    $userAvailability->isAvailable = $value['isAvailable'];
                    $userAvailability->startTime = $value['startTime'];
                    $userAvailability->endTime = $value['endTime'];
                    $userAvailability->save();
                }
            }
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => trans('messages.success'),
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * delete account
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function deleteAccount(Request $request) {
        try {
            $data = json_decode($request->getContent(), true);
            $user = User::where('id', $data['user_id'])->first();
            $user->is_deleted = '1';
            $user->save();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => trans('messages.success'),
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * list all faq
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function listFAQ(Request $request) {
        try {
            $data = json_decode($request->getContent(), true);
            if(!empty($data['category'])){
                $data = Faq::where('category', $data['category'])->get();
            }else{
                $data = Faq::get();
            }
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data,
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([

                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * get CMS page
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function cmsPage(Request $request) {
        try {
            $data = json_decode($request->getContent(), true);
            $data = Cms::where('title' , $data['title'])->first();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data,
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * get FAQ category list
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function getFAQCategory(Request $request) {
        try {
            $data = FaqCategory::get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data,
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * update zendesk id
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function updateZendeskID(Request $request) {
        try {
            $data = json_decode($request->getContent(), true);
            $user = User::where('id' , $data['user_id'])->first();
            $user->zenddesk_id = $data['zenddesk_id'];
            $user->save();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => trans('messages.success'),
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * send curl
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    function send_curl($url, $post_data, $headers, $auth){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $html_response = curl_exec($ch);
        $curl_info = curl_getinfo($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($html_response, 0, $header_size);
        $body = substr($html_response, $header_size);
        curl_close($ch);
        return ['headers' => $headers,'body' => $body];     
    }

    /**
     * get headers
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    function get_header_keys($header_string){
        $headers = [];
        $exploded = explode("\n", $header_string);
        if(!empty($exploded)){
            foreach ($exploded as $key => $header) {
                if (!$key) {
                    $headers[] = $header;
                } else {
                    $header = explode(':', $header);
                    $headers[trim($header[0])] = isset($header[1]) ? trim($header[1]) : "";
                }
            }
        }    
        return $headers;
    }

    /**
     * save notification sound for customer
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    function saveNotificationSound(Request $request) {
        try {
            $data = json_decode($request->getContent(), true);
            $user = User::where('id' , $data['user_id'])->first();
            $user->notification_sound = $data['notification_sound'];
            $user->save();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => trans('messages.success'),
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * save notification sound for customer
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    function addNewBankAccount(Request $request) {
        try {
            $data = json_decode($request->getContent(), true);
            $userDetail = Profile::where('user_id' , $data['user_id'])->first();
            $stripe_account_id = $userDetail->stripe_account_id;
            $external_account = [
                    "object" => "bank_account",
                    "country" => "GB",
                    "currency" => "GBP",
                    "routing_number" => $data['routing_number'],//"108800",
                    "account_number" => $data['account_number'],//"00012345"
            ];
            $bank_account = \Stripe\Account::createExternalAccount($stripe_account_id,[
                'external_account' => $external_account,
            ]);
            if($bank_account){
                Profile::where('user_id',$data['user_id'])->update(['stripe_external_bank'=>$bank_account->id]);
            }
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => trans('messages.success'),
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * invite Coupn code
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    function inviteCoupon(Request $request) {
        try {
            $data = json_decode($request->getContent(), true);
            $couponDetail = Vouchers::where(['name'=> 'Invite'])->first();            
            if($couponDetail){
                $userCoupons = new UserCoupons();
                $userCoupons->user_id = $data['user_id'];
                $userCoupons->coupon_id = $couponDetail->id;
                $userCoupons->code = $couponDetail->code;
                $userCoupons->discount = $couponDetail->discount_amount;
                $userCoupons->is_fixed = $couponDetail->is_fixed;
                $userCoupons->save();
            }
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => trans('messages.success'),
            ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }
}
