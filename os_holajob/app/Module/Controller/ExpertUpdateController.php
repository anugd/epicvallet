<?php
namespace App\Module\Controller;

use Illuminate\Http\Request;
use App\Module\Model\User;
use App\Module\Model\ExpertAvailablity;
use App\Module\Model\ServiceImages;
use App\Module\Model\EmergencyContacts;
use App\Module\Model\Profile;
use App\Module\Model\UserAddresses;
use App\Module\Model\UserServices;
use App\Module\Model\UserLanguages;
use App\Module\Model\UserDocuments;
use \App\Module\Service\SmsService;
use App\Module\Model\Notification;
use Validator;
use App\Module\BaseResponse;
use \App\Module\Model\OtpLog;
use Helper;

/**
 * Description of ExpertUpdateController
 * @author Shweta trivedi
 */

class ExpertUpdateController extends BaseController {

    /**
     * set expert availability
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function setAvailablity(Request $request) {
        $validated_data = json_decode($request->getContent(), true);
        $validator = Validator::make($validated_data, [
            'available' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => implode('<br />', $validator->errors()->all())
            ], 422);
        }
        try {
            $authUser = auth()->user();
            if(!$authUser){
                return response()->json([
                    'requestStatus' => 'error',
                    'message' => trans('messages.invalid_user') 
                ], 500);
            }
            $user = User::where('id', $authUser->id)->first();
            $user->available = $validated_data['available'] ;
            $user->save();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => trans('messages.availability_succ'),
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * set notification
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function setNotification(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $notificationDetail = Notification::where([
                'user_id' => $data['user_id'],
                'type' => $data['type'],
                'notification_for' => $data['notification_for']
            ])->first();
            if(!empty($notificationDetail)){
                $notificationDetail->status = $data['status'] ;
            }else{
                $notificationDetail = new Notification;
                $notificationDetail->user_id = $data['user_id'];
                $notificationDetail->type = $data['type'];
                $notificationDetail->notification_for = $data['notification_for'];
                $notificationDetail->status = $data['status'];
            }
            $notificationDetail->save();
            return response()->json([
                'requestStatus' => 'success',
                'message' => trans('messages.success')
            ]);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * get notification function
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function getNotification(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $notificationDetail = Notification::where(['user_id' => $data['user_id']])->get();
            return response()->json([
                'requestStatus' => 'success',
                'data' => $notificationDetail
            ]);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Update Identity Front and back Document in database
     * @author Shweta trivedi
    */
    
    public function updateIdentityDocument(Request $request) {
      $data = json_decode($request->getContent(), true);
      $validator = Validator::make($data, [
            'front_image' => 'required',
            'back_image' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => implode('<br />', $validator->errors()->all())
            ], 422);
        }
        try {
            $authUser = auth()->user();
            if(!$authUser){
                return response()->json([
                    'requestStatus' => 'error',
                    'message' => trans('messages.invalid_user') 
                ], 500);
            }
            $document = UserDocuments::where('user_id', $authUser->id)->first();   
            $document->front_image = $data['front_image'];
            $document->back_image = $data['back_image'];
        
            if($document->save()){
                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'message' => trans('messages.server_error')
                ], 201);
            }else {
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => trans('messages.something_wrong'),
                ], 500);
            }
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * upload profile image
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function updateProfileImage(Request $request) {
        
        //validated data
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
                'image_path' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => implode('<br />', $validator->errors()->all())
                ], 422);
            }
            
            try {
                $authUser = auth()->user();
                if(!$authUser){
                    return response()->json([
                        'requestStatus' => 'error',
                        'message' => trans('messages.invalid_user') 
                    ], 500);
                } 
                $profile = User::where('id', $authUser->id)->first();  
                User::where('id', $authUser->id)->update(array('profile_image' => $data['image_path']));
                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'message' => trans('messages.profile_img_saved')  
                ], 201);
        }catch (\Exception $e) {
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => trans('messages.something_wrong'),
                    'dev' => $e->getMessage()
                ], 500);
        }
    }

    /**
     * Update Biographic, Emergency and Insurance information API
     * @author Shweta trivedi
     */

    public function updateOtherInformation(Request $request) {
        try {          
           $authUser = auth()->user();
           if(!$authUser) {
                return response()->json([
                    'requestStatus' => 'error',
                    'message' => trans('messages.invalid_user')
                ], 500);
            }
            //validated data
            $validated_data = json_decode($request->getContent(), true);
            $validator = Validator::make($validated_data, [
                'about' => 'required',
                'user_services' => 'required',
                'user_languages' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json([
                            'requestStatus' => 'invalid',
                            'message' => implode('<br />', $validator->errors()->all())
                        ], 422);
            }
            
            $profile = Profile::where('user_id', $authUser->id)->first();
            $profile->about = $validated_data['about'];
            if($profile->save()){
                //Save Expert Services
                $userservices  = UserServices::where('user_id',$authUser->id)->delete();
                for ($i = 0; $i < count($validated_data['user_services']); $i++)
                {
                    $services = new UserServices;
                    $services->user_id  =  $authUser->id;
                    $services->service_id = $validated_data['user_services'][$i];
                    $services->save();
                }
                
                $userlangs  = UserLanguages::where('user_id',$authUser->id)->delete();
                for ($i = 0; $i < count($validated_data['user_languages']); $i++)
                {
                    $languages = new UserLanguages;
                    $languages->user_id = $authUser->id;
                    $languages->language_id = $validated_data['user_languages'][$i];
                    $languages->save();
                }

                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'message' => trans('messages.profile_updated') ,
                ], 201);
            }
            else {
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => trans('messages.something_wrong'),
                ], 500);
            }
            
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * User updateBasicInformation Logic
    */

    public function updateBasicInformation(Request $request) {
        try {         
            $authUser = auth()->user();
            if(!$authUser) {
                return response()->json([
                    'requestStatus' => 'error',
                    'message' => trans('messages.invalid_user')
                ], 500);
            }
            //validated data
            $validated_data = json_decode($request->getContent(), true);
            $validator = Validator::make($validated_data, [
                'firstname' => 'required',
                'lastname' => 'required',
                'mobile_number' => 'required|min:10|numeric',
                'dob' => 'required',
                'gender' => 'required',
                'city' => 'required',
                'postal_code' => 'required',
                'address_line1' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => implode('<br />', $validator->errors()->all())
                ], 422);
            }
            
            $user = User::where('id', $authUser->id)->first();
            $user->first_name = $validated_data['firstname'];
            $user->last_name  = $validated_data['lastname'];
            $user->fullname = $validated_data['firstname']." ".$validated_data['lastname'];
            //    $user->mobile_number = $validated_data['mobile_number'];
            //    $user->email = $validated_data['email'];
            if($user->save()){
                $profile = Profile::where('user_id', $authUser->id)->first();
                $profile->dob = $validated_data['dob'];
                $profile->gender = $validated_data['gender'];
                if($profile->save()){
                    $address = UserAddresses::where('user_id', $authUser->id)->first();
                    $address->city = $validated_data['city'];
                    $address->state = isset($validated_data['state'])?$validated_data['state']: "";
                    $address->postal_code = $validated_data['postal_code'];
                    $address->address_line1 = $validated_data['address_line1'];
                    $address->address_line2 = isset($validated_data['address_line2']) ? $validated_data['address_line2'] : "";
                    if($address->save()){
                        return BaseResponse::response([
                            'requestStatus' => 'success',
                            'message' => trans('messages.profile_updated') ,
                        ], 201);
                    }
                }
            }else {
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => trans('messages.something_wrong'),
                ], 500);
            }
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }
}
