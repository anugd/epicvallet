<?php

namespace App\Module\Controller;

use Illuminate\Http\Request;
use App\Module\Model\Languages;
use App\Module\BaseResponse;

/**
 * Description of ServiceController
 *
 * @author ShwetaTrivedi
 */
class LanguagesController extends BaseController {

    /**
     * @param Request $request
     * Show Logic
    */

    public function show(Request $request) {
        $language = Languages::get()->toArray();
        return BaseResponse::response([
            'requestStatus' => 'success',
            'data' => $language,
        ],200);
    }
}
