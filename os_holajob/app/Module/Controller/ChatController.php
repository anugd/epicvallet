<?php

namespace App\Module\Controller;

use Illuminate\Http\Request;
use App\Module\Model\Chat;
use App\Module\Model\ChatDetail;
use App\Module\BaseResponse;
use DB;

/**
 * Description of Chat Controller
 *
 * @author Amit Sharma
 */

class ChatController extends BaseController {
    
    /**
     * list all chats for users
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function listChat(Request $request) {
        $data = json_decode($request->getContent(), true);
        $response = Chat::with('Message','Sender','Receiver','Sender.profile','Receiver.profile','Message.Sender','Message.Receiver')->where('status','1')->where(function ($query) use ($data){
                $query->where('sender' , $data['user_id'])
                ->orWhere('receiver' , $data['user_id']);
        })->get();
       	return BaseResponse::response([
           'requestStatus' => 'success',
           'data' => $response,
        ],200);
    }

    /**list all messages corrosponsing to chat id
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function listMessages(Request $request) {
        $data = json_decode($request->getContent(), true);
        ChatDetail::where('chat_id', $data['chat_id'])->update(array('is_read' => 1));
        $response = ChatDetail::with([
            'User' => function($q){
                $q->select('id','fullname as name','profile_image as avatar');
            }
        ])->where('chat_id', $data['chat_id'])->orderBy('_id', 'desc')->get();
        return BaseResponse::response([
           'requestStatus' => 'success',
           'data' => $response,
        ],200);
    }

    /**
     * upload chat image
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function uploadChatImage(Request $request) {
        $request->validate([
            'image' => 'required|mimes:jpeg,bmp,png'
        ]);
        $path = $request->file('image')->store('',['disk' => 'chatImage']);
        return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => ['image_path' => 'chat/'.$path]
            ], 201);
        return BaseResponse::response([
           'requestStatus' => 'success',
           'data' => $response,
        ],200);
    }

    /**
     * update message status
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function updateMessageStatus(Request $request) {
        $data = json_decode($request->getContent(), true);
        ChatDetail::where('id', $data['message_id'])->update(array('is_read' => 1));
        return BaseResponse::response([
           'requestStatus' => 'success'
        ],200);
    }

    /**
     * get chat id based on sender and receiver
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function getChatID(Request $request) {
        $data = json_decode($request->getContent(), true);
        $response = Chat::where(['status' =>'1','receiver'=> $data['receiver'], 'sender'=> $data['sender']])
        ->orWhere(function($qry) use($data){
            $qry->where('status','1');
            $qry->where('receiver', $data['sender']);
            $qry->where('sender', $data['receiver']);                   
        })->first();
        if ($response){
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $response->id,
            ],200);
        }else{
            $id = DB::table('chat')->insertGetId([
                'sender' => $data['sender'],
                'receiver' => $data['receiver']
            ]);
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $id,
            ],200);
        }
    }
}
