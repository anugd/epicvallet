<?php

namespace App\Module\Controller;

use Illuminate\Http\Request;
use App\Module\Service\SmsService;
use Carbon\Carbon;
use App\Module\BaseResponse;
use \App\Module\Model\User;
use \App\Module\Model\OtpLog;
use \App\Module\Model\Profile;
use Validator;
use Illuminate\Support\Facades\Auth;
use Session;

/**
 * Description of UserController
 *
 * @author Shweta Trivedi
 */
class WebDashboardController extends BaseController {
    
    /**
     * Function is created for otp view page
     *
     * @param $request
     * @author Amit Sharma
     * @return 
     */

    public function otp(Request $request) {
        if($request->session()->has('user_id')){
            $value = $request->session()->get('user_id');
            $user = User::where('id', $value)->first();
            $request->session()->put('otp_user_id', $value);
            $request->session()->put('mobile_number', $user->mobile_number);
            $request->session()->put('email', $user->email);
        }
        return view('auth.otp');
    }

    /**
     * Function is created tovalidate OTP from web
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function validateOtp(Request $request) {
        $validator = Validator::make($request->all(), [
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
                Session::flash('message', 'OTP required!');
                return redirect()->back()->withErrors($validator)->withInput();
        }
        $user_id = $request->user_id ;
        $user = User::where('id', $user_id)->first();
        $log = (new OtpLog())->checkOtpLog($user_id, $request->otp);
        if ($log) {
            if (Carbon::now() < $log->expires_at) {
                if($user->user_type_id == 2)
                {
                    $profile_complete_step = 4;
                    $is_profile_complete = 0;
                }
                else
                {
                    $profile_complete_step = 2;
                    $is_profile_complete = 1;
                }
                $user->mobile_verified = 1;
                $user->profile_complete_step = $profile_complete_step;
                $user->is_profile_complete = $is_profile_complete;
                $user->status = 1;
                if($user->save()){
                    return redirect()->route('login')->with('success', 'Registration successfully');
                }else{
                    Session::flash('message', trans('messages.otp_expired'));
                    return redirect()->back()->withInput();
                }
            }else{
                Session::flash('message', trans('messages.otp_expired'));
                return redirect()->back()->withInput();
            }
        }else{
            Session::flash('message', trans('messages.otp_invalid'));
            return redirect()->back()->withInput();
        }
    }

    /**
     * Function is created for congratulation view
     *
     * @param $request
     * @author Amit Sharma
     * @return 
     */

    public function congratulation(Request $request) {
        return view('auth.congratulation');
    }

    /**
     * Function is created for KYC view
     *
     * @param $request
     * @author Amit Sharma
     * @return 
     */

    public function KYC(Request $request) {
        return view('auth.KYC');
    }

    /**
     * Function is created show dashbaord
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function dashboard(Request $request) {
       $authUser = auth()->user();
       if(!$authUser){
            return response()->json([
                'requestStatus' => 'error',
                'message' => trans('messages.invalid_user')
            ], 500);
        }
        $data['user_id'] = $authUser->id;
        return view('customer.dashboard',$data);
    }

    /**
     * Function is created for expert dashboard
     *
     * @param $request
     * @author Amit Sharma
     * @return 
     */

    public function expertDashboard(Request $request) {
        return view('expert.dashboard');
    }
}
