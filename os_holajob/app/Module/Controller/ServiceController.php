<?php

namespace App\Module\Controller;

use Illuminate\Http\Request;
use App\Module\Model\Services;
use App\Module\BaseResponse;

/**
 * Description of ServiceController
 *
 * @author ShwetaTrivedi
 */
class ServiceController extends BaseController {

    protected $service;

    function __construct(Services $services) {
        $this->service = $services;
    }

    /**
     * Function is created to list all services for parent which we sent
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function find(Request $request) {
        $cat = $this->service->find($request);
        $parent = $this->service->getParentData($request);
        return BaseResponse::response([
            'requestStatus' => 'success',
            'data' => $parent,
        ],200);
    }

    /**
     * Function is created to store image in local storage and return path
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function storeImage(Request $request) {
        $request->validate([
            'service_image' => 'required|mimes:jpeg,bmp,png'
        ]);
        $path = $request->file('service_image')->storePublicly('/service');
        return BaseResponse::response([
            'requestStatus' => 'success',
            'data' => ['url' => $path]
        ], 201);
    }

    /**
     * Function is created to create new service
     *
     * @param $request
     * @author Amit Sharma
     * @return $data
     */

    public function create(Request $request) {
        $data = $this->validationRule($request);
        $result = $this->service->createService($data);
        if ($result) {
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $result
            ], 201);
        }
        return BaseResponse::response([
            'requestStatus' => 'error',
            'data' => trans('messages.something_wrong')
        ], 500);
    }

    /**
     * Function is created to list validation rule for create service
     *
     * @param $request
     * @author Amit Sharma
     * @return 
     */

    protected function validationRule(Request $request, $update = false) {
        $rule = [
            'service' => 'required|max:200',
            'service_image' => 'required',
            'service_icon' => 'nullable',
            'parent_id' => 'nullable|numeric',
            'price' => 'required',
            'status' => 'nullable',
        ];
        $update && ($rule['id'] = 'required|numeric');
        return $request->validate($rule);
    }

    /**
     * Function is created to update service
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function update(Request $request) {
        $data = $this->validationRule($request, true);
        $result = $this->service->updateService($data);
        if ($result) {
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $result
            ], 202);
        }
        return BaseResponse::response([
            'requestStatus' => 'error',
            'data' => trans('messages.error')
        ], 400);
    }

    /**
     * Function is created to delete service
     *
     * @param $request
     * @author Amit Sharma
     * @return $success
     */

    public function delete($id) {
        $result = $this->service->deleteService($id);
        if ($result) {
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $result
            ], 202);
        }
        return BaseResponse::response([
            'requestStatus' => 'error',
            'data' => trans('messages.error')
        ], 400);
    }
}
