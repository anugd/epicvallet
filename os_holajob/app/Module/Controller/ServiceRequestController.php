<?php

namespace App\Module\Controller;

use Illuminate\Http\Request;
use App\Module\Model\ServiceRequest;
use App\Module\Model\ServicePayment;
use App\Module\Model\ServiceReport;
use App\Module\Model\ServiceFeedback;
use App\Module\Model\Profile;
use App\Module\Model\Payouts;
use App\Module\Model\UserWallet;
use App\Module\Model\SmsService;
use App\Module\Model\AdminSetting;
use App\Module\Model\UserAddresses;
use App\Module\Model\UserCoupons;
use App\Module\Model\Chat;
use App\Module\BaseResponse;
use App\Helpers\UserNotification as UserNotification;
use DB;
use DateTime;
use Carbon\Carbon;
use PDF;
use File;

/**
* Description of ServiceController
*
* @author ShwetaTrivedi
*/

class ServiceRequestController extends BaseController {

    protected $servicerequest;
    public $stripeKey = '';

    function __construct(ServiceRequest $servicesrequest) {
        $this->servicerequest = $servicesrequest;
        if (config('app.MODE') == 'live') {
            \Stripe\Stripe::setApiKey(config('app.STRIPE_LIVE_KEY'));
            $this->stripeKey = config('app.STRIPE_LIVE_KEY');
        } else {
            \Stripe\Stripe::setApiKey(config('app.STRIPE_TEST_KEY'));
            $this->stripeKey = config('app.STRIPE_TEST_KEY');
        }
    }

    /**
     * @param Request $request
     * requet Logic
    */

    public function request(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            if(isset($data['user_id']) && !empty($data['user_id']) && isset($data['service_id']) && !empty($data['service_id']) && isset($data['status']) && !empty($data['status'])){
                $servicerequest = new ServiceRequest();
                $servicerequest->user_id = $data['user_id'];
                $servicerequest->service_id = $data['service_id'];
                $servicerequest->status = $data['status'];
                $servicerequest->save();
                $resultflag = trans("message.success");
            }else{
                $resultflag = trans("message.error");
            }
            return BaseResponse::response([
               'requestStatus' => 'success',
               'data' => $resultflag,
            ],200);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'data' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * User scheduleOngoingListing Logic
    */

    public function scheduleOngoingListing(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $page = $data['page'];
            $limit = $data['limit'];
            $status = $data['status'];
            $offset = ($page-1) * $limit;
            $query = ServiceRequest::with('ServiceLocation','customer','expert','service')->where('expert_id', $data['user_id'])->where('status', $status);
            $totalRecord = $query->get()->count();
            $data = $query->offset($offset)->limit($limit)->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data,
                'total' => $totalRecord,
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * servicesListingCustomer Logic
    */

    public function servicesListingCustomer(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $page = $data['page'];
            $limit = $data['limit'];
            $offset = ($page-1) * $limit;
            $query = ServiceRequest::with('expert','service','service.ServiceParent','expert.profile')->where('user_id', $data['user_id'])->orderBy('status', 'asc');
            $totalRecord = $query->get()->count();
            $data = $query->offset($offset)->limit($limit)->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data,
                'total' => $totalRecord,
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * listExpertInvoice Logic
    */

    public function listExpertInvoice(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $status = (!empty($data['status'])) ? $data['status'] : '';
            $page = $data['page'];
            $limit = $data['limit'];
            $offset = ($page-1) * $limit;
            $query = ['expert_id' => $data['user_id']];
            if($status){
                $query['status'] = $status;
            }
            $query = ServiceRequest::with(['ServiceFeedback' => function ($query) use($data) {
                $query->where('receiver', '=', $data['user_id']);
                },'customer','expert','service','customer.profile','ServicePayment','service.ServiceParent'])->where($query);
            $totalRecord = $query->get()->count();
            $data = $query->offset($offset)->limit($limit)->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data,
                'total' => $totalRecord,
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * list customer invoice Logic
    */

    public function listCustomerInvoice(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $page = $data['page'];
            $limit = $data['limit'];
            $offset = ($page-1) * $limit;
            $query = ServiceRequest::with(['ServicePayment','ServiceFeedback' => function ($query) use($data) { $query->where('receiver', '=', $data['user_id']);}
            ,'customer','expert','service','service.ServiceParent'])->where('user_id', $data['user_id'])->whereIn('status', [2,4]);
            $totalRecord = $query->get()->count();
            $data = $query->offset($offset)->limit($limit)->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data,
                'total' => $totalRecord,
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * start Service Logic
    */

    public function startService(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $request = $data['request_id'];
            $startTime = date('Y-m-d h:i:s');
            ServiceRequest::where('id', $request)->update(array('service_start_time' => $startTime,'status' => '3'));
            $data = ServiceRequest::with('customer','expert','service')->where('id', $request)->first();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('service_not_started'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * cancel service Logic
    */

    public function cancelService(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $request = $data['request_id'];
            ServiceRequest::where('id', $request)->update([
                'status' => 4,
                'cancel_reason' => (!empty($data['reason'])) ? $data['reason'] : ''
            ]);
            $data = ServiceRequest::with('customer','expert','service')->where('id', $request)->first();
            if(!empty($data->expert)){
                UserNotification::send_cancelserviceexpert_mail($data);
            }
            if(!empty($data->customer)){
                $message = "User booking for the service ".$data->service->service." on date ".$data->booking_date." has been cancelled.";
                UserNotification::send_cancelservicecustomer_mail($data);
                UserNotification::send_push_notification($message,$data->customer->player_id,$data->customer->id,'booking_cancelled');
            }
            return BaseResponse::response([
                'requestStatus' => 'success',
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('service_not_started'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * requestQRForServiceStart Logic
    */

    public function requestQRForServiceStart(Request $request){
        $data = json_decode($request->getContent(), true);
        $serviceRequest = ServiceRequest::where('id', $data['request_id'])->first();
        if(!empty($serviceRequest)){
            $checkIFServiceStarted = ServiceRequest::where(['user_id'=> $serviceRequest->user_id,
            'status'=>3])->first();
            if(!$checkIFServiceStarted){
                // $strLength = strlen($data['request_id']);
                // $length = 5 - $strLength;
                // $randLength = '';
                // for($i = 0;$i < $length;$i++){
                //     $randLength .= 9;
                // }
                // $OTP = $data['request_id'].'A';
                // if($randLength){
                //     $OTP .= rand(0, $randLength);
                // }
                $OTP = rand(0, 999999);
                try {
                    $filename = time().'.png';
                    \QrCode::format('png')->size(350)->generate($OTP, public_path('qrcodes/'.$filename));
                    ServiceRequest::where('id', $data['request_id'])->update(['otp' => $OTP]);
                    return response()->json([
                        'requestStatus' => 'success',
                        'message'=> trans('messages.code_sent'),
                        'image' => 'qrcodes/'.$filename,
                        'OTP' => $OTP
                    ]);
                }catch (\Exception $e){

                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => trans('messages.something_wrong')
                    ], 401);
                }
            }else{
                return response()->json([
                    'requestStatus'=>'invalid',
                    'message' => trans('messages.service_already_running')
                ], 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('service_not_started')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * verfiyQRForServiceStart Logic
    */

    public function verfiyQRForServiceStart(Request $request){
        $data = json_decode($request->getContent(), true);
        if(!empty($data['otp'])){
            $serviceRequest = ServiceRequest::where(['otp' => $data['otp'] ])->first();
            if(!empty($serviceRequest)){
                try{
                    $checkIFServiceStarted = ServiceRequest::where(['expert_id'=> $serviceRequest->expert_id,'status'=>3])->first();
                    if(!$checkIFServiceStarted){
                        $startTime = $data['date'];
                        ServiceRequest::where('id', $serviceRequest->id)->update(['service_start_time' => $startTime,'status' => '3']);
                        return response()->json([
                            'requestStatus' => 'success',
                            'message'=> trans('messages.job_started'),
                            'data' => $serviceRequest->user_id
                        ]);
                    }else{
                        return response()->json([
                            'requestStatus'=>'invalid',
                            'message' => trans('messages.service_already_running')
                        ],401);
                    }
                }catch (\Exception $e){

                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => trans('messages.something_wrong')
                    ], 401);
                }
            }else{
                return response()->json([
                    'requestStatus'=>'invalid',
                    'message' => trans('messages.invalid_QR_code')
                ], 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('messages.invalid_QR_code')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * requestQRForServiceBreakStart Logic
    */

    public function requestQRForServiceBreakStart(Request $request){
        $data = json_decode($request->getContent(), true);
        $serviceRequest = ServiceRequest::where('id', $data['request_id'])->first();
        if(!empty($serviceRequest)){
            // $strLength = strlen($data['request_id']);
            // $length = 5 - $strLength;
            // $randLength = '';
            // for($i = 0;$i < $length;$i++){
            //     $randLength .= 9;
            // }
            // $OTP = $data['request_id'].'B';
            // if($randLength){
            //     $OTP .= rand(0, $randLength);
            // }
            $OTP = rand(0, 999999);
            try {
                $filename = time().'.png';
                \QrCode::format('png')->size(350)->generate($OTP, public_path('qrcodes/'.$filename));
                ServiceRequest::where('id', $data['request_id'])->update(['otp' => $OTP]);
                return response()->json([
                    'requestStatus' => 'success',
                    'image' => 'qrcodes/'.$filename,
                    'OTP' => $OTP
                ]);
            }catch (\Exception $e){

                return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => trans('messages.something_wrong')
            ], 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('messages.something_wrong')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * verfiyQRForServiceBreakStart Logic
    */

    public function verfiyQRForServiceBreakStart(Request $request){
        $data = json_decode($request->getContent(), true);
        if($data['otp']){
            $serviceRequest = ServiceRequest::with(['customer','customer.Profile','service'])->where(['otp' => $data['otp'] ])->first();
            if(!empty($serviceRequest)){
                try{
                    $break_start = $data['date'];
                    ServiceRequest::where('id', $serviceRequest->id)->update(['break_start'=>$break_start]);
                    return response()->json([
                        'requestStatus' => 'success',
                        'message'=> trans('messages.break_started'),
                        'data' => $serviceRequest->user_id
                    ]);
                }catch (\Exception $e){
                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => trans('messages.invalid_QR_code')
                    ], 401);
                }
            }else{
                return response()->json([
                    'requestStatus'=>'invalid',
                    'message' => trans('messages.invalid_QR_code')
                ], 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('messages.invalid_QR_code')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * requestQRForServiceBreakEnd Logic
    */

    public function requestQRForServiceBreakEnd(Request $request){
        $data = json_decode($request->getContent(), true);
        $serviceRequest = ServiceRequest::where('id', $data['request_id'])->first();
        if(!empty($serviceRequest)){
            // $strLength = strlen($data['request_id']);
            // $length = 5 - $strLength;
            // $randLength = '';
            // for($i = 0;$i < $length;$i++){
            //     $randLength .= 9;
            // }
            // $OTP = $data['request_id'].'C';
            // if($randLength){
            //     $OTP .= rand(0, $randLength);
            // }
            $OTP = rand(0, 999999);
            try {
                $filename = time().'.png';
                \QrCode::format('png')->size(350)->generate($OTP, public_path('qrcodes/'.$filename));
                ServiceRequest::where('id', $data['request_id'])->update(['otp' => $OTP]);
                return response()->json([
                    'requestStatus' => 'success',
                    'image' => 'qrcodes/'.$filename,
                    'OTP' => $OTP
                ]);
            }catch (\Exception $e){

                return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => trans('messages.something_wrong')
                ], 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('messages.not_found')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * verfiyQRForServiceBreakEnd Logic
    */

    public function verfiyQRForServiceBreakEnd(Request $request){
        $data = json_decode($request->getContent(), true);
        if(!empty($data['otp'])){
            $serviceRequest = ServiceRequest::with(['customer','customer.Profile','service'])->where(['otp' => $data['otp'] ])->first();
            if(!empty($serviceRequest)){
                try{
                    $break_end = $data['date'];
                    $break_start = strtotime($serviceRequest->break_start);
                    $break_end = strtotime($break_end);
                    $breakInterval  = abs($break_end - $break_start);
                    $breakInterval = $breakInterval + $serviceRequest->break_duration;
                    ServiceRequest::where('id', $serviceRequest->id)->update([
                        'break_duration'=>$breakInterval,
                        'break_start'=>null
                    ]);
                    return response()->json([
                        'requestStatus' => 'success',
                        'message'=> 'Break started',
                        'data' => $serviceRequest->user_id
                    ]);
                }catch (\Exception $e){

                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => trans('messages.invalid_QR_code')
                    ], 401);
                }
            }else{
                return response()->json([
                    'requestStatus'=>'invalid',
                    'message' => trans('messages.invalid_QR_code')
                ], 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('messages.invalid_QR_code')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * requestQRForServiceEnd Logic
    */

    public function requestQRForServiceEnd(Request $request){
        $data = json_decode($request->getContent(), true);
        $serviceRequest = ServiceRequest::where('id', $data['request_id'])->first();
        if(!empty($serviceRequest)){
            // $strLength = strlen($data['request_id']);
            // $length = 5 - $strLength;
            // $randLength = '';
            // for($i = 0;$i < $length;$i++){
            //     $randLength .= 9;
            // }
            // $OTP = $data['request_id'].'A';
            // if($randLength){
            //     $OTP .= rand(0, $randLength);
            // }
            $OTP = rand(0, 999999);
            try {
                $filename = time().'.png';
                \QrCode::format('png')->size(350)->generate($OTP, public_path('qrcodes/'.$filename));
                ServiceRequest::where('id', $data['request_id'])->update(['otp' => $OTP]);
                return response()->json([
                    'requestStatus' => 'success',
                    'image' => 'qrcodes/'.$filename,
                    'OTP' => $OTP
                ]);
            }catch (\Exception $e){
                return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => trans('messages.something_wrong')
                ], 401);
            }
        }else{
            return response()->json(
                ['requestStatus'=>'invalid',
                'message' => trans('messages.not_found')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * verfiyQRForServiceEnd Logic
    */

    public function verfiyQRForServiceEnd(Request $request){
        $data = json_decode($request->getContent(), true);
        if(!empty($data['otp'])){
            $serviceRequest = ServiceRequest::with(['customer','customer.Profile','service'])->where(['otp' => $data['otp'] ])->first();
            if(!empty($serviceRequest)){
                try{
                    $endTime = $data['date'];
                    ServiceRequest::where('id', $serviceRequest->id)->update([
                        'service_end_time' => $endTime,'status' => '2']);
                    return response()->json([
                        'requestStatus' => 'success',
                        'message'=> trans('messages.job_completed'),
                        'data' => [
                            'customer_id' => $serviceRequest->user_id,
                            'request_id' => $serviceRequest->id
                        ]
                    ]);
                }catch (\Exception $e){

                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => trans('messages.something_wrong')
                    ], 401);
                }
            }else{
                return response()->json([
                    'requestStatus'=>'invalid',
                    'message' => trans('messages.invalid_code')
                ], 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('messages.invalid_code')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * download Logic
    */

    public function download($sid = null) {
        $invoice  = ServiceRequest::with('expert','customer','service','ServicePayment')->where('id',$sid)->first();
        $cusaddress = UserAddresses::where('user_id',$invoice->user_id)->where('is_primary', 1)->first();
        $expertaddress = UserAddresses::where('user_id',$invoice->expert_id)->first();
        $data = array(
            'expertaddress' => $expertaddress,
            'invoice' => $invoice,
            'cusaddress' => $cusaddress
        );
        $path = public_path().'/invoices/';
        File::makeDirectory($path, $mode = 0777, true, true);
        $name = '/invoices/'.$sid.time().'-invoice.pdf';
        $pdf = PDF::loadView('customer.pdf.invoice',$data)->save(public_path().$name);
        ServiceRequest::where('id', $sid)->update(['invoice_url' => $name]);
        return true;
    }

    /**
     * @param Request $request
     * makePaymetnForService Logic
    */

    public function makePaymentForService(Request $request){
        $data = json_decode($request->getContent(), true);
        $serviceRequest = ServiceRequest::with(['customer','customer.Profile','service'])->where(['id' => $data['request_id']])->first();
        if(!empty($serviceRequest) && !empty($serviceRequest->service_end_time)){
            try{
                $adminCommissionPercent = $data['admin_commission_percent'];
                // calculate earning
                $totalEarnings = ServicePayment::where('expert_id',$serviceRequest->expert_id)->sum('sub_total');
                $withDraws = Payouts::where('user_id',$serviceRequest->expert_id)->sum('amount');
                $closing_balance = round($totalEarnings - $withDraws ,2);
                // service payment
                $servicePayment = new ServicePayment();
                $servicePayment->service_id = $data['request_id'];
                $servicePayment->expert_id = $serviceRequest->expert_id;
                $servicePayment->job_duration = $data['job_duration'];
                $servicePayment->per_hour_rate = $serviceRequest->service->price;
                $servicePayment->total_price = $data['total_price'];
                $servicePayment->service_fee = $data['fee'];
                $servicePayment->admin_commission_per = $adminCommissionPercent;
                if(!empty($data['coupon_code'])){
                    $servicePayment->voucher_id = $data['coupon_code'];
                    $servicePayment->voucher_discount = $data['coupon_discount'];
                }else{
                    $userCoupon = UserCoupons::where(['user_id' => $data['user_id'],'status' => '1'])->first();
                    if(!empty($userCoupon)){
                        $servicePayment->voucher_id = $userCoupon->id;
                        if($userCoupon->is_fixed){
                            $servicePayment->voucher_discount = $userCoupon->discount_amount;
                        }else{
                            $servicePayment->voucher_discount = ($servicePayment->total_price)* ($userCoupon->discount_amount / 100);
                        }
                        UserCoupons::where(['id' => $userCoupon->id])->update(['status' => '0']);
                    }
                }
                $servicePayment->payment_method = $data['payment_method'];
                $stripeAmount = $servicePayment->total_price;
                // $servicePayment->stripe_payment_amount = $stripeAmount;
                $servicePayment->sub_total = $data['sub_total'];
                $servicePayment->expert_closing_balance = $closing_balance + $servicePayment->sub_total;
                $servicePayment->save();
                if(!empty($servicePayment->voucher_discount)){
                    $stripeAmount = $servicePayment->total_price - $servicePayment->voucher_discount;
                }
                $this->download($data['request_id']);
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randomString = $data['request_id'].'-';
                for ($i = 0; $i < 10; $i++) {
                    $index = rand(0, strlen($characters) - 1);
                    $randomString .= $characters[$index];
                }
                // updated chat status
                $response = Chat::where(['status' =>'1','receiver'=> $serviceRequest->user_id, 'sender'=> $serviceRequest->expert_id])
                ->orWhere(function($qry) use($serviceRequest){
                    $qry->where('status','1');
                    $qry->where('receiver', $serviceRequest->expert_id);
                    $qry->where('sender', $serviceRequest->user_id);
                })->first();
                if(!empty($response)){
                    Chat::where('id', $response->id)->update(['status' =>'0']);
                }
                if($stripeAmount < 0){
                    ServicePayment::where('service_id', $data['request_id'])->update(['stripe_txn_id' => $randomString,'payment_status' => 1]);
                    return response()->json([
                        'requestStatus' => 'success',
                        'message'=> trans('messages.success')
                    ]);
                }else{
                    if($data['payment_method']){
                        $finalAmount = 100 * $stripeAmount;
                        $stripe = \Stripe\Stripe::setApiKey($this->stripeKey);
                        $charge = \Stripe\Charge::create(array(
                            "amount" => ceil($finalAmount),
                            "currency" => "GBP",
                            "customer" => $serviceRequest->customer->profile->stripe_account_id
                        ));
                        if($charge->status == 'succeeded'){
                            ServicePayment::where('service_id', $data['request_id'])->update(['stripe_txn_id' => $charge->balance_transaction,'payment_status' => 1]);
                        }
                        return response()->json([
                            'requestStatus' => 'success',
                            'message'=> trans('messages.success'),
                        ]);
                    }else{
                        $balance = DB::select( DB::raw("SELECT (SUM(IF(flag = 1, amount, 0)) - SUM(IF(flag = 2, amount, 0))) as balance FROM user_wallet WHERE user_id ='$serviceRequest->user_id'") );
                        if($balance['0']->balance > $stripeAmount){
                            ServicePayment::where('service_id', $data['request_id'])->update(['stripe_txn_id' => $randomString,'payment_status' => 1]);
                            $wallet = new UserWallet();
                            $wallet->user_id = $serviceRequest->user_id;
                            $wallet->flag = 2;
                            $wallet->amount = $stripeAmount;
                            $wallet->description = "Debited For Service Id ".$data['request_id'];
                            $wallet->save();
                            return response()->json([
                                'requestStatus' => 'success',
                                'message'=> trans('messages.success')
                            ]);
                        }else{
                            return response()->json([
                                'requestStatus' => 'fail',
                                'message'=> trans('messages.low_balance')
                            ]);
                        }
                    }
                }
            }catch (\Exception $e){
                return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => trans('messages.something_wrong'),
                    'm' => $e->getMessage()
                ], 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('messages.ask_seeker_to_end_job')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * finishJobCustomer Logic
    */

    public function finishJobCustomer(Request $request){
        $data = json_decode($request->getContent(), true);
        $serviceRequest = ServiceRequest::with(['service'])->where(['id' => $data['request_id']])->first();
        if(!empty($serviceRequest)){
            try{
                $adminSettings = AdminSetting::where('type','commission_fees')->first();
                $adminCommission = $adminSettings->value / 100;
                $endTime = $data['date'];
                $breakInterval  = $serviceRequest->break_duration;
                $datetime1 = strtotime($serviceRequest->service_start_time);
                $datetime2 = strtotime($endTime);
                $interval  = abs($datetime2 - $datetime1);
                $interval = $interval - $breakInterval;
                $hours   = ceil($interval / 3600);
                $servicePayment = [];
                $servicePayment['expert_id'] = $serviceRequest->expert_id;
                $servicePayment['job_duration'] = $hours;
                $servicePayment['service_name'] = $serviceRequest->service->service;
                $servicePayment['service_start_time'] = $serviceRequest->service_start_time;
                $servicePayment['service_end_time'] = $endTime;
                $servicePayment['sub_total'] = round($serviceRequest->service->price * $hours,2);
                $servicePayment['fee'] = round($servicePayment['sub_total'] * $adminCommission, 2);
                $servicePayment['admin_commission_percent'] = $adminCommission;
                $servicePayment['total_price'] = round($servicePayment['sub_total'] - $servicePayment['fee'],2);
                return response()->json([
                    'requestStatus' => 'success',
                    'data'=> $servicePayment
                ]);
            }catch (\Exception $e){

                return response()->json(
                    ['requestStatus' => 'invalid', 'message' => trans('messages.something_wrong')]
                , 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('messages.no_job_found')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * deductJobPriceAndAddToWallet Logic
    */

    public function deductJobPriceAndAddToWallet(Request $request){
        $data = json_decode($request->getContent(), true);
        $serviceRequest = ServiceRequest::with(['service','customer.profile'])->where(['id' => $data['request_id']])->first();
        if(!empty($serviceRequest)){
            try{
                $stripe_account_id = $serviceRequest->customer->profile->stripe_account_id;
                $adminSettings = AdminSetting::where('type','commission_fees')->first();
                $adminCommission = $adminSettings->value / 100;
                $amount = $serviceRequest->service->price * 4;
                $finalAmount = 100 * round($amount + ($amount * $adminCommission),2);
                $charge = \Stripe\Charge::create(array(
                    "amount" => $finalAmount,
                    "currency" => "GBP",
                    "customer" => $stripe_account_id
                ));
                $objWallet = new UserWallet();
                $objWallet->user_id = $serviceRequest->user_id;
                $objWallet->flag = 1;
                $objWallet->amount = $finalAmount;
                $objWallet->description = "Credited from ".$stripe_account_id;
                $objWallet->save();
                return response()->json([
                    'requestStatus' => 'success'
                ]);
            }catch (\Exception $e){

                return response()->json(
                    ['requestStatus' => 'invalid', 'message' => trans('messages.something_wrong')]
                , 401);
            }
        }else{
            return response()->json([
                'requestStatus'=>'invalid',
                'message' => trans('messages.no_job_found')
            ], 401);
        }
    }

    /**
     * @param Request $request
     * listPayoutsForExpert Logic
    */

    public function listPayoutsForExpert(Request $request){
        $data = json_decode($request->getContent(), true);
        try{
            $user_id = $data['user_id'];
            $earnedMoney = ServicePayment::where('expert_id',$user_id)->sum('sub_total');
            $withDraws = Payouts::where('user_id',$user_id)->sum('amount');
            if(isset($earnedMoney[0]['revenue'])){
                $earnedMoney = round($earnedMoney[0]['revenue'] - $withDraws, 2);
            }else{
                $earnedMoney =  0;
            }
            $accountDetail = Profile::where(['user_id'=> $user_id])->first();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => [
                    'earnedMoney' => $earnedMoney,
                    'accountDetail' => $accountDetail
                ]
            ], 201);
        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * requestPayout Logic
    */

    public function requestPayout(Request $request){
        $data = json_decode($request->getContent(), true);
        try{
            $user_id = $data['user_id'];
            $amount = $data['amount'];
            $profileDetail = Profile::with('User')->where(['user_id'=> $user_id])->first();
            if(!empty($profileDetail->stripe_account_id)){
                $charge = \Stripe\Transfer::create([
                    "amount" => 100 * $amount,
                    "currency" => "GBP",
                    "destination" => $profileDetail->stripe_account_id
                ]);
                if($charge->id){
                    /** Payout Listing */
                    $payoutResponse = \Stripe\Payout::create([
                        "amount" => 100 * $amount,
                        "currency" => "GBP",
                        "destination" => $profileDetail->stripe_external_bank,
                        "source_type" => "card"
                    ],['stripe_account' => $profileDetail->stripe_account_id]);
                    if($payoutResponse->id){
                        $payout = new Payouts();
                        $payout->user_id = $user_id;
                        $payout->payout_request_id = $payoutResponse->id;
                        $payout->stripe_txn_id = $payoutResponse->balance_transaction;
                        $payout->amount = $amount;
                        $payout->status = 1;
                        $payout->stripe_account_id = $profileDetail->stripe_account_id;
                        $payout->stripe_external_bank = $profileDetail->stripe_external_bank;
                        $payout->save();
                    }

                    $message = "A new payout request for amount ".$amount." is initiated from your Hela Job account.";

                    UserNotification::send_requestPayout_mail($profileDetail->User->fullname, $profileDetail->User->email, $amount);
                    UserNotification::send_push_notification($message,$profileDetail->User->player_id,$profileDetail->User->id,'payment_notification');

                    return BaseResponse::response([
                        'requestStatus' => 'success',
                        'message' => trans('messages.success')
                    ], 201);
                }
            }else{
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => trans('messages.no_account_found'),
                ], 500);
            }
        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * listPayoutsTransection Logic
    */

    public function listPayoutsTransection(Request $request){
        $data = json_decode($request->getContent(), true);
        try {
            $query = ServiceRequest::with(['ServiceFeedback' => function ($query) use($data) {
                $query->where('receiver', '=', $data['user_id']);
                },'customer','expert','service','customer.profile','ServicePayment','service.ServiceParent'])->where('expert_id', $data['user_id'])->whereIn('status', [2]);
            if(!empty($data['filter'])){
                if($data['filter'] == 'week'){
                    $query = $query->whereBetween('booking_date', [
                        Carbon::parse('last monday')->startOfDay(),
                        Carbon::parse('next friday')->endOfDay(),
                    ]);
                }else if($data['filter'] == 'month'){
                    $query = $query->whereBetween('booking_date', [
                        Carbon::now()->startOfMonth(),
                        Carbon::now()->endOfMonth(),
                    ]);
                }else if($data['filter'] == 'quarter'){
                    $date = new \Carbon\Carbon('');
                    $firstOfQuarter = $date->firstOfQuarter();
                    $lastOfQuarter = $date->lastOfQuarter();
                    $query = $query->whereBetween('booking_date', [$firstOfQuarter,$lastOfQuarter]);
                }
            }
            $totalRecord = $query->get()->count();
            $data = $query->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data,
                'totalRecord' => $totalRecord
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * listPayoutsTransection Logic
    */

    public function listServicesHistoryExeprt(Request $request){
        $data = json_decode($request->getContent(), true);
        try {
            $status = (!empty($data['status'])) ? $data['status'] : '2';
            $query = ServiceRequest::with(['ServiceFeedback' => function ($query) use($data) {
                $query->where('receiver', '=', $data['user_id']);
                },'customer','expert','service','customer.profile','ServicePayment','service.ServiceParent'])->where('expert_id', $data['user_id'])->whereIn('status', [$status]);
            $data = $query->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * listCustomerTransection Logic
    */

    public function listCustomerTransection(Request $request){
        $data = json_decode($request->getContent(), true);
        try {
            $data = ServiceRequest::with(['ServiceFeedback' => function ($query) use($data) {
                $query->where('receiver', '=', $data['user_id']);
                },'customer','expert','service','customer.profile','ServicePayment','service.ServiceParent'])->where('user_id', $data['user_id'])->whereIn('status', [2])->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * listCustomerTransection Logic
    */

    public function listExpertHistoryForCustomer(Request $request){
        $data = json_decode($request->getContent(), true);
        try {
            $data = ServiceRequest::with(['ServiceFeedback' => function ($query) use($data) {
                $query->where('receiver', '=', $data['user_id']);
                },'customer','expert','service','customer.profile','ServicePayment','service.ServiceParent'])->where(
                ['user_id' => $data['user_id'],'expert_id' => $data['expert_id']])->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $data
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * serviceRating Logic
    */

    public function serviceRating(Request $request) {
        $data = json_decode($request->getContent(), true);
        try{
            $serviceFeedback = ServiceFeedback::with('Sender' , 'Receiver')->where([
                'service_request_id' => $data['service_request_id'],
                'receiver' => $data['receiver']
            ])->first();
            if(!$serviceFeedback){
                $serviceFeedback = new ServiceFeedback();
            }
            $serviceFeedback->receiver = $data['receiver'];
            $serviceFeedback->service_request_id = (!empty($data['service_request_id'])) ? $data['service_request_id'] : '';
            $serviceFeedback->sender = $data['sender'];
            $serviceFeedback->rate = $data['rate'];
            $serviceFeedback->description = $data['description'];
            $serviceFeedback->save();
            $serviceFeedback = ServiceFeedback::with('Sender' , 'Receiver')->where(['id' => $serviceFeedback->id])->first();
            if($data['rate'] == 1){
                $message = $serviceFeedback->Sender->fullname."rated you 100%";
            }else{
                $message = $serviceFeedback->Sender->fullname."rated you 0%";
            }
            UserNotification::send_push_notification($message, $serviceFeedback->Receiver->player_id,$serviceFeedback->Receiver->id,'rating_notification');
            return BaseResponse::response([
                'requestStatus' => 'success'
            ], 201);


        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * listReviews Logic
    */


    public function listReviews(Request $request) {
        $data = json_decode($request->getContent(), true);
        try{
            $serviceFeedback = ServiceFeedback::with('Service','Sender','Receiver')->where('receiver', $data['user_id'])->get();
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => $serviceFeedback
            ], 201);
        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * serviceReport Logic
    */

    public function serviceReport(Request $request) {
        $data = $request->all();
        try {
            $serviceFeedback = new ServiceReport();
            $serviceFeedback->service_id = (!empty($data['service_id'])) ? $data['service_id'] : null;
            $serviceFeedback->user_id = $data['user_id'];
            $serviceFeedback->title = $data['title'];
            $serviceFeedback->type = $data['type'];
            $serviceFeedback->detail = $data['detail'];
            if($request->file('image')){
                $path = $request->file('image')->store('',['disk' => 'serviceReports']);
                $serviceFeedback->image = 'serviceReports/'.$path;
            }
            if($serviceFeedback->save()){
                return BaseResponse::response([
                    'requestStatus' => 'success',
                ], 201);
            }else{
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => trans('messages.something_wrong'),
                ], 500);
            }
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.something_wrong'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * reshceduleService Logic
    */

    public function reshceduleService(Request $request) {
        $data = json_decode($request->getContent(), true);
        try{
            if(!empty($data['dates'])){
                foreach($data['dates'] as $key=>$value){
                    $serviceRequest = new ServiceRequest();
                    $serviceRequest->user_id = $data['user_id'];
                    $serviceRequest->expert_id = $data['expert_id'];
                    $serviceRequest->service_id = $data['service_id'];
                    $serviceRequest->booking_date = $value['serviceDate'];
                    $serviceRequest->service_start_time = $value['startTime'];
                    // $serviceRequest->service_end_time = $value['endTime'];
                    $serviceRequest->latitude = $data['latitude'];
                    $serviceRequest->longitude = $data['longitude'];
                    $serviceRequest->address = $data['address'];
                    // $serviceRequest->service_end_time = $value['endTime'];
                    $serviceRequest->status = '1';
                    $serviceRequest->save();
                }
                $profileDetail = Profile::where(['user_id'=> $data['user_id']])->first();
                $charge = \Stripe\Charge::create(array(
                    "amount" => $data['total'] * 100,
                    "currency" => "GBP",
                    "customer" => $profileDetail->stripe_account_id
                ));
                $objWallet = new UserWallet();
                $objWallet->user_id = $serviceRequest->user_id;
                $objWallet->flag = 1;
                $objWallet->amount = $serviceRequest->service->price * 4;
                $objWallet->description = "Credited from ".$profileDetail->stripe_account_id;
                $objWallet->save();
                return BaseResponse::response([
                    'requestStatus' => 'success'
                ], 201);
            }else{
                return BaseResponse::response([
                    'requestStatus' => 'error',
                    'message' => trans('messages.error'),
                    'dev' => $e->getMessage()
                ], 500);
            }
        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('messages.error'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }
}
