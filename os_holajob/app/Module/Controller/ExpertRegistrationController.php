<?php
namespace App\Module\Controller;
use Illuminate\Http\Request;
use App\Module\Model\User;
use App\Module\Model\ExpertAvailablity;
use App\Module\Model\ServiceImages;
use App\Module\Model\EmergencyContacts;
use App\Module\Model\Profile;
use App\Module\Model\UserAddresses;
use App\Module\Model\UserServices;
use App\Module\Model\UserLanguages;
use App\Module\Model\UserDocuments;
use \App\Module\Service\SmsService;
use Validator;
use App\Module\BaseResponse;
use \App\Module\Model\OtpLog;
use Helper;

/**
 * Description of ExpertRegistrationController
 * @author Shweta trivedi
*/

class ExpertRegistrationController extends BaseController {

    /**
     * @param Request $request
     * register Logic
    */

    public function register(Request $request) {
        $validated_data = json_decode($request->getContent(), true);
        $validator = Validator::make($validated_data, [
            'email' => 'required|string|email|max:255|unique:users',
            'firstname' => 'required',
            'mobile_number' => 'required|min:10|numeric|unique:users',
            'lastname' => 'required',
            'password' => 'required',
            'dob' => 'required',
            'gender' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => implode('<br />', $validator->errors()->all())
            ], 422);
        }
        $model = '';
        $otp = '';
        try {
            \DB::transaction(function () use (&$model, $validated_data, &$otp) {
                $userData['fullname']=$validated_data['firstname']." ".$validated_data['lastname'];
                $userData['first_name']=$validated_data['firstname'];
                $userData['last_name']=$validated_data['lastname'];
                $userData['username']=$validated_data['firstname'];
                $userData['email']=$validated_data['email'];
                $userData['mobile_number']= $validated_data['mobile_number'];
                $userData['password'] = bcrypt($validated_data['password']);
                $userData['user_type_id'] = 2;
                $userData['profile_complete_step'] = 1;
                $model = User::create($userData);
                if(!empty($model)){
                    $profile = new Profile;
                    $profile->user_id =  $model['id'];
                    $profile->dob =  $validated_data['dob'];
                    $profile->gender =  $validated_data['gender'];
                    $profile->save();
                    //Save Address
                    $address = new UserAddresses;
                    $address->user_id =  $model['id'];
                    $address->city =  $validated_data['city'];
                    $address->postal_code =  $validated_data['postal_code'];
                    $address->address_line1 =  $validated_data['address_line1'];
                    $address->address_line2 =  $validated_data['address_line2'];
                    $address->is_primary =  1;
                    $address->save();
                    $model->profile_complete_step = 2;
                    $model->save();
                }
                $otp = (new SmsService())->sendOTP($validated_data['mobile_number']);
                (new OtpLog())->saveMobileVerificationOtp($model['id'], $otp);
                $data =  $model->toArray();
                $data['otp'] = $otp;
                $model->verification_token = $this->sendMail($data);
                $model->save();
            });
            $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($model);
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => 'Registration successful, an OTP has been sent to your mobile number and email address for verification!',
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => 3600,
                'data' => array('OTP'=>$otp, 'user_id'=>$model['id']),
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => 'Some issue occured in saving data.',
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Social Signup Step1 - API to update user after social login
     * @author Shweta trivedi
    */

     public function socialRegister(Request $request) {
        $validated_data = json_decode($request->getContent(), true);
        $validator = Validator::make($validated_data, [
            'user_id' => 'required',
            'email' => 'required|string|email|max:255',
            'firstname' => 'required',
            'lastname' => 'required',
            'mobile_number' => 'required|min:10|numeric',
            'dob' => 'required',
            'gender' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => implode('<br />', $validator->errors()->all())
                            ], 422);
        }
        try {
            $model = '';$otp = '';
            \DB::transaction(function () use (&$model, $validated_data, &$otp) {
                $user = User::where('id', $validated_data['user_id'])->first();
                $user->fullname =$validated_data['firstname']." ".$validated_data['lastname'];
                $user->first_name = $validated_data['firstname'];
                $user->last_name = $validated_data['lastname'];
                $user->username = $validated_data['firstname'];
                $user->email = $validated_data['email'];
                $user->mobile_number = $validated_data['mobile_number'];
                $user->user_type_id = 2;
                $user->profile_complete_step = 1;
                $user->save();
                if($user->save()){
                    $profile = Profile::where('user_id',$validated_data['user_id'])->first();
                    if(empty($profile)){
                        $profile = new Profile;
                    }
                    $profile->user_id =  $validated_data['user_id'];
                    $profile->dob =  $validated_data['dob'];
                    $profile->gender =  $validated_data['gender'];
                    $profile->save();
                    //Save Address
                    $address = UserAddresses::where('user_id',$validated_data['user_id'])->first();
                    if(empty($address)){
                        $address = new UserAddresses;
                    }
                    $address->user_id = $validated_data['user_id'];
                    $address->city =  $validated_data['city'];
                    $address->postal_code =  $validated_data['postal_code'];
                    $address->address_line1 =  $validated_data['address_line1'];
                    $address->address_line2 =  $validated_data['address_line2'];
                    $address->is_primary =  1;
                    $address->save();
                    $user->profile_complete_step = 2;
                    $user->save();
                }
                $otp = (new SmsService())->sendOTP($validated_data['mobile_number']);
                (new OtpLog())->saveMobileVerificationOtp($validated_data['user_id'], $otp);
                $user = User::where('id', $validated_data['user_id'])->first();
                $data =  $user->toArray();
                $data['otp'] = $otp;
                $user->verification_token = $this->sendMail($data);
                $user->save();
            });
            $tokenData = User::where('id',$validated_data['user_id'])->first();
            $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($tokenData);
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => 'Registration successful, an OTP has been sent to your mobile number and email address for verification!',
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => 3600,
                'data' => array('OTP'=>$otp, 'user_id'=>$validated_data['user_id']),
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => 'Some issue occured in saving data.',
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Signup Step - Upload identity document front and back
     * @author Shweta trivedi
    */

    public function uploadIdentityDocument(Request $request) {
        try {
            //validate header
            $token = $request->header('Authorization');
            if(empty($token))
            {
                return response()->json([
                        'requestStatus' => 'error',
                        'message' => 'Access Token is required'
                    ], 500);
            }
            $authUser = auth()->user();
            if(!$authUser)
            {
                    return response()->json([
                        'requestStatus' => 'error',
                        'message' => 'Invalid User'
                    ], 500);
            }
            //validated data
            $request->validate([
                'document_image' => 'required|mimes:jpeg,bmp,png'
            ]);
            $path = $request->file('document_image')->storePublicly('public/document');
            return BaseResponse::response([
                'requestStatus' => 'success',
                'data' => ['document_image' => $path]
            ], 201);
         }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => 'Some issue occured in saving data.',
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Signup Step - Store Identity Front and back Document in database
     * @author Shweta trivedi
    */

    public function storeIdentityDocument(Request $request) {
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'user_id' => 'required',
            'front_image' => 'required',
            'back_image' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => implode('<br />', $validator->errors()->all())
                            ], 422);
        }
        try {
            $document = new UserDocuments ;
            $document->user_id = $data['user_id'] ;
            $document->front_image = $data['front_image'];
            $document->back_image = $data['back_image'];
            $document->save();

            $user = User::where('id',$data['user_id'])->first();
            $user->profile_complete_step = 2;
            if($user->save())
            {
                return BaseResponse::response([
                        'requestStatus' => 'success',
                        'message' => "Document saved successfully."
                            ], 201);
            }
            else {
                return BaseResponse::response([
                            'requestStatus' => 'error',
                            'message' => 'Some issue occured in saving data.',
                                ], 500);
            }
        }catch (\Exception $e) {
            return BaseResponse::response([
                        'requestStatus' => 'error',
                        'message' => 'Some issue occured in saving data.',
                        'dev' => $e->getMessage()
            ], 500);
        }
    }


    /**
     * Signup Step - Upload Profile image
     * @author Shweta trivedi
     */

    public function uploadProfileImage(Request $request) {
        try {
           $token = $request->header('Authorization');
           if(empty($token))
           {
               return response()->json([
                    'requestStatus' => 'error',
                    'message' => 'Access Token is required'
                ], 500);
           }
           $authUser = auth()->user();
           if(!$authUser)
           {
                return response()->json([
                    'requestStatus' => 'error',
                    'message' => 'Invalid User'
                ], 500);
            }
            //validated data
            $request->validate([
                'profile_image' => 'required|mimes:jpeg,bmp,png'
            ]);
            $path = $request->file('profile_image')->store('',['disk' => 'user']);
            return BaseResponse::response([
                    'requestStatus' => 'success',
                    'data' => ['image_path' => 'avatars/'.$path]
                ], 201);
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => 'Some issue occured in saving data.',
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Signup Step - Store Profile image path
     * @author Shweta trivedi
    */

    public function storeProfileImage(Request $request) {
         try {
           //validate header
           $token = $request->header('Authorization');
           if(empty($token)) {
               return response()->json([
                    'requestStatus' => 'error',
                    'message' => 'Access Token is required'
                ], 500);
           }
           $authUser = auth()->user();
           if(!$authUser) {
                return response()->json([
                    'requestStatus' => 'error',
                    'message' => 'Invalid User'
                ], 500);
            }
          //validated data
          $data = json_decode($request->getContent(), true);
          $validator = Validator::make($data, [
                'user_id' => 'required',
                'image_path' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json([
                            'requestStatus' => 'invalid',
                            'message' => implode('<br />', $validator->errors()->all())
                                ], 422);
            }

            $user = User::where('id',$data['user_id'])->first();
            $user->profile_complete_step = 3;
            $user->profile_image = $data['image_path'];
            if($user->save())
            {
                return BaseResponse::response([
                        'requestStatus' => 'success',
                        'message' => "Profile image saved successfully."
                            ], 201);
            }
            else {
                return BaseResponse::response([
                            'requestStatus' => 'error',
                            'message' => 'Some issue occured in saving data.',
                                ], 500);
            }
      }catch (\Exception $e) {
            return BaseResponse::response([
                        'requestStatus' => 'error',
                        'message' => 'Some issue occured in saving data.',
                        'dev' => $e->getMessage()
                            ], 500);
        }
    }

    /**
     * Signup Step - Store Biographic, Emergency and Insurance information
     * @author Shweta trivedi
    */

    public function storeOtherInformation(Request $request) {
        try {
            $token = $request->header('Authorization');
            if(empty($token)) {
                return response()->json([
                        'requestStatus' => 'error',
                        'message' => 'Access Token is required'
                    ], 500);
            }
            $authUser = auth()->user();
            if(!$authUser) {
                return response()->json([
                    'requestStatus' => 'error',
                    'message' => 'Invalid User'
                ], 500);
            }
            //validated data
            $validated_data = json_decode($request->getContent(), true);
            $validator = Validator::make($validated_data, [
                'user_id' => 'required',
                'about' => '',
                'emergency_contact_name' => '',
                'emergency_contact_number' => 'min:10|numeric',
                'emergency_contact_relationship' => '',
                'insurance_number' => '',
                'user_services' => '',
                'user_languages' => ''
            ]);
            if ($validator->fails()) {
                return response()->json([
                            'requestStatus' => 'invalid',
                            'message' => implode('<br />', $validator->errors()->all())
                                ], 422);
            }
            $profile = Profile::where('user_id',$validated_data['user_id'])->first();
            if(empty($profile)){
                $profile = new Profile;
            }
            $profile->user_id = $validated_data['user_id'];
            $profile->about = $validated_data['about'];
            $profile->emergency_contact_name =$validated_data['emergency_contact_name'];
            $profile->emergency_contact_number =$validated_data['emergency_contact_number'];
            $profile->emergency_contact_relationship = $validated_data['emergency_contact_relationship'];
            $profile->insurance_number = $validated_data['insurance_number'];
            $profile->save();

            //Save Expert Services
            for ($i = 0; $i < count($validated_data['user_services']); $i++){
                $services = new UserServices;
                $services->user_id  =  $validated_data['user_id'];
                $services->service_id = $validated_data['user_services'][$i];
                $services->save();
            }
            for ($i = 0; $i < count($validated_data['user_languages']); $i++){
                $languages = new UserLanguages;
                $languages->user_id =  $validated_data['user_id'];
                $languages->language_id = $validated_data['user_languages'][$i];
                $languages->save();
            }
            $data = array('user_id'=>$validated_data['user_id'],'profile_complete_step'=>5,'is_profile_complete'=>1,'status'=>0);
            $this->updateProfileStep($data);
            $data = User::with('profile','addresses','documents')->where('id', $validated_data['user_id'])->first()->toArray();
            $tokenData = User::where('id', $validated_data['user_id'])->first();
            $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($tokenData);
            return BaseResponse::response([
                'requestStatus' => 'success',
                'message' => 'Profile data saved successfully',
                'data' => $data,
                'token' => $token,
            ], 201);
        } catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => 'Some issue occured in saving data.',
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * sendEmailVerificationMail Logic
    */

    private function sendEmailVerificationMail($user) {
        $verification = \Illuminate\Support\Str::random(45);
        \Mail::to($user['email'])->send(new \App\Module\Mail\EmailVerification($validated_data));
        return $verification;
    }

    /**
     * @param Request $request
     * sendOTP Logic
    */

    private function sendOTP($user) {
        return (new SmsService())->sendOTP($request->mobile_number);
    }

    /**
     * @param Request $request
     * validate Logic
    */

    private function validate(Request $request) {
        $rule = [
            'profile_pic' => '',
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile' => 'required|min:10|numeric',
            'password' => 'required|min:6',
            'dob' => '',
            'about' => '',
            'emergency_contact_name' => '',
            'emergency_contact_number' => '',
            'emergency_contact_relationship' => '',
            'insurance_number' => '',
            'user_services.*' => '',
            'user_languages.*' => '',
        ];
        return $request->validate($rule);
    }

    /**
     * @param Request $request
     * sendMail Logic
    */

    private function sendMail($user) {
        $validated_data = $user ;
        $verification = \Illuminate\Support\Str::random(45);
        $validated_data['verification_token'] = $verification;
        \Mail::to($validated_data['email'])->send(new \App\Module\Mail\EmailVerification($validated_data));
        return $verification;
    }

    /**
     * @param Request $request
     * updateProfileStep Logic
    */

    private function updateProfileStep($data) {
        try {
            $user = User::where('id', $data['user_id'])->first();
            $user->profile_complete_step = $data['profile_complete_step'];
            $user->is_profile_complete = $data['is_profile_complete'];
            $user->status = $data['status'];
            if($user->save())
                return true;
            else
                return false;
        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => 'Some issue occured in saving data.',
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /*
        updatelocationExpert

    */

    public function updatelocationExpert(Request $request){
        $data = json_decode($request->getContent(), true);
        try {
            if(!empty($data['formatted_address']) && !empty($data['user_id']) && !empty($data['latitude']) && !empty($data['longitude'])){
                $currentlocation = UserAddresses::where('user_id', $data['user_id'])->first();
                if(!$currentlocation){
                    $currentlocation = new UserAddresses();
                }
                $currentlocation->user_id = $data['user_id'];
                $currentlocation->latitude = $data['latitude'];
                $currentlocation->current_latitude = $data['latitude'];
                $currentlocation->longitude = $data['longitude'];
                $currentlocation->current_longitude = $data['longitude'];
                $currentlocation->formatted_address = $data['formatted_address'];
                $currentlocation->is_primary = 1;
                $currentlocation->save();
                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'message' => 'Saved',
                ],200);
            }else{
                $responseflag  =  "There is an error. please try again later.";
                if(!isset($data['user_id']) || empty($data['user_id'])){
                    $responseflag  =  "Invalid user id.";
                }elseif(!isset($data['latitude']) || empty($data['latitude']) || !isset($data['longitude']) || empty($data['longitude'])){
                  $responseflag  =  "Invalid latitude longitude.";
                }elseif(!isset($data['formatted_address']) || empty($data['formatted_address'])){
                    $responseflag  =  "Invalid address.";
                }
                return BaseResponse::response([
                  'requestStatus' => 'error',
                  'message' => $responseflag,
                ],200);
            }

        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => 'Some issue occured in saving data.',
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * updatelocationCustomer Logic
    */

    public function updatelocationCustomer(Request $request){
        $data = json_decode($request->getContent(), true);
        try {
            if(!empty($data['formatted_address']) && !empty($data['user_id']) && !empty($data['latitude']) && !empty($data['longitude'])){
                $currentlocation = UserAddresses::where(['user_id'=> $data['user_id'],'is_primary' => 1])->first();
                if(!$currentlocation){
                    $currentlocation = new UserAddresses();
                }
                $currentlocation->user_id = $data['user_id'];
                $currentlocation->latitude = $data['latitude'];
                $currentlocation->current_latitude = $data['latitude'];
                $currentlocation->longitude = $data['longitude'];
                $currentlocation->current_longitude = $data['longitude'];
                $currentlocation->formatted_address = $data['formatted_address'];
                $currentlocation->is_primary = 1;
                $currentlocation->save();
                return BaseResponse::response([
                    'requestStatus' => 'success',
                    'message' => 'Saved',
                ],200);
            }else{
                $responseflag  =  "There is an error. please try again later.";
                if(!isset($data['user_id']) || empty($data['user_id'])){
                    $responseflag  =  "Invalid user id.";
                }elseif(!isset($data['latitude']) || empty($data['latitude']) || !isset($data['longitude']) || empty($data['longitude'])){
                  $responseflag  =  "Invalid latitude longitude.";
                }elseif(!isset($data['formatted_address']) || empty($data['formatted_address'])){
                    $responseflag  =  "Invalid address.";
                }
                return BaseResponse::response([
                  'requestStatus' => 'error',
                  'message' => $responseflag,
                ],200);
            }

        }catch (\Exception $e) {
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => 'Some issue occured in saving data.',
                'dev' => $e->getMessage()
            ], 500);
        }
    }
}
