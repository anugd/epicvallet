<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class VerifyAdmin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('admin')->check()){
            return $next($request);
        }else{
            if(Auth::check()){
                return redirect()->intended('/expert-dashboard');
            }else{
                return redirect()->intended('/admin/login');
            }
        }
        return $next($request);
    }

}
