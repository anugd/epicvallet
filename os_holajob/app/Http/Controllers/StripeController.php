<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Response;
use \App\Module\Model\User;
use \App\Module\Model\Profile;
use \App\Module\Model\UserCard;
use \App\Module\Model\Payouts;
use \App\Module\Model\UserWallet;
use DB;

class StripeController extends Controller{

    public $stripeKey = '';

    /*
     * @method: __construct
     * @params: NA
     * @description: Loads Stripe API Key
     * @return: void
     * @createdBy: Shweta Trivedi
     *///

    public function __construct() {
        if (config('app.MODE') == 'live') {
            \Stripe\Stripe::setApiKey(config('app.STRIPE_LIVE_KEY'));
            $this->stripeKey = config('app.STRIPE_LIVE_KEY');
        } else {
            \Stripe\Stripe::setApiKey(config('app.STRIPE_TEST_KEY'));
            $this->stripeKey = config('app.STRIPE_TEST_KEY');
        }
    }

    /**
        createAccount

    */

    public function createAccount(Request $request){
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'user_id' => 'required',
            'routing_number'=> 'required',
            'account_number'=> 'required',
        ]);
        if ($validator->fails()) {
         return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => implode('<br />', $validator->errors()->all())
                        ], 422);
        }
        try{
            $user = User::with('profile','addresses','documents')->where('id', $data['user_id'])->first();
            if($user){
                $dobDay = date('d', strtotime($user->profile->dob)) ;
                $dobMonth = date('m', strtotime($user->profile->dob)) ; //01
                $dobYear = date('Y', strtotime($user->profile->dob)) ; ;
                $firstName = $user->fullname;
                $lastName = $user->lastname;
                $mobileNumber = $user->mobile_number;
                $type = "individual";
                $addressLine1 = $user->addresses[0]->address_line1;
                $addressPostalCode = $user->addresses[0]->postal_code;
                $addressCity = $user->addresses[0]->city;
                $addressState = $user->addresses[0]->state;
                $documentsFront = $user->documents[0]->front_image;
                $documentsBack = $user->documents[0]->back_image;
                $userProfile = array(
                    'dobDay' => $dobDay,
                    'dobMonth' => $dobMonth,
                    'dobYear' => $dobYear,
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'type' => $type,
                    'mobileNumber' => $mobileNumber,
                    'addressLine1' => $addressLine1,
                    'addressPostalCode' => $addressPostalCode,
                    'addressCity' => $addressCity,
                    'addressState' => $addressState,
                    'documentsFront' => $documentsFront,
                    'documentsBack' => $documentsBack
                );
                \Stripe\Stripe::setApiKey($this->stripeKey);
                try{
                    $response = \Stripe\Account::create([
                        "type" => "custom",
                        "email" => $user->email,
                        "external_account" => [
                            "object" => "bank_account",
                            "country" => "GB",
                            "currency" => "GBP",
                            "routing_number" => $data['routing_number'],//"108800",
                            "account_number" => $data['account_number'],//"00012345"
                        ],
                        "payout_schedule" => [
                            "interval" => 'manual',
                        ],
                        "tos_acceptance" => [
                            "date" => strtotime("now"),
                            "ip" => $_SERVER['REMOTE_ADDR'],
                        ],
                    ]);
                }catch (\Stripe\Error\Base $e) {
                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => $e->getMessage()
                    ], 401);
                }
                if (is_object($response) && !empty($response->id)) {
                    $profile = Profile::where('user_id', $user->id)->first();
                    $profile->stripe_account_id = $response->id;
                    $profile->stripe_external_bank = $response->external_accounts->data['0']->id;
                    $profile->sripe_status = "0";
                    $profile->save();
                    $verified1 = $this->accountVerificationStep1($response->id,$userProfile);
                    $verified2 = $this->accountVerificationStep2($response->id,$userProfile);
                    $stripeData = $this->accountRetrieve($response->id);
                    if($verified1 && $verified2){
                        $profile->sripe_status = "1";
                        $profile->save();
                        return response()->json([
                            'requestStatus' => 'success',
                            'message' => trans('messages.stripe_verified')
                        ]);
                    }else{
                        if( !$verified1 ){
                            return response()->json([
                                'requestStatus' => 'invalid',
                                'message' => trans('messages.stripe_address_not_verfied')
                            ], 401);
                        }
                        if(!$verified2){
                            return response()->json([
                                'requestStatus' => 'invalid',
                                'message' => trans('messages.stripe_document_not_verfied')
                            ], 401);
                        }
                    }
                }else{
                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => $e->getMessage()
                    ], 401);
                }
            }
        }catch (Exception $e) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => $e->getMessage()
            ], 401);
        }
    }

    /*
     * @description: Stripe identity-verification step 1
     * @return: void
     * @createdBy: Shweta Trivedi
    *///

    public function accountVerificationStep1($stripeAccountId,$user){
        try{
            \Stripe\Stripe::setApiKey($this->stripeKey);
            $account = \Stripe\Account::retrieve($stripeAccountId);
            $account->legal_entity->dob->day = $user['dobDay'] ;
            $account->legal_entity->dob->month = $user['dobMonth'];
            $account->legal_entity->dob->year = $user['dobYear'];
            $account->legal_entity->first_name = $user['firstName'];
            $account->legal_entity->last_name = $user['lastName'];
            $account->legal_entity->type = $user['type'];
            $account->legal_entity->phone_number = $user['mobileNumber'] ;
            $account->legal_entity->address->line1 = $user['addressLine1'] ;
            $account->legal_entity->address->postal_code = $user['addressPostalCode'] ;
            $account->legal_entity->address->city = $user['addressCity'] ;
            $account->legal_entity->address->state = $user['addressState'] ;
            if($account->save()){
                return true;
            }
        }catch (\Stripe\Error\Base $e) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => $e->getMessage()
            ], 401);
        }
    }

    /*
     * @description: Stripe identity-verification step 2
     * @return: void
     * @createdBy: Shweta Trivedi
    *///

    public function accountVerificationStep2($stripeAccountId,$user){
        try{
            \Stripe\Stripe::setApiKey($this->stripeKey);
            $path = public_path($user['documentsFront']);
            if (file_exists($path)) {
                $fp = fopen($path, 'r');
                $file_obj = \Stripe\FileUpload::create([
                    "purpose" => "identity_document",
                    "file" => $fp,
                ], ["stripe_account" => $stripeAccountId]);
                $file = $file_obj->id;
                $account = \Stripe\Account::retrieve($stripeAccountId);
                $account->legal_entity->personal_id_number = 123456789;
                $account->legal_entity->verification->document = $file;
                if($account->save()){
                    return true;
                }
            }else{
                return false;
            }
        }catch (\Stripe\Error\Base $e) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => $e->getMessage()
            ], 401);
        }
    }

    public function accountRetrieve($stripeAccountId){
        \Stripe\Stripe::setApiKey($this->stripeKey);
        $account = \Stripe\Account::retrieve($stripeAccountId);
        return $account;
    }

    /**
     * Feature to add card for API and web
     * @param Request $request
    */

    public function createCard(Request $request) {
        if($request->is('api/*')){
            $data = json_decode($request->getContent(), true);
        }else{
            $data = $request->all();
        }
        $validator = Validator::make($data, [
            'user_id' => 'required',
            'card_no' => ['required'],
            'card_month' => ['required'],
            'card_year' => ['required'],
            'card_cvv' => ['required'],
            'set_primary' => ['required'],
        ]);
        if($validator->fails()){
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => implode('<br />', $validator->errors()->all())
            ]);
        }
        $stripe = \Stripe\Stripe::setApiKey($this->stripeKey);
        try{
            $user = User::with('profile')->where('id', $data['user_id'])->first();
            $cardInfo = \Stripe\Token::create([
                "card" => [
                    "number" => $data['card_no'],
                    "exp_month" => $data['card_month'],
                    "exp_year" => $data['card_year'],
                    "cvc" => $data['card_cvv'],
                ]
            ]);
            if(!isset($cardInfo['id'])){
                return response()->json([
                'requestStatus' => 'invalid',
                'message' => "Stripe token empty."]);
            }else{
                $customer = \Stripe\Customer::create(array(
                    "source" => $cardInfo['id'],
                    "description" => $user->email,
                ));
                if(isset($customer->id) && !empty($customer->id)) {
                    $profile = Profile::where('user_id',$data['user_id'])->first();
                    if(!empty($profile)){
                        $profile->stripe_account_id = $customer->id;
                        $profile->save();
                    }else{
                        $profile = new Profile();
                        $profile->user_id = $data['user_id'];
                        $profile->stripe_account_id = $customer->id;
                        $profile->save();
                    }
                }
                if($data['set_primary'] == 1){
                    $userCard = UserCard::where('user_id', $data['user_id'])->where('set_primary',1)->first();
                    if($userCard){
                        $userCard->set_primary = 0;
                        $userCard->save();
                    }
                }

                $card_id = $cardInfo['card']['id'];
                $objCard = new UserCard();
                $objCard->user_id = $data['user_id'];
                $objCard->stripe_card_id = $card_id;
                $objCard->brand = $cardInfo['card']->brand;
                $objCard->card_number = $cardInfo['card']->last4;
                $objCard->exp_month = $cardInfo['card']->exp_month;
                $objCard->exp_year = $cardInfo['card']->exp_year;
                $objCard->set_primary = $data['set_primary'];

                if ($objCard->save()) {
                    return response()->json([
                        'requestStatus' => 'success',
                        'message' => trans('messages.card_added_success'),
                    ]);
                } else {
                    return response()->json([
                        'requestStatus' => 'error',
                        'message' => trans('messages.process_card_failed')
                    ]);
                }
             }
        }catch (\Stripe\Error\Base $e) {
            return response()->json([
            'requestStatus' => 'invalid',
            'message' => $e->getMessage()]);
        }
    }

     /**
     * Feature to get all user cards for API and web
     * @param Request $request
     */

    public function getCard(Request $request) {
        if( $request->is('api/*')){
            $data = json_decode($request->getContent(), true);
        }else{
            $data = $request->all();
        }

        $validator = Validator::make($data, [
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'requestStatus' => 'error',
                'message' => implode('<br />', $validator->errors()->all())
            ]);
        }
        try{
            $userCard = UserCard::where('user_id', $data['user_id'])->where('set_primary', 1)->get()->toArray();
            if ($userCard) {
                return response()->json([
                    'requestStatus' => 'success',
                    'data' => $userCard,
                ]);
            }else{
                return response()->json([
                    'requestStatus' => 'error',
                    'message' => trans('messages.empty_card')
                ]);
            }
        }catch (\Stripe\Error\Base $e) {
            return response()->json([
                'requestStatus' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Feature to add Money to wallet for API and web
     * @param Request $request
    */

    public function addMoney(Request $request) {
        if($request->is('api/*')){
            $data = json_decode($request->getContent(), true);
        }else{
            $data = $request->all();
        }
        $validator = Validator::make($data, [
            'user_id' => 'required',
            'card_id' => ['required'],
            'amount' => ['required'],
        ]);
        if($validator->fails()) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => implode('<br />', $validator->errors()->all())
            ]);
        }
        $stripe = \Stripe\Stripe::setApiKey($this->stripeKey);
        try{
            $profile = Profile::where('user_id',$data['user_id'])->first();
            if(!empty($profile)){
                $stripe_account_id = $profile->stripe_account_id;
            }else{
                return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => trans('messages.stripe_account_not_created')
                ]);
            }
            $amountInCents = 100 * $data['amount'];
            $charge = \Stripe\Charge::create(array(
                "amount" => $amountInCents,
                "currency" => "GBP",
                "customer" => $stripe_account_id
            ));
            $objWallet = new UserWallet();
            $objWallet->user_id = $data['user_id'];
            $objWallet->flag = 1;
            $objWallet->amount = $data['amount'];
            $objWallet->description = "Credited from ".$stripe_account_id;
            if ($objWallet->save()) {
                return response()->json([
                    'requestStatus' => 'success',
                    'message' => trans('messages.amount_added'),
                ]);
            }else{
                return response()->json([
                    'requestStatus' => 'error',
                    'message' => trans('messages.error')
                ]);
            }
        }catch (\Stripe\Error\Base $e) {
            return response()->json([
            'requestStatus' => 'invalid',
            'message' => $e->getMessage()]);
        }
    }

    /**
        listWalletAmount

    */

    public function listWalletAmount(Request $request) {
        if($request->is('api/*')){
            $data = json_decode($request->getContent(), true);
        }else{
            $data = $request->all();
        }
        $validator = Validator::make($data, [
            'user_id' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => implode('<br />', $validator->errors()->all())
            ]);
        }
        try{
            $user_id = $data['user_id'];
            $userWalletLists = UserWallet::where('user_id',$user_id)->get();
            $userWallet = DB::select( DB::raw("SELECT (SUM(IF(flag = 1, amount, 0)) - SUM(IF(flag = 2, amount, 0))) as balance FROM user_wallet WHERE user_id =".$user_id));
            return response()->json([
                'requestStatus' => true,
                'data' => [
                    'data' => $userWalletLists,
                    'amount' => $userWallet['0']->balance,
                ]
            ]);
        }catch (\Exception $e){
            return BaseResponse::response([
                'requestStatus' => 'error',
                'message' => trans('Server Error'),
                'dev' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param Request $request
     * updateStripeBank Account Logic
    */

    public function updateStripeBankAccount(Request $request){
        $data = json_decode($request->getContent(), true);
        try{
            $user = User::with('profile','addresses','documents')->where('id', $data['user_id'])->first();
            if($user){
                $dobDay = date('d', strtotime($user->profile->dob)) ;
                $dobMonth = date('m', strtotime($user->profile->dob)) ; //01
                $dobYear = date('Y', strtotime($user->profile->dob)) ; ;
                $name = explode(' ',$data['fullname']);
                $firstName = $name['0'];
                $lastName = (!empty($name['1'])) ? $name['1'] : $user->lastname;
                $mobileNumber = $user->mobile_number;
                $type = "individual";
                $addressLine1 = $data['address_line1'];
                $$addressLine2 = $data['address_line2'];
                $addressPostalCode = $data['postal_code'];
                $addressCity = $data['city'];
                $addressState = $data['state'];
                $documentsFront = $user->documents[0]->front_image;
                $documentsBack = $user->documents[0]->back_image;
                // update User detail
                $userSave = User::where('id', $data['user_id'])->first();
                $userSave->first_name = $firstName;
                $userSave->last_name  = $lastName;
                $userSave->fullname = $firstName." ".$lastName;
                $userSave->save();
                // update profile detail
                $address = UserAddresses::where('user_id', $data['user_id'])->first();
                $address->city = $addressCity;
                $address->state = $addressState;
                $address->postal_code = $addressPostalCode;
                $address->address_line1 = $addressLine1;
                $address->address_line2 = $addressLine2;
                $address->save();
                $userProfile = array(
                    'dobDay' => $dobDay,
                    'dobMonth' => $dobMonth,
                    'dobYear' => $dobYear,
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'type' => $type,
                    'mobileNumber' => $mobileNumber,
                    'addressLine1' => $addressLine1,
                    'addressPostalCode' => $addressPostalCode,
                    'addressCity' => $addressCity,
                    'addressState' => $addressState,
                    'documentsFront' => $documentsFront,
                    'documentsBack' => $documentsBack
                );
                \Stripe\Stripe::setApiKey($this->stripeKey);
                try{
                    $response = \Stripe\Account::create([
                            "type" => "custom",
                            "email" => $user->email,
                            "external_account" => [
                            "object" => "bank_account",
                            "country" => "GB",
                            "currency" => "GBP",
                            "routing_number" => $data['routing_number'],//"108800",
                            "account_number" => $data['account_number'],//"00012345"
                        ],
                        "tos_acceptance" => [
                            "date" => strtotime("now"),
                            "ip" => $_SERVER['REMOTE_ADDR'],
                        ],
                    ]);
                }catch (\Stripe\Error\Base $e) {
                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => $e->getMessage()
                    ], 401);
                }
                if (is_object($response) && !empty($response->id)) {
                    $profile = Profile::where('user_id', $user->id)->first();
                    $profile->stripe_account_id = $response->id;
                    $profile->sripe_status = "0";
                    $profile->save();
                    $verified1 = $this->accountVerificationStep1($response->id, $userProfile);
                    $verified2 = $this->accountVerificationStep2($response->id, $userProfile);
                    $stripeData = $this->accountRetrieve($response->id);
                    if($verified1 && $verified2){
                        $profile->sripe_status = "1";
                        $profile->save();
                        return response()->json([
                            'requestStatus' => 'success',
                            'message' => trans('messages.stripe_verified')
                        ]);
                    }else{
                        if(!$verified1){
                            return response()->json([
                                'requestStatus' => 'invalid',
                                'message' =>  trans('messages.stripe_address_not_verfied')
                            ], 401);
                        }
                        if(!$verified2){
                            return response()->json([
                                'requestStatus' => 'invalid',
                                'message' => trans('messages.stripe_document_not_verfied')
                            ], 401);
                        }
                    }
                }else{
                    return response()->json([
                        'requestStatus' => 'invalid',
                        'message' => $e->getMessage()
                    ], 401);
                }
            }
        }catch (Exception $e) {
            return response()->json([
                'requestStatus' => 'invalid',
                'message' => $e->getMessage()
            ], 401);
        }
    }


    public function CancelPayout(Request $request){
        $payid = $request->payout;
        $account = $request->account;

        try{
            \Stripe\Stripe::setApiKey($this->stripeKey);
            $payout = \Stripe\Payout::retrieve($payid,["stripe_account" => $account]);
            $response = $payout->cancel();
            if (is_object($response) && !empty($response->id)) {
                   $payouts = Payouts::where('payout_request_id',$response->id)->get();
                   $payouts->admin_approve = "1";
                   $payouts->save();
                   return response()->json([
                       'requestStatus' => 'ok',
                       'message' => $response->status
                   ], 200);
            }
        }catch (\Stripe\Error\Base $e) {
            return response()->json([
                    'requestStatus' => 'invalid',
                    'message' => $e->getMessage()
            ]);
        }
    }


}
