<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Datatables;
use Zendesk;
use DB;

//378089581714
//378088918994
class TicketController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(){
        $user = Auth::user();
        $userZenddeskId = $user->zenddesk_id;
        $tickets = [];
        if($userZenddeskId) {
            $tickets = Zendesk::users($userZenddeskId)->tickets()->requested();
        }

        return view('common.tickets.index', compact('tickets'));
    }

    /**
 	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
    */

	public function create()
    {
        return view('common.tickets.create');
	}

  	/**
 	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
    */

    public function store(request $request)
    {
        $request->validate([
			'subject' => 'required',
			'description' => 'required'
		]);

        $user = Auth::user();
        $userZenddeskId = $user->zenddesk_id;
        $requester_id   = $userZenddeskId;
        if($userZenddeskId == '') {
            $zendDeskUserData = Zendesk::users()->create([
                "name" => $user->first_name.''.$user->last_name,
                "email" => $user->email,
                "role" => "end-user"
            ]);
            if($zendDeskUserData) {
                $requester_id   = $zendDeskUserData->user->id;
                DB::table('users')->where('id', $user->id)->update(['zenddesk_id' => $requester_id]);

            }
        }
        $a = Zendesk::tickets()->create([
            'subject' => $request->subject,
            'comment' => [
                'body' => $request->description
            ],
            'requester_id' => $requester_id,
            'submitter_id' => $requester_id,
            //'priority' => $request->priority
        ]);
        return redirect('tickets')->with('success', trans('messages.ticket_added'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */

    public function destroy($id){
        Zendesk::tickets($id)->delete();
        return redirect('tickets')->with('success', trans('messages.ticket_deleted'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */

    public function view($id) {
        $ticket_id = $id;
        $comments = Zendesk::tickets($id)->comments()->sideload(['users'])->findAll();
        return view('common.tickets.view',compact('comments','ticket_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */

    public function addReply(request $request) {

        $ticket_id = $request->ticket_id;
        $user = Auth::user();
        $userZenddeskId = $user->zenddesk_id;
        $requester_id   = $userZenddeskId;
        Zendesk::tickets()->update($ticket_id,[
             'comment' => [
                 'body' => $request->reply,
                 "author_id" => $requester_id
             ]
        ]);

        $comments = Zendesk::tickets($ticket_id)->comments()->sideload(['users'])->findAll();
        return view('common.tickets.view',compact('comments','ticket_id'));
    }
}
