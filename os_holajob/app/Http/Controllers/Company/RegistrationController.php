<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Module\Model\User;
use App\Module\Model\Profile;
use App\Module\Model\UserAddresses;
use App\Module\Model\OtpLog;
use App\Module\Service\SmsService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Helper;
use Session;

class RegistrationController extends Controller
{
  public function register(Request $request) {

      $validation = $request->validate([
        'company_name' => 'required',
        'mobile_number2' => 'required|min:10|numeric|unique:users,mobile_number',
        'company_address1' => 'required',
        'company_address2' => 'required',
        'username2' => 'required',
        'email2' => 'required|string|max:255|unique:users,email',
        'password2' => 'required',
        'terms2' => 'required',
      ],
      [
        'company_name.required' => trans('messages.company_name_required'),
        'mobile_number2.required' => trans('messages.company_mobile_required'),
        'company_address1.required' => trans('messages.company_address_required'),
        'company_address2.required' => trans('messages.company_address2_required'),
        'username2.required' => trans('messages.company_username_required'),
        'email2.required'    => trans('messages.company_email_required'),
        'password2.required' => trans('messages.company_password_required'),
      ]);

       $model = '';$otp = '';
        try {

            \DB::transaction(function () use (&$model, $request, &$otp) {
                $userData['username']= $request->username2;
                $userData['email']= $request->email2;
                $userData['mobile_number']=  $request->mobile_number2;
                $userData['password'] = bcrypt( $request->password2);
                $userData['user_type_id'] = 1;
                $model = User::create($userData);
                if(!empty($model)){
                    $profile = new Profile();
                    $profile->user_id = $model['id'];
                    $profile->company_name = $request->company_name;
                    $profile->company_phone = $request->mobile_number2;
                    $profile->company_address1 = $request->company_address1;
                    $profile->company_address2 = $request->company_address2;
                    $profile->save();
                 }
                $otp = (new SmsService())->sendOTP($request->mobile_number2);
                (new OtpLog())->saveMobileVerificationOtp($model['id'], $otp);
                $data =  $model->toArray();
                $data['otp'] = $otp;
                $model->verification_token = $this->sendMail($data);
                $model->save();
            });
            return redirect()->route('otp-verification')
                        ->with('user_id',$model['id']);
        } catch (\Exception $e) {
            return back()->withInput($request->input())->with('message2', trans('messages.something_wrong').$e->getMessage()); //
        }
  }
  private function sendOTP($user) {
      return (new SmsService())->sendOTP($user->mobile_number);
  }
  /**
   * To Send OTP via email
   * @author Shweta
   * @param Request $request
   */
  private function sendMail($user) {
      $validated_data = $user ;
      $verification = \Illuminate\Support\Str::random(45);
      $validated_data['verification_token'] = $verification;
      \Mail::to($validated_data['email'])->send(new \App\Module\Mail\EmailVerification($validated_data));
      return $verification;
  }
}
