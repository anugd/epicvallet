<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Response;
use \App\Module\Model\User;
use \App\Module\Model\UserWallet;
use DB;

/**
 * Description of ExpertUpdateController
 * @author Shweta trivedi
*/

class WalletController extends Controller {

    public function __construct() {}

    /**
     * @param Request $request
     * User getUserDetail Logic
    */

    public function index(){
        $user = Auth::user();
        $balance = DB::select( DB::raw("SELECT (SUM(IF(flag = 1, amount, 0)) - SUM(IF(flag = 2, amount, 0))) as balance FROM user_wallet WHERE user_id ='$user->id'") );
        $transactions = UserWallet::where('user_id',$user->id)->orderBy('created_at', 'DESC')->get();

        $data = array(
            'balance' => $balance,
            'transactions' => $transactions
        );
        return view('customer.wallet.index',$data);
    }

    /**
     * Get all  the transactions
     *
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request){
        $user = Auth::user();
        $data = $request->all();
        $from = $data['start'];
        $to = $data['end'];
        if(!empty($from) && !empty($to)){
            $transactions = UserWallet::select('id')->where('user_id',$user->id)->whereBetween('created_at', [$from ,$to])->orderBy('created_at', 'DESC')->get();
        }
        elseif (!empty($from) && empty($to)) {
            $transactions = UserWallet::select('id')->where('user_id',$user->id)->whereDate('created_at', '>', $from)->orderBy('created_at', 'DESC')->get();
        }
        elseif (empty($from) && !empty($to)){
            $transactions = UserWallet::select('id')->where('user_id',$user->id)->whereDate('created_at', '<', $to)->orderBy('created_at', 'DESC')->get();
        }
        else{
            $transactions = UserWallet::select('id')->where('user_id',$user->id)->orderBy('created_at', 'DESC')->get();
        }
        return \DataTables::of($transactions)
        ->editColumn('amount', function($transaction){
                return html_entity_decode("&pound;").$transaction->amount;
        })
        ->editColumn('created_at', function($transaction){
                 $strtotime = strtotime($transaction->created_at);
                 return date("d M y h:i A.", $strtotime);
        })
        ->editColumn('flag', function($transaction){
                 $flag = $transaction->flag;
                 if($flag  == 1){
                     return "Credited";
                 }else{
                     return "Debited";
                 }
        })
        ->make(true);
    }
}
