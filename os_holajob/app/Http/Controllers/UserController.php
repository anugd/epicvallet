<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Module\Model\UserDocuments;
use App\Module\Model\UserLanguages;
use App\Module\Model\UserAddresses;
use App\Module\Model\UserServices;
use App\Module\Model\RelationShip;
use Validator;
use Response;
use App\Helpers\UserNotification as UserNotification;
use \App\Module\Model\ServiceRequest;
use \App\Module\Model\ServicePayment;
use \App\Module\Model\User;
use \App\Module\Model\Profile;
use \App\Module\Model\Payouts;
use \App\Module\Model\Languages;
use \App\Module\Model\Services;
use DataTables;
use Session;
use File;
use DB;

/**
* Description of ExpertUpdateController
* @author Shweta trivedi
*/
class UserController extends Controller {

    protected $servicerequest;
    public $stripeKey = '';

    function __construct(ServiceRequest $servicesrequest) {
        $this->servicerequest = $servicesrequest;
        if (config('app.MODE') == 'live') {
            \Stripe\Stripe::setApiKey(config('app.STRIPE_LIVE_KEY'));
            $this->stripeKey = config('app.STRIPE_LIVE_KEY');
        } else {
            \Stripe\Stripe::setApiKey(config('app.STRIPE_TEST_KEY'));
            $this->stripeKey = config('app.STRIPE_TEST_KEY');
        }
    }

    /**
     * @param Request $request
     * changepassword Logic
    */

    public function changepassword($type) {
        return view('common.changepassword');
    }

    /**
     * @param Request $request
     * address update
    */

    public function address($type) {
        $user = Auth::user();
        $address = UserAddresses::where(['user_id' => $user->id, 'is_primary' => 1])->first();

        $homeaddress = UserAddresses::where(['user_id' => $user->id, 'is_home' => 1])->first();
        $workaddress = UserAddresses::where(['user_id' => $user->id, 'is_job' => 1])->first();

        $data = array(
            'address' => $address,
            'homeaddress' => $homeaddress,
            'workaddress' => $workaddress
        );

        if($type == "expert"){
            return view('expert.address',$data);
        }else{
            return view('customer.address',$data);
        }
    }

    /**
     * @param Request $request
     * validateoldpassword Logic
    */

    public function validateoldpassword(Request $request){
        $data = $request->all();
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ],[
            'password.required' => trans('messages.password_required'),
        ]);
        $users  = User::where('id',$user->id)->first();
        if($users){
            if(Hash::check($data['password'],$users->password)){
                return "true";
            }else{
                return "false";
            }
        }else{
            return "false";
        }

    }

    /**
     * @param Request $request
     * updatepassword Logic
    */

    public function updatepassword(Request $request){
        $data = $request->all();
        $user = Auth::user();
        $userobj  = User::where('id',$user->id)->first();
        if(!empty($userobj)){
            $newpassword = 'new-password';
            $userobj->password = bcrypt( $request->$newpassword );
            $userobj->save();
            return back()->with('success', trans('messages.update_password'));
        }else{
            return back()->with('success',  trans('messages.error'));
        }
    }


    /**
     * @param Request $request
     * updatepassword address
    */

    public function updateaddress(Request $request){
        $user = Auth::user();
        $validation = $request->validate([
            'address_line1' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
        ],
        [
            'address_line1.required' =>  trans('messages.address_line1_required'),
            'city.required'    => trans('messages.expert_city_required'),
            'postal_code.required'    =>  trans('messages.expert_postcode_required'),
        ]);

        $address = UserAddresses::where(['user_id' => $user->id, 'is_primary' => 1])->first();
        if(empty($address)){
            $address = new UserAddresses;
            $address->is_primary = 1;
            $address->user_id = $user->id;
        }
        $address->city = isset($request->city) && !empty($request->city) ? $request->city : "";
        $address->postal_code =   isset($request->postal_code) && !empty($request->postal_code) ? $request->postal_code : "";
        $address->address_line1 =  isset($request->address_line1) && !empty($request->address_line1) ? $request->address_line1 : "";
        $address->address_line2 =   isset($request->address_line2) && !empty($request->address_line2) ? $request->address_line2 : "";
        if($address->save()){
            return back()->with('success',trans('messages.profile_updated'));
        }else{
            return back()->with('error', trans('messages.something_wrong'));
        }
    }

    /**
     * @param Request $request
     * updatepassword address
    */

    public function updateworkaddress(Request $request){
        $user = Auth::user();
        $validation = $request->validate([
            'address_line1' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
        ],
        [
            'address_line1.required' =>  trans('messages.address_line1_required'),
            'city.required'    => trans('messages.expert_city_required'),
            'postal_code.required'    =>  trans('messages.expert_postcode_required'),
        ]);

        $address = UserAddresses::where(['user_id' => $user->id, 'is_job' => 1])->first();
        if(empty($address)){
            $address = new UserAddresses;
            $address->is_job = 1;
            $address->user_id = $user->id;
        }
        $address->city = isset($request->city) && !empty($request->city) ? $request->city : "";
        $address->postal_code =   isset($request->postal_code) && !empty($request->postal_code) ? $request->postal_code : "";
        $address->address_line1 =  isset($request->address_line1) && !empty($request->address_line1) ? $request->address_line1 : "";
        $address->address_line2 =   isset($request->address_line2) && !empty($request->address_line2) ? $request->address_line2 : "";
        if($address->save()){
            return back()->with('success',trans('messages.profile_updated'));
        }else{
            return back()->with('error', trans('messages.something_wrong'));
        }
    }

    /**
     * @param Request $request
     * updatepassword address
    */

    public function updatehomeaddress(Request $request){
        $user = Auth::user();
        $validation = $request->validate([
            'address_line1' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
        ],
        [
            'address_line1.required' =>  trans('messages.address_line1_required'),
            'city.required'    => trans('messages.expert_city_required'),
            'postal_code.required'    =>  trans('messages.expert_postcode_required'),
        ]);

        $address = UserAddresses::where(['user_id' => $user->id, 'is_home' => 1])->first();
        if(empty($address)){
            $address = new UserAddresses;
            $address->is_home = 1;
            $address->user_id = $user->id;
        }
        $address->city = isset($request->city) && !empty($request->city) ? $request->city : "";
        $address->postal_code =   !empty($request->postal_code) ? $request->postal_code : "";
        $address->address_line1 =  !empty($request->address_line1) ? $request->address_line1 : "";
        $address->address_line2 =   !empty($request->address_line2) ? $request->address_line2 : "";
        if($address->save()){
            return back()->with('success',trans('messages.profile_updated'));
        }else{
            return back()->with('error', trans('messages.something_wrong'));
        }
    }

    /**
     * @param Request $request
     * expertBasicInfo Logic
    */

    public function expertBasicInfo(Request $request){

        $user = Auth::user();
        $validation = $request->validate([
            'username' => 'required',
            'fullname' => 'required',
            'gender' => 'required',
            'dob3' => 'required',
            'email' => 'required|string|max:255|unique:users,email,'.$user->id,
            'expertProfileImage' => 'mimes:jpeg,jpg,png,gif',
            'phone_number' => 'required|min:10|numeric',
            'about' => 'required',
        ],
        [
            'username.required' =>  trans('messages.username_required'),
            'fullname.required' =>  trans('messages.fullname_required'),
            'gender.required'    => trans('messages.gender_required'),
            'dob3.required' =>  trans('messages.dob3_required'),
            'email.required'    =>  trans('messages.email_required'),
            'phone_number.required' =>  trans('messages.phone_number_required'),
            'about.required' => trans('messages.about_required'),
        ]);
        $model =  User::where('id',$user->id)->first();
        $model->fullname  = $request->fullname;
        $model->username  =$request->username;
        $model->last_name  = $request->email;
        $model->mobile_number  = $request->phone_number;
        $model->email  = $request->email;
        $model->available  = isset($request->available) ? 1 : 0;
        if($request->file('expertProfileImage')){
            $path = $request->file('expertProfileImage')->store('',['disk' => 'user']);
            $model->profile_image = "avatars/".$path;
        }
        if($model->save()){
            $profile = Profile::where('user_id',$user->id)->first();
            if(empty($profile)){
                $profile = new Profile;
                $profile->user_id = $model->id;
            }
            $profile->dob =  $request->dob3;
            $profile->gender = $request->gender;
            $profile->about = $request->about;
            $profile->emergency_contact_name =  !empty($request->fullname_relation) ? $request->fullname_relation : "" ;
            $profile->emergency_contact_number = !empty($request->phone_relation) ? $request->phone_relation : "";
            $profile->emergency_contact_relationship = !empty($request->relationship) ? $request->relationship : "";

            if($profile->save()){
                $languages = $request->languages;
                $skills = $request->skills;
                $userlangs  = UserLanguages::where('user_id',$user->id)->delete();
                if(!empty($languages)){
                    foreach ($languages as $languagekey => $language) {
                        $newlanguage = new UserLanguages;
                        $newlanguage->language_id = $language;
                        $newlanguage->user_id = $user->id;
                        $newlanguage->save();
                    }
                }
                $userservices  = UserServices::where('user_id',$user->id)->delete();
                if(!empty($skills)){
                    foreach ($skills as $skillkey => $skill) {
                        $newlanguage = new UserServices;
                        $newlanguage->service_id = $skill;
                        $newlanguage->user_id = $user->id;
                        $newlanguage->save();
                    }
                }
                return back()->with('success',trans('messages.profile_updated'));
            }else{
                return back()->with('error',trans('messages.error'));
            }
        }
    }

    /**
     * @param Request $request
     * customerBasicInfo Logic
    */

    public function customerBasicInfo(Request $request){
        $user = Auth::user();
        $validation = $request->validate([
            'fullname' => 'required',
            'gender' => 'required',
            'dob3' => 'required',
            'expertProfileImage' => 'mimes:jpeg,jpg,png',
        ],
        [
            'fullname.required' => trans('messages.username_required'),
            'gender.required'  => trans('messages.fullname_required'),
            'dob3.required' => trans('messages.gender_required'),
            'expertProfileImage.mimes' => trans('messages.image_format'),

        ]);
        $model =  User::where('id',$user->id)->first();
        $model->fullname  = $request->fullname;
        if($request->file('expertProfileImage')){
            $path = $request->file('expertProfileImage')->store('',['disk' => 'user']);
            $model->profile_image = "avatars/".$path;
        }

        if($model->save()){
            $profile = Profile::where('user_id',$user->id)->first();
            $profile->dob =  $request->dob3;
            $profile->gender = $request->gender;
            if($profile->save()){
                return back()->with('success',trans('messages.profile_updated'));
            }else{
                return back()->with('error',trans('messages.error'));
            }
        }
    }

    public function primaryaddress(Request $request){
        $user = Auth::user();

        $address = UserAddresses::where(['user_id' => $user->id, 'is_primary' => 1])->get();
        if(!empty($address)){
            foreach ($address as $key => $addres) {
                $addres->is_primary = 0;
                $addres->save();
            }
        }

        $addressp = UserAddresses::where(['id' => $request->address,'user_id' => $user->id])->first();
        $addressp->is_primary = 1;
        if($addressp->save()){
            return back()->with('success',trans('messages.profile_updated'));
        }else{
            return back()->with('error', trans('messages.something_wrong'));
        }

    }

    /**
     * @param Request $request
     * profile Logic
    */

    public function profile($type){
        $user = Auth::user();
        $userobj  = User::with('profile')->where('id',$user->id)->first();
        $userlangs  = UserLanguages::with('Language')->where('user_id',$user->id)->get();
        $userservices  = UserServices::with('Service')->where('user_id',$user->id)->get();
    	$address = UserAddresses::where(['user_id' => $user->id, 'is_primary' => 1])->first();
        $releationShip = RelationShip::pluck('name', 'id');

        $data = array(
            'userobj' => $userobj,
            'userlangs' => $userlangs,
            'userservices' => $userservices,
            'address' => $address,
            'releationShip' => $releationShip
        );

        if($type == "expert"){
            return view('expert.profile',$data);
        }
        else{
            return view('customer.profile',$data);
        }
    }

    /**
     * @param Request $request
     * dashboard Logic
    */

    public function dashboard() {
        $user = Auth::user();
        $earnedMoney = ServiceRequest::select(DB::raw('SUM(service_payment.sub_total) As revenue'))
        ->leftJoin('service_payment', 'service_payment.service_id', '=', 'service_request.id')
        ->where('service_request.expert_id', $user->id)
        ->where('service_request.status', '2')
        ->get();
        $totalpayout = Payouts::where(['user_id' => $user->id, 'status' => 1])->sum('amount');
        if(isset($earnedMoney[0]['revenue'])){
            $earnedMoney = $earnedMoney[0]['revenue'];
        }else{
            $earnedMoney =  0;
        }
        $balance = ($earnedMoney - $totalpayout);
        $data = array(
            'balance' => $balance,
            'earnedMoney' => $earnedMoney,
            'totalpayout'=> $totalpayout
        );
        return view('expert.dashboard',$data);
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function revenue() {
        $user = Auth::user();
        $totalpayout = ServicePayment::where(['expert_id' => $user->id])->sum('sub_total');
        $data = array(
           'totalpayout'=> $totalpayout
        );
        return view('expert.revenue',$data);
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function revenueData() {
        $user = Auth::user();
		$revenue = ServicePayment::where('expert_id',$user->id)->with('expert','service','service.service' )->get();

        return Datatables::of($revenue)
            ->addColumn('service_name',function($data){
                // return '<a href="#" title="View Detail" data-toggle="modal" data-target="#viewModal" dataId = '.$data->id.' class="viewDetail">'.$data->service->service->service.'</a>';
                return $data->service->service->service;
			})
            ->addColumn('total_paid',function($data){
                return html_entity_decode("&pound;").round($data->total_price - $data->voucher_discount,2);
			})
			->editColumn('booking_date', function($data) {
				return date('d/m/Y', strtotime($data->booking_date));
			})
			->editColumn('per_hour_rate', function($data) {
				return html_entity_decode("&pound;").round($data->per_hour_rate,2);
			})
			->editColumn('total_price', function($data) {
				return html_entity_decode("&pound;").round($data->total_price,2);
			})
			->editColumn('service_fee', function($data) {
				return html_entity_decode("&pound;").round($data->service_fee,2);
			})
			->editColumn('sub_total', function($data) {
				return html_entity_decode("&pound;").round($data->sub_total,2);
			})
            ->editColumn('payment_status', function($data) {

				if($data->payment_status == '0'){
					return 'Pending';
				}else if($data->payment_status == '1'){
					return 'Completed';
				}
            })
            ->rawColumns(['status', 'service_name'])
            ->make(true);
	}

    /**
     * @param Request $request
     * revenue Logic
    */

	public function revenueDetail(Request $request) {
		$id = $request->id;
		$revenue = ServicePayment::with('expert','service','service.service')->where('id',$id)->first();

        $data = array(
            'services' => $revenue
        );
		return view('elements.revenueDetail',$data);
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function customerprofile(){
        $user = Auth::user();

        $userobj  = User::with('profile')->where('id',$user->id)->first();
        $userlangs  = UserLanguages::with('Language')->where('user_id',$user->id)->get();
        $userservices  = UserServices::with('Service')->where('user_id',$user->id)->get();
        $data = array(
            'userobj' => $userobj,
            'userlangs' => $userlangs,
            'userservices' => $userservices
        );
        return view('customer.profile',$data);
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function document(){
        $user = Auth::user();
        $documents = UserDocuments::where('user_id', $user->id)->first();
        $data = array(
            "documents" => $documents
        );
        return view('expert.document',$data);
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function skills(){
        $data = [];
        $services = Services::where('parent_id' , '=' , 0 )->get();
        foreach ($services as $keyservice => $service) {
            $svpobject = new \stdClass();
            $svpobject->id = $service->id;
            $svpobject->text = $service->service;
            $onecategoryservices = Services::where('parent_id' , '=' , $service->id )->get();
            $childdata  = [];
            foreach ($onecategoryservices as $onecategoryservicekey => $onecategoryservice) {
                $svcobject = new \stdClass();
                $svcobject->id =  $onecategoryservice->id;
                $svcobject->text = $onecategoryservice->service;
                $childdata[] = $svcobject;
            }
            $svpobject->children = $childdata;

            $data[] = $svpobject;
        }
        echo json_encode($data);
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function language(){
        $languages = Languages::select('id', 'language as text')->get();
        echo json_encode($languages);
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function submitDocument(Request $request){
        $user = Auth::user();
        $rules =  [
            'address_proof' => 'required_without_all:identity_proof,exp_certificate,identity_face|mimes:jpeg,png,jpg,pdf|max:2048',
            'identity_proof' => 'required_without_all:address_proof,exp_certificate,identity_face|mimes:jpeg,png,jpg,pdf|max:2048',
            'identity_face' => 'required_without_all:address_proof,exp_certificate,identity_proof|mimes:jpeg,png,jpg,pdf|max:2048',
            'exp_certificate' => 'required_without_all:address_proof,identity_proof,identity_face|mimes:jpeg,png,jpg,pdf|max:2048',
        ];
        $message =  [
            'address_proof.required_without_all' => trans('messages.submit_document'),
            'identity_proof.required_without_all' => trans('messages.submit_document'),
            'exp_certificate.required_without_all' =>trans('messages.submit_document')
        ];
        $validator = Validator::make(Input::all(), $rules, $message)->validate();
        if($request->hasfile('address_proof')){
            $image = $request->file('address_proof');
            $filename = $user->id.time() . '.' . $image->getClientOriginalExtension();
            $path = public_path().'/document';
            File::makeDirectory($path, $mode = 0777, true, true);
            $image->move($path,$filename);
            $document = UserDocuments::where('user_id', $user->id)->first();
            if(empty($document)){
                $document = new UserDocuments;
                $document->user_id =  $user->id;
            }
            $document->front_image = 'document/'.$filename;
            $document->save();
        }

        if($request->hasfile('identity_proof')){
            $image = $request->file('identity_proof');
            $filename = $user->id.time() . '.' . $image->getClientOriginalExtension();
            $path = public_path().'/document';
            File::makeDirectory($path, $mode = 0777, true, true);
            $image->move($path,$filename);
            $document = UserDocuments::where('user_id', $user->id)->first();
            if(empty($document)){
                $document = new UserDocuments;
                $document->user_id =  $user->id;
            }
            $document->back_image = "document/".$filename;
            $document->save();
        }

        if($request->hasfile('identity_face')){
            $image = $request->file('identity_face');
            $filename = $user->id.time() . '.' . $image->getClientOriginalExtension();
            $path = public_path().'/document';
            File::makeDirectory($path, $mode = 0777, true, true);
            $image->move($path,$filename);
            $document = UserDocuments::where('user_id', $user->id)->first();
            if(empty($document)){
                $document = new UserDocuments;
                $document->user_id =  $user->id;
            }
            $document->face = "document/".$filename;
            $document->save();
        }

        if($request->hasfile('exp_certificate')){
            $image = $request->file('exp_certificate');
            $filename = $user->id.time() . '.' . $image->getClientOriginalExtension();
            $path = public_path().'/document';
            File::makeDirectory($path, $mode = 0777, true, true);
            $image->move($path,$filename);

            $document = UserDocuments::where('user_id', $user->id)->first();
            if(empty($document)){
                $document = new UserDocuments;
                $document->user_id =  $user->id;
            }
            $document->experience_certificate = "document/".$filename;
            $document->save();
        }
        return back()->with('success',trans('messages.doc_updated'));
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function portfolio(){
        $user = Auth::user();
        $userobj  = User::with(['profile', 'ExpertAddress', 'UserDocuments', 'profile','UserLanguages','UserLanguages.Language','UserServices', 'UserServices.Services',])->where('id',$user->id)->first();
        $svs  = ServiceRequest::with('User','Services')->where('expert_id',$user->id)->where('status', 2)->get();
        $data = array(
            'userobj' => $userobj,
            'svs' => $svs
        );
        return view('expert.portfolio',$data);
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function  payoutRequest(Request $request) {
        $user = Auth::user();
        $data['user_id'] = $user->id;
        $data['amount'] = $request->amount;
        $this->requestPayoutLogic($data);
    }

    /**
     * @param Request $request
     * revenue Logic
    */

    public function requestPayoutLogic($data) {
        try {
            $user_id = $data['user_id'];
            $amount = $data['amount'];
            $profileDetail = Profile::with('User')->where(['user_id'=> $user_id])->first();
            if(!empty($profileDetail->stripe_account_id)) {
                $charge = \Stripe\Transfer::create([
                    "amount" => 100 * $amount,
                    "currency" => "GBP",
                    "destination" => $profileDetail->stripe_account_id
                ]);
                if($charge->id){
                    /** Payout Listing */
                    $payoutResponse = \Stripe\Payout::create([
                        "amount" => 100 * $amount,
                        "currency" => "GBP",
                        "destination" => $profileDetail->stripe_external_bank,
                        "source_type" => "card"
                    ],['stripe_account' => $profileDetail->stripe_account_id]);
                    if($payoutResponse->id){
                        $payout = new Payouts();
                        $payout->user_id = $user_id;
                        $payout->payout_request_id = $payoutResponse->id;
                        $payout->stripe_txn_id = $payoutResponse->balance_transaction;
                        $payout->amount = $amount;
                        $payout->status = 1;
                        $payout->stripe_account_id = $profileDetail->stripe_account_id;
                        $payout->save();
                    }
                    UserNotification::send_requestPayout_mail($profileDetail->User->fullname, $profileDetail->User->email, $amount);
                    echo json_encode(['requestStatus' => 'success','message' => trans('messages.payout_success')]);
                }
            }else{
                echo json_encode(['requestStatus' => 'error','message' => trans('messages.no_account_asso')]);
            }
        }catch (\Exception $e) {
            echo json_encode(['requestStatus' => 'error','message' =>$e->getMessage()]);

        }
    }

    public function myaccont(Request $request){
        return view('customer.account');
    }

    public function verifyemail(Request $request){
        try {
            $user = User::where('verification_token', $request->token)->first();
            if(!empty($user)){
                $user->verification_token  = '';
                $user->email_verified_at = date('Y-m-d H:i:s');
                if($user->save()){
                    $data = array(
                        'status' => "success"
                    );
                }else{
                    $data = array(
                        'status' => "failed"
                    );
                }
            }else{
                var_dump("test");
                exit;
                $data = array(
                    'status' => "failed"
                );
            }
            return view('auth.emailresponse',$data);
        }catch (\Exception $e) {
            $data = array(
                'status' => "failed"
            );
            return view('auth.emailresponse',$data);
        }
    }
    /**
    * @param Request $request
    * addBankAccount for expert
    */
    public function addBankAccount() {
        $user_id = $user = Auth::user()->id;
        $profileDetail = Profile::with('User')->where(['user_id'=> $user_id])->first();
        return view('expert.addBankAccount',compact('profileDetail'));
    }

    /**
    * @param Request $request
    * addBankAccount for expert
    */

    public function updateBankAccount(Request $request) {
        $user = Auth::user();
        $user_id = $user->id;
        $profileDetail = Profile::with('User')->where(['user_id'=> $user_id])->first();
        if(!empty($profileDetail->stripe_account_id)) {
                try{ 
                    $bank_account = \Stripe\Account::createExternalAccount(
                        $profileDetail->stripe_account_id,
                        [
                          'external_account' => $request->stripeToken,
                          'default_for_currency' => true
                          
                        ]
                    );
                    echo json_encode(['requestStatus' => 'Success','message' =>trans('messages.bank_account_added_success')]);
                }
                catch (\Exception $e) {
                    echo json_encode(['requestStatus' => 'error','message' =>$e->getMessage()]);
        
                }
            } 
        }
    
    
}
