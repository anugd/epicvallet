<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Module\Model\ServiceRequest;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use \App\Module\Model\User;
use \App\Module\Model\Vouchers;
use DataTables;
/**
 * Description of ExpertUpdateController
 * @author Shweta trivedi
 */
class ServiceController extends Controller {

    public function __construct() {

    }

    /**
     * @param Request $request
     * list services Logic
    */

    public function index() {

        $user = Auth::user();
        if($user->user_type_id == 1){
            $invoices  = ServiceRequest::with('customer','expert','service')->where('user_id',$user->id)->get();
        }else{
            $invoices  = ServiceRequest::with('customer','expert','service')->where('expert_id',$user->id)->get();
        }
        $data = array(
            'invoices' => $invoices,
            'user' => $user
        );
        return view('common.service.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function servicesHistoryData(Request $request) {
        $user = Auth::user();
        $data = $request->all();
        $status = $data['status'];
        if($status != null){
            if($user->user_type_id == 1){
                $services = ServiceRequest::with('ServiceFeedback','customer','expert','service','ServicePayment')->where('user_id',$user->id)->where('status', $status )->orderBy('service_start_time', 'DESC')->get();
            }else{
                $services = ServiceRequest::with('ServiceFeedback','customer','expert','service','ServicePayment')->where('expert_id',$user->id)->where('status', $status )->orderBy('service_start_time', 'DESC')->get();
            }
        }else{
            if($user->user_type_id == 1){
                $services = ServiceRequest::with('ServiceFeedback','customer','expert','service','ServicePayment')->where('user_id',$user->id)->get();
            }else{
                $services = ServiceRequest::with('ServiceFeedback','customer','expert','service','ServicePayment')->where('expert_id',$user->id)->get();
            }
        }
        return DataTables::of($services)
            ->addColumn('service',function($data){
                return '<a href="#" title="View Detail" data-toggle="modal" data-target="#viewServiceModal" dataId = '.$data->id.' class="viewDetail">'.$data->service->service.'</a>';
            })
            ->addColumn('name',function($data) use($user){
                if($user->user_type_id == 1){
                    return isset($data->expert->fullname) && !empty($data->expert->fullname) ? $data->expert->fullname : "" ;
                }else{
                    return isset($data->customer->fullname) && !empty($data->customer->fullname) ? $data->customer->fullname : "" ;
                }

            })
            ->addColumn('booking_date',function($data) use($user){
                if(!empty($data->booking_date)){
                    return date('d/m/Y', strtotime($data->booking_date));
                }else{
                    return '--';
                }

            })
            ->addColumn('booking_time',function($data) use($user){
                if(!empty($data->booking_date)){
                    return date('h:i A', strtotime($data->booking_date));
                }else{
                    return '--';
                }

            })
            ->editColumn('status', function($data) {
                if($data->status == '0'){
                    return 'Pending';
                }else if($data->status == '1'){
                    return 'Booked';
                }else if($data->status == '2'){
                    return 'Completed';
                }else if($data->status == '3'){
                    return 'On-Going';
                }else if($data->status == '4'){
                    return 'Cancelled';
                }
            })->rawColumns(['status', 'service'])->make(true);
    }

    public function servicesHistoryDetailExpert(Request $request) {
		$id = $request->id;
		$services = ServiceRequest::with('ServiceFeedback','customer','expert','service','ServicePayment')->where('id',$id)->first();

        $data = array(
            'services' => $services
        );
		return view('elements.servicesHistoryDetailExpert',$data);
    }

    public function servicesHistoryDetailCustomer(Request $request) {
		$id = $request->id;
		$services = ServiceRequest::with('ServiceFeedback','customer','expert','service','ServicePayment')->where('id',$id)->first();

        $data = array(
            'services' => $services
        );
		return view('elements.servicesHistoryDetailCustomer',$data);
    }

}
