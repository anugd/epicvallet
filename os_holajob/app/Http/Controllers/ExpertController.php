<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module\Model\User;

class ExpertController extends Controller
{
    public function index(){
        $experts =  User::all();
        $data = array(
            'experts' => $experts,
        );
        return view('experts.index', $data);
    }
}
