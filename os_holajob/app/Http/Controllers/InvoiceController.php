<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Module\Model\User;
use \App\Module\Model\UserAddresses;
use App\Module\Model\ServiceRequest;
use Response;
use Validator;
use Datatables;
use PDF;
use File;

/**
 * Description of ExpertUpdateController
 * @author Shweta trivedi
*/

class InvoiceController extends Controller {

    public function __construct() {}

    /**
     * @param Request $request
     * User invoice Logic
    */

    public function index(){
        $user = Auth::user();
        if($user->user_type_id == 1){
            $invoices  = ServiceRequest::with('ServicePayment','customer','expert','service')->where('status', 2)->where('user_id',$user->id)->get();
        }else{
            $invoices  = ServiceRequest::with('ServicePayment','customer','expert','service')->where('status', 2)->where('expert_id',$user->id)->get();
        }
        $data = array(
            'invoices' => $invoices,
            'user' => $user
        );
        return view('customer.invoice.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


    public function download(Request $request) {
        $sid = "27";
        $invoice  = ServiceRequest::with('expert','customer','service','ServicePayment')->where('id',$sid)->first();
        $cusaddress = UserAddresses::where('user_id',$invoice->user_id)->where('is_primary', 1)->first();
        $expertaddress = UserAddresses::where('user_id',$invoice->expert_id)->first();
        $data = array(
            'expertaddress' => $expertaddress,
            'invoice' => $invoice,
            'cusaddress' => $cusaddress
        );
        $path = public_path().'/invoices/';
        File::makeDirectory($path, $mode = 0777, true, true);
        $name = '/invoices/'.$sid.time().'-invoice.pdf';
        $pdf = PDF::loadView('customer.pdf.invoice',$data)->save(public_path().$name);
        return $name;
    }
}
