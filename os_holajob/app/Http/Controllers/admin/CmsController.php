<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Module\Model\Cms;
use Helper;
use Session;
use DataTables;
use DB;
use Storage;
use File;

class CmsController extends Controller
{
	//view all Cms list
	public function index() {
		return view('admins.cms.index');
	}

	//get all Cms ajax data
	public function getAllCms() {

        $cms = Cms::where('is_deleted',0)->orderBy('created_at', 'DESC')->get();
        return DataTables::of($cms)
            ->editColumn('checkmark', function($data) {
                return '<input type="checkbox" value='. $data->id .'>';
            })

			->editColumn('status', function ($data) {
				if($data->status == 1){
					$checked = "checked";
					$title = "Active";
				}else{
					$checked = "";
					$title = "De-Active";
				}
				
				$status = '<label class="switch" title="'.$title.'">
								<input  class="updateStatus" status ="'.$data->status.'" value="'.$data->id.'" id="status_'.$data->id.'" type="checkbox" '.$checked.'>
							  	<span class="slider round"></span>
				 		  </label>';
				return
					$status;
			})
            ->editColumn('action',function($data) {
				$action = "<div class='row'>";
				$action .= "<div class='col-sm-1'><a id='edit' class = 'fa fa-edit' title='Edit' data-toggle='tooltip'  href='cms/edit/$data->id'></a></div>";
				$action .= "<div class='col-sm-1'><a id='delete' class = 'fa fa-trash deleteFaq' title='Delete' data-toggle='tooltip' delId = '$data->id' href = '#'></a></div>";
				
				$action .= "</div>";
                return $action;
			})
			->escapeColumns([])
            ->make(true);
	}

	/**
 	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function create()
    {
        return view('admins.cms.create');
	}

  	/**
 	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
    public function store(request $request)
    {

		$request->validate([
			'title'=>'required',
			'message'=>'required'
		]);

		$cms = new Cms([
			'title' => $request->title,
			'message' => $request->message,
			'status' => $request->status
		]);
		$cms->save();

		return redirect('admin/cms')->with('success', trans('messages.cms_created'));

	}

	/**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function edit($id)
    {
		$cms = Cms::find($id);
        return view('admins.cms.edit', compact('cms'));
	}

 	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

		$request->validate([
			'title'=>'required',
			'message'=>'required'
		]);


		$cms = Cms::find($request->id);
		$cms->title = $request->title;
		$cms->message = $request->message;
		$cms->status = $request->status;
		$cms->save();

		return redirect('admin/cms')->with('success', trans('messages.cms_updated'));
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

	public function destroy($id)
	{
		//$Cms = Cms::find($id);

		echo $result = DB::table('cms')->where('id', $id)->update(['is_deleted' => 1]);
		//$Cms->delete();

		return redirect('admin/cms')->with('success', trans('messages.cms_deleted'));
	}
	public function updateStatus(Request $request) {

        $id = $request->id;
		if(isset($id) && !empty($id) && !is_array($id))
        {
            $catIds[] = $id;
        }
        else if (isset($id) && !empty($id) && is_array($id))
        {
            $catIds = $id;
		}
		$status = $request->status;

        echo $result = DB::table('cms')->whereIn('id', $id)->update(['status' => $status]);

    }

}
