<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Module\Model\UserAddresses;
use App\Module\Model\User;
use Helper;
use Session;
use DataTables;
use DB;
use Storage;
use File;

class CustomerController extends Controller
{
	//view all Customers list
	public function index() {
		return view('admins.customers.index');
	}

	//get all Customers ajax data
	public function getAllCustomers() {
		$customers = User::where('user_type_id',1)->orderBy('created_at', 'DESC')->get();
        return DataTables::of($customers)
            ->editColumn('checkmark', function($data) {
                return '<input type="checkbox" value='. $data->id .'>';
            })
			->editColumn('status', function ($data) {
				if($data->status == 1){
					$checked = "checked";
				}else{
					$checked = "";
				}
				$status = '<label class="switch">
								<input class="updateStatus" status ="'.$data->status.'" value="'.$data->id.'" id="status_'.$data->id.'" type="checkbox" '.$checked.'>
							  	<span class="slider round"></span>
						  </label>';
				return $status;
			})
            ->editColumn('action',function($data){
				$action = "<div class='row'>";
				$action .= "<div><a id='edit' class='fa fa-edit' title='Edit' data-toggle='tooltip'  href='customers/edit/$data->id'></a></div>";
				$action .= "</div>";
                return $action;
			})
			->escapeColumns([])
            ->make(true);
	}

	public function updateStatus(Request $request) {
        $id = $request->id;
		if(isset($id) && !empty($id) && !is_array($id)){
            $catIds[] = $id;
        }
        else if (isset($id) && !empty($id) && is_array($id)){
            $catIds = $id;
		}
		$status = $request->status;
        echo $result = DB::table('users')->whereIn('id', $id)->update(['status' => $status]);
	}

	/**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/

	public function edit($id){
		$user = User::find($id);
		$address = UserAddresses::where(['user_id' => $id, 'is_primary' => 1])->first();
		$data = array(
			'user' => $user,
			'address' => $address
		);
        return view('admins.customers.edit', $data);
	}
	/**
 	* @param Request $request
	* User sendForgetPassMail Logic
    */

    private function sendResetPassMail($user) {
		$validated_data = $user ;
		$validated_data['new_password'] = $user->password ;
		\Mail::to($validated_data->email)->send(new \App\Module\Mail\AdminUpdateUserPassword($validated_data));
		unset($validated_data['new_password']);
        return true;
    }
 	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 	
    public function update(Request $request){
		$request->validate([
			'first_name'=>'required',
			'last_name'=>'required',
			"city" => 'required',
			"state" => 'required',
			"postal_code" => 'required',
			"address_line1" => 'required'
			
		]);

		$user = User::find($request->id);
	
		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->fullname = $user->first_name." ".$request->last_name;
		
		if(!empty($request->password)) {
			$user->password = bcrypt($request->password);
		}

		if($request->file('expertProfileImage')){
			$path = $request->file('expertProfileImage')->store('',['disk' => 'user']);
			$user->profile_image = "avatars/".$path;
		}

		if($user->save()){
			if(!empty($request->password)) {
				$user->password = $request->password;
				$this->sendResetPassMail($user);
			}
			$address = UserAddresses::where(['user_id' => $request->id, 'is_primary' => 1])->first();
			if(empty($address)){
				$address = new UserAddresses;
				$address->is_primary = 1;
				$address->user_id = $request->id;
			}
			$address->city = isset($request->city) && !empty($request->city) ? $request->city : "";
			$address->state =  isset($request->state) && !empty($request->state) ? $request->state : "";
			$address->postal_code =   isset($request->postal_code) && !empty($request->postal_code) ? $request->postal_code : "";
			$address->address_line1 =  isset($request->address_line1) && !empty($request->address_line1) ? $request->address_line1 : "";
			$address->address_line2 =   isset($request->address_line2) && !empty($request->address_line2) ? $request->address_line2 : "";
			if($address->save()){
				return back()->with('success', trans('messages.customer_updated'));
			}else{
				return back()->with('error', trans('messages.something_wrong'));
			}
		}else{
			return back()->with('error', trans('messages.something_wrong'));
		}
	}

}
