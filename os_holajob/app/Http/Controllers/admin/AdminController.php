<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Helpers\UserNotification as UserNotification;
use Illuminate\Support\Facades\Crypt;
use App\Module\Model\Admin;
use App\Module\Model\User;
use App\Module\Model\ServiceRequest;
use App\Module\Model\ServicePayment;
use App\Module\Model\Payout;
use App\Module\Model\AdminSetting;
use App\Module\Model\Services;
use App\Module\Model\UserServices;
use Helper;
use Session;
use DataTables;
use DB;
use Auth;
use Hash;
use Chart;

use Carbon\Carbon;

class AdminController extends Controller
{
    public function login(Request $request) {
        if(auth()->guard('web')->check()){
            return redirect()->intended('/expert-dashboard');
        }
        if (Auth()->guard('admin')->check()){
            return redirect()->intended('admin/dashboard');
        }
        if ($request->isMethod('post')) {
            $data = $request->all();
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
            ],[
                'email.required'    =>  trans('messages.email_required'),
                'password.required' =>  trans('messages.password_required'),
            ]);
            if ($auth = auth()->guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password']], $request->has('remember'))) {
                return redirect()->intended('admin/dashboard');
            }
            return back()->withInput($request->only('email','remember'))->with('message',  trans('messages.invalid_credentials'));
        }
        return view('admins.login');
    }

    public function resetpassword(){
        return view('admins.passwords.email');
    }

    /*
     * Function Name 	: forgot_password
     * Method           : POST
     * Description      : Admin forgot password
    */

    public function forgot_password(Request $request){
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ],[
            'email.required' => trans('messages.email_required'),
        ]);
        $admin = Admin::where('email',$data['email'])->first();
        if(!empty($admin)){
            $time = time();
            $admin->reset_link = $time;
            $admin->save();
            $encrypted = Crypt::encryptString($admin->id.':admin:'.$time);
            $url = url("/admin/reset_password/{$encrypted}");
            $chk = UserNotification::send_resetpwd_mail( $admin->first_name , $admin->email , $url );
            return back()->withInput($request->only('email','remember'))->with('message', trans('messages.email_reset'));
        }else{
            return back()->withInput($request->only('email','remember'))->with('messageerror', trans('messages.email_not_exist'));
        }
    }

    /**

    */

    public function save_resetpassword(Request $request){
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ],[
            'password' => trans('messages.password_required'),
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors('all')->toArray();
        }
        $admin = Admin::where('id',$data['user_id'])->first();
        if(!empty($admin)){
            if($admin->email == $data['email']){
                $admin->password = bcrypt($data['password']);
                $admin->save();
                Session::flash('loginmessage', trans('messages.update_password'));
            }else{
                Session::flash('loginmessage', trans('messages.invalid_email'));
            }
            return redirect()->intended('admin/login');
        }
    }

    /**

    */

    public function reset_password($token){
        //echo $token;
        $decrypted = Crypt::decryptString($token);
        $user = explode(':',$decrypted);
        $user_id = $user[0];
        $type = $user[1];
        $time_sent = $user[2];
        $admin = Admin::where('id', $user_id )->first(['id','reset_link']);
        if($admin['reset_link'] != $time_sent ){
            Session::flash('loginmessage', trans('messages.invalid_token'));
            return redirect()->intended('admin/login');
        }
        return view('admins.passwords.reset',array('user_id' => $user_id));
    }

    /* END */
    /*
    * Function Name 	: logout
    * Method           : GET
    * Description      : Logout Admin
    */

    public function logout(){
        auth()->guard('admin')->logout();
        return redirect()->intended('admin/login');;
    }

    /*
     * Function Name 	: dashboard
     * Method           : GET
     * Description      : Admin Dashboard View
    */

    public function dashboard(){
        $admin = auth()->guard('admin');
        $data['total_services'] = \DB::table('service_request')->count();
        $data['total_customers'] = \DB::table('users')->where('user_type_id',1)->count();
        $data['total_experts'] = \DB::table('users')->where('user_type_id',2)->count();
        $data['total_payouts'] = \DB::table('payouts')->sum('amount');
        $allservices['pending'] = $this->getServicesAllCount(0);
        $allservices['booked'] = $this->getServicesAllCount(1);
        $allservices['completed'] = $this->getServicesAllCount(2);
        $allservices['onGoing'] = $this->getServicesAllCount(3);
        $allservices['cancelled'] = $this->getServicesAllCount(4);
        $data['servicesAllChart'] =  $this->servicesAllReport($allservices);
        $paymentAll['admin'] = $this->getPaymentsAllCount(2);//admin
        $paymentAll['expert'] = $this->getPaymentsAllCount(3);//job seeker
        $data['paymentReportAllChart'] =  $this->paymentAllReport($paymentAll);
        $payout =  $this->payoutReport();
        $data['payoutChart'] =  $this->payoutReportChart($payout);
        $customers = $this->getUserMonthlyCount(1);//passing user type
        $experts =  $this->getUserMonthlyCount(2);//passing user type
        $data['usersChart'] = $this->usersReport($experts, $customers);
        return view('admins.dashboard',compact('data'))->withUser($admin);;
    }
    
    // function to get payouts monthly
    public function payoutReport() {

        $commissions = DB::table('payouts')
        ->select(DB::raw('SUM(amount) as total_sum, YEAR(created_at) as year, MONTH(created_at) as month'))
        ->groupBy(DB::raw('YEAR(created_at) ASC, MONTH(created_at) ASC'))->get();


        $mcount = [];
        $arr = [];

        foreach ($commissions as $key => $value) {
            $mcount[($value->month)-1] = round($value->total_sum,2);
        }

        for($i = 0; $i <= 11; $i++){
            if(!empty($mcount[$i])){
                $arr[$i] = $mcount[$i];
            }else{
                $arr[$i] = 0;
            }
        }

        return $arr;
    }
    // function to get payouts Chart
    public function payoutReportChart($payout) {
        return  \Chart::title([
            'text' => 'Payouts',
        ])
        ->chart([
            'type'     => 'column', // pie , columnt ect
            'renderTo' => 'payoutChart', // render the chart into your div with id

        ])
        ->subtitle([
            'text' => '',
        ])
        ->credits([
            "enabled"=> false
        ])
        ->colors([
            '#0c2959'
        ])
        ->xaxis([
            'categories' => ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            'crosshair' => true,
            'labels'     => [
                'rotation'  => 15,
                'align'     => 'top',
                'formatter' => 'startJs:function(){return this.value }:endJs',
            ],
        ])

        ->yaxis([
            "min" => 1,
            "title" => [
                "text" => 'Amount(&#xa3;)',
                'useHTML' => true
            ]
        ])
        ->legend([
            'layout'        => 'vertical',
            'align'         => 'right',
            'verticalAlign' => 'middle',
        ])
        ->series(
            [

                [
                    'name'  => 'Payout(£)',
                    "data" => $payout
                ]
            ]
        )
        ->display();
    }
    // function to get user monthly count
    public function getUserMonthlyCount($type = 1) {
        $from = date('Y-01-01');
        $to = date('Y-12-31');
        $users = User::select('id', 'created_at')->where('user_type_id', $type )->whereBetween('created_at', [$from, $to])
        ->get()
        ->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('m'); // grouping by months
        });

        $usermcount = [];
        $arr = [];
       //dd($users);
        foreach ($users as $key => $value) {
            $usermcount[(int)$key-1] = count($value);
        }
        //dd($usermcount);
        for($i = 0; $i <= 11; $i++){
            if(!empty($usermcount[$i])){
                $arr[$i] = $usermcount[$i];
            }else{
                $arr[$i] = 0;
            }
        }
        return $arr;

    }
    // function to show user monthly count
    public function usersReport($experts, $customers) {


        return  \Chart::title([
            'text' => 'User Report',
        ])
        ->chart([
            'type'     => 'column', // pie , columnt ect
            'renderTo' => 'userReportChart', // render the chart into your div with id
        ])
        ->subtitle([
            'text' => 'Customers & Job Seekers',
        ])
        ->credits([
            "enabled"=> false
        ])
        ->colors([
            '#0c2959',
            '#4FACDE'

        ])
        ->xaxis([
            'categories' => ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            'crosshair' => true,
            'labels'     => [
                'rotation'  => 15,
                'align'     => 'top',
                'formatter' => 'startJs:function(){return this.value }:endJs',
            ],
        ])
        ->yaxis([
            "min" => 1,
            "title" => [
                "text" => 'No. of users'
            ]
        ])
        ->legend([
            'layout'        => 'vertical',
            'align'         => 'right',
            'verticalAlign' => 'middle',
        ])
        ->series(
            [

                [
                    'name'  => 'Job Seeker',
                    "data" => $experts

                ],
                [
                    'name'  => 'Customer',
                    "data" => $customers

                ]
            ]
        )
        ->display();
    }
    // function to get services monthly count
    public function getServicesMonthlyCount($status) {

        $from = date('Y-01-01');
        $to = date('Y-12-31');

        $services = ServiceRequest::select('id', 'created_at')->where('status', $status )->whereBetween('created_at', [$from, $to])
        ->get()
        ->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('m'); // grouping by months
        });

        $mcount = [];
        $arr = [];

        foreach ($services as $key => $value) {
            $mcount[(int)$key-1] = count($value);
        }

        for($i = 0; $i <= 11; $i++){
            if(!empty($mcount[$i])){
                $arr[$i] = $mcount[$i];
            }else{
                $arr[$i] = 0;
            }
        }
        return $arr;

    }
    // function to show services monthly report
    public function servicesReport($services) {
        return  \Chart::title([
            'text' => 'Service Request Monthly Report',
        ])
        ->chart([
            'type'     => 'column', // pie , columnt ect
            'renderTo' => 'servicesReportChart', // render the chart into your div with id
        ])
        ->subtitle([
            'text' => '',
        ])
        ->credits([
            "enabled"=> false
        ])
        ->colors(['#FFD700','#BDB76B','#006400','#7FFFD4','#FF0000'])
        ->xaxis([
            'categories' => ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            'crosshair' => true,
            'labels'     => ['rotation'  => 15,'align'     => 'top','formatter' => 'startJs:function(){return this.value }:endJs',
            ],
        ])
        ->yaxis([
            "min" => 0,
            "title" => [
                "text" => 'No. of services'
            ]
        ])
        ->legend([
            'layout'        => 'vertikal',
            'align'         => 'right',
            'verticalAlign' => 'middle',
        ])
        ->series(
            [
                [
                    'name'  => 'Pending Services',
                    "data" => $services['pending']
                ],
                [
                    'name'  => 'Booked Services',
                    "data" => $services['booked']
                ],
                [
                    'name'  => 'Completed Services',
                    "data" => $services['completed']
                ],
                [
                    'name'  => 'On-Going Services',
                    "data" => $services['onGoing']
                ],
                [
                    'name'  => 'Cancelled Services',
                    "data" => $services['cancelled']
                ],
            ]
        )
        ->display();
    }
    // function to get services monthly count
    public function getServicesAllCount($status) {

        $services = ServiceRequest::select('id', 'status')->where('status', $status )
        ->get();

        return $services->count();

    }
    //function to show chart of all services
    public function servicesAllReport($services) {
        return \Chart::title([
            'text' => 'Service Request Report',
        ])
        ->chart([
            'type'     => 'pie', // pie , columnt ect
            'renderTo' => 'servicesAllReportChart', // render the chart into your div with id
        ])

        ->subtitle([
            'text' => '',
        ])
        ->credits([
            "enabled"=> false
        ])
        ->colors(['#FFD700','#BDB76B','#006400','#7FFFD4','#FF0000'])
        ->xaxis()
        ->yaxis()
        ->legend([
            'layout'        => 'vertikal',
            'align'         => 'right',
            'verticalAlign' => 'middle']
        )
        ->series(
            [

                [   "name" => 'Services',
                    'data'  => [['Pending - '.$services['pending'],$services['pending']],
                    ['Booked - '.$services['booked'],$services['booked']],
                    ['Completed - '.$services['completed'],$services['completed']],
                    ['On-Going - '.$services['onGoing'],$services['onGoing']],
                    ['Cancelled - '.$services['cancelled'],$services['cancelled']]],
                    "sliced"=> true,
                    "selected"=> true

                ],
            ]
        )
        ->display();
    }

    // function to get payments monthly count
    public function getPaymentsMonthlyCount($type) {

        if($type =='2'){
            $select = 'service_fee';
        }else{
            $select = 'sub_total';
        }

        $commissions = DB::table('service_payment')
        ->select(DB::raw('SUM('.$select.') as total_sum, YEAR(created_at) as year, MONTH(created_at) as month'))
        ->groupBy(DB::raw('YEAR(created_at) ASC, MONTH(created_at) ASC'))->get();

        $mcount = [];
        $arr = [];

        foreach ($commissions as $key => $value) {
            $mcount[(int)$value->month-1] = round($value->total_sum,2);
        }

        for($i = 0; $i <= 11; $i++){
            if(!empty($mcount[$i])){
                $arr[$i] = $mcount[$i];
            }else{
                $arr[$i] = 0;
            }
        }
        return $arr;

    }
    //function to show chart of monthly payments
    public function paymentReport($payment) {
        return  \Chart::title([
            'text' => 'Payment Report',
        ])
        ->chart([
            'type'     => 'column', // pie , columnt ect
            'renderTo' => 'paymentReportChart', // render the chart into your div with id
        ])
        ->subtitle([
            'text' => 'Admin & Job Seekers',
        ])
        ->credits([
            "enabled"=> false
        ])
        ->colors([
            '#0c2959',
            '#4FACDE'

        ])
        ->xaxis([
            'categories' => ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            'crosshair' => true,
            'labels'     => [
                'rotation'  => 15,
                'align'     => 'top',
                'formatter' => 'startJs:function(){return this.value }:endJs',
            ],
        ])
        ->yaxis([
            "min" => 0,
            "title" => [
                "text" => 'Amount(£)'
            ]
        ])
        ->legend([
            'layout'        => 'vertikal',
            'align'         => 'right',
            'verticalAlign' => 'middle',
        ])
        ->series(
            [
                [
                    'name'  => 'Admin',
                    "data" => $payment['admin']

                ],
                [
                    'name'  => 'Job seekers',
                    "data" => $payment['expert']

                ]
            ]
        )
        ->display();
    }
    // function to get payments monthly count
    public function getPaymentsAllCount($type) {
        if($type =='2'){
            $select = 'service_fee';
        }else{
            $select = 'sub_total';
        }
        $commissions = DB::table('service_payment')
        ->select(DB::raw('SUM('.$select.') as total_sum'))->first();
        return round($commissions->total_sum,2);
    }

    //function to show chart of all payments

    public function paymentAllReport($payment) {
        return \Chart::title([
            'text' => 'Payment Report',
        ])
        ->chart([
            'type'     => 'pie', // pie , columnt ect
            'renderTo' => 'paymentReportAllChart', // render the chart into your div with id
        ])
        ->subtitle([
            'text' => '',
        ])
        ->credits([
            "enabled"=> false
        ])
        ->colors(['#FFD700','#BDB76B'])
        ->xaxis()
        ->yaxis()
        ->legend([
            'layout'        => 'vertikal',
            'align'         => 'right',
            'verticalAlign' => 'middle']
        )
        ->series(
            [
                [   "name" => 'Amount(£)',
                    'data'  => [['Admin : £'.$payment['admin'],$payment['admin']],
                    ['Job Seeker : £'.$payment['expert'],$payment['expert']]]
                ],
            ]
        )->display();
    }


    /**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function changePassword()
    {
        $title = 'Change Password';
        return view('admins.profile.changepassword', compact('title'));
	}

 	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request)
    {
        $auth = auth()->guard('admin')->user();
        if (!(Hash::check($request->get('current_password'),  $auth->password))) {
            // The passwords matches
            return redirect()->back()->with("error", trans('messages.current_password_not_match'));
        }
        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error",trans('messages.new_password_same_as_old'));
        }
        if(strcmp($request->get('new_password_confirmation'), $request->get('new_password')) != 0){
            //Current password and new password are same
            return redirect()->back()->with("error",trans('messages.new_old_password_not_matched'));
        }
        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = $auth;
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return redirect()->back()->with("success",trans('messages.change_password_success'));

    }
    /**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function changeProfile()
    {
        $title = 'Change Profile';
        $auth = auth()->guard('admin')->user();
        return view('admins.profile.changeprofile', compact('title','auth'));
	}

 	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $auth = auth()->guard('admin')->user();

        $validatedData = $request->validate([
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
        ]);
        //Change Password
        $user = $auth;
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->save();
        return redirect()->back()->with("success",trans('messages.profile_updated'));

    }
    public function checkOldPassword(Request $request) {


        $auth = auth()->guard('admin')->user();

        if (!(Hash::check($request->get('current_password'),  $auth->password))) {
            return "false";
        }
        return "true";

    }


    /**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function changeCommission()
    {
        $data['title'] = 'Change Commission';
        $data['adminSetting'] = AdminSetting::where('type','commission_fees')->first();
        return view('admins.profile.changecommission', compact('data'));
	}

 	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCommission(Request $request)
    {

        $validatedData = $request->validate([
            'commission_fees' => 'required'
        ]);
        //Change Password
        $as = AdminSetting::where('type','commission_fees')->first();//alwayz one record in db
        $as->type = 'commission_fees';
        $as->value = $request->commission_fees;
        $as->save();
        return redirect()->back()->with("success",trans('messages.commission_updated'));

    }
    /**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
    
    public function jobDispatch()
    {
        $title = 'Dispatch Job';
        $data['customers'] =  User::where(['user_type_id'=>1,'status'=>1,'is_deleted'=>0])->get();
        $data['experts'] = [];
        // $data['userservices']  = UserServices::with('Services')->select('service_id','Services.*')->distinct('service_id')->get();

        $data['userservices'] = DB::select( DB::raw("SELECT distinct service_id,services.service FROM user_services left join services on user_services.service_id = services.id") );
        return view('admins.services.jobDispatch', compact('title','data'));
    }

    /**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
    
    public function getServicesUser(Request $request)
    {
        $services = UserServices::with('User')->where('service_id',$request->service_id)->get();
        echo json_encode($services);
    }
 	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function jobDispatchSave(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required',
            //'expert_id' => 'required',
            'service_id' => 'required',
            'address' => 'required',
            'booking_date'=> 'required'
        ]);
        $servicerequest = new ServiceRequest();
        $servicerequest->user_id = $request->user_id;
        $servicerequest->service_id = $request->service_id;
        $servicerequest->expert_id = $request->expert_id;
        $servicerequest->booking_date = $request->booking_date;
        $servicerequest->request_sent_date_time = date('Y-m-d H:i:s');
        $servicerequest->address = $request->address;
        $servicerequest->latitude = $request->latitude;
        $servicerequest->longitude = $request->longitude;
        $servicerequest->manual_job = 1;
        $servicerequest->schedule = 1;
        $servicerequest->status = !empty($request->expert_id)?1:0;
        $servicerequest->save();
        return redirect()->back()->with("success",trans("messages.job_dispatch_request_maually"));
    }
}
