<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Module\BaseResponse;
use App\Http\Controllers\Controller;
use App\Module\Model\ServiceRequest;
use App\Module\Model\Payouts;
use DataTables;
use Auth;

class PayoutController extends Controller
{
    public function index(){
        return view('admins.payout.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function list()
    {
        $payouts = Payouts::with('User')->orderBy('created_at', 'DESC')->get();
        return DataTables::of($payouts)
        ->editColumn('user_id', function($data) {
            return $data->User->fullname;
        })
        ->editColumn('amount', function($data) {
            return html_entity_decode("&pound;").$data->amount;
        })
        ->editColumn('status', function($data) {
            if($data->status == '1'){
                return "Success";
            }else{
                return "Failed";
            }
        })
        ->editColumn('created_at', function($data) {
            return date("d M y h:i A." ,strtotime($data->created_at));
        })
        ->editColumn('action', function($data) {
            if($data->admin_approve == 0){
                $action = '<button account="'.$data->stripe_account_id.'" payout="'.$data->payout_request_id.'" class="btn btn-danger cancel_payout">Cancel</button>';
            }else{
                $action = '<span class="badge badge-danger">Canceled by you</span>';
            }
            return $action;
        })
        ->make(true);
    }

    public function PayoutWebhook(Request $request) {
        $response = json_decode($request->getContent(), true);

        if (!empty($response['data']['object']['id'])) {
            $payoutid = $response['data']['object']['id'];
            $status = $response['data']['object']['status'];
            $payout = Payouts::where('payout_request_id',$payoutid)->first();
            if(!empty($payout)){
                $payout->status = $status;
                $payout->save();
            }

            return BaseResponse::response([
                'requestStatus' => 'success'
            ]);
        }

    }


}
