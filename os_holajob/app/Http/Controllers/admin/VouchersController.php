<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Response;
use \App\Module\Model\User;
use \App\Module\Model\Vouchers;
use Datatables;
use File;
/**
 * Description of ExpertUpdateController
 * @author Shweta trivedi
 */
class VouchersController extends Controller {

    public function __construct() {}

    public function index(){
        $vouchers  = Vouchers::orderBy('id', 'DESC')->get();
        $data = array(
            'vouchers' => $vouchers,
        );
        return view('admins.vouchers.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        return view('admins.vouchers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
        $voucherstart = $request->get('voucher_start');
        $voucherexpirey =  $request->get('voucher_expiry');
        $vouchers = new Vouchers([
          'name' => $request->get('name'),
          'code'=> $request->get('code'),
          'discount_amount'=> $request->get('discount_amount'),
          'starts_at'=> $voucherstart." 00:00:00",
          'expires_at'=> $voucherexpirey." 00:00:00",
          'description'=> $request->get('description')
        ]);
        $vouchers->save();
        return redirect('admin/vouchers')->with('success', trans('messages.voucher_added'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $voucher = Vouchers::find($id);
        return view('admins.vouchers.edit', compact('voucher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id){
        $vouchers = Vouchers::find($id);
        $vouchers->name  = $request->get('name');
        $vouchers->code = $request->get('code');
        $vouchers->discount_amount = $request->get('discount_amount');
        $vouchers->starts_at = $request->get('voucher_start');
        $vouchers->expires_at = $request->get('voucher_expiry');
        $vouchers->description = $request->get('description');
        $vouchers->save();
        return redirect('admin/vouchers')->with('success', trans('messages.voucher_updated'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $share = Vouchers::find($id);
        echo $share->delete();

        //return redirect('admin/vouchers')->with('success', 'Stock has been deleted Successfully');
    }
}
