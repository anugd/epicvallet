<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Module\Model\Faq;
use App\Module\Model\FaqCategory;
use Helper;
use Session;
use DataTables;
use DB;
use Storage;
use File;

class FAQController extends Controller
{
	//view all faqs list
	public function index() {
		return view('admins.faqs.index');
	}

	//get all faqs ajax data
	public function getAllFaqs() {

        $faqs = Faq::where('is_deleted',0)->orderBy('created_at', 'DESC')->get();
        return DataTables::of($faqs)
            ->editColumn('checkmark', function($data) {
                return '<input type="checkbox" value='. $data->id .'>';
            })

			->editColumn('status', function ($data) {
				if($data->status == 1){
					$checked = "checked";
					$title = "Active";
				}else{
					$checked = "";
					$title = "De-Active";
				}
				
				$status = '<label class="switch" title="'.$title.'">
								<input  class="updateStatus" status ="'.$data->status.'" value="'.$data->id.'" id="status_'.$data->id.'" type="checkbox" '.$checked.'>
							  	<span class="slider round"></span>
				 		  </label>';
				return
					$status;
			})
            ->editColumn('action',function($data){
				$action = "<div class='row'>";
				$action .= "<div class='col-sm-1'><a id='edit' class = 'fa fa-edit' title='Edit' data-toggle='tooltip'  href='faqs/edit/$data->id'></a></div>";
				$action .= "<div class='col-sm-1'><a id='delete' class = 'fa fa-trash deleteFaq' title='Delete' data-toggle='tooltip' delId = '$data->id' href = '#'></a></div>";
				$action .= "</div>";
                return $action;
			})
			->escapeColumns([])
            ->make(true);
	}

	/**
 	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function create(){
		$category = FaqCategory::pluck('name', 'id');
        return view('admins.faqs.create',compact('category'));
	}

  	/**
 	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
    public function store(request $request)
    {

		$request->validate([
			'title'=>'required',
			'category'=>'required',
			'message'=>'required'
		]);

		$faq = new Faq([
			'title' => $request->title,
			'category' => $request->category,
			'message' => $request->message,
			'status' => $request->status
		]);
		$faq->save();
		return redirect('admin/faqs')->with('success', trans('messages.faq_created'));
	}

	/**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function edit($id)
    {
		$faq = Faq::find($id);
		$category = FaqCategory::pluck('name', 'id');
        return view('admins.faqs.edit', compact('faq','category'));
	}

 	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$request->validate([
			'title'=>'required',
			'category'=>'required',
			'message'=>'required'
		]);
		$faq = Faq::find($request->id);
		$faq->title = $request->title;
		$faq->category = $request->category;
		$faq->message = $request->message;
		$faq->status = $request->status;
		$faq->save();
		return redirect('admin/faqs')->with('success', trans('messages.faq_updated'));
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

	public function destroy($id)
	{
		echo $result = DB::table('faqs')->where('id', $id)->update(['is_deleted' => 1]);
		//return redirect('admin/faqs')->with('success', 'Faq has been deleted successfully.');
	}
	public function updateStatus(Request $request) {

        $id = $request->id;
		if(isset($id) && !empty($id) && !is_array($id))
        {
            $catIds[] = $id;
        }
        else if (isset($id) && !empty($id) && is_array($id))
        {
            $catIds = $id;
		}
		$status = $request->status;

        echo $result = DB::table('faqs')->whereIn('id', $id)->update(['status' => $status]);

    }

}
