<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Module\Model\UserAddresses;
use \App\Module\Model\UserLanguages;
use \App\Module\Model\UserServices;
use \App\Module\Model\User;
use \App\Module\Model\Profile;
use \App\Module\Model\Languages;
use \App\Module\Model\Services;
use \App\Module\Model\RelationShip;
use Helper;
use Session;
use DataTables;
use DB;
use Storage;
use File;

class ExpertController extends Controller
{
	//view all Experts list
	public function index() {
		return view('admins.experts.index');
	}

	//get all Experts ajax data
	public function getAllExperts() {
        $experts = User::where('user_type_id',2)->orderBy('created_at', 'DESC')->get();
        return DataTables::of($experts)
            ->editColumn('checkmark', function($data) {
				return  '<input type="checkbox" class="checkBoxClass" name="type" value="'. $data->id .'" />';
            })
			->editColumn('status', function ($data) {
				if($data->status == 1){
					$checked = "checked";
				}else{
					$checked = "";
				}
				$status = '<label class="switch">
								<input class="updateStatus" status ="'.$data->status.'" value="'.$data->id.'" id="status_'.$data->id.'" type="checkbox" '.$checked.'>
							  	<span class="slider round"></span>
						  </label>';
				return $status;
			})
            ->editColumn('action',function($data){

				$action = "<div class='row'>";
				$action .= "<div class='col-sm-1'><a id='edit' class = 'fa fa-edit' title='Edit' data-toggle='tooltip'  href='experts/edit/$data->id'></a></div>";
				$action .= "</div>";
                return $action;
			})
			->editColumn('approval',function($data){
				if($data->admin_verified == 0){
					$action = '<button user="'.$data->id.'" status="accept" class="btn btn-primary btn-approval">Accept</button>&nbsp; &nbsp;<button user="'.$data->id.'" status="reject" class="btn-approval btn btn-danger">Decline</button>';
				}elseif($data->admin_verified == 1){
					$action = '<span class="badge badge-success">Accepted</span>';
				}elseif($data->admin_verified == 2){
					$action = '<span class="badge badge-danger">Rejected</span>';
				}
				return $action;
			})
			->escapeColumns([])
            ->make(true);
	}

	public function updateStatus(Request $request) {

        $id = $request->id;
		if(isset($id) && !empty($id) && !is_array($id))
        {
            $catIds[] = $id;
        }
        else if (isset($id) && !empty($id) && is_array($id))
        {
            $catIds = $id;
		}
		$status = $request->status;

        echo $result = DB::table('users')->whereIn('id', $id)->update(['status' => $status]);

	}

	public function confirmation(Request $request){
		$request->validate([
			'user'=>'required',
			'status'=>'required'
		]);

		$userobj  = User::where('id',$request->user)->first();

		if(!empty($userobj)){
		  $newpassword = 'new-password';
		  $userobj->shuftipro_verified = 1;
		  if($request->status == 'accept'){
			  $userobj->shuftipro_verified = 1;
			  $userobj->admin_verified = 1;
		  }else{
			  $userobj->admin_verified = 2;
		  }
		  $userobj->save();
		  echo "error";
		}else{
		  echo "ok";
		}

	}


	public function skills(){
		$data = [];
		$services = Services::where('parent_id' , '=' , 0 )->get();
		foreach ($services as $keyservice => $service) {
			$svpobject = new \stdClass();
			$svpobject->id = $service->id;
			$svpobject->text = $service->service;
			$onecategoryservices = Services::where('parent_id' , '=' , $service->id )->get();
			$childdata  = [];
			foreach ($onecategoryservices as $onecategoryservicekey => $onecategoryservice) {
				$svcobject = new \stdClass();
				$svcobject->id =  $onecategoryservice->id;
				$svcobject->text = $onecategoryservice->service;
				$childdata[] = $svcobject;
			}
			$svpobject->children = $childdata;
			$data[] = $svpobject;
		}
		echo json_encode($data);
	}

	public function language(){
		$languages = Languages::select('id', 'language as text')->get();
		echo json_encode($languages);
	}

	/**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/

	public function edit($id){
		$user = User::with('profile')->find($id);
		$address = UserAddresses::where(['user_id' => $id, 'is_primary' => 1])->first();
		$userlangs  = UserLanguages::with('Language')->where('user_id',$user->id)->get();
		$userservices  = UserServices::with('Services')->where('user_id',$user->id)->get();
		$releationShip = RelationShip::pluck('name', 'id');
		$data = array(
			'user' => $user,
			'address' => $address,
			'userlangs' => $userlangs,
			'userservices' => $userservices,
			'releationShip' => $releationShip,
		);
        return view('admins.experts.edit', $data);
	}

	/**
 	* @param Request $request
	* User sendForgetPassMail Logic
    */

    private function sendResetPassMail($user) {
		$validated_data = $user ;
		$validated_data['new_password'] = $user->password ;
		\Mail::to($validated_data->email)->send(new \App\Module\Mail\AdminUpdateUserPassword($validated_data));
		unset($validated_data['new_password']);
        return true;
	}
	
 	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	
    public function update(Request $request){
		$request->validate([
			'first_name'=>'required',
			'last_name'=>'required',
			"city" => 'required',
			"state" => 'required',
			"postal_code" => 'required',
			"address_line1" => 'required'
		]);
		$user = User::find($request->id);
		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->fullname = $user->first_name." ".$request->last_name;
		$user->available  = isset($request->available) ? 1 : 0;
		if(!empty($request->password)) {
			$user->password = bcrypt($request->password);
		}
		if($request->file('expertProfileImage')){
			$path = $request->file('expertProfileImage')->store('',['disk' => 'user']);
			$user->profile_image = "avatars/".$path;
		}
		if($user->save()){
			if(!empty($request->password)) {
				$user->password = $request->password;
				$this->sendResetPassMail($user);
			}
			$address = UserAddresses::where(['user_id' => $request->id, 'is_primary' => 1])->first();
			if(empty($address)){
				$address = new UserAddresses;
				$address->is_primary = 1;
				$address->user_id = $request->id;
			}
			$address->city = !empty($request->city) ? $request->city : "";
			$address->state =  !empty($request->state) ? $request->state : "";
			$address->postal_code =   !empty($request->postal_code) ? $request->postal_code : "";
			$address->address_line1 =  !empty($request->address_line1) ? $request->address_line1 : "";
			$address->address_line2 =   !empty($request->address_line2) ? $request->address_line2 : "";

			if($address->save()){
				$profile = Profile::where('user_id',$request->id)->first();
				if(empty($profile)){
					$profile = new Profile;
					$profile->user_id = $user->id;
				}
				
				$profile->dob =  $request->dob3;
				$profile->gender = $request->gender;
				$profile->about = $request->about;
				$profile->emergency_contact_name =  !empty($request->fullname_relation) ? $request->fullname_relation : "" ;
				$profile->emergency_contact_number = !empty($request->phone_relation) ? $request->phone_relation : "";
				$profile->emergency_contact_relationship = !empty($request->relationship) ? 
				$request->relationship : "";
				if($profile->save()){
					$languages = $request->languages;
					$skills = $request->skills;

					$userlangs  = UserLanguages::where('user_id',$user->id)->delete();
					if(!empty($languages)){
						foreach ($languages as $languagekey => $language) {
							$newlanguage = new UserLanguages;
							$newlanguage->language_id = $language;
							$newlanguage->user_id = $user->id;
							$newlanguage->save();
						}
					}

					$userservices  = UserServices::where('user_id',$user->id)->delete();
					if(!empty($skills)){
						foreach ($skills as $skillkey => $skill) {
							$newlanguage = new UserServices;
							$newlanguage->service_id = $skill;
							$newlanguage->user_id = $user->id;
							$newlanguage->save();
						}
					}
				}

				return  back()->with('success', trans('messages.job_seeker_updated'));
			}else{
				return  back()->with('error', trans('messages.something_wrong'));
			}
		}else{
			return  back()->with('error', trans('messages.something_wrong'));
		}
	}


}
