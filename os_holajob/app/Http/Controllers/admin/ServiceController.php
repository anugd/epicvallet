<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Module\Model\Services;
use App\Module\Model\Languages;
use App\Module\Model\ServiceRequest;
use App\Module\Model\ServicePayment;
use Helper;
use Session;
use DataTables;
use DB;
use Storage;
use File;
use Alert;

class ServiceController extends Controller
{
	//view all services list
	public function index() {
		return view('admins.services.index');
	}

	//get all services ajax data
	public function getAllServices() {
		$services = Services::with('Language')->where('is_deleted',0)->orderBy('id', 'DESC')->get();
        return DataTables::of($services)
            ->editColumn('checkmark', function($data) {
                return '<input type="checkbox" value='. $data->id .'>';
            })
			->editColumn('service_image', function ($data) {
				if($data->service_image) {
					$url = asset('service/'.$data->service_image);

					return '<img src="'.$url.'" border = "0" width = "40" class = "img-rounded" align = "center"/>';
				}
			})
			->editColumn('language_id', function ($data) {

				if($data->language_id) {
					return $data->language->language;
				}
			})
			->editColumn('status', function ($data) {
				if($data->status == 1){
					$checked = "checked";
					$title = "Active";
				}else{
					$checked = "";
					$title = "De-Active";
				}
				$status = '<label class="switch" title="'.$title.'">
								<input  class="updateStatus" status ="'.$data->status.'" value="'.$data->id.'" id="status_'.$data->id.'" type="checkbox" '.$checked.'>
							  	<span class="slider round"></span>
				 		  </label>';
				return
					$status;
			})
            ->editColumn('action',function($data) {
				$action = "<div class='row'>";
				$action .= "<div class='col-sm-1'><a id='edit' class = 'fa fa-edit' title='Edit' data-toggle='tooltip'  href='services/edit/$data->id'></a></div>";
				$action .= "<div class='col-sm-1'><a id='delete' class = 'fa fa-trash deleteCat' title='Delete' data-toggle='tooltip' delId ='$data->id' href = '#'></a></div>";
				$action .= "</div>";
                return $action;
			})
			->escapeColumns([])
            ->make(true);
	}

	/**
 	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/

	public function create(){
		$services = Services::pluck('service', 'id');//all services to select parent service
		$languages = Languages::pluck('language', 'id');//all language to select language
        return view('admin.services.create',compact('services','languages'));
	}

  	/**
 	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/

    public function store(request $request){
		$request->validate([
			'language_id'=>'required',
			'service'=>'required',
			// 'service_image'=>'required',
			// 'service_icon'=>'required',
			'price'=> 'required|numeric'
		]);
		$image = $request->file('service_image');
		$extension = $image->getClientOriginalExtension();
		$service_image = $image->getFilename().'.'.$extension;
		Storage::disk('service')->put($service_image,  File::get($image));
		$service = new Service([
			'parent_id' => $request->parent_id,
			'language_id' => $request->language_id,
			'service' => $request->service,
			// 'service_image' => $service_image,
			// 'service_icon' => $request->service_icon,
			'price' => $request->price,
			'status' => $request->status
		]);
		$service->save();
		return redirect('admin/services')->with('success', trans('messages.service_added'));
	}

	/**
 	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/

	public function edit($id){
		$service = Services::query()->find($id);
		$services = Services::pluck('service', 'id');//all services to select parent service
		$languages = Languages::pluck('language', 'id');//all language to select language
        return view('admins.services.edit', compact('service','services','languages'));
	}

 	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request){
		$request->validate([
			'language_id'=>'required',
			'service'=>'required',
			//'service_image'=>'required',
			//'service_icon'=>'required',
			'price'=> 'required|numeric'
		]);
		// if(!empty($request->file('service_image'))) {
		// 	$service_image = $request->service_image_old;

		// 	Storage::disk('service')->delete($service_image);
		// 	$image = $request->file('service_image');
		// 	$extension = $image->getClientOriginalExtension();
		// 	$service_image = $image->getFilename().'.'.$extension;
		// 	Storage::disk('service')->put($service_image,  File::get($image));

		// }else {
		// 	$service_image = $request->service_image_old;
		// }

		$service = Services::query()->find($request->id);
		$service->parent_id = $request->parent_id;
		$service->language_id = $request->language_id;
		$service->service = $request->service;
		//$service->service_image = $service_image;
		//$service->service_icon = $request->service_icon;
		$service->price = $request->price;
		$service->status = $request->status;
		$service->save();
		return redirect('admin/services')->with('success',trans('messages.service_added'));
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

	public function destroy($id){
		echo $result = DB::table('services')->where('id', $id)->update(['is_deleted' => 1]);
		//return redirect('admin/services')->with('success', 'Service has been deleted successfully.');
	}

	/**
     * Update status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

	public function updateStatus(Request $request) {
        $id = $request->id;
		if(isset($id) && !empty($id) && !is_array($id)){
            $catIds[] = $id;
        }
        else if (isset($id) && !empty($id) && is_array($id)){
            $catIds = $id;
		}
		$status = $request->status;
        echo $result = DB::table('services')->whereIn('id', $id)->update(['status' => $status]);
    }

	/**
     * Service History
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

	public function servicesHistory(){
        return view('admins.services.serviceHistory');
    }

	/**
     * servicesHistoryData
     *
     * @return \Illuminate\Http\Response
     */

    public function servicesHistoryData(){
        $services = ServiceRequest::with('ServiceFeedback','customer','expert','service','ServicePayment')->orderBy('id', 'DESC')->get();
        return DataTables::of($services)
            ->addColumn('action',function($data){
                return '<a href="#" title="View Detail" data-toggle="modal" data-target="#viewModal" dataId = '.$data->id.' class="viewDetail"><i class="fa fa-eye" aria-hidden="true"></i></a>';
			})
			->editColumn('address', function ($data) {

				if($data->address) {
					if(strlen($data->address) > 10) {
						$add = '<span title="'.$data->address.'">'.substr($data->address,0,10)."..."."</span>";
					}else{
						$add = $data->address;
					}
					return  $add;
				}
			})
			->addColumn('customer_fullname', function($data) {
                return (!empty($data->customer)) ? $data->customer->fullname : '--';
            })
            ->addColumn('expert_fullname', function($data) {
                return (!empty($data->expert)) ? $data->expert->fullname : '--';
            })
            ->editColumn('status', function($data) {
				if($data->status == '0'){
					return '<span class="badge badge-danger">Pending</span>';
				}else if($data->status == '1'){
					return '<span class="badge badge-primary">Booked</span>';
				}else if($data->status == '2'){
					return '<span class="badge badge-success">Completed</span>';
				}else if($data->status == '3'){
					return '<span class="badge badge-primary">On-Going</span>';
				}else if($data->status == '4'){
					return '<span class="badge badge-danger">Cancelled</span>';
				}
            })
            ->rawColumns(['status', 'action','address'])
            ->make(true);
	}

	/**
     * servicesHistoryDetail
     *
     * @return \Illuminate\Http\Response
     */

	public function servicesHistoryDetailAdmin(Request $request) {
		$id = $request->id;
		$services = ServiceRequest::with('ServiceFeedback','customer','expert','service','ServicePayment')->where('id',$id)->first();

        $data = array(
            'services' => $services
        );
		return view('elements.servicesHistoryDetailAdmin',$data);
	}

	/**
     * revenue
     *
     * @return \Illuminate\Http\Response
     */

	public function revenue(){
        return view('admins.services.revenue');
    }

	/**
     * revenueData
     *
     * @return \Illuminate\Http\Response
     */

    public function revenueData() {
		$revenue = ServicePayment::with('expert','service','service.service')->orderBy('created_at', 'DESC')->get();
        return DataTables::of($revenue)
            ->addColumn('action',function($data){
                return '<a href="#" title="View Detail" data-toggle="modal" data-target="#viewModal" dataId = '.$data->id.' class="viewDetail"><i class="fa fa-eye" aria-hidden="true"></i></a>';
			})
			->editColumn('service', function($data) {
				return (!empty($data->service->service)) ? $data->service->service->service : 'N/A';
			})
			->editColumn('per_hour_rate', function($data) {
				return html_entity_decode("&pound;").round($data->per_hour_rate,2);
			})
			->editColumn('total_price', function($data) {
				return html_entity_decode("&pound;").round($data->total_price,2);
			})
			->editColumn('service_fee', function($data) {
				return html_entity_decode("&pound;").round($data->service_fee,2);
			})
			->editColumn('sub_total', function($data) {
				return html_entity_decode("&pound;").round($data->sub_total,2);
			})
            ->editColumn('payment_status', function($data) {

				if($data->payment_status == '0'){
					return '<span class="badge badge-danger">Pending</span>';
				}else if($data->payment_status == '1'){
					return '<span class="badge badge-success">Completed</span>';
				}
            })
            ->rawColumns(['status', 'action','payment_status'])
            ->make(true);
	}

	public function revenueDetail(Request $request) {
		$id = $request->id;
		$revenue = ServicePayment::with('expert','service','service.service')->where('id',$id)->first();

        $data = array(
            'services' => $revenue
        );
		return view('elements.revenueDetail',$data);
	}

}
