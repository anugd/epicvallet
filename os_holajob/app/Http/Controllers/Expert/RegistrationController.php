<?php

namespace App\Http\Controllers\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Module\Model\User;
use App\Module\Model\Profile;
use App\Module\Model\UserAddresses;
use App\Module\Model\OtpLog;
use App\Module\Service\SmsService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Module\Mail\AdminVerification;
use App\Module\Mail\EmailVerification;
use Helper;
use Session;



class RegistrationController extends Controller
{
  public function register(Request $request) {

      $validation = $request->validate([
        'first_name3' => 'required',
        'last_name3' => 'required',
        'gender' => 'required',
        'dob3' => 'required',
        'home_address1' => 'required',
        'home_address2' => 'required',
        'city' => 'required',
        'post_code' => 'required',
        'mobile_number3' => 'required|min:10|numeric|unique:users,mobile_number',
        'email3' => 'required|string|max:255|unique:users,email',
        'password3' => 'required',
        'eTerms1' => 'required',
        'eTerms2' => 'required',
        'eTerms3' => 'required',
        'eTerms4' => 'required',
        'eTerms5' => 'required'
      ],
      [
        'first_name3.required' => trans('messages.expert_firstname_required'),
        'last_name3.required' => trans('messages.expert_lastname_required'),
        'gender.required' => trans('messages.expert_gender_required'),
        'dob3.required'    => trans('messages.expert_dob_required'),
        'home_address1.required' => trans('messages.expert_address_required'),
        'home_address2.required' => trans('messages.expert_address2_required'),
        'city.required' => trans('messages.expert_city_required'),
        'post_code.required' => trans('messages.expert_postcode_required'),
        'mobile_number3.required' => trans('messages.expert_mobile_required'),
        'email3.required'    =>trans('messages.expert_email_required') ,
        'password3.required' => trans('messages.expert_password_required'),
        'email3.unique' => trans('messages.expert_email_exists'),
        'mobile_number3.unique' => trans('messages.expert_mobile_exists')
      ]);

        $model = '';
        $otp = '';
        try {
            \DB::transaction(function () use (&$model, $request, &$otp) {
                $userData['fullname']= $request->first_name3." ". $request->last_name3;
                $userData['first_name']=$request->first_name3 ;
                $userData['last_name']= $request->last_name3;
                $userData['username']= $request->first_name3;
                $userData['email']= $request->email3;
                $userData['mobile_number']=  $request->mobile_number3;
                $userData['password'] = bcrypt( $request->password3);
                $userData['user_type_id'] = 2;
                $userData['profile_complete_step'] = 1;
                $model = User::create($userData);
                if(!empty($model)){
                    $profile = new Profile;
                    $profile->user_id =  $model['id'];
                    $profile->dob =  $request->dob3;
                    $profile->gender = $request->gender;
                    $profile->save();
                    //Save Address
                    $address = new UserAddresses;
                    $address->user_id = $model['id'];
                    $address->city = $request->city3 ;
                    $address->postal_code = $request->post_code ;
                    $address->address_line1 =  $request->home_address1;
                    $address->address_line2 =  $request->home_address2;
                    $address->is_primary =  1;
                    $address->save();
                    $model->profile_complete_step = 2;
                    $model->save();
                 }
                $otp = (new SmsService())->sendOTP($request->mobile_number3);
                (new OtpLog())->saveMobileVerificationOtp($model['id'], $otp);
                $data =  $model->toArray();
                $data['otp'] = $otp;
                $model->verification_token = $this->sendMail($data);
                $this->requestAdminConfirm($data);
                $model->save();
            });
            return redirect()->route('congratulation')->with('success', trans('messages.registration_success'));
        } catch (\Exception $e) {
            return back()->withInput($request->input())->with('message3', trans('messages.something_wrong')); //$e->getMessage()
        }
    }

    private function sendOTP($user) {
        return (new SmsService())->sendOTP($user->mobile_number);
    }

    /**
     * To Send OTP via email
     * @author Shweta
     * @param Request $request
     */

    private function sendMail($user) {
        $validated_data = $user ;
        $verification = \Illuminate\Support\Str::random(45);
        $validated_data['verification_token'] = $verification;
        \Mail::to($validated_data['email'])->send(new EmailVerification($validated_data));
        return $verification;
    }

    /**
     * To Send OTP via email
     * @author Shweta
     * @param Request $request
     */

    private function requestAdminConfirm($user) {
        \Mail::to('iamadmin@yopmail.com')->send(new AdminVerification($user));
    }
}
