<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use \App\Module\Model\User;
use \App\Module\Model\Profile;
use \App\Module\Model\OtpLog;
use Helper;
use Auth;
use Storage;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/service-history';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        if (Auth::check())
        {
          return redirect()->to('/service-history');
        }
    }

    public function loginauth(){
        if(auth()->guard('web')->check()){
            return redirect()->intended('/service-history');
        }
        if (Auth()->guard('admin')->check()){
            return redirect()->intended('admin/dashboard');
        }
        return view('auth.login');
    }


    public function login(Request $request) {

        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }


        if ($this->guard()->validate($this->credentials($request))) {

            $user = $this->guard()->getLastAttempted();
            if ($user->mobile_verified == 1 && $this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            } else {
                if($user->mobile_verified == 0){
                    return redirect('/otp-verification')->with('user_id',$user->id);
                }else{
                    $this->incrementLoginAttempts($request);
                    return redirect()
                        ->back()
                        ->withInput($request->only($this->username(), 'remember'))
                        ->withErrors(['active' => trans('messages.active_login')]);
                }
            }
        }

        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }


    protected function credentials(Request $request)
    {
         if(is_numeric($request->get('email'))){
            return [
                'mobile_number' => $request->get($this->username()),
                'password' => $request->password,
                'user_type_id' => 1,
                'status' => '1'
            ];
         }
        return [
            'email' => $request->get($this->username()),
            'password' => $request->password,
            'user_type_id' => 1,
            'status' => '1'
        ];
    }

    public function expertLogin(Request $request)
    {

        $this->validate($request, [
            'email2'   => 'required',
            'password2' => 'required|min:6'
        ],
        [
            'email2.required'    => trans('messages.required_email'),
            'password2.required' => trans('messages.required_password'),
        ]);
        $remember_me = $request->has('remember') ? true : false;

        if(is_numeric($request->get('email'))){
            if (Auth::attempt(['mobile_number' => $request->email2, 'password' => $request->password2, 'user_type_id'=>2], $remember_me)) {

                // if(auth()->user()->is_logged == 1){
                //     Auth::logout();
                //     return back()->withInput($request->only('email', 'user_type_id','remember'))->with('message', 'Your account is already logged in on another device.');
                // }

                if(auth()->user()->status == 0){
                    Auth::logout();
                    return back()->withInput($request->only('email', 'user_type_id','remember'))->with('message', trans('messages.account_disabled'));
                }

                if(auth()->user()->admin_verified == 1){
                    return redirect()->to('/service-history');
                }
                elseif (auth()->user()->admin_verified == 2) {
                    Auth::logout();
                    return back()->withInput($request->only('email', 'user_type_id','remember'))->with('message', trans('messages.account_rejected'));
                }
                else{
                    Auth::logout();
                    return back()->withInput($request->only('email', 'user_type_id','remember'))->with('message', trans('messages.account_confirmation'));
                }
            }
         }
        if (Auth::attempt(['email' => $request->email2, 'password' => $request->password2,  'user_type_id'=>2], $remember_me)) {
            // if(auth()->user()->is_logged == 1){
            //     Auth::logout();
            //     return back()->withInput($request->only('email', 'user_type_id','remember'))->with('message', 'Your account is already logged in on another device.');
            // }

            if(auth()->user()->status == 0){
                Auth::logout();
                return back()->withInput($request->only('email', 'user_type_id','remember'))->with('message', trans('messages.account_disabled'));
            }

            if(auth()->user()->admin_verified == 1){
                return redirect()->to('/service-history');
            }
            elseif (auth()->user()->admin_verified == 2) {
                Auth::logout();
                return back()->withInput($request->only('email', 'user_type_id','remember'))->with('message', trans('messages.account_rejected'));
            }
            else{
                Auth::logout();
                return back()->withInput($request->only('email', 'user_type_id','remember'))->with('message', trans('messages.account_confirmation'));
            }
        }
        return back()->withInput($request->only('email', 'user_type_id','remember'))->with('message', trans('messages.wrong_email_password'));
    }

    public function socialLogin($social)
    {
       return  Socialite::driver($social)->redirect();
    }

    public function handleProviderCallback($social)
    {
        $userSocial = Socialite::driver($social)->user();
        $user = User::where(['email' => $userSocial->getEmail()])->first();
        if($user){
            Auth::login($user);
            return redirect()->route('home');
        }else{

            $model = User::create([
               'fullname'     => $userSocial->name,
               'email'    => $userSocial->email,
               'user_type_id'=>1,
            ]);

            if (!empty($model)) {
                $profile = new Profile();
                $profile->user_id = $model['id'];
                $prepend = $model['id']."-";
                if( $social == "facebook"){
                    if(isset($userSocial->avatar_original) && !empty($userSocial->avatar_original)){
                        $url = $userSocial->avatar_original;
                        $contents =  file_get_contents($url);
                        $basename = $prepend.$userSocial->name.".jpg";
                        Storage::disk('user')->put($basename, $contents);
                        User::where('id', $model['id'])->update(array('profile_image' => 'avatars/'.$prepend.$userSocial->name.".jpg"));
                    }
                    $profile->facebook_id = $userSocial->id;
                }else{
                    if(isset($userSocial->avatar) && !empty($userSocial->avatar)){
                        $url = $userSocial->avatar;
                        $contents =  file_get_contents($url);
                        $basename = basename($url);
                        $imgname = $prepend.$basename;
                        Storage::disk('user')->put($imgname, $contents);
                        User::where('id', $model['id'])->update(array('profile_image' => $imgname));
                    }
                    $profile->google_id = $userSocial->id;
                }
                $profile->save();
            }
            Auth::login($model);
            return redirect()->route('home');
        }
    }

    public function logout() {
    	Auth::logout();
    	return redirect('/');
    }

    public function authUserType(){
        $authUser = auth()->user();
        if($authUser->user_type_id ==1)
            return redirect()->to('/dashboard');
        else
            return redirect()->to('/service-history');
    }


}
