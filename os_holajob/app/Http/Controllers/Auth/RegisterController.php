<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Module\Model\User;
use App\Module\Model\Profile;
use App\Module\Model\OtpLog;
use App\Module\Service\SmsService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Helper;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/otp-verification';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
            'mobile_number' => ['required','min:10','numeric','unique:users'],
            'terms' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Module\Model\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['firstname'],//250319
            'last_name' => $data['lastname'],
            'username' => $data['username'],
            'email' => $data['email'],
            'mobile_number' => $data['mobile_number'],
            'user_type_id' => $data['user_type_id'],
            'password' => bcrypt($data['password']),
        ]);

        $otp = Helper::generate_otp();
        $otpLog = OtpLog::create([
            'otp' =>  $otp,
            'user_id'=>$user['id'],
            'type' =>  2,
            'expires_at' => now(),
        ]);

        if(!empty($data['company_name'])){
            $profile = new Profile;
            $profile->user_id =  $user['id'] ;
            $profile->company_name =  $data['company_name'];
            $profile->company_registration_no =  $data['company_registration_no'];
            $profile->company_no = $data['company_no'];
            $profile->save();
        }
        $otp = $this->sendOTP($user);
                (new OtpLog())->saveMobileVerificationOtp($user['id'], $otp);
                $data =  $user->toArray();
                $data['otp'] = $otp ;
                $user->verification_token = $this->sendMail($data);
                $user->save();
        return $user;
    }
    private function sendOTP($user) {
        return (new SmsService())->sendOTP($user->mobile_number);
    }


    /**
     * To Send OTP to mobile
     * @author Shweta
     * @param Request $request
     */
    public function resendOTP(Request $request){

        $user_id = $request->user_id ;
        $user = User::where('id', $user_id)->first();
        if(!empty($user)){
            //$otp = "123456";//To test API when Error “Insufficient-Credits” in Bulksms
            $otp = (new SmsService())->sendOTP($user['mobile_number']);
            $otp_expiry_time =  date('Y-m-d H:i:s', strtotime("+3 min"));
             try {
                (new OtpLog())->saveMobileVerificationOtp($user['id'], $otp);
                $user['otp'] = $otp ;
                //$remember_token = $this->sendForgetPassMail($user);
                //User::where('id', $user['id'])->update(array('remember_token' => $remember_token));
                $otpLog = OtpLog::create([
                    'otp' =>  $otp,
                    'user_id' =>$user['id'],
                    'type' =>  3,
                    'expires_at' => $otp_expiry_time,
                ]);
                return response()->json([
                'requestStatus' => 'success', 'message'=> trans('messages.otp_sent')
                ]);
            } catch (\Exception $e) {
                $error_message = $e->getMessage();
                return response()->json(['requestStatus' => 'invalid', 'message' => $error_message]);
            }
        }else{
            return response()->json(['requestStatus' => 'invalid', 'message' => trans('messages.user_not_found')]);
        }

         return response()->json(['requestStatus' => 'invalid', 'message' => trans('messages.user_not_found')]);
    }

     /**
     * To Send OTP via email
     * @author Shweta
     * @param Request $request
     */
    public function resendOTPEmail(Request $request){
        $user_id = $request->user_id ;
        $user = User::where('id', $user_id)->first();
        if(!empty($user)){
            //$otp = "123456";//To test API when Error “Insufficient-Credits” in Bulksms
            $otp = (new SmsService())->sendOTP($user['mobile_number']);
            $otp_expiry_time =  date('Y-m-d H:i:s', strtotime("+3 min"));
             try {
                (new OtpLog())->saveMobileVerificationOtp($user['id'], $otp);
                $user['otp'] = $otp ;
                $remember_token = $this->sendForgetPassMail($user);
                User::where('id', $user['id'])->update(array('remember_token' => $remember_token));
                $otpLog = OtpLog::create([
                    'otp' =>  $otp,
                    'user_id' =>$user['id'],
                    'type' =>  3,
                    'expires_at' => $otp_expiry_time,
                ]);
                return response()->json([
                'requestStatus' => 'success', 'message'=> trans('messages.otp_sent')
                ]);
            } catch (\Exception $e) {
                $error_message = $e->getMessage();
                return response()->json(['requestStatus' => 'invalid', 'message' => $error_message]);
            }
        }else{
            return response()->json(['requestStatus' => 'invalid', 'message' => trans('messages.user_not_found')]);
        }

         return response()->json(['requestStatus' => 'invalid', 'message' => trans('messages.user_not_found')]);
    }

    /**
     * To Send OTP via email
     * @author Shweta
     * @param Request $request
     */
    private function sendMail($user) {
        $validated_data = $user ;
        $verification = \Illuminate\Support\Str::random(45);
        $validated_data['verification_token'] = $verification;
        \Mail::to($validated_data['email'])->send(new \App\Module\Mail\EmailVerification($validated_data));
        return $verification;
    }
    /**
     * To Send Forget Password email
     * @author Shweta
     * @param Request $request
     */
    private function sendForgetPassMail($user) {
        $validated_data = $user ;
        $verification = \Illuminate\Support\Str::random(45);
        $validated_data['remember_token'] = $verification;
        \Mail::to($validated_data['email'])->send(new \App\Module\Mail\ForgetPassEmailVerification($validated_data));
        return $verification;
    }


    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath())->with('user_id', $user->id);
    }
}
