<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Module\Model\ServiceRequest;
use Illuminate\Support\Facades\Auth;
use App\Module\Model\Payouts;
use DataTables;

class PayoutController extends Controller
{
    public function index(){
        return view('expert.payout.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $user = Auth::user();
        $payouts = Payouts::with('User')->where('user_id',$user->id)->get();
        return DataTables::of($payouts)
        ->editColumn('amount', function($data) {
            return html_entity_decode("&pound;").$data->amount;
        })
        ->editColumn('status', function($data) {
            if($data->status == '1'){
                return "Success";
            }else{
                return "Failed";
            }
        })
        ->editColumn('created_at', function($data) {
            return date("d M y h:i A" ,strtotime($data->created_at));
        })
        ->editColumn('action', function($data) {
            return '
            <a href="#" title="View Detail" data-toggle="modal" data-target="#viewPayoutModal" dataId = '.$data->id.' class="viewDetail"><i class="fa fa-eye" aria-hidden="true"></i></a>';
        })
        ->make(true);
    }
    public function payoutDetail(Request $request) {

		$payouts = Payouts::with('User')->where('id',$request->id)->first();

        $data = array(
            'payouts' => $payouts
        );
		return view('elements.payoutDetail',$data);
	}

}
