/**
 * Sockets
 *
 * @author      :: Amit Sharma
 * @module      :: Service
 * @description :: TODO: You might write a short summary of how this service works and what it represents here.
 * @email       :: samit.sdei@gmail.com
 *
 */
const PushNotificationService = require('./pushNotificationService');

module.exports = (io, Knex, test, nodemailerMailgun) => {
    console.log(test);
    io.on('connection', function (socket) {
        socket.on('join', function (data) {
            socket.join(data.id, function(){
                io.to(data.id).emit('joined');
            });
        });
        socket.on('cancelRequest',function(data){
            var service = data.service;
            var expert = data.expert;
            var user = data.user;
            Knex('service_request').where({service_id: service}).then((resprequest) =>{
            var update_declined_experts = [];
            if(resprequest[0]){
                if(resprequest[0].declined_experts){
                    var update_declined_experts = JSON.parse(resprequest[0].declined_experts);
                }
                var latitude = resprequest[0].latitude;
                var longitude = resprequest[0].longitude;
                Knex.select('id', 'user_id'  , 'current_latitude ', 'current_longitude ',
                    Knex.raw("SQRT(POW(69.1 * (current_latitude - " + latitude + "), 2) + POW(69.1 * (" + longitude + " - current_longitude) * COS(current_latitude / 57.3), 2)) As distance"))
                    .from('user_addresses')
                    .leftJoin('users', 'users.id', 'user_addresses.user_id as expert_id')
                    .leftJoin('user_services', 'user_services.user_id', 'user_addresses.user_id')
                    .where({'user_services.service_id' : service,'users.user_type_id':2,'users.shuftipro_verified':1,'users.available':1})
                    .whereNotIn('users.id', update_declined_experts)
                    .having('distance', '<', 25)
                    .orderBy('distance', 'asc').limit(1).then((respexpert) =>{
                        var newexpert = respexpert[0].expert_id;
                        update_declined_experts.push(newexpert);
                        let requestSenTime = new Date();
                        Knex('service_request')
                        .update({declined_experts : JSON.stringify(update_declined_experts),request_sent_date_time : requestSenTime})
                        .where({id : service}).then();

                        Knex('service_request').select(['service_request.*','services.service','services.price'])
                        .where({ "service_request.id": service })
                        .leftJoin('services', 'services.id', 'service_request.service_id')
                        .then((respservice) =>{
                            Knex('users').select(['users.*', 'user_addresses.id as address_id', 'user_addresses.current_latitude as latitude', 'user_addresses.current_longitude as longitude'])
                            .where({ 'users.id': user })
                            .leftJoin('user_addresses', 'user_addresses.user_id', 'users.id')
                            .then((respuser) =>{
                                let responseObject = {
                                    category: respservice[0].service,
                                    price: respservice[0].price,
                                    address : respuser[0].formatted_address,
                                    requestId: respservice[0].id,
                                    name : respuser[0].fullname,
                                    latitude : respuser[0].latitude,
                                    longitude : respuser[0].longitude,
                                    customer_id : resprequest.user_id,
                                    requestSenTime : requestSenTime
                                };
                                io.to(newexpert).emit('sendRequest', responseObject);
                                io.to('amit').emit('sendRequest', responseObject);
                                PushNotificationService.sendPushNotification(Knex,newexpert,i18n.__('new_job_request'),responseObject,1);
                            });
                        });
                    }).catch((err) =>{
                        io.to(user).emit('noExpert');
                        io.to('amit').emit('noExpert');
                        PushNotificationService.sendPushNotification(Knex,user,i18n.__('no_expert_found'),{},"",'booking_declined');
                        PushNotificationService.sendMailNotification(Knex,user,i18n.__('no_expert_found'),{},nodemailerMailgun,'booking_declined');
                    });
                }
            });
        });

        socket.on('acceptRequest',function(data){
            let expert = data.expert;
            let service = data.service;
            let user = data.user;
            Knex('service_request').where({id: service, status : 0}).update({status : 1,expert_id:expert}).then((result) =>{
                if(result){
                    Knex('users').select(['users.*', 'user_addresses.id as address_id', 'user_addresses.current_latitude as latitude', 'user_addresses.current_longitude as longitude','profile_image'])
                    .where({ 'users.id': expert })
                    .leftJoin('user_addresses', 'user_addresses.user_id', 'users.id')
                    .then((respuser) =>{
                        Knex('service_request')
                        .select(['service_request.*','users.fullname as fullname','service_feedback.id as feedbackid','services.price as svcprice','services.id as sid','services.parent_id as parent_sid', 'rate', 'services.service as name','parentService.service as parent_service_name'])
                        .where({ 'service_request.id': service })
                        .leftJoin('users', 'users.id', 'service_request.user_id')
                        .leftJoin('services', 'services.id', 'service_request.service_id')
                        .leftJoin('services as parentService', 'parentService.id', 'services.parent_id')
                        .leftJoin('service_feedback', 'service_feedback.service_request_id', 'service_request.id')
                        .then((respexpertreq) =>{
                            let receiver = respexpertreq['0'].user_id;
                            let sender = respexpertreq['0'].expert_id;
                            // Knex.select('*').from('chat').where({'sender' : sender,'receiver' : receiver}).orWhere({'sender' : receiver,'receiver' : sender}).then((data) =>{
                            //     if(!data.length){
                            Knex.insert({sender : sender,receiver : receiver,})
                            .into('chat').then((chatinitiated) =>{
                                let expertobj = {
                                    expert_id  : expert,
                                    request_id  : service,
                                    expert_image  : respuser[0].profile_image,
                                    expert_name  : respuser[0].fullname,
                                    expert_feedback  : respexpertreq[0].rate,
                                    expert_latitude  : respuser[0].latitude,
                                    expert_longitude  : respuser[0].longitude,
                                    job_category  : respexpertreq[0].name,
                                    service_parent_name  : respexpertreq[0].parent_service_name,
                                    job_cost  : respexpertreq[0].svcprice,
                                    id : chatinitiated['0'],
                                    sender : {
                                        id : expert,
                                        fullname: respexpertreq[0].fullname
                                    },
                                    receiver : {
                                        id : respexpertreq[0].user_id,
                                        fullname: respuser[0].fullname
                                    }
                                }
                                io.to(user).emit('accRequestResponse', expertobj);
                                io.to('amit').emit('accRequestResponse', expertobj);
                                PushNotificationService.sendPushNotification(Knex,user,i18n.__('job_accepted_by')+respuser[0].fullname,expertobj,"",'booking_confirmed');
                                PushNotificationService.sendMailNotification(Knex,user,i18n.__('job_accepted_by')+respuser[0].fullname,expertobj,nodemailerMailgun,'booking_confirmed');
                            });
                            // }else{
                            //     let expertobj = {
                            //         expert_id  : expert,
                            //         request_id  : service,
                            //         expert_image  : respuser[0].profile_image,
                            //         expert_name  : respuser[0].fullname,
                            //         expert_feedback  : respexpertreq[0].rate,
                            //         expert_latitude  : respuser[0].latitude,
                            //         expert_longitude  : respuser[0].longitude,
                            //         job_category  : respexpertreq[0].name,
                            //         service_parent_name  : respexpertreq[0].parent_service_name,
                            //         job_cost  : respexpertreq[0].svcprice,
                            //         id : data[0].id,
                            //         sender : {
                            //             id : expert
                            //         },
                            //         receiver : {
                            //             id : respexpertreq[0].user_id,
                            //             fullname :respexpertreq[0].fullname
                            //         }
                            //     }
                            //     io.to(user).emit('accRequestResponse', expertobj);
                            //     io.to('amit').emit('accRequestResponse', expertobj);
                            //
                            //     PushNotificationService.sendPushNotification(Knex,user,i18n.__('job_accepted_by')+respuser[0].fullname,expertobj,"",'booking_confirmed');
                            //     PushNotificationService.sendMailNotification(Knex,user,i18n.__('job_accepted_by')+respuser[0].fullname,expertobj,nodemailerMailgun,'booking_confirmed');
                            //
                            // }
                            //});
                        });
                    });
                }else{
                    io.to(expert).emit('alreadyTaken');
                    io.to('amit').emit('alreadyTaken');
                    PushNotificationService.sendPushNotification(Knex,expert,i18n.__('service_not_avialable'),{});
                }
            });
        });

        socket.on('requestService',function(data){
            var latitude = data.latitude;
            var longitude = data.longitude;
            var service = data.service;
            var user = data.user;
            var booking_date = data.booking_date;
            var address = data.address;
            var schedule = data.schedule;
            var saveObject = {
                user_id: user,
                service_id: service,
                latitude: latitude,
                longitude : longitude,
                address : address,
                schedule : schedule
            }
            if(booking_date){
                saveObject.booking_date = booking_date;
            }else{
                saveObject.booking_date = new Date();
            }
            var days = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
            var getday = new Date();
            var gettime = DisplayCurrentTime();
            var update_declined_experts = [user];
            Knex.insert(saveObject).into('service_request').then((requestId) =>{
                io.to(user).emit('sendRequest', {requestId: requestId[0]});
                io.to('amit').emit('sendRequest', {requestId: requestId[0]});
                if(schedule == '0'){
                    Knex.select('user_addresses.id as useraddr_id', 'user_addresses.user_id as expert_id','current_latitude','current_longitude ',
                        Knex.raw("SQRT(POW(69.1 * (current_latitude - " + latitude + "), 2) + POW(69.1 * (" + longitude + " - current_longitude) * COS(current_latitude / 57.3), 2)) As distance"))
                    .from('user_addresses')
                    .leftJoin('users', 'users.id', 'user_addresses.user_id')
                    .leftJoin('user_services', 'user_services.user_id', 'user_addresses.user_id')
                    .leftJoin('user_weekly_availability', 'user_weekly_availability.user_id', 'user_addresses.user_id')
                    .where({'user_services.service_id':service,'users.user_type_id':2,'users.shuftipro_verified':1,'users.available':1})
                    .where({'user_weekly_availability.day': days[getday.getDay()] })
                    .andWhere('user_weekly_availability.startTime' ,'<' , gettime)
                    .andWhere('user_weekly_availability.endTime' ,'>' , gettime)
                    .having('distance', '<', 25)
                    .orderBy('distance', 'asc')
                    .limit(1)
                    .then((resp) =>{
                        console.log(days[getday.getDay()]);
                        console.log(gettime);
                        console.log(resp);
                        return false;
                        if(resp.length >  0){
                            var newexpert = resp[0].expert_id;
                            Knex('service_request').select(['service_request.*','services.service','services.price'])
                            .where({ "service_request.id": requestId[0] })
                            .leftJoin('services', 'services.id', 'service_request.service_id')
                            .then((respservice) =>{
                                Knex('users').select(['users.*', 'user_addresses.id as address_id', 'user_addresses.current_latitude as latitude', 'user_addresses.current_longitude as longitude'])
                                .where({ 'users.id': user })
                                .leftJoin('user_addresses', 'user_addresses.user_id', 'users.id')
                                .then((respuser) =>{
                                    update_declined_experts.push(newexpert);
                                    let requestSenTime = new Date();
                                    Knex('service_request').update({declined_experts : JSON.stringify(update_declined_experts), request_sent_date_time : requestSenTime}).where({id : requestId[0]}).then();
                                    let responseObject = {
                                        category: respservice[0].service,
                                        price: respservice[0].price,
                                        address : respuser[0].formatted_address,
                                        requestId: requestId[0],
                                        name : respuser[0].fullname,
                                        latitude : respuser[0].latitude,
                                        longitude : respuser[0].longitude,
                                        customer_id : user,
                                        requestSenTime : requestSenTime
                                    }
                                    io.to(newexpert).emit('sendRequest',responseObject);
                                    io.to('amit').emit('sendRequest',responseObject);
                                    PushNotificationService.sendPushNotification(Knex,newexpert,i18n.__('new_job_request'),responseObject,1);
                                });
                            });
                        }else{
                            Knex('service_request').update({status : 4}).where({id : service}).then();
                            io.to(user).emit('noExpert');
                            io.to('amit').emit('noExpert');
                            PushNotificationService.sendPushNotification(Knex,user,i18n.__('no_expert_found'),{},"",'booking_declined');
                            PushNotificationService.sendMailNotification(Knex,user,i18n.__('no_expert_found'),{},nodemailerMailgun,'booking_declined');
                        }
                    });
                }
            });
        })

        socket.on('sendMessage',function(data){
            let sender = data.sender;
            let receiver = data.receiver;
            let message = data.message;
            let chat_id = data.chat_id;
            let image = data.image;
            Knex.insert({
                sender : sender,
                receiver : receiver,
                chat_id : chat_id,
                text : message,
                image : image
            },['id']).into('chat_details').then((response) =>{
                Knex('chat_details').select(['*']).where({"chat_details._id": response[0] })
                .leftJoin('users', 'chat_details.sender', 'users.id')
                .then((newMessageFullDetail) =>{
                    io.to(receiver).emit('newMessage', newMessageFullDetail);
                    PushNotificationService.sendPushNotification(Knex,receiver,i18n.__('new_message') +newMessageFullDetail[0].fullname ,newMessageFullDetail['0'],"",'expert_chat');
                    PushNotificationService.sendMailNotification(Knex,receiver,i18n.__('new_message') +newMessageFullDetail[0].fullname ,newMessageFullDetail['0'],nodemailerMailgun,'expert_chat');
                });
            });
        });

        socket.on('expertChangeLocation',function(data){
            let user_id = data.user_id;
            let lat = data.lat;
            let long = data.long;
            let request_id = data.request_id;
            Knex('service_request').where({ "id": request_id }).then((serviceDetail) =>{
                let responseObject = {
                    lat : lat,
                    long : long,
                    request_id : request_id
                };
                io.to(serviceDetail['0'].user_id).emit('updatedExpertLocation', responseObject);
                Knex('user_addresses').update({
                    current_latitude : lat,
                    current_longitude : long,
                    longitude : long,
                    latitude : lat,
                }).where({user_id : user_id}).then();
            });
        });

        socket.on('closeQRCodePopUP',function(data){
            let user_id = data.user_id;
            io.to(user_id).emit('closeQRCodePopUP', {});
        });

        socket.on('paymentSuccess', function (data) {
            let service_id = data.service_id;
            Knex.select('*').from('service_request').where({"id": service_id }).then((serviceDetail) => {
                Knex.select('*').from('service_payment').where({ "service_id": service_id }).then((servicePayment) => {
                    Knex.select('*').from('services').where({ "id": serviceDetail['0'].service_id }).then((service) => {
                        Knex.select('*').from('users').where({ "id": serviceDetail['0'].user_id }).then((userDetail) => {
                            let returnObject = {
                                'serviceDetail': serviceDetail['0'],
                                'servicePayment': servicePayment['0'],
                                'service': service['0'],
                                'userDetail': userDetail['0']
                            };
                            io.to(serviceDetail['0'].expert_id).emit('paymentSuccess', returnObject);
                            PushNotificationService.sendPushNotification(Knex,serviceDetail['0'].expert_id,i18n.__('job_completed') +userDetail[0].fullname ,returnObject,"",'job_completion_notification');
                            PushNotificationService.sendMailNotification(Knex,serviceDetail['0'].expert_id,i18n.__('job_completed') +userDetail[0].fullname ,returnObject,"", nodemailerMailgun ,'job_completion_notification');
                            io.to('amit').emit('paymentSuccess', returnObject);
                        })
                    });
                });
            });
        });
    });

    function DisplayCurrentTime() {
        var date = new Date();
        var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        time = hours + ":" + minutes;
        return time;
    }
};
