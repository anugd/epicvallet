/**
 * Cron
 *
 * @author      :: Amit Sharma
 * @module      :: Service
 * @description :: TODO: You might write a short summary of how this service works and what it represents here.
 * @email       :: samit.sdei@gmail.com
 *
 */

const cron = require('cron').CronJob;
const PushNotificationService = require('./pushNotificationService');

module.exports = {
	scheduledServiceRuns: (io, Knex,i18n) => {
		let job = new cron({
	        cronTime: '*/30 * * * * *',
	        onTick: () => {
	        	Knex('service_request').select(['service_request.*','services.service','services.price'])
	        	.where({'schedule' : '1', 'service_request.status' : '0'})
	        	.leftJoin('services', 'services.id', 'service_request.service_id')
	        	.then((data) =>{
	                if(data.length){
	                    data.map((da, i) =>{
							var mySQLDate = da.booking_date.toString();
							var date = new Date(Date.parse(mySQLDate.replace('-', '/', 'g')));
							if (date <= new Date()){
								var update_declined_experts = [da.user_id];
                                var latitude = da.latitude;
                                var logitude = da.longitude;
                                Knex.select('user_addresses.id as useraddr_id', 'user_addresses.user_id as expert_id','current_latitude','current_longitude',
								Knex.raw("SQRT(POW(69.1 * (current_latitude - " + latitude + "), 2) + POW(69.1 * (" + logitude + " - current_longitude) * COS(current_latitude / 57.3), 2)) As distance"))
                            	.from('user_addresses')
                            	.leftJoin('users', 'users.id', 'user_addresses.user_id')
                            	.leftJoin('user_services', 'user_services.user_id', 'user_addresses.user_id')
								.leftJoin('user_weekly_availability', 'user_weekly_availability.user_id', 'user_addresses.user_id')
                            	.where({'user_services.service_id' : da.service_id,'users.user_type_id':2,'users.shuftipro_verified':1,'users.available':1})                            	
								.where({'user_weekly_availability.day': days[getday.getDay()] })
								.andWhere('user_weekly_availability.startTime' ,'<' , gettime)
								.andWhere('user_weekly_availability.endTime' ,'>' , gettime)
                            	.having('distance', '<', 25)
    							.orderBy('distance', 'asc').limit(1).then((respexpert) =>{
                                	if(respexpert.length){
                                        var newexpert = respexpert[0].expert_id;
                                        update_declined_experts.push(newexpert);
                                        let requestSenTime = new Date();
                                        Knex('service_request').where({'id': da.id}).update({declined_experts : JSON.stringify(update_declined_experts), request_sent_date_time : requestSenTime}).then();
                                        	Knex('users').where({'id' : da.user_id}).then((userDetail) =>{
                                        		let responseObject = {
                                    				category: da.service,
                                    				price: da.price,
                                    				address : da.address,
                                    				requestId: da.id,
                                    				name : userDetail[0].fullname,
                                    				latitude : latitude,
													longitude: logitude,
                                    				customer_id : da.user_id,
                                    				requestSenTime : requestSenTime
												};
												io.to(newexpert).emit('sendRequest', responseObject);
												io.to('amit').emit('sendRequest', responseObject);
                                    			PushNotificationService.sendPushNotification(Knex,newexpert,i18n.__('new_job_request')+userDetail[0].fullname,responseObject, 1);
                                        	});
                                   }else{
										Knex('service_request').update({ status: 4, cancel_reason : new Date()}).where({id : da.id}).then();
                                       	io.to(data.user_id).emit('noExpert');
                                       	io.to('amit').emit('noExpert');
                                       	PushNotificationService.sendPushNotification(Knex,data.user_id,i18n.__('no_expert_found'),{},"",'booking_declined');
				                        PushNotificationService.sendMailNotification(Knex,data.user_id,i18n.__('no_expert_found'),{},nodemailerMailgun,'booking_declined');
                                    }
                                }).catch((err) =>{
                                	console.log(err);
                                })
                            }
	                    });
	                }
	            });
	        },
	        start: false,
	    });
	    job.start();
	    console.log('cron run', job.running);
	},
	onGoingServiceThirty: (io, Knex,i18n) => {
		let job = new cron({
	        cronTime: '*/5 * * * * *',
	        onTick: () => {
	        	Knex('service_request').select(['service_request.*','services.service','services.price'])
	        	.where('service_request.status','0')
	        	.leftJoin('services', 'services.id', 'service_request.service_id')
	        	.then((data) =>{
	                if(data.length){
	                    data.map((da, i) =>{
							var mySQLDate = da.booking_date.toString();
							var date = new Date(Date.parse(mySQLDate.replace('-', '/', 'g')));
							if (date <= new Date()) {
								var startDate = new Date(da.request_sent_date_time);
								var endDate   = new Date();
								var seconds = (endDate.getTime() - startDate.getTime()) / 1000;
								if (seconds > 30){
									var service = da.id;
									var update_declined_experts = [];
									if(da.declined_experts){
										var update_declined_experts = JSON.parse(da.declined_experts);
									}
									var lastExpert = update_declined_experts[update_declined_experts.length -1];
									if(lastExpert){
										io.to(lastExpert).emit('closePopup', {});
										io.to('amit').emit('closePopup', {});
									}
									var latitude = da.latitude;
									var longitude = da.longitude;
									Knex.select('user_addresses.id as useraddr_id', 'user_addresses.user_id as expert_id','current_latitude','current_longitude',
									Knex.raw("SQRT(POW(69.1 * (current_latitude - " + latitude + "), 2) + POW(69.1 * (" + longitude + " - current_longitude) * COS(current_latitude / 57.3), 2)) As distance"))
									.from('user_addresses')
									.leftJoin('users', 'users.id', 'user_addresses.user_id')
									.leftJoin('user_services', 'user_services.user_id', 'user_addresses.user_id')
									.where({'user_services.service_id' : da.service_id,'users.user_type_id':2,'users.shuftipro_verified':1,'users.available':1})
									.whereNotIn('users.id', update_declined_experts)
									.having('distance', '<', 25)
									.orderBy('distance', 'asc').limit(1).then((respexpert) =>{
										if(respexpert.length){
											var newexpert = respexpert[0].expert_id;
											update_declined_experts.push(newexpert);
											let requestSenTime = new Date();
											Knex('service_request').update({declined_experts : JSON.stringify(update_declined_experts),request_sent_date_time : requestSenTime}).where({id : service}).then();
											Knex('users').where({id: da.user_id}).then((userDetail) =>{
												let responseObject = {
													category: da.service,
													price: da.price,
													requestId: service,
													name : userDetail[0].fullname,
													address : da.address,
													latitude : latitude,
													longitude : longitude,
													customer_id : da.user_id,
													requestSenTime : requestSenTime
												};
												io.to(newexpert).emit('sendRequest', responseObject);
												io.to('amit').emit('sendRequest', responseObject);
												PushNotificationService.sendPushNotification(Knex,newexpert,i18n.__('new_job_request')+userDetail[0].fullname,responseObject,1);
											});
										}else{
											io.to(da.user_id).emit('noExpert');
											io.to('amit').emit('noExpert');
											Knex('service_request').update({status : 4})
											.where({id : service}).then();
										}
									}).catch((err) =>{
										console.log(err);
										// io.to(da.user_id).emit('noExpert');
										// io.to('amit').emit('noExpert');
										// PushNotificationService.sendPushNotification(Knex,da.user_id,'No Expert found for your job',{});
										// PushNotificationService.sendMailNotification(Knex,da.user_id,'No Expert found for your job',{});
									});
								}
							}
	                    });
	                }
	            });
	        },
	        start: false,
	    });
	    job.start();
	    console.log('cron run', job.running);
	},

	cancelServieIfTimeLappsed: (io, Knex,i18n) => {
        let job = new cron({
            cronTime: '0 */2 * * * *',
            onTick: () => {
                Knex.select('*').from('service_request').where({status: 1}).then((data) =>{
                    if(data.length){
                        data.map((da, i) =>{
                            var startDate = new Date(da.booking_date);
                            var endDate   = new Date();
                            var newDateObj = new Date(endDate.getTime() + 10 * 60000);
                            var seconds = (endDate.getTime() - startDate.getTime()) / 1000;
                            if (seconds > 1800){
	                            delete da.declined_experts;
	                            delete da.declined_experts;
	                            delete da.created_at;
	                            delete da.updated_at;
	                            delete da.request_sent_date_time;
	                            da.schedule = 1;
	                            da.status = 0;
	                            da.booking_date = newDateObj;
                                Knex('service_request').update({status : 4}).where({id : da.id}).then();
                                delete da.id;
                                Knex.insert(da).into('service_request').then();
                                io.to(da.user_id).emit('jobReschedule', da);
                                io.to('amit').emit('jobReschedule', da);
                                PushNotificationService.sendPushNotification(Knex,da.user_id,i18n.__('job_not_completed'),da);
                            }
                        })
                    }
                });
            },
            start: false,
        });
        job.start();
        console.log('cron run', job.running);
    },
    test: (io, Knex,i18n) => {
		let job = new cron({
	        cronTime: '*/5 * * * * *',
	        onTick: () => {
				// var a = io.to('558').emit('sendRequest', {'asd' : 'asd'});
				// console.log(a);
	        },
	        start: false,
	    });
	    job.start();
	    console.log('cron run', job.running);
	}

};
