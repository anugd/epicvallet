/**
* PushNotificationService
*
* @author      :: Amit Sharma
* @module      :: Service
* @description :: TODO: You might write a short summary of how this service works and what it represents here.
* @email       :: samit.sdei@gmail.com
*
*/

var OneSignal = require('onesignal-node');

module.exports = {
    sendPushNotification: (Knex, userId, message, resData, addButton,type) => {
        Knex.select('*').from('users').where('id',userId).limit(1).then((data) =>{

            if(data[0].player_id)
            {
                let playerIds = [data[0].player_id];
                var appAuthKey = 'OTM1Mjk5ZTctNThmZS00MmNmLWE1MjQtMTZhNTE5YzEwMDI4';
                var app = {
                    appAuthKey: 'OTM1Mjk5ZTctNThmZS00MmNmLWE1MjQtMTZhNTE5YzEwMDI4',
                    appId: '8396a643-0729-4b4b-b285-4a27cb6ed8ad'
                }
                if(JSON.parse(data[0].user_type_id == '1')){
                    var appAuthKey = 'ZjA1OTMwYmUtYTdkMy00OGVlLThjNmItYTU0Yjc0ODliMTcx';
                    app = {
                        appAuthKey: 'ZjA1OTMwYmUtYTdkMy00OGVlLThjNmItYTU0Yjc0ODliMTcx',
                        appId: '3b2cff23-7d6c-492d-93cd-a3739f6c6663'
                    }
                }
                var myClient = new OneSignal.Client({
                    userAuthKey: appAuthKey,
                    app: app
                });
                var notificationObjectCreate = {
                    contents: {
                        en: message,
                    },
                    headings: { "en": "Helajob Notification" },
                    include_player_ids: playerIds,
                    data: resData,
                    priority: 10,
                    small_icon : 'icon'
                };
                if (addButton){
                    notificationObjectCreate.buttons = [
                        { "id": "accept", "text": "Accept", "icon": "ic_menu_share" },
                        { "id": "reject", "text": "Cancel", "icon": "ic_menu_send" }
                    ]
                }
                var notificationObject = new OneSignal.Notification(notificationObjectCreate);
                if(type != undefined)
                {
                    console.log(type);
                    Knex.select('*').from('notifications').where('notification_for',type).where('type',2).limit(1).then((notirecord) =>{
                        if(notirecord[0])
                        {
                            if(notirecord[0].status == 1){
                                myClient.sendNotification(notificationObject, function (err, httpResponse,data) {
                                    if(err){
                                        console.log('Something went wrong...');
                                    }else{
                                        console.log(data);
                                    }
                                });
                            }
                        }else{
                            myClient.sendNotification(notificationObject, function (err, httpResponse,data) {
                                if(err){
                                    console.log('Something went wrong...');
                                }else{
                                    console.log(data);
                                }
                            });
                        }
                    });
                }else{
                    myClient.sendNotification(notificationObject, function (err, httpResponse,data) {
                        if(err){
                            console.log('Something went wrong...');
                        }else{
                            console.log(data);
                        }
                    });
                }
            }
        });
    },
    sendMailNotification: (Knex, userId, message, resData,nodemailerMailgun,type) => {

        Knex.select('*').from('users').where('users.id',userId).limit(1).then((userrcd) =>{
            if(userrcd[0] && type != undefined){

                Knex.select('*').from('notifications').where('notification_for',type).where('type',1).limit(1).then((notirecord) =>{
                    let mailOptions = {
                        from: "samit.sdei@gmail.com",
                        to: userrcd[0].email,
                        subject: 'Booking Confirmed',
                        html: '<p>'+"message"+'</p>'
                    };
                    if(notirecord[0])
                    {
                        if(notirecord[0].status == 1){
                            nodemailerMailgun.sendMail(mailOptions, (err, info) => {
                                if (err) {
                                    console.log('Error: ', err);
                                } else {
                                    console.log('Response: ', info);
                                }
                            });
                        }
                    }else{
                        nodemailerMailgun.sendMail(mailOptions, (err, info) => {
                            if (err) {
                                console.log('Error: ', err);
                            } else {
                                console.log('Response: ', info);
                            }
                        });
                    }
                });
            }
        });
    }
};
