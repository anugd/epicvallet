var socket = require( 'socket.io' );
var express = require('express');
var i18n = require("i18n");

var nodemailer  = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');



const cron = require('cron').CronJob;
var options = {
    client: 'mysql',
    connection: {
        host: '34.211.31.84',
        user: 'holajob',
        password: 'holajob',
        database: 'db_holajob',
        charset  : 'utf8'
    }
};

let optionsmail = {
        "host": "smtp.gmail.com",
        "port": 587,
        "auth": {
            "user": "samit.sdei@gmail.com",
            "pass": "amit@123as!!"
        },
        debug: true
}


i18n.configure({
    locales:['en', 'de'],
    directory: __dirname + '/node_services/locales'
});

i18n.setLocale('en');

var Knex = require('knex')(options);
var app = express();
var server = require('http').createServer(app);
var io = socket.listen( server );
var port = process.env.PORT || 3001;
let nodemailerMailgun = nodemailer.createTransport(optionsmail);


var request = require('request');
require('./node_services/socketService')(io, Knex, i18n , nodemailerMailgun);
/* Cron job */
const jobs = require('./node_services/cronService');
jobs.scheduledServiceRuns(io, Knex, i18n);
jobs.onGoingServiceThirty(io, Knex, i18n);
jobs.cancelServieIfTimeLappsed(io, Knex, i18n);
// jobs.test(io, Knex);

/**
    Start Server
*/
server.listen(port, function () {
    console.log('Server listening at port %d', port);
});
