<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'App\Http\Controllers\Auth\LoginController@loginauth');
Route::get('/login', 'App\Http\Controllers\Auth\LoginController@loginauth');

Route::get('/socket', function () {
    return view('common.socket');
});

Route::get('email/verify/{token}', "App\Http\Controllers\UserController@verifyemail");

Route::get('otp-verification', 'App\Module\Controller\WebDashboardController@otp')->name('otp-verification');
Route::post('validate-otp', 'App\Module\Controller\WebDashboardController@validateOtp')->name('validate-otp');

Route::get('congratulation', 'App\Module\Controller\WebDashboardController@congratulation')->name('congratulation');

Route::get('KYC', 'App\Module\Controller\WebDashboardController@KYC')->name('KYC');

Route::get('/dashboard', 'App\Module\Controller\WebDashboardController@dashboard');
Route::get('/expert-dashboard', 'App\Module\Controller\WebDashboardController@expertDashboard')->name('expert-dashboard');

Route::get('/expert-register-home', 'App\Http\Controllers\webExpert\Registration@index');
Route::get('/invoice/download', 'App\Http\Controllers\InvoiceController@download');

Route::get('/test/one', 'App\Http\Controllers\Auth\LoginController@one');

Route::group([ 'middleware' => 'auth.user'], function () {
    
    Route::get('/service/servicesHistoryDetailExpert', 'App\Http\Controllers\ServiceController@servicesHistoryDetailExpert')->name('service.servicesHistoryDetailExpert');

    Route::get('/service/servicesHistoryDetailCustomer', 'App\Http\Controllers\ServiceController@servicesHistoryDetailCustomer')->name('service.servicesHistoryDetailCustomer');

    Route::get('/expert/servicehistory', 'App\Http\Controllers\ServiceController@servicesHistoryData')->name('expert.servicehistory');


    Route::resource('/service-history', 'App\Http\Controllers\ServiceController');
    Route::resource('/account', 'App\Http\Controllers\AccountController');
    Route::resource('/wallet', 'App\Http\Controllers\WalletController');

    Route::get('/payouts', 'App\Http\Controllers\PayoutController@index');
    Route::get('/payouts/list', 'App\Http\Controllers\PayoutController@list')->name('payouts.list');
    Route::get('/payouts/payoutDetail', 'App\Http\Controllers\PayoutController@payoutDetail')->name('payouts.payoutDetail');

    Route::resource('/invoice', 'App\Http\Controllers\InvoiceController');
    Route::post('user/payoutRequest', 'App\Http\Controllers\UserController@payoutRequest')->name('user.payoutRequest');
    Route::get('/account/change-password/{type}', 'App\Http\Controllers\UserController@changepassword');
    Route::get('/address/{type}', 'App\Http\Controllers\UserController@address');
    Route::get('/user/validate-old-password', 'App\Http\Controllers\UserController@validateoldpassword')->name('/user/validate-old-password');
    Route::post('/user/update-password', 'App\Http\Controllers\UserController@updatepassword')->name('/user/update-password');
    Route::post('/user/update-address', 'App\Http\Controllers\UserController@updateaddress')->name('/user/update-address');
    Route::post('/user/update-home-address', 'App\Http\Controllers\UserController@updatehomeaddress')->name('/user/update-home-address');
    Route::post('/user/update-work-address', 'App\Http\Controllers\UserController@updateworkaddress')->name('/user/update-work-address');
    Route::get('/account/profile/{type}', 'App\Http\Controllers\UserController@profile');
    Route::get('/account/payments/addBankAccount', 'App\Http\Controllers\UserController@addBankAccount');
    Route::post('/account/payments/updateBankAccount', 'App\Http\Controllers\UserController@updateBankAccount')->name('/account/payments/updateBankAccount');
    
    
    Route::get('/expert/dashboard', 'App\Http\Controllers\UserController@dashboard');
    Route::post('user/primaryaddress', 'App\Http\Controllers\UserController@primaryaddress')->name('user.primaryaddress');


    Route::get('/expert/revenue', 'App\Http\Controllers\UserController@revenue');
    Route::get('/expert/revenueData', 'App\Http\Controllers\UserController@revenueData')->name('expert.revenueData');
    Route::get('/expert/revenueDetail', 'App\Http\Controllers\UserController@revenueDetail')->name('expert.revenueDetail');

    Route::get('/customer/profile', 'App\Http\Controllers\UserController@customerprofile');

    Route::get('/user/portfolio', 'App\Http\Controllers\UserController@portfolio');
    Route::get('/user/upload-document', 'App\Http\Controllers\UserController@document');
    Route::post('/user/submit-document', 'App\Http\Controllers\UserController@submitDocument')->name('/user/submit-document');;
    Route::post('/user/submit-basic-info', 'App\Http\Controllers\UserController@expertBasicInfo')->name('/user/submit-basic-info');;
    Route::post('/user/customer-basic-info', 'App\Http\Controllers\UserController@customerBasicInfo')->name('/user/customer-basic-info');;

    Route::post('/user/submit-other-info', 'App\Http\Controllers\UserController@expertOtherInfo')->name('/user/submit-other-info');;



    Route::get('/user/language', 'App\Http\Controllers\UserController@language')->name('user.language');
    Route::get('/user/skills', 'App\Http\Controllers\UserController@skills')->name('user.skills');

    Route::post('/wallet/transactions', 'App\Http\Controllers\WalletController@transactions')->name('/wallet/transactions');

    Route::get('/tickets/destroy/{id}', 'App\Http\Controllers\TicketController@destroy')->name('tickets.destroy');
    Route::get('/tickets/view/{id}', 'App\Http\Controllers\TicketController@view')->name('tickets.view');
    Route::post('/tickets/addReply', 'App\Http\Controllers\TicketController@addReply')->name('tickets.addReply');


    Route::resource('/tickets', 'App\Http\Controllers\TicketController');

});

Route::post('/expert-register', 'App\Http\Controllers\Expert\RegistrationController@register')->name('expert-register');

Route::post('/company-register', 'App\Http\Controllers\Company\RegistrationController@register')->name('company-register');

Route::post('resend-OTP', 'App\Http\Controllers\Auth\RegisterController@resendOTP');
Route::post('resend-OTP-email', 'App\Http\Controllers\Auth\RegisterController@resendOTPEmail');
Route::get('/home', 'App\Module\Controller\WebDashboardController@dashboard');


Route::post('/payoutwebhook', 'App\Http\Controllers\admin\PayoutController@PayoutWebhook');

Route::namespace('App\Http\Controllers')->group(function () {
    Auth::routes();
    Route::get('/login/{social}', 'Auth\LoginController@socialLogin')->where('social', 'twitter|facebook|linkedin|google|github|bitbucket');
    Route::get('/login/{social}/callback', 'Auth\LoginController@handleProviderCallback')->where('social', 'twitter|facebook|linkedin|google|github|bitbucket');
    //Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::post('/expert-login', 'Auth\LoginController@expertLogin')->name('expert-login');
    Route::post('stripe/add-card', "StripeController@createCard");
    Route::post('stripe/cards', "StripeController@getCard");
    Route::post('stripe/add-money', "StripeController@addMoney");
});

Route::get('user/validate-email/{id}', 'App\Module\Controller\UserController@validateEmail');
include(__DIR__.'/adminRoutes.php');
