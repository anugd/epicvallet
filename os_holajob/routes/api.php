<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
Route::prefix('v1')->group(function() {
    Route::post('logout', 'App\Module\Controller\UserController@logout');
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('user', 'App\Module\Controller\UserController@getAuthUser');
    });

    Route::prefix('user')->group(function() {
        Route::post('register', 'App\Module\Controller\UserController@registerUser');
        Route::post('forgot_password', 'App\Module\Controller\UserController@forgotPassword');
        Route::post('login', 'App\Module\Controller\LoginController@login');
        Route::post('updatelocationExpert', 'App\Module\Controller\ExpertRegistrationController@updatelocationExpert');
        Route::post('updatelocationCustomer', 'App\Module\Controller\ExpertRegistrationController@updatelocationCustomer');
        Route::post('addLocation', 'App\Module\Controller\UserController@addLocation');
        Route::post('listLocations', 'App\Module\Controller\UserController@listLocations');
        Route::post('deleteLocations', 'App\Module\Controller\UserController@deleteLocations');
        Route::group(['middleware' => 'jwt.auth'], function () {
           Route::post('register/validate-otp', 'App\Module\Controller\UserController@validateOtp');
           Route::post('resend_OTP', 'App\Module\Controller\UserController@resendOTP');
        });
    });

    Route::prefix('expert')->group(function() {
        Route::post('register', 'App\Module\Controller\ExpertRegistrationController@register');
        Route::post('social-register','App\Module\Controller\ExpertRegistrationController@socialRegister');
        Route::group(['middleware' => 'jwt.auth'], function () {
            Route::post('save-other-information', 'App\Module\Controller\ExpertRegistrationController@storeOtherInformation');
            Route::post('upload-profile-image', 'App\Module\Controller\ExpertRegistrationController@uploadProfileImage');
            Route::post('save-profile-image', 'App\Module\Controller\ExpertRegistrationController@storeProfileImage');
            Route::post('upload-document', 'App\Module\Controller\ExpertRegistrationController@uploadIdentityDocument');
            Route::post('save-document', 'App\Module\Controller\ExpertRegistrationController@storeIdentityDocument');
            // Update user details
            Route::post('set-availablity', 'App\Module\Controller\ExpertUpdateController@setAvailablity');
            Route::post('setNotification', 'App\Module\Controller\ExpertUpdateController@setNotification');
            Route::post('getNotification', 'App\Module\Controller\ExpertUpdateController@getNotification');
            Route::post('update-document', 'App\Module\Controller\ExpertUpdateController@updateIdentityDocument');
            Route::post('update-profile-image', 'App\Module\Controller\ExpertUpdateController@updateProfileImage');
            Route::post('update-other-information', 'App\Module\Controller\ExpertUpdateController@updateOtherInformation');
            Route::post('update-basic-information', 'App\Module\Controller\ExpertUpdateController@updateBasicInformation');
        });
    });
    //email verification
    Route::get('user/email-verification/{code}', 'App\Module\Controller\ServiceController@find');
    //Service Resource
    Route::get('service', 'App\Module\Controller\ServiceController@find');
    Route::post('service', 'App\Module\Controller\ServiceController@create');
    Route::post('service/image', 'App\Module\Controller\ServiceController@storeImage');
    Route::put('service', 'App\Module\Controller\ServiceController@update');
    Route::put('service/deleteAll', 'App\Module\Controller\ServiceController@deleteBulk');
    Route::delete('service/{id}', 'App\Module\Controller\ServiceController@delete');
    //All Languages
    Route::get('languages', 'App\Module\Controller\LanguagesController@show');
    Route::post('service/request', 'App\Module\Controller\ServiceRequestController@request');

    /* Amit Sharma */
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('updateDeviceToken', 'App\Module\Controller\UserController@updateDeviceToken');
        Route::post('listChat', 'App\Module\Controller\ChatController@listChat');
        Route::post('getChatID', 'App\Module\Controller\ChatController@getChatID');
        Route::post('updateMessageStatus', 'App\Module\Controller\ChatController@updateMessageStatus');
        Route::post('listMessages', 'App\Module\Controller\ChatController@listMessages');
        Route::post('uploadChatImage', 'App\Module\Controller\ChatController@uploadChatImage');
        Route::post('service/cancelService','App\Module\Controller\ServiceRequestController@cancelService');
        Route::post('service/requestQRForServiceStart','App\Module\Controller\ServiceRequestController@requestQRForServiceStart');
        Route::post('service/verfiyQRForServiceStart','App\Module\Controller\ServiceRequestController@verfiyQRForServiceStart');
        Route::post('service/requestQRForServiceBreakStart','App\Module\Controller\ServiceRequestController@requestQRForServiceBreakStart');
        Route::post('service/verfiyQRForServiceBreakStart','App\Module\Controller\ServiceRequestController@verfiyQRForServiceBreakStart');
        Route::post('service/requestQRForServiceBreakEnd','App\Module\Controller\ServiceRequestController@requestQRForServiceBreakEnd');
        Route::post('service/verfiyQRForServiceBreakEnd','App\Module\Controller\ServiceRequestController@verfiyQRForServiceBreakEnd');
        Route::post('service/requestQRForServiceEnd','App\Module\Controller\ServiceRequestController@requestQRForServiceEnd');
        Route::post('service/verfiyQRForServiceEnd','App\Module\Controller\ServiceRequestController@verfiyQRForServiceEnd');
        Route::post('service/startService', 'App\Module\Controller\ServiceRequestController@startService');
        Route::post('scheduleOngoingListing', 'App\Module\Controller\ServiceRequestController@scheduleOngoingListing');
        Route::post('servicesListingCustomer', 'App\Module\Controller\ServiceRequestController@servicesListingCustomer');
        Route::post('listExpertInvoice', 'App\Module\Controller\ServiceRequestController@listExpertInvoice');
        Route::post('listCustomerInvoice', 'App\Module\Controller\ServiceRequestController@listCustomerInvoice');
        Route::post('getUserDetail', 'App\Module\Controller\UserController@getUserDetail');
        Route::post('getExpertDetail', 'App\Module\Controller\UserController@getExpertDetail');
        Route::post('getUserListing', 'App\Module\Controller\UserController@getUserListing');
        Route::post('shuftiProKYC', 'App\Module\Controller\UserController@shuftiProKYC');
        Route::post('ShuftiProStatus', 'App\Module\Controller\UserController@ShuftiProStatus');
        Route::post('listPayoutsForExpert', 'App\Module\Controller\ServiceRequestController@listPayoutsForExpert');
        Route::post('phoneEmailVerifiedStatus', 'App\Module\Controller\UserController@phoneEmailVerifiedStatus');
        Route::post('checkCreditCard', 'App\Module\Controller\UserController@checkCreditCard');
        Route::post('verifyCouponCode', 'App\Module\Controller\UserController@verifyCouponCode');
        Route::post('applyCouponCode', 'App\Module\Controller\UserController@applyCouponCode');
        Route::post('addMoneyToWallet', 'App\Http\Controllers\StripeController@addMoney');
        Route::post('listWalletAmount', 'App\Http\Controllers\StripeController@listWalletAmount');
        Route::post('requestPayout', 'App\Module\Controller\ServiceRequestController@requestPayout');
        Route::post('listPayoutsTransection', 'App\Module\Controller\ServiceRequestController@listPayoutsTransection');
        Route::post('listCustomerTransection', 'App\Module\Controller\ServiceRequestController@listCustomerTransection');
        Route::post('listExpertHistoryForCustomer', 'App\Module\Controller\ServiceRequestController@listExpertHistoryForCustomer');
        Route::post('serviceRating', 'App\Module\Controller\ServiceRequestController@serviceRating');
        Route::post('listReviews', 'App\Module\Controller\ServiceRequestController@listReviews');
        Route::post('finishJobCustomer', 'App\Module\Controller\ServiceRequestController@finishJobCustomer');
        Route::post('makePaymentForService', 'App\Module\Controller\ServiceRequestController@makePaymentForService');
        Route::post('updateCustomerProfile', 'App\Module\Controller\UserController@updateCustomerProfile');
        Route::post('serviceReport', 'App\Module\Controller\ServiceRequestController@serviceReport');
        Route::post('deleteAccount', 'App\Module\Controller\UserController@deleteAccount');
        Route::post('listWeeklyAvailablity', 'App\Module\Controller\UserController@listWeeklyAvailablity');
        Route::post('saveWeeklyAvailablity', 'App\Module\Controller\UserController@saveWeeklyAvailablity');
        Route::post('deductJobPriceAndAddToWallet', 'App\Module\Controller\ServiceRequestController@deductJobPriceAndAddToWallet');
        Route::post('listServicesHistoryExeprt', 'App\Module\Controller\ServiceRequestController@listServicesHistoryExeprt');
        Route::post('reshceduleService', 'App\Module\Controller\ServiceRequestController@reshceduleService');
        Route::post('listFAQ', 'App\Module\Controller\UserController@listFAQ');
        Route::post('getFAQCategory', 'App\Module\Controller\UserController@getFAQCategory');
        Route::post('cmsPage', 'App\Module\Controller\UserController@cmsPage');
        Route::post('updateZendeskID', 'App\Module\Controller\UserController@updateZendeskID');
        Route::post('expertServiceStatusChange', 'App\Module\Controller\ServiceRequestController@expertServiceStatusChange');
        Route::post('saveNotificationSound', 'App\Module\Controller\UserController@saveNotificationSound');
        Route::post('inviteCoupon', 'App\Module\Controller\UserController@inviteCoupon');
    });
    Route::post('addNewBankAccount', 'App\Module\Controller\UserController@addNewBankAccount');
    Route::post('stripeKYCWebhook', 'App\Module\Controller\UserController@stripeKYCWebhook');
    Route::post('shuftiProAddress', 'App\Module\Controller\UserController@shuftiProAddress');
    Route::post('test', 'App\Module\Controller\UserController@test');
    /* Amit Sharma */

    //Stripe API
    Route::post('stripe/KYC', "App\Http\Controllers\StripeController@createAccount");
    Route::post('stripe/updateStripeBankAccount', "App\Http\Controllers\StripeController@updateStripeBankAccount");
    Route::post('stripe/add-card', "App\Http\Controllers\StripeController@createCard");
    Route::post('stripe/cards', "App\Http\Controllers\StripeController@getCard");
    Route::post('social-login', "App\Module\Controller\UserController@authSocial");
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('user/login', 'App\Module\Controller\LoginController@login');
Route::middleware('jwt.auth')->get('users', function(Request $request) {
    return auth()->user();
});
