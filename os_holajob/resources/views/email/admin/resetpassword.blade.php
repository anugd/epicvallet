
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <title>{{ $title }}</title>
    <style type="text/css">
        body {
            padding: 0;
            margin: 0;
            background-color: #f5f5f4;
            font-family: arial;
            font-size: 13px;
        }

        table {
            border-collapse: collapse !important;
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        td {
            font-family: arial, sans-serif;
            color: #010101;
        }

        img {
            border: 0!important;
        }
        h1, h2, h3, h4, h5, h6, p {
            margin: 0;
            padding: 0;
            border: 0;
        }
        .content {
            padding-left: 50px;
            padding-right: 50px;
        }
        .footer-content {
            padding-left: 60px;
            padding-right: 60px;
        }
        .share_links {
            padding-right: 40px;
        }

        @media all and (max-width: 767px) {
            body,
            table,
            td,
            p,
            a,
            li,
            blockquote {
                -webkit-text-size-adjust: none !important;
            }
            a {
                white-space: nowrap;
            }
            table [class="contenttable"],
            .contenttable {
                width: 100% !important;
            }
            .responsive-image img {
                height: auto !important;
                max-width: 100% !important;
            }
            img {
                max-width: 100% !important;
                height: auto;
            }
            .mob-hidden {
                display: none;
            }
            /*---Content----*/
            .content, .footer-content {
                padding-left: 15px !important;
                padding-right: 15px !important;
            }
            .Share_Links a:not(:last-child) {
                margin-right: 15px;
            }
            td.mail_heading {
                font-size: 30px !important;
                padding-bottom: 20px !important;
            }

        }
    </style>
</head>

<body text="#706c72" link="#706c72" vlink="#706c72" alink="#706c72" marginheight="0" marginwidth="0" bgcolor="#f3f3f3" style="background-color:#f3f3f3; color:#706c72;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#f3f3f3" style="background-color:#f3f3f3; color:#706c72; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" align="center">
        <tbody>
            <tr>
                <td style="height: 20px;" class="mob-hidden">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#fff; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; font-family: Arial, Helvetica, sans-serif;" align="center" class="contenttable">
                        <tbody>
                            <tr>
                                <td class="content">
                                     <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#fff; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; font-family: Arial, Helvetica, sans-serif;" align="center" class="contenttable">
                                        <tr>
                                            <td align="center" valign="middle" style="padding-top: 40px; padding-bottom: 40px;">
                                                <a href="" style="display: inline-block;"><img src="{{ url('/img/logo.png') }}" title="holajob" alt="logo"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="mail_heading" style="font-size: 40px; font-weight: bold; padding-bottom: 40px;">{{ $title }}</td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="Message" style="padding-bottom: 40px;">
                                                <p style="color: #5c5c5c; line-height: 30px; font-size: 20px;">Hey <span class="UserName">{{ $first_name }},</span></p>
                                                <p style="color: #5c5c5c; line-height: 30px; font-size: 20px;">It looks like you requested a new password.

                                                    If that sounds right, you can enter new password by clicking on the button below.</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding-bottom: 50px;">
                                                <a href="{{ $url }}" style="background-color: #6B7AB4;color: #fff;text-decoration: none;padding: 15px 50px;    font-size: 20px;    text-transform: uppercase;" title="Reset Password">Reset Password</a>
                                            </td>
                                        </tr>
                                     </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 20px;" class="mob-hidden">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</body>
