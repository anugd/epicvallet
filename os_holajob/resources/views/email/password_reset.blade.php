<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>HelaJob</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body style="padding: 10px; background-color: #e6e6e6;">
        <table style="width: 100%; margin: 0; padding: 0;" border="0">
            <tr>
                <td style="background-color: #ffffff; border-bottom: 1px solid #e6e6e6; padding: 20px 10px;">
                    <span style="color: #333; font-size: 25px;">HelaJob</span>
                </td>
            </tr>
            <tr>
                <td style="background-color: #ffffff; border-bottom: 1px solid #e6e6e6; padding: 20px 10px; font-size: 17px;">
                    Hello {{ $data['username'] }},<br/><br/>

                    Click on Below link to reset your password :- <br/>
                    <b><a href="{{ Url('/password/reset/'.$data['brokerToken']) }}" style="background-color: #6B7AB4;color: #fff;text-decoration: none;padding: 15px 50px;font-size: 20px;text-transform: uppercase;"> Reset <a> </b>
                    <br/>
                    Thanks!
                </td>
            </tr>
            <tr>
                <td style="background-color: #ffffff; border-bottom: 1px solid #e6e6e6; padding: 20px 10px; text-align: center;">
                    <span style="font-size: 15px;">Copyright &copy; 2019</span>
                </td>
            </tr>
        </table>
    </body>
</html>
