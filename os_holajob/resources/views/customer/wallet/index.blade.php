@extends('layouts.app')

@section('content')
<div class="tab-pane" id="Mywallet">
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header bg-white pr-md-0">
                                <h4>{{ trans('messages.hela_job_wallet') }}</h4>
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" id="user_id">
                                <div class="text-primary h3 mt-2">
                                    £ {{ !is_null($balance) && !is_null($balance[0]->balance)?number_format($balance[0]->balance) : 0 }}
                                </div>
                                <div class="add-money-button-wrap">
                                    <a href="#" class="btn-add-money"><i class="fa fa-plus-circle"></i>{{ trans('messages.add_money') }}</a>
                                </div>
                                <div class="row pt-3 pb-2 no-gutters">
                                    <div class="col d-flex align-items-center justify-content-around card-header-form">
                                        <div class="filter-label text-primary">{{ trans('messages.from') }}</div>
                                        <div class="date">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" autocomplete="off" onkeydown="return false" class="form-control transaction-filter-start"  required id="tansaction_start" name="tansaction_start" />
                                                    <span class="input-group-addon calendar-addon"><i class="fa fa-calendar" aria-hidden="true"></i> </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col d-flex align-items-center justify-content-around">
                                        <div class="text-primary filter-label">{{ trans('messages.to') }}</div>
                                        <div class="date">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" autocomplete="off" onkeydown="return false" class="form-control transaction-filter-end"  required id="tansaction_end" name="tansaction_end" />
                                                    <span class="input-group-addon calendar-addon"><i class="fa fa-calendar" aria-hidden="true"></i> </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col filter-label text-right">
                                        <input type="button" class="btn btn-filter btn-outline-primary dateTransactionSearch" value="Filter">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-body--account pr-md-0">
                                <div class="table-responsive">
                                    <table class="table mb-0" id="transactions-table">
                                        <thead>
                                            <th>{{ trans('messages.date') }}</th>
                                            <th>{{ trans('messages.description') }}</th>
                                            <th>{{ trans('messages.amount') }}</th>
                                            <!-- <th>{{ trans('messages.type') }}</th> -->
                                        </thead>
                                        <tbody>
                                            @php ($status = [1 => 'Credited', 2 => 'Debited' ])
                                            @foreach($transactions as $transaction)
                                            <tr>
                                                <td>{{ date("d/m/y",strtotime($transaction->created_at)) }}</td>
                                                <td>{{$transaction->description}}</td>
                                                <td>&pound;{{number_format($transaction->amount)}}</td>
                                                <!-- <td>{{$status[$transaction->flag]}}</td> -->
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 add-money-holder hela-job-wallet">
                        <div class="card add-money-to-wallet">
                            <form id="addmoney_form">
                                <div class="card-header bg-white border-bottom-0 pb-0 pl-md-0">
                                    <div class="alert alert-success-money"></div>
                                    <h4 class="pt-4 pb-4">{{ trans('messages.add_money_wallet') }}</h4>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="amount" name="amount" placeholder="{{ trans('messages.amount_placeholder') }}" maxlength="6">
                                            <span class="input-group-addon calendar-addon"><i class="fa fa-pound-sign" aria-hidden="true"></i> </span>
                                        </div>
                                        <span class="invalid-feedback amount_err" role="alert" style="display: none" >
                                            <strong>{{ trans('messages.amount_required') }}</strong>
                                        </span>
                                    </div>
                                    <div class="saved-card">
                                        <h6>{{ trans('messages.already_saved_card') }}</h6>
                                        <div class="card-list"></div>
                                        <span class="invalid-feedback card_id_err" role="alert" style="display: none" >
                                            <strong>{{ trans('messages.card_required') }}</strong>
                                        </span>
                                        <div class="col-md-12 add-wallet-money">
                                            <button type="button" class="btn btn-md btn-primary" onclick="addMoney()">{{ trans('messages.add_money') }}</button>
                                            <div id="wait-money" class="loader" style="display:none;"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </form>
                            <div class="card-body pl-md-0">
                                <div class="add-card">
                                    <div class="alert alert-success-card"></div>
                                    <div class="alert alert-danger-card"></div>
                                    <h6 class="mb-3">{{ trans('messages.add_card_label') }}</h6><hr />
                                    <form id="addcard_form">
                                        <div class="form-group">
                                            <label for="carddetails">{{ trans('messages.card_number') }}</label>
                                            <input type="number" class="form-control" id="card_no" name="card_no" placeholder="{{ trans('messages.card_number') }}" maxlength="16">
                                            <span class="invalid-feedback card_err" role="alert" style="display: none" >
                                                <strong>{{ trans('messages.card_number_required') }}</strong>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-12" for="expirydate">{{ trans('messages.expiry_date') }}</label>
                                                <div class="col-6">
                                                    <input type="number" class="form-control" id="card_month" name="card_month" placeholder="MM">
                                                    <span class="invalid-feedback month_err" role="alert" style="display: none" >
                                                        <strong>{{ trans('messages.card_month_required') }}</strong>
                                                    </span>
                                                </div>
                                                <div class="col-6">
                                                    <input type="number" class="form-control" id="card_year" name="card_year" placeholder="YYYY">
                                                    <span class="invalid-feedback year_err" role="alert" style="display: none" >
                                                        <strong>{{ trans('messages.card_year_required') }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="cvv">{{ trans('messages.cvv') }}</label>
                                                    <input type="text" class="form-control" id="card_cvv" name="card_cvv" placeholder={{ trans('messages.cvv_placeholder') }} maxlength="3">
                                                    <span class="invalid-feedback cvv_err" role="alert" style="display: none" >
                                                        <strong>{{ trans('messages.cvv_required') }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group pl-4">
                                            <input type="checkbox" checked class="form-control form-control form-check-input form-check-input-custom" id="set_primary" placeholder="{{ trans('messages.set_primary') }}" name="set_primary" value="1">
                                            <label for="set_primary">{{ trans('messages.set_primary') }} </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-md btn-primary" onclick="addCard()">{{ trans('messages.add_card_button') }}</button>
                                        <div id="wait-card" class="loader" style="display:none;"></div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>
</div>
<script type="text/javascript" src="{{ URL::asset('js/wallet.js') }}"></script>
<script>
$(function() {
    transactiontable = $('#transactions-table').DataTable({
        "language": {
            "search": ""
        },
        "bInfo" : false,
        "bLengthChange": false,
        "order": [[ 0, "desc" ]]
    });

    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var min = $('.transaction-filter-start').val();
            var max = $('.transaction-filter-end').val();
            var date = data[0] || 0;
            if(min != ""  && max != ""){
                if (new Date(date) >= new Date(min) &&  new Date(date) <= new Date(max)){
                    return true;
                }
                return false;
            }else if (min != ""  && max == "") {
                if (new Date(date) >= new Date(min)){
                    return true;
                }
                return false;
            }else if (min == ""  && max != "") {
                if (new Date(date) <= new Date(max)){
                    return true;
                }
                return false;
            }else{
                return true;
            }
        }
    );

    $('#transactions-table_filter input').addClass('custom-form-control');
    $('#transactions-table_filter input').attr('placeholder', "Search");

    $('.transaction-filter-start').datepicker({
        format: 'yyyy-mm-dd',
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.transaction-filter-end').datepicker('setStartDate', minDate);
    });

    $('.transaction-filter-end').datepicker({
        format: 'yyyy-mm-dd',
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
    }).on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('.transaction-filter-start').datepicker('setEndDate', maxDate);
    });

    $('.dateTransactionSearch').on('click', function() {
        transactiontable.draw();
    });

    $(".btn-add-money").click(function(){
        $(".add-money-holder").css('display','block')
    });
});
</script>
@stop
