@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <div class="row">
                <div class="col-md-6">
                    <h4>{{ trans('messages.my_account') }}</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('elements.account')
            <div class="tab-content">
                <div class="tab-pane fade show active">
                    <div class="profile-pic">
                        @if (File::exists(public_path($userobj->profile_image)))
                        @php $src = !empty($userobj) && !empty($userobj->profile_image) ? $userobj->profile_image : "img/download.jpeg"; @endphp
                        @else
                        @php $src = "img/download.jpeg"; @endphp
                        @endif
                        <img src="{{ asset($src)}}" width="75" height="75" class="expert-profileimage" alt="Profile Image"/>
                        <a class="change-picture">Change Picture</a>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            @if(Session::has('success'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                            @endif
                        </div>
                        <form method="POST" enctype="multipart/form-data" class="expert-profile-form" action="{{ route('/user/customer-basic-info') }}" id="basic-customer-profile">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h5 class="mb-4 mt-2">Basic Information</h5>
                                        <input type="file" class="expertProfileImage" id="expertProfileImage" name="expertProfileImage"  />
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="fullname"> Full Name</label>
                                            <input id="fullname" placeholder="Full Name" type="text" class="form-control" name="fullname" value="{{ !empty($userobj) && !empty($userobj->fullname) ? $userobj->fullname : ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="username"> Username </label>
                                            <input id="username" placeholder="Username" disabled="true" type="text" class="form-control" name="username" value="{{ !empty($userobj) && !empty($userobj->username) ? $userobj->username : ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="email">Email</label>
                                            <input id="email" placeholder="Email" type="email" disabled="true" class="form-control" name="email" value="{{ !empty($userobj) && !empty($userobj->email) ? $userobj->email : ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="phone_number">Phone number</label>
                                            <input id="phone_number" placeholder="Phone number" disabled="true" type="text" class="form-control" name="phone_number" value="{{ !empty($userobj) && !empty($userobj->mobile_number) ? $userobj->mobile_number : ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="gender" class="d-block">Gender</label>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="male" name="gender" class=" custom-control-input" value="M" {{ !empty($userobj->Profile) && ($userobj->Profile->gender  == 'M') ? 'checked' : 'checked'}}>
                                                <label class="custom-control-label" for="male">Male</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="female" name="gender" class=" custom-control-input" value="F" {{ !empty($userobj->Profile) && ($userobj->Profile->gender == 'F') ? 'checked' : ""}}>
                                                <label class="custom-control-label" for="female">Female</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="other" name="gender" class=" custom-control-input" value="O" {{ !empty($userobj->Profile) && ($userobj->Profile->gender == 'O') ? 'checked' : ""}}>
                                                <label class="custom-control-label" for="other">Other</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="dob3">Date of Birth</label>
                                            <div class="input-group">
                                                <span class="input-group-addon calendar-addon"><i class="fa fa-calendar" aria-hidden="true"></i> </span>
                                                <input id="dob3" placeholder="Date of birth" type="text" class="form-control expert-dob" autocomplete="off" name="dob3" value="{{ !empty($userobj->Profile) && !empty($userobj->Profile->dob) ? $userobj->Profile->dob : ""}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="text-right">
                                                <button type="submit" class="btn btn-primary btn-md mt-4">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(function() {

    $('#basic-customer-profile').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            fullname: {
                required: true
            },
            username: {
                required: true,
            },
            email: {
                required: true,
                email : true
            },
            phone_number: {
                required: true,
                intlTelNumber : true
            },
            gender: {
                required: true,
            },
            dob3: {
                required: true,
            }
        },
        messages: {
            fullname : {
                required : "<?php echo trans('messages.expert_fullname_required');?>",
            },
            username : {
                required : "<?php echo trans('messages.expert_username_required');?>",
            },
            email : {
                required : "<?php echo trans('messages.expert_email_required');?>",
                email : "<?php echo trans('messages.invalid_email');?>"
            },
            phone_number: {
                required : "<?php echo trans('messages.expert_mobile_required');?>",
                intlTelNumber : "<?php echo trans('messages.valid_international_number');?>"
            },
            gender : {
                required : "<?php echo trans('messages.expert_gender_required');?>",
            },
            dob3 : {
                required : "<?php echo trans('messages.expert_dob_required');?>",
            }
        },
        submitHandler: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            return true;
        }
    });

    jQuery.validator.addMethod("intlTelNumber", function(value, element) {
        phone_number = value.replace( /\s+/g, "" );
        return this.optional( element ) || phone_number.length > 11 &&
        phone_number.match(/^\+[1-9]{1}[0-9]{3,14}$/);
    }, "Please enter a valid International Phone Number");


    $('.expert-dob').datepicker({
        format: 'mm-dd-yyyy',
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
    });


    $(".change-picture").click(function(){
        $("#expertProfileImage").click();
    });

    $("#expertProfileImage").change(function() {
        readURL(this);
    });

});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.expert-profileimage').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@stop
