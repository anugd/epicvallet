<!DOCTYPE html>
<html>
<head>
<title>Invoice</title>
<style>
@media print {
  @page { margin: 0; }
  body { margin: 1.6cm; }
}
</style>
</head>
<body>	
<table>
	<tbody>
		<tr>
			<td colspan="2"><img src="img/logo.png" /></td>
		</tr>
		<tr>
			<td style="vertical-align: top;">
				<table>
					<tbody>
						<tr>
							<td>
								<table>
									<tbody>
										<tr>
											<th style="vertical-align: top;text-align: left;width: 100px;">Job Seeker : </th>
											<td>
												<table>
													<tbody>
														<tr>
															<td>{{ $invoice->expert->fullname }}</td>
														</tr>
														<tr>
															<td>{{ $expertaddress->formatted_address }}</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tbody>
										<tr>
											<th style="vertical-align: top;text-align: left;width: 100px;">Customer : </th>
											<td>
												<table>
													<tbody>
														<tr>
															<td>{{ $invoice->customer->fullname }}</td>
														</tr>
														<tr>
															<td>{{ $cusaddress->formatted_address }}</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 50%;vertical-align: top;">
				<table style="width:100%;">
					<thead>
						<tr>
							<th colspan="2" style="text-transform:uppercase;text-transform: uppercase;background-color: #cccc;width: 100%;padding: 5px;">Invoice</th>
						</tr>
						<tr>
							<th height="15"></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="text-transform:uppercase;width: 50%;">Invoice #</td>
							<td style="text-transform:uppercase;width: 50%;text-align: right;">{{ $invoice->id }}</td>
						</tr>
						<tr>
							<td style="text-transform:uppercase;width: 50%;">Date</td>
							<td style="text-transform:uppercase;width: 50%;text-align: right;">{{ $invoice->booking_date }}</td>
						</tr>
						<tr>
							<td style="text-transform:uppercase;width: 50%;">Total Amount</td>
							<td style="text-transform:uppercase;width: 50%;text-align: right;">&pound;  {{ !empty($invoice->ServicePayment) ? $invoice->ServicePayment->total_price : "" }}</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="20"></td>
		</tr>
		<tr>
			<td colspan="2">
				<table border="1" style="width:100%;border-collapse: collapse;">
					<thead>
						</tr>
							<th style="text-transform:uppercase;text-align: left;padding: 5px;background-color:#ccc">description / memo</th>
							<th style="text-transform:uppercase;padding: 5px;text-align: right;background-color:#ccc">Amount</th>
						<tr>
					</thead>
					<tbody>
						<tr>
							<td style="padding: 15px;"> Service :  {{ $invoice->service->service }} / Price per hour</td>
							<td style="padding: 15px;">&pound;  {{ !empty($invoice->ServicePayment) ? $invoice->ServicePayment->per_hour_rate : "" }}</td>
						</tr>
						<tr>
							<td style="padding: 15px;">Job Duration / hour</td>
							<td style="padding: 15px;"> {{ !empty($invoice->ServicePayment) ? $invoice->ServicePayment->job_duration .' Hours' : "" }} </td>
						</tr>
						<tr>
							<td	style="padding: 15px;">Total Amount</td>
							<td style="padding: 15px;">&pound;  {{ !empty($invoice->ServicePayment) ?  $invoice->ServicePayment->total_price : "" }} </td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align : center;">
				<br /><br />
				****************** End Of Invoice *******************
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>
