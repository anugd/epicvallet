@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <h4>{{ trans('messages.invoice')}}</h4>
        </div>
        <div class="card-body dt-no-border-pd">
            <table class="table mb-0" id="expert-invoice">
                <thead>
                    <th>{{ trans('messages.invoice_id') }}</th>
                    <th>{{ trans('messages.date') }}</th>
                    <th>{{ trans('messages.service_name') }}</th>
                    <th>{{ trans('messages.service_price') }}</th>
                    <th>{{ trans('messages.duration') }}</th>
                    <th>{{ trans('messages.cost') }}</th>
                    <th>{{ trans('messages.discount') }}</th>
                    <th>{{ trans('messages.total_cost') }}</th>
                    <th>{{ trans('messages.download_invoice') }}</th>
                </thead>
                <tbody>
                    @php ($invstatus = [ 0 => 'Pending', 1 => 'Booked', 2 => 'Completed', 3 => 'On-Going', 4 => 'Canceled'])
                    @foreach($invoices as $invoice)
                    <tr>
                        <td>{{$invoice['id']}}</td>
                        <td>{{ date('d/m/Y', strtotime($invoice['booking_date']))}}</td>
                        <td>{{$invoice['service']['service']}}</td>
                        <td>£{{$invoice->ServicePayment->per_hour_rate}}/h</td>
                        <td>{{$invoice->ServicePayment->job_duration}}h</td>
                        <td>£{{$invoice->ServicePayment->total_price}}</td>
                        <td>£{{($invoice->ServicePayment->voucher_discount) ? $invoice->ServicePayment->voucher_discount : '0'}}</td>
                        <td>£{{$invoice->ServicePayment->total_price - $invoice->ServicePayment->voucher_discount}}</td>

                        @if(!empty($invoice->invoice_url))
                        <td> <a target="_blank" href="{{ asset( $invoice->invoice_url ) }}"> <i class="fa fa-download" aria-hidden="true"></i> </a></td>
                        @else
                        <td> -- </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
$(function() {
    $('#expert-invoice').DataTable({
        "language": {
            "search": ""
        },
        "bInfo" : false,
        "bLengthChange": false,
        "columnDefs": [ {
            "targets": -1,
            "orderable": false
        } ],
    });

    $('#expert-invoice_filter input').addClass('form-control custom-form-control');
    $('#expert-invoice_filter input').attr('placeholder', "Search");
});
</script>

@stop
