@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <div class="row">
                <div class="col-md-6">
                    <h4>{{ trans('messages.my_account') }}</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('elements.account')
            <div class="tab-content">
                <div class="tab-pane fade show active" id="profile">
                    <div class="row">
                        <div class="col-md-12">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            @if(Session::has('success'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <h5 class="mb-4 mt-3">{{ trans('messages.home') }}</h5>
                            <form method="post" action="{{ route('/user/update-home-address') }}" id="home-address-form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="address_line1">Address Line 1</label>
                                            <input id="address_line1" placeholder="Address Line 1" type="text" class="form-control" name="address_line1" value="{{ !empty($homeaddress) && !empty($homeaddress->address_line1) ? $homeaddress->address_line1 : ""}}" >
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="address_line2">Address Line 2</label>
                                            <input id="address_line2" placeholder="Address Line 2" type="text" class="form-control" name="address_line2" value="{{ !empty($homeaddress) && !empty($homeaddress->address_line2) ? $homeaddress->address_line2 : ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="city">City</label>
                                            <input id="city" placeholder="City" type="text" class="form-control" name="city" value="{{ !empty($homeaddress) && !empty($homeaddress->city) ? $homeaddress->city : ""}}" >
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="postal_code">Postal Code</label>
                                            <input id="postal_code" placeholder="Postal Code" type="text" class="form-control" name="postal_code" value="{{ !empty($homeaddress) && !empty($homeaddress->postal_code) ? $homeaddress->postal_code : ""}}" >
                                        </div>
                                        <div class="form-group">
                                            <div class="text-right">
                                                @if( !empty($homeaddress) && ($homeaddress->is_primary == 1))
                                                <input  class="btn btn-primary btn-md mt-4" value="Default">&nbsp;&nbsp;
                                                @elseif(!empty($homeaddress))
                                                <button type="button" class="btn btn-outline-primary btn-md mt-4 make-default"  addr-id="{{ $homeaddress->id }}">Make Default</button>&nbsp;&nbsp;
                                                @endif
                                                <button type="submit" class="btn btn-primary btn-md mt-4">Save</button>
                                            </div>
                                            </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <h5 class="mb-4 mt-3">{{ trans('messages.work') }}</h5>
                            <form method="post" action="{{ route('/user/update-work-address') }}" id="work-address-form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="address_line1">Address Line 1</label>
                                            <input id="address_line1" placeholder="Address Line 1" type="text" class="form-control" name="address_line1" value="{{ !empty($workaddress) && !empty($workaddress->address_line1) ? $workaddress->address_line1 : ""}}" >
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="address_line2">Address Line 2</label>
                                            <input id="address_line2" placeholder="Address Line 2" type="text" class="form-control" name="address_line2" value="{{ !empty($workaddress) && !empty($workaddress->address_line2) ? $workaddress->address_line2 : ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="city">City</label>
                                            <input id="city" placeholder="City" type="text" class="form-control" name="city" value="{{ !empty($workaddress) && !empty($workaddress->city) ? $workaddress->city : ""}}" >
                                        </div>
                                        <div class="form-group">
                                            <label class="text-primary font-weight-bold" for="postal_code">Postal Code</label>
                                            <input id="postal_code" placeholder="Postal Code" type="text" class="form-control" name="postal_code" value="{{ !empty($workaddress) && !empty($workaddress->postal_code) ? $workaddress->postal_code : ""}}" >
                                        </div>
                                        <div class="form-group">
                                            <div class="text-right">
                                                @if( !empty($workaddress) && ($workaddress->is_primary == 1))
                                                <input class="btn btn-primary btn-md mt-4" value="Default">&nbsp;&nbsp;
                                                @elseif(!empty($workaddress))
                                                <button type="button" class="btn btn-outline-primary btn-md mt-4 make-default" addr-id="{{ $workaddress->id }}" >Make Default</button>&nbsp;&nbsp;
                                                @endif
                                                <button type="submit" class="btn btn-primary btn-md mt-4">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {

    $(".make-default").click(function(){
        var _this = $(this);
        var address = $(this).attr('addr-id');
        $.ajax({
            type: "POST",
            url: "{{ route('user.primaryaddress') }}",
            beforeSend: function(){
                _this.attr("disabled", true);
                _this.prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            },
            data: {
                address:address
            },
            success: function(data) {
                window.location.reload()
            }
        });

    })

    $('#home-address-form').validate({
        rules: {
            address_line1 : {
                required: true
            },
            city : {
                required: true
            },
            postal_code : {
                required: true,
                number: true,
                maxlength: 8
            }
        },
        messages: {
            address_line1 : {
                required: "<?php echo trans('messages.expert_address_required');?>"
            },
            city : {
                required: "<?php echo trans('messages.expert_city_required');?>"
            },
            postal_code : {
                required: "<?php echo trans('messages.expert_postcode_required');?>",
                maxlength : "<?php echo trans('messages.expert_postcode_max_length_required');?>",
                number : "<?php echo trans('messages.digit_allowed');?>"
            }
        },
        submitHandler: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            return true;
        }
    });

    $('#work-address-form').validate({
        rules: {
            address_line1 : {
                required: true
            },
            city : {
                required: true
            },
            postal_code : {
                required: true,
                number: true,
                maxlength: 8
            }
        },
        messages: {
            address_line1 : {
                required: "<?php echo trans('messages.expert_address_required');?>"
            },
            city : {
                required: "<?php echo trans('messages.expert_city_required');?>"
            },
            postal_code : {
                required: "<?php echo trans('messages.expert_postcode_required');?>",
                maxlength : "<?php echo trans('messages.expert_postcode_max_length_required');?>",
                number : "<?php echo trans('messages.digit_allowed');?>"
            }
        },
        submitHandler: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            return true;
        }
    });

});
</script>
@stop
