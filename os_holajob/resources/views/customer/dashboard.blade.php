@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<main class="col-md-9 ml-sm-auto col-lg-12 pt-4 px-4">
			<div class="tab-content">
				<div class="tab-pane fade show active" id="Servicehistory">
					<div class="card shadow">
						<div class="card-header bg-white">
							<p>Service history</p>
						</div>
						<div class="card-body">
							<table class="table mb-0">
								<thead>
									<th>Service Name</th>
									<th>Expert Name</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Start Time</th>
									<th>End Time</th>
									<th>Status</th>
									<th></th>
								</thead>
								<tbody>
									<tr>
										<td>Barista</td>
										<td>John Doe</td>
										<td>16/03/2019</td>
										<td>---------------</td>
										<td>08:30 AM</td>
										<td>---------------</td>
										<td>In progress</td>
										<td></td>
									</tr>
									<tr>
										<td>Bartender</td>
										<td>Brad William</td>
										<td>19/03/2019</td>
										<td>16/03/2019</td>
										<td>08:30 AM</td>
										<td>04:00 PM</td>
										<td>In progress</td>
										<td></td>
									</tr>
									<tr>
										<td>Barista</td>
										<td>John Doe</td>
										<td>16/03/2019</td>
										<td>---------------</td>
										<td>08:30 AM</td>
										<td>---------------</td>
										<td>In progress</td>
										<td></td>
									</tr>
									<tr>
										<td>Barista</td>
										<td>John Doe</td>
										<td>16/03/2019</td>
										<td>---------------</td>
										<td>08:30 AM</td>
										<td>---------------</td>
										<td>In progress</td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="card-footer bg-white text-right">
							<nav aria-label="Page navigation example">
								<ul class="pagination justify-content-end">
									<li class="page-item">
										<a class="page-link" href="#" aria-label="Previous">
											<span aria-hidden="true">&laquo;</span>
											<span class="sr-only">Previous</span>
										</a>
									</li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item">
										<a class="page-link" href="#" aria-label="Next">
											<span aria-hidden="true">&raquo;</span>
											<span class="sr-only">Next</span>
										</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="Invoice">
					<div class="card shadow">
							<div class="card-header bg-white">
								<p>Invoices</p>
							</div>
							<div class="card-body">
								<table id="invoice-table" class="table mb-0">
									<thead>
										<th>Invoice ID</th>
										<th>Date</th>
										<th>Service</th>
										<th>Service Price/hr</th>
										<th>Duration</th>
										<th>Cost</th>
										<th>Discount</th>
										<th>Total Cost</th>
									</thead>
									<tbody>
										<tr>
											<td>14223</td>
											<td>07/03/19</td>
											<td>Chef</td>
											<td>£9/h</td>
											<td>4h</td>
											<td>£36</td>
											<td>£1</td>
											<td>£35</td>
										</tr>
										<tr>
											<td>14223</td>
											<td>07/03/19</td>
											<td>Chef</td>
											<td>£9/h</td>
											<td>4h</td>
											<td>£36</td>
											<td>£1</td>
											<td>£35</td>
										</tr>
										<tr>
											<td>14223</td>
											<td>07/03/19</td>
											<td>Chef</td>
											<td>£9/h</td>
											<td>4h</td>
											<td>£36</td>
											<td>£1</td>
											<td>£35</td>
										</tr>
										<tr>
											<td>14223</td>
											<td>07/03/19</td>
											<td>Chef</td>
											<td>£9/h</td>
											<td>4h</td>
											<td>£36</td>
											<td>£1</td>
											<td>£35</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<div class="tab-pane fade" id="Mywallet">
				<div class="row">
						<div class="col-md-8 p-0">
							<div class="card shadow">
								<div class="card-header bg-white">
									<h4>Hela Job wallet</h4>
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" id="user_id">
									<div class="text-primary h3 mt-2">£89.22</div>
									<!--<div class="add-money d-flex">
										<div class="circle mr-2">
											<span>+</span>
										</div>
										<span>Add Money to wallet</span>
									</div>-->
									<div class="row pt-3 pb-2 no-gutters">
										<div class="col d-flex align-items-center justify-content-around card-header-form">
											<div class="text-primary">
												Form
											</div>
											<div class="">
											  <span class="mr-1"> 07/03/19 </span>
												<svg xmlns="http://www.w3.org/2000/svg" width="18" height="16" viewBox="0 0 22.282 20.89"><defs></defs><g transform="translate(-2.444 -10.992)"><g transform="translate(2.444 10.992)"><path class="text-primary--svg" d="M18.8,10.093H16.016V9.4a.7.7,0,1,0-1.393,0v.7H7.66V9.4a.658.658,0,0,0-.7-.7.658.658,0,0,0-.7.7v.7H3.482A3.448,3.448,0,0,0,0,13.574V26.108A3.448,3.448,0,0,0,3.482,29.59H18.8a3.448,3.448,0,0,0,3.482-3.482V13.574A3.448,3.448,0,0,0,18.8,10.093ZM1.393,13.574a2.142,2.142,0,0,1,2.089-2.089H6.267v.7a.658.658,0,0,0,.7.7.658.658,0,0,0,.7-.7v-.7h6.963v.7a.7.7,0,1,0,1.393,0v-.7H18.8a2.142,2.142,0,0,1,2.089,2.089v2.089H1.393V13.574Zm19.5,12.534A2.142,2.142,0,0,1,18.8,28.2H3.482a2.142,2.142,0,0,1-2.089-2.089V17.056h19.5Z" transform="translate(0 -8.7)"/><path class="a" d="M44.9,194.2a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0,1.264,1.264,0,0,0-.418.975,1.5,1.5,0,0,0,.418.975A1.264,1.264,0,0,0,44.9,194.2Z" transform="translate(-40.021 -176.791)"/><path class="a" d="M44.9,142a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0A1.393,1.393,0,0,0,44.9,142Z" transform="translate(-40.021 -128.765)"/><path class="a" d="M97.1,194.2a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,97.1,194.2Z" transform="translate(-88.047 -176.791)"/><path class="a" d="M97.1,142a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,97.1,142Z" transform="translate(-88.047 -128.765)"/><path class="a" d="M149.3,194.2a1.379,1.379,0,0,0,.975-2.368,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,149.3,194.2Z" transform="translate(-136.072 -176.791)"/><path class="a" d="M149.3,142A1.428,1.428,0,0,0,150.7,140.6a1.5,1.5,0,0,0-.418-.975,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,149.3,142Z" transform="translate(-136.072 -128.765)"/><path class="a" d="M201.507,194.2a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0,1.264,1.264,0,0,0-.418.975,1.5,1.5,0,0,0,.418.975A1.264,1.264,0,0,0,201.507,194.2Z" transform="translate(-184.098 -176.791)"/><path class="a" d="M201.507,142A1.428,1.428,0,0,0,202.9,140.6a1.5,1.5,0,0,0-.418-.975,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,201.507,142Z" transform="translate(-184.098 -128.765)"/></g></g></svg>
											</div>
										</div>
										<div class="col d-flex align-items-center justify-content-around">
											<div class="text-primary">
												To
											</div>
											<div class="date">
												<span class="mr-1"> 20/03/19 </span>
												<svg xmlns="http://www.w3.org/2000/svg" width="18" height="16" viewBox="0 0 22.282 20.89"><defs></defs><g transform="translate(-2.444 -10.992)"><g transform="translate(2.444 10.992)"><path class="text-primary--svg" d="M18.8,10.093H16.016V9.4a.7.7,0,1,0-1.393,0v.7H7.66V9.4a.658.658,0,0,0-.7-.7.658.658,0,0,0-.7.7v.7H3.482A3.448,3.448,0,0,0,0,13.574V26.108A3.448,3.448,0,0,0,3.482,29.59H18.8a3.448,3.448,0,0,0,3.482-3.482V13.574A3.448,3.448,0,0,0,18.8,10.093ZM1.393,13.574a2.142,2.142,0,0,1,2.089-2.089H6.267v.7a.658.658,0,0,0,.7.7.658.658,0,0,0,.7-.7v-.7h6.963v.7a.7.7,0,1,0,1.393,0v-.7H18.8a2.142,2.142,0,0,1,2.089,2.089v2.089H1.393V13.574Zm19.5,12.534A2.142,2.142,0,0,1,18.8,28.2H3.482a2.142,2.142,0,0,1-2.089-2.089V17.056h19.5Z" transform="translate(0 -8.7)"/><path class="a" d="M44.9,194.2a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0,1.264,1.264,0,0,0-.418.975,1.5,1.5,0,0,0,.418.975A1.264,1.264,0,0,0,44.9,194.2Z" transform="translate(-40.021 -176.791)"/><path class="a" d="M44.9,142a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0A1.393,1.393,0,0,0,44.9,142Z" transform="translate(-40.021 -128.765)"/><path class="a" d="M97.1,194.2a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,97.1,194.2Z" transform="translate(-88.047 -176.791)"/><path class="a" d="M97.1,142a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,97.1,142Z" transform="translate(-88.047 -128.765)"/><path class="a" d="M149.3,194.2a1.379,1.379,0,0,0,.975-2.368,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,149.3,194.2Z" transform="translate(-136.072 -176.791)"/><path class="a" d="M149.3,142A1.428,1.428,0,0,0,150.7,140.6a1.5,1.5,0,0,0-.418-.975,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,149.3,142Z" transform="translate(-136.072 -128.765)"/><path class="a" d="M201.507,194.2a1.264,1.264,0,0,0,.975-.418,1.346,1.346,0,0,0,0-1.95,1.346,1.346,0,0,0-1.95,0,1.264,1.264,0,0,0-.418.975,1.5,1.5,0,0,0,.418.975A1.264,1.264,0,0,0,201.507,194.2Z" transform="translate(-184.098 -176.791)"/><path class="a" d="M201.507,142A1.428,1.428,0,0,0,202.9,140.6a1.5,1.5,0,0,0-.418-.975,1.346,1.346,0,0,0-1.95,0,1.346,1.346,0,0,0,0,1.95A1.264,1.264,0,0,0,201.507,142Z" transform="translate(-184.098 -128.765)"/></g></g></svg>
											</div>
										</div>
										<div class="col">
											<button type="button" class="btn btn-filter btn-outline-primary">Filter</button>
										</div>
										<div class="col-md-4 d-flex align-items-center justify-content-around">
											<div class="filter-text text-primary col-md-4 p-0">	Filter by </div>
											<select class="dropdown form-control border col-md-8 rounded" id="exampleFormControlSelect1">
												<option>Status</option>
												<option>Another action</option>
												<option>Something else here</option>
												<option>4</option>
												<option>5</option>
											</select>
										</div>
									</div>
								</div>
								<div class="card-body card-body--account">
									<div class="table-responsive">
										<table class="table mb-0">
											<thead>
												<th>Date</th>
												<th>Description</th>
												<th>Amount</th>
											</thead>
											<tbody>
												<tr>
													<td>Amount</td>
													<td>Credited from \r_1E1BujKdzrvVNwJCHGMmcmJh</td>
													<td>£36</td>
												</tr>
												<tr>
													<td>08/03/19</td>
													<td>Debited to \r_1E1BujKddsfsdfwJCHGMmcmJh</td>
													<td>£55</td>
												</tr>
												<tr>
													<td>Amount</td>
													<td>Credited from \r_1E1BujKdzrvVNwJCHGMmcmJh</td>
													<td>£36</td>
												</tr>
												<tr>
													<td>08/03/19</td>
													<td>Debited to \r_1E1BujKddsfsdfwJCHGMmcmJh</td>
													<td>£55</td>
												</tr>
												<tr>
													<td>08/03/19</td>
													<td>Debited to \r_1E1BujKddsfsdfwJCHGMmcmJh</td>
													<td>£55</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 p-0">
							<div class="card shadow">

    <form id="addmoney_form">							<div class="card-header bg-white border-bottom-0 pb-0">
									<h4 class="pt-4 pb-4">Add Money to wallet</h4>

                                                                        <div class="alert alert-danger "></div>
    <div class="alert alert-success "></div>
			<div class="input-group">
				<div class="input-group-prepend">
          <div class="input-group-text bg-white border-top-0 p-1 border-left-0 border-right-0 border-bottom">£</div>
        </div>
      	<input type="number" class="form-control" id="amount" name="amount" placeholder="Enter Amount" maxlength="6">
          <span class="invalid-feedback amount_err" role="alert" style="display: none" >
						<strong>The Amount field is required.</strong>
					</span>
				</div>
			</div>
                                                        </form>								<div class="card-body">
									<div class="saved-card">                                              										<h6>Already saved cards</h6>
										<div class="card-list"></div>
   <button type="button" class="btn btn-md btn-primary" onclick="addMoney()">Add Money</button></div>
									<div class="add-card pt-5">
<h6 class="mb-3">Add credit/debit card</h6>
<div id="wait" class="loader" style="display:none;"></div>    
<!--    <div class="alert alert-danger"></div>
    <div class="alert alert-success "></div>-->
    <form id="addcard_form">
										<div class="form-group">
												<label for="carddetails">Enter your card details</label>
												<input type="number" class="form-control" id="card_no" name="card_no" placeholder="Enter your card details" maxlength="16">
                                                                       <span class="invalid-feedback card_err" role="alert" style="display: none" >
   <strong>The card details field is required.</strong>
</span>
											</div>
											<div class="form-group">
												<div class="row">
												  <label class="col-md-12" for="expirydate">Expiry/validity date</label>
													<div class="col-md-6">
														<input type="number" class="form-control" id="card_month" name="card_month" placeholder="MM">
														<span class="invalid-feedback month_err" role="alert" style="display: none" >
															<strong>The card month field is required.</strong>
														</span>
													</div>
													<div class="col-md-6">
														<input type="number" class="form-control" id="card_year" name="card_year" placeholder="YYYY">
														<span class="invalid-feedback year_err" role="alert" style="display: none" >
															<strong>The card year field is required.</strong>
														</span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-md-6">
														<label for="cvv">CVV</label>
														<input type="number" class="form-control" id="card_cvv" name="card_cvv" placeholder="Enter CVV">
														<span class="invalid-feedback cvv_err" role="alert" style="display: none" >
															<strong>The CVV field is required.</strong>
														</span>
													</div>
												</div>
											</div>
											<div class="form-group pl-4">
												<input type="checkbox" class="form-control form-control form-check-input form-check-input-custom" id="set_primary" placeholder="set_primary" name="set_primary" value="1">
												<label for="set_primary">Set As Primary </label>
											</div>



											</div>
											<div class="form-group">
                                                                                            <button type="button" class="btn btn-md btn-primary" onclick="addCard()">Add Card</button>
											</div>
										</form>

									</div>
								</div>
							</div>
						</div>
					</div>
				<div class="tab-pane fade" id="Ticket">
					<div class="card shadow">
						<div class="card-header bg-white">
							<div class="row">
								<div class="col-md-6">
									<h4>Ticket</h4>
								</div>
								<div class="col-md-6">
								<button type="button" class="btn btn-md btn-outline-primary float-right">Add new ticket</button>
								</div>
							</div>
						</div>
						<div class="card-body">
							<table class="table mb-0">
								<thead>
									<th>#ID</th>
									<th>Date</th>
									<th>Service</th>
									<th>Service Price/hr</th>
									<th>Duration</th>
								</thead>
								<tbody>
									<tr>
										<td>14223</td>
										<td>07/03/19</td>
										<td>Chef</td>
										<td>£9/h</td>
										<td>4h</td>
										<td>£36</td>
										<td>£1</td>
										<td>£35</td>
									</tr>
									<tr>
										<td>14223</td>
										<td>07/03/19</td>
										<td>Chef</td>
										<td>£9/h</td>
										<td>4h</td>
										<td>£36</td>
										<td>£1</td>
										<td>£35</td>
									</tr>
									<tr>
										<td>14223</td>
										<td>07/03/19</td>
										<td>Chef</td>
										<td>£9/h</td>
										<td>4h</td>
										<td>£36</td>
										<td>£1</td>
										<td>£35</td>
									</tr>
									<tr>
										<td>14223</td>
										<td>07/03/19</td>
										<td>Chef</td>
										<td>£9/h</td>
										<td>4h</td>
										<td>£36</td>
										<td>£1</td>
										<td>£35</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
    	</div>
		</main>
	</div>
</div>

<!-- <aside class="col-12 col-md-2 p-0 bg-dark">
	<nav class="navbar navbar-expand navbar-dark bg-dark flex-md-column flex-row align-items-start py-2">
		<div class="collapse navbar-collapse">
			<ul class="flex-md-column flex-row navbar-nav w-100 justify-content-between">
				<li class="nav-item">
					<a class="nav-link pl-0 text-nowrap" href="#"><i class="fa fa-bullseye fa-fw"></i> <span class="font-weight-bold">Brand</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link pl-0" href="#"><i class="fa fa-heart-o fa-fw"></i> <span class="d-none d-md-inline">Link</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link pl-0" href="#"><i class="fa fa-book fa-fw"></i> <span class="d-none d-md-inline">Link</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link pl-0" href="#"><i class="fa fa-heart fa-fw"></i> <span class="d-none d-md-inline">Link</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link pl-0" href="#"><i class="fa fa-star fa-fw"></i> <span class="d-none d-md-inline">Link</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link pl-0" href="#"><i class="fa fa-list fa-fw"></i> <span class="d-none d-md-inline">Link</span></a>
				</li>
			</ul>
		</div>
	</nav>
</aside> -->
<script type="text/javascript" src="{{ URL::asset('js/wallet.js') }}"></script>
<script>

    </script>
@endsection
