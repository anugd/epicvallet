@extends('layouts.app')

@section('content')
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> {{@$services->service->service}}(${{$services->service->price}})
                <small class="pull-right">Date: {{@$services->service->created_at}}</small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <span class="text-info">Customer</span>
            <address>
                <strong>{{@$services->customer->fullname}}</strong><br>

                Phone: {{@$services->customer->mobile_number}}<br>
                Email: {{@$services->customer->email}}
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <span class="text-info">Expert</span>
            <address>
                <strong>{{@$services->expert->fullname}}</strong><br>

                Phone: {{@$services->expert->mobile_number}}<br>
                Email: {{@$services->expert->email}}
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <span class="text-info">Service Details</span>
            <br>
            <b>Location:</b> {{@$services->address}}<br>

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Job Duration</th>
                        <th>Per Hour Rate</th>
                        <th>Total Price</th>
                        <th>Voucher Discount</th>
                        <th>Admin Commission</th>
                        <th>Admin Commission Per</th>
                        <th>Expert Amount</th>
                        <th>Payment Status</th>
                        <th>Stripe Txn Id</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($services->ServicePayment) { ?>
                        <tr>
                            <td>{{@$services->ServicePayment->job_duration}}</td>
                            <td>{{@$services->ServicePayment->per_hour_rate}}</td>
                            <td>{{@$services->ServicePayment->total_price}}</td>
                            <td>{{@$services->ServicePayment->voucher_discount}}</td>
                            <td>{{@$services->ServicePayment->service_fee}}</td>
                            <td>{{@$services->ServicePayment->admin_commission_per}}</td>
                            <td>{{@$services->ServicePayment->sub_total}}</td>
                            <td>{{@$services->ServicePayment->payment_status}}</td>
                            <td>{{@$services->ServicePayment->stripe_txn_id}}</td>
                        </tr>
                    <?php } else{ ?>
                        <tr>
                            <td colspan="9">No payemnts yet!!
                            </td>
                        </tr>
                    <?php }  ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
@stop
