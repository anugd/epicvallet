@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <h4>{{ trans('messages.service_history') }}</h4>
        </div>
        <div class="card-body">
            <div class="row filters-service-history">
                <div class="col-md-3 text-left">
                    <div class="date form-inline">
                        <div class="form-group">
                            <label for="" class="pd-rt-7"> Filter by </label>
                            <select name="expert_status_filter" id="expert_status_filter" class="form-control custom-form-control">
                                <option value="">{{ trans('messages.all') }}</option>
                                <option value="0">{{ trans('messages.pending') }}</option>
                                <option value="1">{{ trans('messages.booked') }}</option>
                                <option value="2">{{ trans('messages.completed') }}</option>
                                <option value="3">{{ trans('messages.on_going') }}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <input type="text" id="myInputTextField" class="form-control custom-form-control" placeholder="search">
            </div>
            <table class="table mb-0" id="expert-invoice" style="width:100%;">
                <thead>
                    <th>{{ trans('messages.service_name') }}</th>
                    @if($user->user_type_id == 1)
                    <th>{{ trans('messages.expert_name') }}</th>
                    @else
                    <th>{{ trans('messages.customer_name') }}</th>
                    @endif
                    <th>{{ trans('messages.appointment_date') }}</th>
                    <th>{{ trans('messages.appointment_time') }}</th>
                    <th>{{ trans('messages.address') }}</th>
                    <th>{{ trans('messages.status') }}</th>
                    <!-- <th>{{ trans('messages.action') }}</th> -->
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal right fade viewService-modal" id="viewServiceModal" tabindex="-1" role="dialog" aria-labelledby="viewServiceModalLabel">
    <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content viewServiceModal-content">
                <div class="modal-header viewServiceModal-header">
                    <h4 class="modal-title" id="viewServiceModalLabel">{{ trans('messages.service_detail') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body viewServiceModal-body"></div>
            </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
<script>
$(function() {
        expertinvoice = $('#expert-invoice').DataTable({
        processing: true,
        serverSide: true,
        "bInfo" : false,
        "bLengthChange": false,
        dom: 't' ,
        "language": {
            "search": ""
        },
        "columnDefs": [ {
            "targets": -1,
            "orderable": false
        } ],
        ajax: {
            url: '{{ route('expert.servicehistory') }}',
            data: function(d) {
                d.status = $('#expert_status_filter').val();
            }
        },
        columns: [
            {data: 'service',defaultContent: "--"},
            {data: 'name',defaultContent: "--"},
            {data: 'booking_date',defaultContent: "--"},
            {data: 'booking_time',defaultContent: "--"},
            {data: 'address',defaultContent: "--"},
            {data: 'status',defaultContent: "--"},
            // {data: 'action',defaultContent: ""}
        ]
    });

    $('#expert-invoice_filter input').addClass('form-control custom-form-control');
    $('#expert-invoice_filter input').attr('placeholder', "Search");
    $('#expert-invoice_length select').addClass("form-control db-length-input custom-form-control");

    $('#expert_status_filter').on('change', function() {
        expertinvoice.draw();
    });

    $('#myInputTextField').keyup(function(){
          expertinvoice.search($(this).val()).draw() ;
    })
});

$(document).on('click','.viewDetail',function() {
    var dataId = $(this).attr('dataId');
    viewDetail(dataId);
});

function viewDetail(dataId) {

    $('.viewServiceModal-body').html('');
    var userType = "{{Auth::user()->user_type_id}}";
    if(userType == 1) {
        url = 'service/servicesHistoryDetailCustomer';
    }else{
        url = 'service/servicesHistoryDetailExpert';
    }
    $.ajax({
        type: "GET",
        url: url,
        data: { id : dataId},
        success: function(data){
            $('.viewServiceModal-body').html(data);
        }
    });
}
</script>

@stop
