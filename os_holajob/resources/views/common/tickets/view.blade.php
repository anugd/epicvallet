<div class="row">
    <div class="col-md-7">
        <div class="msg_card_body">
        <?php
            if(isset($comments) && !empty($comments)) {
                $com = $comments->comments;
                $users = $comments->users;
                foreach($com as $comment) {
                $key = array_search($comment->author_id, array_column($users, 'id'));
            ?>

            <?php if($comment->author_id == Auth::user()->zenddesk_id) { ?>
            <div class="d-flex justify-content-end align-items-center mb-4 mr-3">
                <div class="msg_cotainer_send">
                    {{$comment->body}}
                    <span class="msg_time_send"> {{date('H:i A',strtotime($comment->created_at))}}</span>
                </div>
                <div class="img_cont_msg">
                        @php
						$userobj  = App\Module\Model\User::with('profile')->where('id',Auth::user()->id)->first();
						@endphp
				        @if (file_exists(public_path($userobj->profile_image)))
				            @php $src = !empty($userobj) && !empty($userobj->profile_image) ? $userobj->profile_image : "img/download.jpeg"; @endphp
				        @else
				            @php $src = "img/download.jpeg"; @endphp
                        @endif
                        <img src="{{ asset($src)}}" width="40" height="40" class="rounded-circle" />
                </div>
            </div>
            <?php } else { ?>
            <div class="d-flex justify-content-start align-items-center mb-4 mr-3">
                <div class="img_cont_msg">
                    <img src="http://bootdey.com/img/Content/avatar/avatar1.png" class="rounded-circle user_img_msg">
                </div>
                <div class="msg_cotainer">
                    {{$comment->body}}
                    <span class="msg_time">{{date('H:i A',strtotime($comment->created_at))}}</span>
                </div>
            </div>
            <?php }  ?>
            <?php } } ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="msg_text_body d-flex align-items-end">
        <form method="post" action="{{ route('tickets.addReply') }}" id="tickets-reply-form" class="w-100">
                <div class="form-group">

                    <input type="hidden" id="ticket_id" name="ticket_id" value="{{$ticket_id}}">

                    <input type="text" id="reply" name="reply" class="form-control form-control--text border p-2 rounded" name="message" placeholder="Type message...">
                </div>
                <div class="form-group text-right">
                    <!-- <button type="button" class="btn btn-md btn-outline-primary">Add attachment</button> -->

                    <button type="button" class="btn btn-primary addReplyBtn">{{ trans('messages.send_reply') }}</button>

                </div>
            </form>
        </div>
    </div>
</div>
<script>
$(function() {
    $('#tickets-reply-form').validate({
        errorPlacement: function(error, element) {
            if (element.hasClass('textarea')) {
                error.insertAfter(element.next('div'));  // textarea
                element.next('div').addClass('error').removeClass('valid');
            }
            else if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            }
            else{
                error.insertAfter(element);
            }
        },
        ignore:[],
        errorClass: "error",
        rules: {
            reply: {
                required: true,
            }
        },
        messages: {
            reply: {
                required: '<?php echo trans('messages.reply_required'); ?>'
            }
        }
        
    });
    
});
</script>
