@extends('layouts.app')
@section('content')
    <div class="tab-pane">
        <div class="card shadow">
            <div class="card-header bg-white">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ trans('messages.add_new_ticket') }}</h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('tickets.store') }}" id="tickets-form">
                  @csrf
                  <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name"> {{ trans('messages.subject') }}</label>
                                <input type="text" placeholder="{{ trans('messages.subject') }}" required ="true" class="form-control" id="subject" name="subject"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="code"> {{ trans('messages.description') }} </label>
                                <textarea id="description" name="description" required class="form-control textarea"></textarea>
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="code"> {{ trans('messages.priority') }} </label>
                                <select name = "priority" class="form-control">
                                <?php 
                                $options = array('low'=>'Low','normal'=>'Normal','high'=>'High','urgent'=>'Urgent');
                                    foreach($options as $option) { 
                                ?>
                                    <option value={{$option}}>{{$option}}</option>
                                <?php
                                }
                                ?>
                                </select>
                                
                            </div>
                        </div>
                      <div class="col-md-12">
                          <button type="submit" class="btn btn-primary">{{ trans('messages.submit_button') }}</button>&nbsp;&nbsp;
                          <a href="{{ route('tickets.index')}}" class="btn btn-md btn-outline-primary"> {{ trans('messages.cancel_button') }} </a>
                      </div>
                  </div>
                </form>
            </div>
        </div>
    </div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js" defer></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js" defer></script>
<script>

    $(function() {
        $('textarea').ckeditor();
        $('#tickets-form').validate({
            errorPlacement: function(error, element) {
                if (element.hasClass('textarea')) {     
                    error.insertAfter(element.next('div'));  // textarea
                    element.next('div').addClass('error').removeClass('valid');
                } 
                else if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } 
                else{
                    error.insertAfter(element);
                }
            },
            ignore:[],
            rules: {
                subject: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                subject: {
                    required: "<?php echo trans('messages.ticket_subject_required');?>"
                },
                description: {
                    required: "<?php echo trans('messages.ticket_description_required');?>"
                }
            },
            onsubmit: function(form)
            {
                $(form).find(':submit').attr("disabled", true);
                $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
                return true;
            }
        });
    });
    </script>
@stop
