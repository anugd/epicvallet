@extends('layouts.app')

@section('content')
<div id="add-ticket">
		<div class="card shadow">
			<div class="card-header bg-white">
				<div class="row">
					<div class="col-md-6">
						<h4>{{trans('messages.add_new_ticket')}}</h4>
					</div>
					<div class="col-md-6">
						<!-- <button type="button" href="{{route('tickets.index')}}" class="btn btn-md btn-outline-primary float-right">Back</button> -->
					</div>
				</div>
            </div>
            <div class="card-body">
					<div class="row">

                            <div class="col-md-6 pr-5  border-right-mobile-none"> 
                                <!-- border-right -->
                            <form method="post" action="{{ route('tickets.store') }}" id="tickets-form">
                            @csrf
                                    <!-- <div class="form-group">
                                        <label class="text-primary font-weight-bold">{{ trans('messages.category') }}</label>
                                        <input id="category" placeholder="Support" type="text" name="category" value="" class="form-control">
                                        <span role="alert" class="invalid-feedback">
                                            <strong>Support</strong>
                                        </span>
                                    </div> -->
                                    <div class="form-group">
                                        <label class="text-primary font-weight-bold">{{ trans('messages.subject') }}</label>

                                        <input type="text" placeholder="{{ trans('messages.subject') }}" required ="true" class="form-control" id="subject" name="subject" placeholder="Jeff Apartment, Near Food Court"/>


                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary font-weight-bold">{{ trans('messages.description') }}</label>
                                        <textarea id="description" name="description" placeholder="Description"  required class="form-control border p-2 textarea" rows="3"></textarea>

                                    </div>
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-md btn-primary">{{ trans('messages.add_ticket_btn') }}</button>

                                    </div>
                            </form>
                            </div>
                            
                        
                    </div>

            </div>
        </div>
	</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js" defer></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js" defer></script>
<script>

    $(function() {
        //$('textarea').ckeditor();
        $('#tickets-form').validate({
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else{
                    error.insertAfter(element);
                }
            },
            ignore:[],
            rules: {
                subject: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                subject: {
                    required: "<?php echo trans('messages.ticket_subject_required');?>"
                },
                description: {
                    required: "<?php echo trans('messages.ticket_description_required');?>"
                }
            },
            onsubmit: function(form)
            {
                $(form).find(':submit').attr("disabled", true);
                $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
                return true;
            }
        });
    });
    </script>
@stop
