@extends('layouts.app')

@section('content')
    <div class="tab-pane">
        <div class="card shadow">
            <div class="card-header bg-white">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ trans('messages.tickets') }}</h4>
                    </div>
                    <div class="col-md-6">
                        <a  class="btn btn-md btn-outline-primary float-right" href="{{ route('tickets.create')}}">{{ trans('messages.add_new_ticket') }}</a>

                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="tickets-table" class="table mb-0">
                    <thead>
                        <th>#{{ trans('messages.id') }}</th>
                        <th>{{ trans('messages.subject') }}</th>
                        <th>{{ trans('messages.requested') }}</th>
                        <th>{{ trans('messages.priority') }}</th>
                        <th>{{ trans('messages.status') }}</th>
                        <th>{{ trans('messages.action') }}</th>
                    </thead>
                    <tbody>
                    <?php if(isset($tickets) && !empty($tickets)) {
                        foreach($tickets->tickets as $ticket) { ?>
                        <tr>
                            <td>{{$ticket->id}}</td>
                            <td>{{$ticket->subject}}</td>
                            <td>{{  date("d M y  h:i A",strtotime($ticket->created_at)) }}</td>
                            <td>{{($ticket->priority)?$ticket->priority:'--'}}</td>
                            <td>{{$ticket->status}}</td>
                            <td>
                                <a id='delete' class = 'btn btn-info view-btn-table' title='View' data-toggle='tooltip' href = '/tickets/view/{{$ticket->id}}'>V</a>
        
                                <a id='delete' delId = '{{$ticket->id}}' class = 'btn btn-danger delete-btn-table deleteTicket' title='Delete'  data-toggle='tooltip' href = '#'>D</a>
                            </td>
                        </tr>
                       <?php } } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
$(function() {
   
    ticketTable = $('#tickets-table').DataTable({
        "language": {
        "search": ""
        },
        "columnDefs": [ {
            "targets": -1,
            "orderable": false
        } ],
    });
    
    $('#tickets-table_filter input').addClass('custom-form-control');
    $('#tickets-table_filter input').attr('placeholder', "Search");
});
$(document).on('click', '.deleteTicket', function(e) {
        var delId = $(this).attr('delId');
        var thiss = $(this);
        swal({
            title: "<?php echo trans('messages.are_you_sure');?>",
            text: "<?php echo trans('messages.perform_action');?>",
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
        .then((isConfirm) => {

            if(isConfirm) {
                $.ajax({
                        url: '/tickets/destroy/'+delId,
                        type: 'GET',
                        data:{ },
                        success: function(ress){
                            
                            if(ress) {
                                
                                ticketTable
                                .row( thiss.parents('tr') )
                                .remove()
                                .draw(false);
                            }
                        }
                    });
                
            }
            else{
                e.preventDefault();
            }
            
        });
    });
</script>
@stop
