@extends('layouts.app')

@section('content')
<div class="tab-pane">
	<div id="Ticket">
		<div class="card shadow">
			<div class="card-header bg-white">
				<div class="row">
					<div class="col-lg-6 col-sm-6 col">
						<h4>{{ trans('messages.tickets') }}</h4>
					</div>
					<div class="col-lg-6 col-sm-6 col form-group">
                        <a  class="btn btn-md btn-outline-primary float-right" href="{{ route('tickets.create')}}">{{ trans('messages.add_new_ticket') }}</a>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4 col-12 mt-1 mb-1">
						<div class="row no-gutters">
							<div class="col-sm-4 col-4">
								From
							</div>
                            <div class="col-sm-8 col-8 input-group">
                                <div class="input-group">
                                    <input id="from"  type="text" class="form-control" autocomplete="off" name="from" value="">
                                    <span class="input-group-addon calendar-addon"><i class="fa fa-calendar" aria-hidden="true"></i> </span>
                                </div>
                            </div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4 col-12 mt-1 mb-1">
						<div class="row no-gutters">
							<div class="col-sm-4 col-4">
								To
                            </div>
                            <div class="col-sm-8 col-8 input-group">
                               <div class="input-group">
                                   <input id="to"  type="text" class="form-control" autocomplete="off" name="to" value="">
                                    <span class="input-group-addon calendar-addon"><i class="fa fa-calendar" aria-hidden="true"></i> </span>
                                </div>

							</div>
						</div>
					</div>
					<div class="col-lg-1 col-md-2 col-sm-3 col-12">
						<button type="button" class="btn btn-filter btn-outline-primary">Filter</button>
					</div>
				</div>
			</div>
			<div class="card-body">
				@if(Session::has('success'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
				@endif
					<table class="table mb-0 " id="tickets-table">
						<thead>
							<th>#ID</th>
                            <!-- <th>{{ trans('messages.category') }}</th> -->
                            <th>{{ trans('messages.short_description') }}</th>
							<th>{{ trans('messages.status') }}</th>
							<th>{{ trans('messages.action') }}</th>

						</thead>
						<tbody>
                            <?php if(isset($tickets) && !empty($tickets)) {
                            foreach($tickets->tickets as $ticket) { ?>
                            <tr>
                                <td>{{$ticket->id}}</td>
                                <!-- <td>--</td> -->
                                <td>{{$ticket->subject}}</td>

                                <td>{{$ticket->status}}</td>
                                <td>
                                    <!-- <a id='view' class = 'btn btn-info view-btn-table' title='View' data-toggle='tooltip' href = '/tickets/view/{{$ticket->id}}'>V</a> -->

                                    <button class="closebtn btn btn-sm btn-outline-primary hide">Close</button>

                                    <button  class="btn btn-sm btn-outline-primary view" ticket_id = "{{$ticket->id}}">View</button>

                                    <!-- <a id='delete' delId = '{{$ticket->id}}' class = 'btn btn-danger delete-btn-table deleteTicket' title='Delete'  data-toggle='tooltip' href = '#'>D</a> -->
                                </td>
                            </tr>
                        <?php } } ?>

						</tbody>
					</table>

				<div id="chat" class="chat-section border-top-dotted mt-2 pt-4 hide">

				</div>
			</div>
		</div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('#to,#from').datepicker({
        format: 'mm-dd-yyyy',
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
    });
    $(document).on('click','.view',function() {

        ticket_id = $(this).attr('ticket_id');

        $.ajax({
            url: '/tickets/view/'+ticket_id,
            type: 'GET',
            data:{ },
            success: function(ress){
                $('#chat').html('');
                $('#chat').html(ress);
            }
        });
        
        $(".closebtn").addClass('hide');
        $(".view").removeClass('hide');

        $(this).prev(".closebtn").removeClass('hide');
        $(this).addClass('hide');
        $("#chat").show();
    });
    $(document).on('click','.closebtn',function() {
        $('#ticket_id').val('');

        $(".closebtn").addClass('hide');
        $(".view").removeClass('hide');

        $(this).next(".view").removeClass('hide');
        $(this).addClass('hide');
        $("#chat").html('');
        $("#chat").hide();
    });


    ticketTable = $('#tickets-table').DataTable({
        "responsive":true,
        "language": {
            "search": ""
        },
        "bInfo" : false,
        "bLengthChange": false,
        "columnDefs": [ {
            "targets": -1,
            "orderable": false
        } ],
    });

    $('#tickets-table_filter input').addClass('custom-form-control');
    $('#tickets-table_filter input').attr('placeholder', "Search");
    $('#tickets-table_length select').addClass("form-control db-length-input");

    $(document).on('click', '.deleteTicket', function(e) {
       var delId = $(this).attr('delId');
       var thiss = $(this);
       swal({
           title: "<?php echo trans('messages.are_you_sure');?>",
           text: "<?php echo trans('messages.perform_action');?>",
           icon: "warning",
           buttons: true,
           dangerMode: true
       })
       .then((isConfirm) => {

           if(isConfirm) {
                $.ajax({
                       url: '/tickets/destroy/'+delId,
                    type: 'GET',
                    data:{ },
                    success: function(ress){

                        if(ress) {

                            ticketTable
                            .row( thiss.parents('tr') )
                            .remove()
                            .draw(false);
                        }
                    }
                });
           }
           else{
               e.preventDefault();
           }

       });
   });
$(document).on('click', '.addReplyBtn', function() {
    var data = $('#tickets-reply-form').serialize();
    $.ajax({
        url: '/tickets/addReply',
        type: 'POST',
        data:data,
        success: function(ress){
            $('#chat').html('');
            $('#chat').html(ress);
        }
    });
});
});
</script>
@stop
