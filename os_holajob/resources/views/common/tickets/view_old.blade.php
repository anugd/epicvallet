@extends('layouts.app')
@section('content')
    <div class="tab-pane">
        <div class="card shadow">
            <div class="card-header bg-white">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ trans('messages.ticket_detail') }}</h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
            <form method="post" action="{{ route('tickets.addReply') }}" id="tickets-reply-form">
                  @csrf
                  <div class="row">
                      
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="code"> {{ trans('messages.reply') }} </label>
                                <input type="hidden" id="ticket_id" name="ticket_id" value="{{$ticket_id}}">
                                <textarea id="reply" name="reply" required class="form-control"></textarea>
                            </div>
                        </div>
                       
                      <div class="col-md-12">
                          <button type="submit" class="btn btn-primary">{{ trans('messages.submit_button') }}</button>&nbsp;&nbsp;
                          <a href="{{ route('tickets.index')}}" class="btn btn-outline-primary"> {{ trans('messages.cancel_button') }} </a>
                      </div>
                  </div>
                </form>
                
                <?php 
                if(isset($comments) && !empty($comments)) {
                    $com = array_reverse($comments->comments);
                    $users = $comments->users;
                    foreach($com as $comment) {
                        $key = array_search($comment->author_id, array_column($users, 'id'));?>
                        <div class="con">
                            <div class="col-sm-3">
                                <span class="requester">
                                    <i class="fa fa-user"></i>{{$users[$key]->name}}
                                </span>
                                <h6 class="requester">{{$users[$key]->email}}</h6>
                            </div>
                            <div class="col-sm-8">
                                <p>{{$comment->body}}</p>
                            </div>
                            <span class="time-right">{{date('Y-m-d H:i A',strtotime($comment->created_at))}}</span>
                        </div>
                <?php } } ?>
            </div>
        </div>
    </div>
<script>
$(function() {
    $('#tickets-reply-form').validate({
        errorPlacement: function(error, element) {
            if (element.hasClass('textarea')) {     
                error.insertAfter(element.next('div'));  // textarea
                element.next('div').addClass('error').removeClass('valid');
            } 
            else if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } 
            else{
                error.insertAfter(element);
            }
        }, 
        ignore:[],
        errorClass: "error",
        rules: {
            reply: {
                required: true,
            }
        },
        messages: {
            reply: {
                required: '<?php echo trans('messages.reply_required'); ?>'
            }
        },
        onsubmit: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>");
            return true;
        }
    });
});
</script>
@stop
