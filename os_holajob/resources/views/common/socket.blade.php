<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Notification</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <?php echo __('messages.test');?>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Project name</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#contact">Contact</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="../navbar/">Default</a></li>
                            <li><a href="../navbar-static-top/">Static top</a></li>
                            <li class="active"><a href="./">Fixed top <span class="sr-only">(current)</span></a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </nav>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container" style="margin-top:10px;">
        <form id="notify-form">
            <div class="row">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <div class="col-md-12">
                    <input name="servicename" id="servicename" class="form-control" type="text"  placeholder="Service Name" required/>
                </div>
            </div>
            <div class="row" style="margin-top:15px;">
                <div class="col-md-12">
                    <textarea name="servicedetails" id="servicedetails" class="form-control" placeholder="Service details" required></textarea>
                </div>
            </div>
            <div class="row" style="margin-top:15px;">
                <div class="col-md-12">
                    <input type="button" onclick="notifyuser();" class="btn btn-primary" value="Submit"/>
                </div>
            </div>

            <div class="row" style="margin-top:15px;">
                <div class="col-md-12">
                    <input type="button" onclick="acceptreq();" class="btn btn-primary" value="Accept"/> &nbsp;&nbsp;
                    <input type="button" onclick="declinereq();" class="btn btn-primary" value="Decline"/>
                    <input type="button" onclick="expertChangeLocation();" class="btn btn-primary" value="Change Location"/>

                    <input type="button" onclick="sendMessage();" class="btn btn-primary" value="Send Message"/>
                    <input type="button" onclick="closeQRCodePopUP();" class="btn btn-primary" value="Close PopUP QR"/>
                    <input type="button" onclick="paymentSuccess();" class="btn btn-primary" value="paymentSuccess"/>
                </div>
            </div>
        </form>
        <div class="notfication-list">
        </div>
    </div>
</body>
<script src='//cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.4/socket.io.min.js'></script>
<script>

var socket = io.connect('//127.0.0.1:3001/');

socket.on('connect', function () {
    socket.emit('join', { id : "amit" });
    socket.on('disconnect', function () {
        console.log('disconnected');
    });
});

function notifyuser(){
    let obj = {
        user : 567,
        address : 'test address',
        latitude : 30.326233733723203,
        longitude: 78.06488948491499,
        service :  3,
        schedule :  0,
        booking_date : '2019-05-26 09:14:40'
    }
    socket.emit('requestService', obj);
}

function closeQRCodePopUP(){
    let obj = {
        user_id : 504
    }
    socket.emit('closeQRCodePopUP', obj);
}

function paymentSuccess(){
    let obj = {
        service_id : 30
    }
    socket.emit('paymentSuccess', obj);
}

function acceptreq(){
    let obj = {
        user : 567 ,
        service : 44,
        expert : 558
    }
    socket.emit('acceptRequest',obj);
}

function sendMessage(){
    let obj = {
        sender :  504,
        receiver :  505,
        message : 'test',
        chat_id : '1',
        image : ''
    }
    socket.emit('sendMessage',obj);
}

function declinereq(){
    let obj = {
        user : 248 ,
        service :  45,
        expert : 247
    }
    socket.emit('cancelRequest', obj);
}

function expertChangeLocation(){
    let obj = {
        user_id : 505,
        lat : '30.326228330790823',
        long : '78.0649168844482',
        request_id : 45
    }
    socket.emit('expertChangeLocation', obj);
}

// socket.on('requestService', function( data ) {
// });

socket.on("sendRequest", function(data) {
    console.log(data);
});

socket.on("paymentSuccess", function(data) {
    console.log(data);
});

socket.on("closeQRCodePopUP", function(data) {
    console.log(data);
});

// socket.on("noExpert", function() {
//     alert("No expert found.");
// });

socket.on("closePopup", function() {
    alert("closePopup");
});

socket.on("joined", function() {
    alert("I am joined");
});

socket.on("joinedtest", function(data) {
    alert(data);
    console.log(data);
});


socket.on("accRequestResponse", function(data) {
   console.log(data);
});


</script>

</html>
