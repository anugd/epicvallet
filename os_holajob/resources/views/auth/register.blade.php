@extends('layouts.login')
<style>
.navbar { display:none !important;}
</style>
@section('content')
<div class="container-fluid reg-wrap">
    <div class="row justify-content-center">
        <div class="col-md-6 p-0">
            <div class="lft-bg" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
                <a href="/">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" viewBox="0 0 77.327 91.256"><defs></defs><g transform="translate(507.878 -5556.753)"><g transform="translate(-507.878 5556.753)"><path fill="#727272" d="M1495.877,315.485v20.627a19.056,19.056,0,0,1,15.18-1.737,18.372,18.372,0,0,1,12.129,10.878,18.975,18.975,0,0,1-8.044,24.031v-.71q0-8.226,0-16.452a8.826,8.826,0,0,0-1.11-3.945,9.724,9.724,0,0,0-2.633-3.116,9.153,9.153,0,0,0-3.081-1.581,9.648,9.648,0,0,0-4.915-.19,8.784,8.784,0,0,0-3.041,1.234,12.051,12.051,0,0,0-2.54,2.288,9.48,9.48,0,0,0-1.62,3.124,9.7,9.7,0,0,0-.342,3.362c-.015,5.1-.007,10.2-.007,15.3v.773a17.557,17.557,0,0,1-5.526-4.944,21.876,21.876,0,0,1-4.1-11.423c-.023-10.8,0-21.594-.042-32.39a1.686,1.686,0,0,1,1.172-1.8c1.968-.941,3.278-1.428,4.954-2.108C1493.552,316.254,1494.6,315.866,1495.877,315.485Z" transform="translate(-1466.867 -300.187)"/><path fill="#1f5ba8" d="M1689.966,258.776a4.949,4.949,0,1,1-4.934-4.946A4.958,4.958,0,0,1,1689.966,258.776Z" transform="translate(-1612.639 -253.83)"/><path fill="#1f5ba8" d="M1418.021,323.733l-.646,20.748c-.005.3.341,7.249.38,7.545a26.23,26.23,0,0,0,6.445,15.19c5.2,5.785,9.375,9.008,17.552,10.655,10.079,2.031,19.907-3.849,21.672-5.123s9.675-6.874,11.652-16.835a35.152,35.152,0,0,0,.789-6.618c0-3.142.005-38.89.005-38.89h9.641v31.2l-.04,9.032s-.3,7.182-2.188,11.377-5.21,11.056-10.981,15.874-11.8,7.477-15.18,8.218-13.391,3.046-22.215-.062a39.129,39.129,0,0,1-18.433-12.791c-6.345-7.979-7.8-13.6-8.131-23.151-.426-12.3,4.063-19.112,6.386-22.242A46.873,46.873,0,0,1,1418.021,323.733Z" transform="translate(-1408.311 -296.368)"/></g></g></svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" viewBox="0 0 197.08 35.856"><defs></defs><g transform="translate(-156.312 -109.48)"><path fill="#1f5ba8" d="M21.072-33.888V-9.312a9.6,9.6,0,0,1-2.544,7.1A9.536,9.536,0,0,1,11.472.336,10.278,10.278,0,0,1,3.792-2.5Q1.008-5.328,1.008-10.512H6.432a6.971,6.971,0,0,0,1.176,4.32,4.274,4.274,0,0,0,3.624,1.536,4.024,4.024,0,0,0,3.312-1.3A5.15,5.15,0,0,0,15.6-9.312V-33.888Zm18.48,7.1a14.5,14.5,0,0,1,6.912,1.632,11.715,11.715,0,0,1,4.8,4.7,14.444,14.444,0,0,1,1.728,7.2A14.546,14.546,0,0,1,51.264-6a11.715,11.715,0,0,1-4.8,4.7A14.5,14.5,0,0,1,39.552.336,14.36,14.36,0,0,1,32.664-1.3a11.868,11.868,0,0,1-4.8-4.728,14.42,14.42,0,0,1-1.752-7.224,14.286,14.286,0,0,1,1.752-7.2,11.909,11.909,0,0,1,4.8-4.7A14.36,14.36,0,0,1,39.552-26.784Zm0,4.752a8.045,8.045,0,0,0-3.888.96,7.073,7.073,0,0,0-2.856,2.952,10.105,10.105,0,0,0-1.08,4.872,10.074,10.074,0,0,0,1.08,4.9,7.1,7.1,0,0,0,2.856,2.928,8.045,8.045,0,0,0,3.888.96,8.045,8.045,0,0,0,3.888-.96A7.1,7.1,0,0,0,46.3-8.352a10.074,10.074,0,0,0,1.08-4.9A10.105,10.105,0,0,0,46.3-18.12a7.073,7.073,0,0,0-2.856-2.952A8.045,8.045,0,0,0,39.552-22.032ZM73.44-26.784a11.727,11.727,0,0,1,6.168,1.632,11.154,11.154,0,0,1,4.248,4.7,15.912,15.912,0,0,1,1.536,7.2A16.027,16.027,0,0,1,83.856-6a11.154,11.154,0,0,1-4.248,4.7A11.727,11.727,0,0,1,73.44.336,10.463,10.463,0,0,1,67.3-1.44a9.288,9.288,0,0,1-3.552-4.7V0H58.272V-35.52h5.472V-20.3a9.288,9.288,0,0,1,3.552-4.7A10.463,10.463,0,0,1,73.44-26.784Zm-1.68,4.8a7.947,7.947,0,0,0-4.128,1.08,7.634,7.634,0,0,0-2.88,3.072A9.64,9.64,0,0,0,63.7-13.248,9.6,9.6,0,0,0,64.752-8.64a7.672,7.672,0,0,0,2.88,3.048,7.947,7.947,0,0,0,4.128,1.08A7.694,7.694,0,0,0,77.616-6.84a8.934,8.934,0,0,0,2.208-6.408,8.952,8.952,0,0,0-2.208-6.384A7.654,7.654,0,0,0,71.76-21.984Z" transform="translate(268 145)"/><path fill="#727272" d="M30.24-33.888V0H24.72V-15.216H8.784V0H3.312V-33.888H8.784V-19.68H24.72V-33.888ZM61.44-14.5a12.7,12.7,0,0,1-.192,2.352H40.9q.144,4.08,2.256,6.048a7.383,7.383,0,0,0,5.232,1.968A7.612,7.612,0,0,0,53.016-5.52a5.747,5.747,0,0,0,2.328-3.7H61.2a11.384,11.384,0,0,1-2.208,4.944A11.412,11.412,0,0,1,54.624-.888a14.314,14.314,0,0,1-6,1.224A13.806,13.806,0,0,1,41.832-1.3,11.608,11.608,0,0,1,37.2-6a14.881,14.881,0,0,1-1.68-7.248,14.776,14.776,0,0,1,1.68-7.2,11.608,11.608,0,0,1,4.632-4.7,13.806,13.806,0,0,1,6.792-1.632,13.8,13.8,0,0,1,6.792,1.608A11.187,11.187,0,0,1,59.88-20.76,12.678,12.678,0,0,1,61.44-14.5Zm-5.52-.144a8.132,8.132,0,0,0-.888-4.344,6.173,6.173,0,0,0-2.688-2.592,8.3,8.3,0,0,0-3.72-.84,7.683,7.683,0,0,0-5.376,1.968A8.04,8.04,0,0,0,40.9-14.64ZM72.192-35.52V0H66.72V-35.52Zm17.232,8.736a10.463,10.463,0,0,1,6.144,1.776,9.558,9.558,0,0,1,3.6,4.7v-6.144h5.472V0H99.168V-6.144a9.558,9.558,0,0,1-3.6,4.7A10.463,10.463,0,0,1,89.424.336,11.727,11.727,0,0,1,83.256-1.3,11.154,11.154,0,0,1,79.008-6a16.027,16.027,0,0,1-1.536-7.248,15.912,15.912,0,0,1,1.536-7.2,11.154,11.154,0,0,1,4.248-4.7A11.727,11.727,0,0,1,89.424-26.784Zm1.68,4.8a7.617,7.617,0,0,0-5.832,2.328,8.994,8.994,0,0,0-2.184,6.408A8.994,8.994,0,0,0,85.272-6.84,7.617,7.617,0,0,0,91.1-4.512a7.947,7.947,0,0,0,4.128-1.08,7.672,7.672,0,0,0,2.88-3.048,9.6,9.6,0,0,0,1.056-4.608,9.64,9.64,0,0,0-1.056-4.584,7.634,7.634,0,0,0-2.88-3.072A7.947,7.947,0,0,0,91.1-21.984Z" transform="translate(153 145)"/></g></svg>
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="inner-form-wrap signup-form-wrap">
                <div class="header mb-4">
                    @if (Route::has('login'))
                    <a class="disable mr-5 h1" href="{{ route('login') }}">{{ __('Sign In') }}</a>
                    @endif
                    <span class="h1">{{ __('Sign Up') }}</span>
                </div>
                <ul class="nav nav-pills nav-pills-custom mb-3">
                    <li class="nav-item">
                        <a class="nav-link @if(old('user_type_id') == '' || old('user_type_id') == 1) {{'active'}} @endif" data-toggle="pill" href="#customer">Customer</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-center  @if(old('user_type_id') == 3)  {{'active'}} @endif" data-toggle="pill" href="#company">Company</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-right @if(old('user_type_id') == 2)  {{'active'}} @endif" data-toggle="pill" href="#expert">Job Seeker</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade @if(old('user_type_id') == '' || old('user_type_id') == 1) {{'show active'}} @endif" id="customer">
                        <form method="POST" action="{{ route('register') }}" id="signup-customer">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" name="user_type_id" value="1">
                                <input id="firstname" placeholder="{{ __('First Name') }}" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}"  autofocus>

                                @if ($errors->has('firstname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">

                                <input id="lastname" placeholder="{{ __('Last Name') }}" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}"  autofocus>

                                @if ($errors->has('lastname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lastname') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="mobile_number" placeholder="{{ __('Phone number') }}" type="text" class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" name="mobile_number" value="{{ old('mobile_number') }}"  maxlength="15">
                                @if ($errors->has('mobile_number'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile_number') }}</strong>
                                </span>
                                @endif
                            </div>



                            <div class="form-group">

                                <input id="username" placeholder="{{ __('Username') }}" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}"  autofocus>
                                @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">

                                <input id="email" placeholder="{{ __('Email Address') }}" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" >
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">

                                <input id="password" placeholder="{{ __('Password') }}" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input type="checkbox" class="form-control{{ $errors->has('terms') ? ' is-invalid' : '' }} form-check-input form-check-input-custom" name="terms" id="terms">
                                    <label for="terms" >
                                        I accept and agree to the <a href="#" class="text-light"><u>Terms & Conditions</u></a> and <a href="#" class="text-light"><u>Privacy Policy</u></a>
                                    </label>
                                    @if ($errors->has('terms'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="margin-left: -18px!important;">{{ trans('auth.required_terms') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-md mt-4">
                                        {{ __('Sign Up') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-------------- Company Registration  ------------------->
                     					<div class="tab-pane fade @if(old('user_type_id') == 3)  {{'active show'}} @endif" id="company">
                    <form method="POST" action="{{ route('company-register') }}" id="signup-company">
                        @csrf

                        <!-- @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                        @endforeach -->

                        @if (Session::has('message2'))
                        <div class="alert alert-danger alert-dismissible fade show">
                            <p class="mb-0 text-center text-uppercase">{{ Session::get('message2') }}<p>
                            </div>
                            @endif
                            <div class="form-group">
                                <input type="hidden" name="user_type_id" value="3">
                                <input id="company_name" placeholder="{{ __('Company Name') }}" type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{ old('company_name') }}"  autofocus>

                                @if ($errors->has('company_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('company_name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">

                                <input id="mobile_number2" placeholder="{{ __('Phone number') }}" type="text" class="form-control{{ $errors->has('mobile_number2') ? ' is-invalid' : '' }}" name="mobile_number2" value="{{ old('mobile_number2') }}"  maxlength="15" >

                                @if ($errors->has('mobile_number2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile_number2') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="company_address1" placeholder="{{ __('Company Address') }}" type="text" class="form-control {{ $errors->has('company_address1') ? ' is-invalid' : '' }}" name="company_address1" value="{{ old('company_address1')}}">
                                @if ($errors->has('company_address1'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('company_address1') }}</strong>
                                </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <input id="company_address2" placeholder="{{ __('Company Address 1') }}" type="text" class="form-control {{ $errors->has('company_address2') ? ' is-invalid' : '' }}" name="company_address2" value="{{ old('company_address2') }}">
                                @if ($errors->has('company_address2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('company_address2') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">

                                <input id="username2" placeholder="{{ __('Username') }}" type="text" class="form-control{{ $errors->has('username2') ? ' is-invalid' : '' }}" name="username2" value="{{ old('username2') }}" autofocus>

                                @if ($errors->has('username2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username2') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="email2" placeholder="{{ __('Email') }}" type="text" class="form-control{{ $errors->has('email2') ? ' is-invalid' : '' }}" name="email2" value="{{ old('email2') }}" >
                                @if ($errors->has('email2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email2') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="password2" placeholder="{{ __('Password') }}" type="password" class="form-control{{ $errors->has('password2') ? ' is-invalid' : '' }}" name="password2" >
                                @if ($errors->has('password2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password2') }}</strong>
                                </span>
                                @endif
                            </div>



                            <div class="form-group">
                                <div class="form-check">
                                    <input type="checkbox" class="form-control{{ $errors->has('terms2') ? ' is-invalid' : '' }} form-check-input form-check-input-custom" name="terms2" id="terms2">
                                    <label for="terms2" >
                                        I accept and agree to the <a href="#" class="text-light"><u>Terms & Conditions</u></a> and <a href="#" class="text-light"><u>Privacy Policy</u></a>
                                    </label>
                                    @if ($errors->has('terms2'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ trans('auth.required_terms') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-md mt-4">
                                        {{ __('Sign Up') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--- Expert Registration tab-->
                    <div class="tab-pane fade @if(old('user_type_id') == 2)  {{'active show'}} @endif" id="expert">

                        @if (Session::has('message3'))
                        <div class="alert alert-danger alert-dismissible fade show">
                            <p class="mb-0 text-center text-uppercase">{{ Session::get('message3') }}<p>
                            </div>
                            @endif
                            <form method="POST" action="{{ route('expert-register') }}" id="expert-register">
                                @csrf
                                <div class="form-group">
                                    <input type="hidden" name="user_type_id" value="2">
                                    <input id="first_name3" placeholder="{{ __('First Name') }}" type="text" class="form-control{{ $errors->has('first_name3') ? ' is-invalid' : '' }}" name="first_name3" value="{{ old('first_name3') }}"  autofocus>
                                    @if ($errors->has('first_name3'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name3') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="user_type_id" value="2">
                                    <input id="last_name3" placeholder="{{ __('Last Name') }}" type="text" class="form-control{{ $errors->has('last_name3') ? ' is-invalid' : '' }}" name="last_name3" value="{{ old('last_name3') }}"  autofocus>
                                    @if ($errors->has('last_name3'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name3') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="gender" class="d-block">Gender</label>
                                    <div class="form-check-inline">
                                        <label class="form-check-label" for="gender">
                                            <input type="radio" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }} form-check-input form-check-input-custom d-inline h-auto"  name="gender" value="M" checked>Male
                                        </label>
                                    </div>
                                    <div class="form-check-inline">
                                        <label class="form-check-label" for="gender">
                                            <input type="radio" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }} form-check-input form-check-input-custom d-inline h-auto" name="gender" value="F">Female
                                        </label>
                                    </div>
                                    <div class="form-check-inline">
                                        <label class="form-check-label" for="gender">
                                            <input type="radio" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }} form-check-input form-check-input-custom d-inline h-auto" name="gender" value="O">Other
                                        </label>
                                    </div>
                                    @if ($errors->has('gender'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ trans('auth.errGender') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group">
                                            <input id="dob3" autocomplete="off" onkeydown="return false" placeholder="{{ __('Date of birth') }}" type="text" class="form-control dob-picker {{ $errors->has('dob3') ? ' is-invalid' : '' }}" name="dob3" value="{{ old('dob3') }}">
                                            <span class="input-group-addon calendar-addon"><i class="fa fa-calendar" aria-hidden="true"></i> </span>
                                        </div>
                                    </div>
                                    @if ($errors->has('dob3'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dob3') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input id="home_address1" placeholder="{{ __('Address Line 1') }}" type="text" class="form-control {{ $errors->has('home_address1') ? ' is-invalid' : '' }}" name="home_address1" value="{{ old('home_address1')}}">
                                    @if ($errors->has('home_address1'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('home_address1') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <input id="home_address" placeholder="{{ __('Address Line 2') }}" type="text" class="form-control {{ $errors->has('home_address2') ? ' is-invalid' : '' }}" name="home_address2" value="{{ old('home_address2') }}">
                                    @if ($errors->has('home_address2'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('home_address2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input id="city" placeholder="{{ __('City') }}" type="text" class="form-control {{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}">
                                    @if ($errors->has('city'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input id="post_code" placeholder="{{ __('Post Code') }}" type="text" class="form-control {{ $errors->has('post_code') ? ' is-invalid' : '' }}" name="post_code" value="{{ old('post_code') }}">
                                    @if ($errors->has('post_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('post_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input id="mobile_number3" placeholder="{{ __('Phone number') }}" type="text" class="form-control{{ $errors->has('mobile_number3') ? ' is-invalid' : '' }}" name="mobile_number3" value="{{ old('mobile_number3') }}"  maxlength="15" >
                                    @if ($errors->has('mobile_number3'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile_number3') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input id="email3" placeholder="{{ __('Email Address') }}" type="text" class="form-control{{ $errors->has('email3') ? ' is-invalid' : '' }}" name="email3" value="{{ old('email3') }}" >
                                    @if ($errors->has('email3'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email3') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input id="password3" placeholder="{{ __('Password') }}" type="password" class="form-control{{ $errors->has('password3') ? ' is-invalid' : '' }}" name="password3" >
                                    @if ($errors->has('password3'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password3') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-control{{ $errors->has('eTerms1') ? ' is-invalid' : '' }} form-check-input form-check-input-custom" name="eTerms1" id="eTerms1">
                                        <label for="eTerms1">
                                            I accept and agree to the <a href="#" class="text-light"><u>Terms & Conditions</u></a> and <a href="#" class="text-light"><u>Privacy Policy</u></a>
                                        </label>

                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-control{{ $errors->has('eTerms2') ? ' is-invalid' : '' }} form-check-input form-check-input-custom" name="eTerms2" id="eTerms2">
                                        <label for="eTerms2">
                                            By signing up you consent Hela Job to conduct a full background check and ID verification on you.
                                        </label>

                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-control{{ $errors->has('eTerms3') ? ' is-invalid' : '' }} form-check-input form-check-input-custom" name="eTerms3" id="eTerms3">
                                        <label for="eTerms3">
                                            By signing up to Hela Job I understand and agree that I must be registered as self employed and declare my tax information on my own. <a href="#" class="text-light"><u>Click here to find out more.</u></a>
                                        </label>

                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-control{{ $errors->has('eTerms4') ? ' is-invalid' : '' }} form-check-input form-check-input-custom" name="eTerms4" id="eTerms4">
                                        <label for="eTerms4">
                                            I confirm that I do not have any unspent convictions.
                                        </label>

                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-control{{ $errors->has('eTerms5') ? ' is-invalid' : '' }} form-check-input form-check-input-custom" name="eTerms5" id="eTerms5">
                                        <label for="eTerms5" id="eterms5Label">
                                            I would like to receive communications including job offers, company updates and newsletters (optional).
                                        </label>
                                        @if ($errors->has('eTerms1') || $errors->has('eTerms2') || $errors->has('eTerms3') || $errors->has('eTerms4')|| $errors->has('eTerms5'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ trans('auth.errAllterms') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary btn-md mt-4">
                                            {{ __('Sign Up') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
$(function() {

    $('#signup-customer').validate({
      errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
      rules: {
            firstname: {
                required: true,
            },
            lastname: {
                required: true,
            },
            mobile_number : {
                required: true,
                intlTelNumber : true
            },
            username:{
                required: true,
            },
            email: {
                required: true,
                email :true
            },
            password:{
                required: true,
            },
            terms : {
                required: true,
            }
      },
      messages: {
        firstname : "<?php echo trans('messages.customer_firstname_required');?>",
        lastname : "<?php echo trans('messages.customer_lastname_required');?>",
        mobile_number : "<?php echo trans('messages.customer_mobile_required');?>",
        username : "<?php echo trans('messages.customer_username_required');?>",
        password : "<?php echo trans('messages.customer_password_required');?>",
        terms :"<?php echo trans('messages.terms_and_conditions');?>",
      },
      submitHandler: function(form)
      {
          $(form).find(':submit').attr("disabled", true);
          $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
          return true;
      }
    });

    $('#signup-company').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            company_name: {
                required: true,
            },
            mobile_number2: {
                required: true,
                intlTelNumber : true
            },
            company_address1 : {
                required: true,
            },
            company_address2:{
                required: true,
            },
            username2: {
                required: true,
            },
            email2:{
                required: true,
                email :true
            },
            password2 : {
                required: true,
            },
            terms2 : {
                required: true,
            }
        },
        messages: {
            company_name : "<?php echo trans('messages.company_name_required');?>",
            mobile_number2 :"<?php echo trans('messages.company_mobile_required');?>",
            company_address1 : "<?php echo trans('messages.company_address_required');?>",
            company_address2 : "<?php echo trans('messages.company_address2_required');?>",
            username2 : "<?php echo trans('messages.company_username_required');?>",
            password2 : "<?php echo trans('messages.company_password_required');?>",
            terms2 :"<?php echo trans('messages.terms_and_conditions');?>",
            email2 :"<?php echo trans('messages.expert_email_required');?>",
        },
        submitHandler: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            return true;
        }
    });

    $('#expert-register').validate({
        errorPlacement: function(error, element) {
            if (element.attr("name") == "eTerms1" || element.attr("name") == "eTerms2" || element.attr("name") == "eTerms3"  || element.attr("name") == "eTerms4"  || element.attr("name") == "eTerms5")
            {
                error.insertAfter("#eterms5Label");
            }
            else{
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        },
        rules: {
            first_name3 : {
                required: true,
            },
            last_name3 :{
                required: true,
            },
            gender :{
                required: true,
            },
            dob3 :{
                required: true,
            },
            home_address1 :{
                required: true,
            },
            home_address2 : {
                required: true,
            },
            city :{
                required: true,
            },
            post_code :{
                required: true,
                alphanumeric: true,
                maxlength: 6
            },
            mobile_number3 :{
                required: true,
                intlTelNumber : true
            },
            email3 :{
                required: true,
                email:true
            },
            password3 :{
                required: true,
            },
            eTerms1 :{
                required: true,
            },
            eTerms2 :{
                required: true,
            },
            eTerms3 :{
                required: true,
            },
            eTerms4 :{
                required: true,
            },
            eTerms5 :{
                required: true,
            },
        },
        groups: {
            TermsandCond: "eTerms1 eTerms2 eTerms3 eTerms4 eTerms5"
        },
        messages: {
            first_name3 : '<?php echo trans('messages.expert_firstname_required');?>',
            last_name3 : '<?php echo trans('messages.expert_lastname_required');?>',
            gender : "<?php echo trans('messages.expert_gender_required');?>",
            dob3 : "<?php echo trans('messages.expert_dob_required');?>",
            home_address1 : "<?php echo trans('messages.expert_address_required');?>",
            home_address2 :"<?php echo trans('messages.expert_address2_required');?>",
            city : "<?php echo trans('messages.expert_city_required');?>",
            post_code : {
                required: "<?php echo trans('messages.expert_postcode_required');?>",
                alphanumeric: "<?php echo trans('messages.expert_postcode_max_length_required');?>"
            },
            password3 : "<?php echo trans('messages.expert_password_required');?>",
            eTerms1 : "<?php echo trans('messages.all_terms_conditions');?>",
            eTerms2 : "<?php echo trans('messages.all_terms_conditions');?>",
            eTerms3 : "<?php echo trans('messages.all_terms_conditions');?>",
            eTerms4 : "<?php echo trans('messages.all_terms_conditions');?>",
            eTerms5 : "<?php echo trans('messages.all_terms_conditions');?>"
        },
        submitHandler: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            return true;
        }
    });

    jQuery.validator.addMethod("intlTelNumber", function(value, element) {
        phone_number = value.replace( /\s+/g, "" );
    return this.optional( element ) || phone_number.length > 11 &&
        phone_number.match(/^\+[1-9]{1}[0-9]{3,14}$/);
    }, "<?php echo trans('messages.valid_international_number');?>");


    $('#dob3').datepicker({
          format: 'yyyy-mm-dd',
          weekStart: 0,
          calendarWeeks: true,
          autoclose: true,
          todayHighlight: true,
          orientation: "auto"
    })


});
</script>
@endsection
