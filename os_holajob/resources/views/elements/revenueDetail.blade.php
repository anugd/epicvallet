<section class="invoice">
	<!-- title row -->
	<div class="col-md-12">
		<div class="col-xs-12">
			<h2 class="page-header">
			<small class="pull-right">Date: {{@$services->created_at}}</small>
			</h2>
		</div>
		<!-- info row -->
		<div class="row">

			<div class="col-sm-6 invoice-col">
				<span class="text-info">Expert</span>
				<address>
					<strong>{{@$services->expert->fullname}}</strong><br>
					Phone: {{@$services->expert->mobile_number}}<br>
					Email: {{@$services->expert->email}}
				</address>
			</div>
			<!-- /.col -->
		</div>

		<div class="row">
			<div class="col-xs-12 table-responsive">
				<table class="table servicehistory-table table-striped table-bordered table-hover">
				<thead>
				<tr>
					<th>ID(#)</th>
					<th>Duration (Hours)</th>
					<th>Rate / Hours</th>
					<th>Sub Total</th>
					<th>Fee</th>
					<th>Total Price</th>
					<th>Payment Method</th>
					<th>Txn Id</th>
				</tr>
				</thead>
				<tbody>
				<?php if($services) { ?>
				<tr>
					<td>{{@$services->id}}</td>
					<td>{{@$services->job_duration}}</td>
					<td>&pound;{{@$services->per_hour_rate}}</td>
					<td>&pound;{{@$services->sub_total}}</td>
					<td>&pound;{{@$services->service_fee}}</td>
					<td>&pound;{{@$services->total_price}}</td>
					<td><?php echo ($services->payment_method == 0) ? "Wallet" : "Stripe";?></td>
					<td>{{@$services->stripe_txn_id}}</td>
				</tr>
				<?php } else{ ?>
					<tr>
						<td colspan="9" class="no-payment">No payemnts yet!! </td>
				</tr>
				<?php }  ?>
				</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
