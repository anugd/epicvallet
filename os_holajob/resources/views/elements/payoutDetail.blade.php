<section class="invoice">
	<!-- title row -->
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header">
			<i class="fa fa-globe"></i> {{@$payouts->payout_request_id}}
			<small class="pull-right">Date: {{@$payouts->created_at}}</small>
			</h2>
		</div>
	</div>
	<!-- info row -->
	<div class="row invoice-info">
		<div class="col-sm-4">
			<span class="text-info">Job Seeker</span>
			<address>
			<strong>{{@$payouts->User->fullname}}</strong><br>
			Phone: {{@$payouts->User->mobile_number}}<br>
			Email: {{@$payouts->User->email}}
			</address>
		</div>
	</div>
	<!-- /.row -->

	<!-- Table row -->
	<div class="row">
		<div class="col-xs-12 table-responsive">
			<table id="payout-table" class="table mb-0">
				<thead>
					<!-- <th>Payot Request Id</th> -->
					<th>Transaction id</th>
					<th>Amount</th>
					<th>Status</th>
					<th>Date</th>
				</thead>
				<tbody>
					<!-- <td>{{@$payouts->payout_request_id}}</td> -->
					<td>{{@$payouts->stripe_txn_id}}</td>
					<td>£{{@$payouts->amount}}</td>
					<td>
						<?php
						if($payouts->status == '1'){
							echo "Success";
						}else{
							echo "Failed";
						}
						?>
					</td>
					<td>{{date("d M y h:i A" ,strtotime($payouts->created_at))}}</td>
				</tbody>
			</table>
		</div>
	</div>
</section>
