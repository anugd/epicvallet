@if( Auth::user()->user_type_id == "1" )
<div class="">
    <ul class="nav nav-pills nav-pills-custom nav-pills-custom--acount my-top-3">
        <li class="nav-item" >
            <a class="nav-link {{ Request::segment(2) == 'profile' ? 'active' : '' }}" href="/account/profile/customer">{{ trans('messages.my_profile') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-center {{ Request::segment(1) == 'address' ? 'active' : '' }}"   href="/address/customer">{{ trans('messages.address') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-center {{ Request::segment(2) == 'change-password' ? 'active' : '' }}"   href="/account/change-password/customer">Change password</a>
        </li>
    </ul>
</div>
@else
<div class="">
    <ul class="nav nav-pills nav-pills-custom nav-pills-custom--acount my-top-3">
        <li class="nav-item" >
            <a class="nav-link {{ Request::segment(2) == 'profile' ? 'active' : '' }}" href="/account/profile/expert">{{ trans('messages.my_profile') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-center {{ Request::segment(1) == 'address' ? 'active' : '' }}"   href="/address/expert">{{ trans('messages.address') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-center {{ Request::segment(2) == 'payments' ? 'active' : '' }}"   href="/account/payments/addBankAccount">Payments</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-center {{ Request::segment(2) == 'change-password' ? 'active' : '' }}"   href="/account/change-password/expert">Change password</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-center {{ request()->is('user/upload-document') ? 'active' : '' }}"   href="/user/upload-document">{{ trans('messages.upload_document') }}</a>
        </li>
    </ul>
</div>
@endif
