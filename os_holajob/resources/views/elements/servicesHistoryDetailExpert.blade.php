<section class="invoice">
	<!-- title row -->
	<div class="col-md-12">
		<div class="col-xs-12">
			<h2 class="page-header">
			<i class="fa fa-globe"></i> {{@$services->service->service}}(£{{$services->service->price}})
			<br>
			
			<div class="col-sm-8"><small class="pull-right">Date: {{($services->booking_date)?$services->booking_date:'--'}}</small></div>
			</h2>
		</div>
		<!-- info row -->
		<div class="row">
			<div class="col-sm-6 invoice-col">
				<span class="text-info">Customer</span>
				<address>
				<strong>{{@$services->customer->fullname}}</strong><br>

				Phone: {{@$services->customer->mobile_number}}<br>
				Email: {{@$services->customer->email}}
				</address>
			</div>
			<div class="col-sm-6 invoice-col">
				<span class="text-info">Expert</span>
				<address>
					<strong>{{@$services->expert->fullname}}</strong><br>
					Phone: {{@$services->expert->mobile_number}}<br>
					Email: {{@$services->expert->email}}
				</address>
			</div>
			<!-- /.col -->
		</div>
		<div class="row">
			<h5 class="text-info col-md-12">Job Details</h5>
			<div class="col-sm-6">
				<b>Location:</b> {{@$services->address}}<br>
				<b>Service Name:</b> {{@$services->service->service}}<br>
				<!-- <b>Job Start Time:</b> {{@$services->service_start_time}}<br>
				<b>Job End Time:</b> {{@$services->service_end_time}}<br> -->
			</div>
			<div class="col-sm-6">
				<!-- <b>Price Per Hour:</b> {{@$services->service->price}}<br> -->
				<b>Duration:</b> {{ (!empty($services->ServicePayment->job_duration)) ? $services->ServicePayment->job_duration : '0'}} Hours<br>
				<b>Discount:</b> {{ (!empty($services->voucher_discount)) ? $services->voucher_discount : '0'}}<br>
				<b>Payment Method:</b> {{ (!empty($services->ServicePayment->payment_method)) ? 'Stripe' : 'Wallet'}}<br>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 table-responsive">
				<table class="table servicehistory-table table-striped table-bordered table-hover">
				<thead>
				<tr>
					<th>ID(#)</th>
					<th>Duration (Hours)</th>
					<th>Rate / Hours (£)</th>
					<th>Discount (£)</th>
					<th>Amount (£)</th>
					<!-- <th>Payment Status</th> -->
					<th>Txn Id</th>
				</tr>
				</thead>
				<tbody>
				<?php if($services->ServicePayment) { ?>
				<tr>
					<td>{{@$services->ServicePayment->id}}</td>
					<td>{{@$services->ServicePayment->job_duration}}</td>
					<td>{{@$services->ServicePayment->per_hour_rate}}</td>
					<td>{{@$services->ServicePayment->voucher_discount}}</td>
					<td>{{@$services->ServicePayment->total_price}}</td>
					<!-- <td>{{@$services->ServicePayment->payment_status}}</td> -->
					<td>{{@$services->ServicePayment->stripe_txn_id}}</td>
				</tr>
				<?php } else{ ?>
					<tr>
						<td colspan="9" class="no-payment">No payemnts yet!! </td>
				</tr>
				<?php }  ?>
				</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
