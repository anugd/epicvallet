@extends('admins.master')


@section('content')
<section class="section">
  <div class="container mt-5">
    <div class="row">
      <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
        <div class="login-brand">
            <a href="/admin" style="text-decoration:none;">Hela Job</a>
        </div>

        <div class="card card-primary">
          <div class="card-header"><h4>Forgot Password</h4></div>
            
          <div class="card-body">
            @if (Session::has('messageerror'))
                  <span class="help-block">
                      <span class="admin-loginform-error">{{ Session::get('messageerror') }}</span>
                  </span>
            @endif
            <p class="text-muted">We will send a link to reset your password</p>
            <form method="POST"  action="{{ url(config('adminlte.password_email_url', 'admin/forgot_password')) }}" id="admin-passwordreset-form">
                {!! csrf_field() !!}
              <div class="form-group  has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">Email</label>
                <input value="{{ isset($email) ? $email : old('email') }}" placeholder="{{ trans('adminlte::adminlte.email') }}" id="email" type="email" class="form-control" name="email" tabindex="1" autofocus>
              </div>
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
              @if (Session::has('message'))
                  <span class="help-block">
                      <span class="admin-reset-success">{{ Session::get('message') }}</span>
                  </span>
              @endif
              
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                  Forgot Password
                </button>
              </div>
            </form>
          </div>
        </div>
        <div class="simple-footer">
          Copyright &copy; Hela Job 2019
        </div>
      </div>
    </div>
  </div>
</section>
@push('scripts')
<script>
    $(function () {

        $("#admin-passwordreset-form").validate({
          errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                email: {
                    required: true,
                    email:true
                },
            },
            messages : {
                email :{
                   required : "<?php echo trans('messages.email_required');?>"
                }
            }
      });

      $(".email").on('keyup',function(){
          $(".help-block").html("");
      })

    });
</script>
@endpush
@endsection
