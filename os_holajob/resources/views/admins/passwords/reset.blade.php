@extends('admins.master')


@section('content')
<section class="section">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                    Hela Job
                </div>

                <div class="card card-primary">
                    <div class="card-header"><h4>Reset Password</h4></div>

                    <div class="card-body">
                        <p class="text-muted">We will send a link to reset your password</p>
                        <form id="admin-passwordupdate-form" action="{{ url(config('adminlte.password_reset_url', 'admin/reset_password')) }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label for="email">Email</label>
                                <input type="hidden" value="{{ $user_id }}" name="user_id"/>
                                <input id="email" type="email" class="form-control" name="email" tabindex="1" required autofocus>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label for="password">New Password</label>
                                <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password" tabindex="2" required>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                <label for="password_confirmation">Confirm Password</label>
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" tabindex="2" required>
                                @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                                    Reset Password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="simple-footer">
                    Copyright &copy; Hela Job 2019
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script>
$(function () {

    $("#admin-passwordupdate-form").validate({
        errorPlacement: function(error, element) {
            if(element.siblings('.glyphicon').length) {
                error.insertAfter(element.siblings('.glyphicon'));
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            email: {
                required: true,
                email:true
            },
            password :{
                required: true,
                minlength: 8
            },
            password_confirmation : {
                required: true,
                minlength: 8,
                equalTo : "#password"
            }
        },
        messages : {
            password :{
                minlength : "The password must be at least 8 characters",
                required : "<?php echo trans('messages.password_required');?>"
            },
            password_confirmation : {
                minlength : "The password must be at least 8 characters",
                required : "<?php echo trans('messages.password_required');?>"
            }
        }
    });
});
</script>
@endpush
@endsection
