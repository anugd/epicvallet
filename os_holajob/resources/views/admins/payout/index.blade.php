@extends('admins.page')

@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Payouts</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body">
                        <div class="col-md-12 no_padd">
                            <table id="payout-table" class="table table-striped datatable">
                                <thead>
                                    <th> Expert</th>
                                    <th> Account Id</th>
                                    <th> Request Id</th>
                                    <th> Transaction id</th>
                                    <th> Amount</th>
                                    <th> Status</th>
                                    <th> Date</th>
                                    <th> Action</th>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
    $(function() {

        payouttable = $('#payout-table').DataTable({
            responsive:true,
            autoWitdh: false,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin.payouts.list') }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'user_id',defaultContent: "--" },
                {data: 'stripe_account_id',defaultContent: "--"},
                {data: 'payout_request_id',defaultContent: "--"},
                {data: 'stripe_txn_id',defaultContent: "--"},
                {data: 'amount',defaultContent: "--"},
                {data: 'status',defaultContent: "--"},
                {data: 'created_at',defaultContent: "--"},
                {data: 'action', orderable : false,searchable : false},
            ]
        });
        new $.fn.dataTable.FixedHeader( payouttable );
        $('#payout-table_filter input').addClass('datatable-default-serach');
        $('#payout-table_filter input').attr('placeholder', "Search");
        $('#payout-table_length select').addClass("form-control db-length-input");

        $(document).on('click','.cancel_payout',function(){
            swal({
            title: "<?php echo trans('messages.are_you_sure');?>",
            text: "<?php echo trans('messages.perform_action');?>",
            icon: "warning",
            buttons: true,
            dangerMode: true
            })
            .then((isConfirm) => {

                if(isConfirm) {
                    var payoutid = $(this).attr('payout');
                    var account = $(this).attr('account');

                    $(this).attr("disabled", true);
                    $(this).prepend("<i class='fa fa-spinner fa-spin'></i>   ");
                    $.ajax({
                        url: '{{ route('payout.cancel') }}',
                        type: 'POST',
                        data:{
                            'payout': payoutid ,
                            'account' : account
                        },
                        success: function(data){

                            if(data.requestStatus == "invalid"){
                                swal("Error", data.message, "error");
                            }else{
                                swal("Success", data.message, "success");
                            }
                            payouttable.draw(false);
                        }
                    });
                }
            });
        });

    });
</script>
@endpush
@endsection
