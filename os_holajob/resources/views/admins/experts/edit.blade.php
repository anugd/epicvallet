@extends('admins.page')
@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Edit Job Seeker</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Note: All fields marked with (<i class="text-danger">*</i>) are required.</h4>
                    </div>
                    <div class="card-body">
                        @if(session()->get('success'))
                            <div class="alert alert-success">
                            {{ session()->get('success') }}
                            </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form method="post" action="{{ route('admin.experts.update') }}" enctype="multipart/form-data" id="update_experts_form">
                        {{ csrf_field() }}

                        {!! Form::hidden('id', $user->id) !!}
                        <div class="row form-row">
                            <input type="file" class="expertProfileImage" id="expertProfileImage" name="expertProfileImage"  />
                            <div class="col-md-12">
                                <div class="profile-pic">
                                    @if (File::exists(public_path($user->profile_image)))
                                    @php $src = !empty($user) && !empty($user->profile_image) ? $user->profile_image : "img/download.jpeg"; @endphp
                                    @else
                                    @php $src = "img/download.jpeg"; @endphp
                                    @endif
                                    <img src="{{ asset($src)}}" width="75" height="75" class="expert-profileimage" alt="Profile Image"/>
                                    <a class="change-picture">Change Picture</a>
                                </div>
                            </div>
                        </div>
                        <div class="row form-row">
                            <div class="col-md-12">
                                <h5>Basic Information</h5>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('first_name', 'First Name<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('first_name',isset($user->first_name)?$user->first_name:"", ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('last_name', 'Last Name<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('last_name', isset($user->last_name)?$user->last_name:"", ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('username', 'Username<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('username',isset($user->username)?$user->username:"", ['class' => 'form-control','required'=>'true','disabled'=>true]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('mobile_number', 'Mobile Number<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('mobile_number',isset($user->mobile_number)?$user->mobile_number:"", ['class' => 'form-control','required'=>'true','disabled'=>true]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('email', 'Email<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('email', isset($user->email)?$user->email:"", ['class' => 'form-control','required'=>'true','disabled'=>true]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="gender" class="d-block">Gender</label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="male" name="gender" class=" custom-control-input" value="M" {{ !empty($user->Profile) && ($user->Profile->gender  == 'M') ? 'checked' : 'checked'}}>
                                        <label class="custom-control-label" for="male">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="female" name="gender" class=" custom-control-input" value="F" {{ !empty($user->Profile) && ($user->Profile->gender == 'F') ? 'checked' : ""}}>
                                        <label class="custom-control-label" for="female">Female</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="other" name="gender" class=" custom-control-input" value="O" {{ !empty($user->Profile) && ($user->Profile->gender == 'O') ? 'checked' : ""}}>
                                        <label class="custom-control-label" for="other">Other</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dob3">Date of Birth</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <span class="input-group-addon calendar-addon"><i class="fa fa-calendar" aria-hidden="true"></i> </span>
                                            </div>
                                        </div>
                                        <input id="dob3" placeholder="Date of birth" type="text" class="form-control expert-dob" autocomplete="off" name="dob3" value="{{ !empty($user->Profile) && !empty($user->Profile->dob) ? $user->Profile->dob : ""}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-row">
                            <div class="col-md-12">
                                <h5>Primary Address</h5>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('city', 'City<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('city', isset( $address->city ) ? $address->city : "", ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('state', 'State<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('state',isset( $address->state ) ? $address->state : "", ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('postal_code', 'Postal Code<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('postal_code', isset( $address->postal_code ) ? $address->postal_code : "", ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('email', 'Address Line 1<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('address_line1', isset( $address->address_line1 ) ? $address->address_line1 : "", ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('address_line2', 'Address Line 2<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('address_line2', isset( $address->address_line2 ) ? $address->address_line2 : "", ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row form-row">
                            <div class="col-md-12">
                                <h5>Other</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="about"> About </label>
                                    <input id="about" placeholder="About (70 characters)" type="text" class="form-control" name="about" value="{{ !empty($user->Profile) && !empty($user->Profile->about) ? $user->Profile->about : ""}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="about"> Available </label>
                                    <div >
                                        <label class="switch-profile" for="available">
                                            <input name="available" id="available" type="checkbox" {{ !empty($user) && ($user->available == 1) ? 'checked' : ''}}>
                                            <span class="slider-profile round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="languages"> Languages experts speak</label>
                                    <select id="languages" multiple class="form-control" name="languages[]">
                                        @foreach($userlangs as $userlang)
                                        <option value="{{ $userlang->language_id  }}" selected="selected">{{ !empty($userlang->Language ) ? $userlang->Language->language : "---" }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="skills">Specify expert skills (up to 3)</label>
                                    <select id="skills" multiple class="form-control" name="skills[]">
                                        @foreach($userservices as $userservice)
                                        <option value="{{ $userservice->service_id  }}" selected="selected">{{ !empty($userservice->Services ) ? $userservice->Services->service : "---" }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row form-row">
                            <div class="col-md-12">
                                <h5 class="text-danger my-3">Emergency Information</h5>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fullname_relation"> Full Name </label>
                                    <input id="fullname_relation" placeholder="Full Name" type="text" class="form-control" name="fullname_relation" value="{{  $user->Profile->emergency_contact_name}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="phone_relation"> Phone number </label>
                                    <input id="phone_relation" placeholder="Phone number" type="text" class="form-control" name="phone_relation" value="{{ $user->profile->emergency_contact_number }}"  maxlength="15">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="relationship"> Relationship </label>
                                    <!-- <?php //print_r($user->profile);?> -->
                                    <select name = "relationship" class="form-control">
                                        <option value=''>-- Select Relationship --</option>
                                        <?php 
                                        foreach($releationShip as $key=>$option) { ?>
                                            <option <?php echo ($key == $user->profile->emergency_contact_relationship) ? 'selected' : '';?> value={{ $key }}>{{ $option }}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                        </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <h5>Password</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                        <label for="password">New Password</label>
                                        <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password" tabindex="2">
                                        @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                        <label for="password_confirmation">Confirm Password</label>
                                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" tabindex="2">
                                        @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@push('scripts')
<script src="{{ asset('plugins/js/bootstrap-datepicker.js') }}" defer></script>
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script>
function isPasswordPresent() {
    return $('#password').val().length > 0;
}
$(function() {
    
    $('#update_experts_form').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        errorClass: "error",
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            city : {
                required: true
            },
            state : {
                required: true
            },
            postal_code : {
                required: true,
                alphanumeric: true,
                maxlength: 8
            },
            address_line1 : {
                required: true
            },
            address_line2 : {
                required: true
            },
            about: {
                required: true
            },
            languages: {
                required: true,
            },
            skills: {
                required: true,
            },
            phone_relation: {
                required: function(element)  { return $("#fullname_relation").val() != ""; },
                intlTelNumber : function(element)  { return $("#fullname_relation").val() != ""; }
            },
            relationship : {
                required: function(element)  { return $("#fullname_relation").val() != ""; }
            },
            password :{
                
                minlength: 8
            },
            password_confirmation : {
                required: {
                    depends: isPasswordPresent,
                    required: true,
                },
                minlength: 8,
                equalTo : "#password"
            }
        },
        messages: {
            first_name: {
                required: "<?php echo trans('messages.expert_firstname_required');?>"
            },
            last_name: {
                required: "<?php echo trans('messages.expert_lastname_required');?>"
            },
            city : {
                required: "<?php echo trans('messages.expert_city_required');?>"
            },
            state : {
                required: "<?php echo trans('messages.expert_state_required');?>"
            },
            postal_code : {
                required: "<?php echo trans('messages.expert_postcode_required');?>",
                maxlength : "<?php echo trans('messages.expert_postcode_max_length_required');?>",
                alphanumeric : "<?php echo trans('messages.expert_postcode_alphanumeric_required');?>"
            },
            address_line1 : {
                required: "<?php echo trans('messages.expert_address_required');?>"
            },
            address_line2 : {
                required: "<?php echo trans('messages.expert_address2_required');?>"
            },
            about : {
                required : "<?php echo trans('messages.expert_about_exists');?>"
            },
            languages : {
                required : "<?php echo trans('messages.expert_language_exists');?>"
            },
            skills : {
                required : "<?php echo trans('messages.expert_skills_required');?>"
            },
            phone_relation: {
                required : "<?php echo trans('messages.expert_valid_mobile_required');?>",
                phoneUS : "<?php echo trans('messages.expert_valid_mobile_required');?>"
            },
            relationship : {
                required : "<?php echo trans('messages.expert_relationship_required');?>"
            },
            password :{
                minlength : "The password must be at least 8 characters",
                
            },
            password_confirmation : {
                minlength : "The password must be at least 8 characters",
                required : "<?php echo trans('messages.password_required');?>",
                equalTo : "<?php echo trans('messages.new_old_password_not_matched');?>",
            }
        },
        submitHandler: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            return true;
        }
    });

    $("#languages").select2({
        placeholder: 'Select language',
        multiple :true,
        ajax: {
            url: '{{ route("admin.expert.language")}}',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $("#skills").select2({
        placeholder: 'Select Skills',
        multiple :true,
        maximumSelectionLength: 3,
        ajax: {
            url: '{{ route("admin.expert.skills")}}',
            dataType: 'json',
            delay: 250,
            multi :true,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $('.expert-dob').datepicker({
        format: 'yyyy-mm-dd',
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
    });

    $(".change-picture").click(function(){
       $("#expertProfileImage").click();
    });

    $("#expertProfileImage").change(function() {
      readURL(this);
    });

    jQuery.validator.addMethod("intlTelNumber", function(value, element) {
        phone_number = value.replace( /\s+/g, "" );
    return this.optional( element ) || phone_number.length > 11 &&
        phone_number.match(/^\+[1-9]{1}[0-9]{3,14}$/);
    }, "<?php echo trans('messages.valid_international_number');?>");

});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('.expert-profileimage').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
</script>
@endpush
@endsection
