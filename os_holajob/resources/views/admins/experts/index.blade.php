@extends('admins.page')

@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Job Seekers</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body">
                        <div class="row">
                            @if(session()->get('success'))
                                <div class="alert alert-success">
                                {{ session()->get('success') }}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 no_padd">

                                <table class="table table-striped datatable seeker-table" id="seeker-table">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="selectAll"></th>
                                            <th>Fullname</th>
                                            <th>Username</th>
                                            <th>Mobile Number</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Approval</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td colspan="8">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <span class="pull-left ">Actions:&nbsp; </span>
                                                        <select name="actions" class="actionAll form-control input-sm pull-left" 'style'=>'width:150px'>
                                                            <option value="">--Select--</option>
                                                            <option value="2">Activate</option>
                                                            <option value="3">Deactivate</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('scripts')
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>

$(document).ready(function () {
        seekertable  =  $('.seeker-table').DataTable({
        responsive:true,
        autoWitdh: false,
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ route('admin.datatable.getAllExperts') }}',
            dataSrc: 'data'
        },
        columns:  [
            {data:'checkmark',name : 'checkmark', orderable : false ,searchable: false},
            {data:'fullname',defaultContent: "--"},
            {data:'username',defaultContent: "--"},
            {data:'mobile_number',defaultContent: "--"},
            {data:'email',defaultContent: "--"},
            {data:'status',defaultContent: "--"},
            {data: 'approval', name : 'approval', orderable : false ,searchable: false,defaultContent: "--"},
            {data: 'action', name : 'action', orderable : false ,searchable: false,defaultContent: "--"},
        ]
    });
    new $.fn.dataTable.FixedHeader( seekertable );
    $('#seeker-table_filter input').addClass('datatable-default-serach');
    $('#seeker-table_filter input').attr('placeholder', "Search");
    $('#seeker-table_length select').addClass("form-control db-length-input");


/* START: SELECT ALL CHECKBOX */

$(document).on('click','#selectAll',function () {
    var status = this.checked;
    $('.checkBoxClass').each(function(){
        this.checked = status;
    });
});
/* END */

$(document).on('click','.btn-approval',function () {
    var user = $(this).attr('user');
    var status = $(this).attr('status');

    $.ajax({
            url: '{{ route('admin.user.confirmation') }}',
            type: 'POST',
            data:{
                'user':user,
                'status':status
            },
            success: function(data){
                seekertable.draw(false);
            }
    });
});



/* START: Activate Deactivate service */
$(document).on('click','.updateStatus', function(e) {

    var thiss = $(this)
    var id = $(this).attr('value')
    var status = $(this).attr('status');
    var status= (status==1)?0:1;

    checkedIds = new Array();
    checkedIds.push(id);//to keep sync with multi select

    updateStatus(status, checkedIds);

});
/* END */

//bulk action select box
$("select.actionAll").on('change', function() {

    if ($(this).val()) {
        checkedIds = new Array();
        $('.checkBoxClass:checked').each(function() {
            checkedIds.push($(this).val());
        });

        if (checkedIds.length !== 0) {

            if ($(this).val() == 2 || $(this).val()== 3){//2=active, 3=deactive
                var field = 'status';
                var status= ($(this).val()==2)?1:0;
                updateStatus(status, checkedIds);
            }
        }
        else{
            swal("Error", "<?php echo trans('messages.select_one_row');?>", "error");
        }
    }
});

//single/bulk update
function updateStatus(status, checkedIds) {
    var msgContent =  (status==1)?'activate':'deactivate';
    var msgContent_ed =  (status==1)?'activated':'deactivated';

    swal({
        title: "<?php echo trans('messages.are_you_sure');?>",
        text: "<?php echo trans('messages.perform_action');?>",
        icon: "warning",
        buttons: true,
        dangerMode: true
    })
    .then((isConfirm) => {

        if(isConfirm) {
            var tableId = '.datatable';
            $.ajax({
                    url: '{{ route('admin.experts.updateStatus') }}',
                    type: 'POST',
                    data:{ 'id':checkedIds,'status':status },
                    success: function(data){
                        seekertable.draw(false);
                        $(".actionAll").val('');
                        $("input:checkbox").prop('checked', false);
                        swal("Success", "<?php echo trans('messages.multiselect_message');?> "+msgContent_ed+".", "success");
                    }
                });
        }else{
            seekertable.draw(false);
        }
    });

    return false;
}
});
</script>
@endpush
@endsection
