    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
        <title>Dashboard &mdash; Hela Job</title>

        <link rel="stylesheet" href="{{ asset('modules/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('modules/ionicons/css/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/datetimepicker.css') }}">
        <link href="{{ asset('plugins/css/jquery.dataTables.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('modules/summernote/summernote-lite.css') }}">
        <link rel="stylesheet" href="{{ asset('modules/flag-icon-css/css/flag-icon.min.css') }}">
        <link href="{{ asset('plugins/css/bootstrap-datepicker.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/model_right_sidebar.css') }}" rel="stylesheet">
        <link href="{{ asset('css/fixedHeader_dataTables.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/responsive_dataTables.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/css/select2.min.css') }}" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet">
        <link href="{{ asset('fa/dist/css/fontawesome-iconpicker.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/admins/demo.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admins/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/style.css') }}">
    </head>

    <body>
        <div id="app">
            <div class="main-wrapper">
                <div class="navbar-bg">
                </div>

                @include('admins.partials.header')

                @include('admins.partials.sidebar')

                <div class="main-content">
                    @yield('content')
                </div>
                <footer class="main-footer">
                    <div class="footer-left text-right" style="color:#1f5ba8">
                        Copyright &copy; {{date('Y')}} Hela Job
                    </div>
                </footer>
            </div>
        </div>

        <script src="{{ asset('modules/jquery.min.js') }}"></script>
        <script src="{{ asset('js/moment.js') }}"></script>
        <script src="{{ asset('modules/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/datetimepicker.js') }}" defer></script>
        <script src="{{ asset('modules/popper.js') }}"></script>
        <script src="{{ asset('modules/tooltip.js') }}"></script>
        <script src="{{ asset('modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('modules/scroll-up-bar/dist/scroll-up-bar.min.js') }}"></script>
        <script src="{{ asset('js/admins/sa-functions.js') }}"></script>
        <script src="{{ asset('plugins/js/jquery.dataTables.min.js') }}" defer></script>
        <script  src="{{ asset('js/dataTables_fixedHeader.min.js') }}" defer></script>
        <script src="{{ asset('js/dataTables_responsive.min.js') }}" defer></script>
        <script src="{{ asset('plugins/js/select2.min.js') }}" defer></script>
        <script src="{{ asset('modules/chart.min.js') }}"></script>
        <script src="{{ asset('modules/summernote/summernote-lite.js') }}"></script>
        <script src="{{ asset('js/admins/scripts.js') }}"></script>
        <script src="{{ asset('js/admins/custom.js') }}"></script>
        <script src="{{ asset('js/admins/demo.js') }}"></script>
        @stack('scripts')

    </body>
</html>
