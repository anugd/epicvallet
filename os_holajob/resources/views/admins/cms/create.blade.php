@extends('admins.page')

@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Add CMS</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Note: All fields marked with (<i class="text-danger">*</i>) are required.</h4>
                    </div>
                    <div class="card-body">

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif

                        {!! Form::open(['route' => 'admin.cms.store', 'class' => 'form','id'=>'create_cms_form']) !!}

                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('title', 'Title<span class="text-danger">*</span>')) !!}
                            {!! Form::text('title', null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('message', 'Description<span class="text-danger">*</span>')) !!}
                            {!! Form::textarea('message', null, ['class' => 'form-control textarea','required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('status', 'Status') !!}
                            {!! Form::checkbox('status', 1, true, ["class" => "minimal"]) !!}
                        </div>

                        {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script>
$(function() {
    $('textarea').ckeditor();
    $('#create_cms_form').validate({
        errorPlacement: function(error, element) {
            if (element.hasClass('textarea')) {     
                error.insertAfter(element.next('div'));  // textarea
                element.next('div').addClass('error').removeClass('valid');
            } 
            else if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } 
            else{
                error.insertAfter(element);
            }
        },
        ignore: [],
        errorClass: "error",
        rules: {

            title: {
                required: true
            },
            message: {
                required: true
            }
        },
        messages: {
            title: {
                required: '<?php echo trans('messages.cms_title_required');?>'
            },
            message: {
                required: '<?php echo trans('messages.cms_message_required');?>'
            }
        }
    });
});
</script>
@endpush
@endsection
