@extends('admins.page')

@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Create Vouchers</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Note: All fields marked with (<i class="text-danger">*</i>) are required.</h4>
                    </div>
                    <div class="card-body">

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form method="post" action="{{ route('admin.vouchers.update', $voucher->id) }}" id="voucher-form">
                            @method('PATCH')
                            @csrf
                            <div class="row form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name"> Name<span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Name" required class="form-control" id="name" name="name" value={{$voucher->name}} />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="code"> Code <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Code" required class="form-control" id="code" name="code" value={{$voucher->code}} />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="discount_amount"> Discount (%)<span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Discount" required class="form-control" id="discount_amount" name="discount_amount" value={{$voucher->discount_amount}} />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <!-- <label for="voucher_type"> Discount type</label>
                                        <select class="form-control" id="voucher_type" required name="voucher_type" value={{$voucher->is_fixed}}>
                                            <option value=""> Please select</option>
                                            <option {{$voucher->is_fixed == 0 ? 'selected' : '' }} value="0">Percentage (%)</option>
                                            <option {{$voucher->is_fixed == 1 ? 'selected' : '' }} value="1"> Fixed</option>
                                        </select> -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="voucher_start"> Start<span class="text-danger">*</span></label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <span class="input-group-addon calendar-addon"><i class="fa fa-calendar" aria-hidden="true"></i> </span>
                                                </div>
                                            </div>
                                            <input type="text"  autocomplete="off" onkeydown="return false" class="form-control voucher-date-start" required id="voucher_start" name="voucher_start" value={{$voucher->starts_at}} />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="voucher_expiry"> Expires <span class="text-danger">*</span></label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <span class="input-group-addon calendar-addon"><i class="fa fa-calendar" aria-hidden="true"></i> </span>
                                                </div>
                                            </div>
                                            <input type="text" autocomplete="off" onkeydown="return false" class="form-control voucher-date-expiry"  required id="voucher_expiry" name="voucher_expiry" value={{$voucher->expires_at}} />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description"> Description <span class="text-danger">*</span> </label>
                                        <textarea class="form-control" placeholder="Enter Voucher Description" required id="description" name="description">{{ $voucher->description}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">Update</button> &nbsp;&nbsp;
                                    <a href="{{ route('admin.vouchers.index')}}" class="btn btn-md btn-outline-primary"> Cancel </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="{{ asset('plugins/js/bootstrap-datepicker.js') }}" defer></script>
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script>
$(function() {

    $('.voucher-date-start').datepicker({
        format: 'yyyy-mm-dd',
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.voucher-date-expiry').datepicker('setStartDate', minDate);
    });


    $('.voucher-date-expiry').datepicker({
        format: 'yyyy-mm-dd',
        weekStart: 0,
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "auto"
    }).on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('.voucher-date-start').datepicker('setEndDate', maxDate);
    });

    $('#voucher-form').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            name: {
                required: true
            },
            code: {
                required: true,
                maxlength: 5
            },
            discount_amount: {
                required: true,
                number: true
            },
            voucher_type: {
                required: true,
            },
            voucher_start: {
                required: true,
            },
            voucher_expirey: {
                required: true,
            },
            description: {
                required: true,
            },
        },
        submitHandler: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            return true;
        }
    });

});
</script>
@endpush
@endsection
