@extends('admins.page')

@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Vouchers</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}
                        </div><br />
                    @endif
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6 text-right">
                               <a class="btn btn-info pull-right" href="{{ route('admin.vouchers.create')}}"> Add voucher </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12 no_padd">
                                <table id="vouchers-table" class="table table-striped datatable ">
                                    <thead>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>description</th>
                                        <th>Discount</th>
                                        <th>Start</th>
                                        <th>Expires</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        @php ($vstatus = [1 => 'Fixed', 0 => 'Percentage%', ])
                                        @foreach($vouchers as $voucher)
                                            <tr>
                                                <td>{{!empty($voucher->name)?$voucher->name:'--'}}</td>
                                                <td>{{!empty($voucher->code)?$voucher->code:'--'}}</td>
                                                <td>{{!empty($voucher->description)?$voucher->description:'--'}}</td>
                                                <td>{{!empty($voucher->discount_amount)?$voucher->discount_amount:'--'}}</td>
                                                <td>{{!empty($voucher->starts_at) ? date("d M y",strtotime($voucher->starts_at)) : "--" }} </td>
                                                <td>{{!empty($voucher->expires_at) ? date("d M y",strtotime($voucher->expires_at)) : "--"}}</td>
                                                <td>
                                                    <a id='edit' class = 'fa fa-edit' title='Edit' data-toggle='tooltip' href="{{ route('admin.vouchers.edit',$voucher->id)}}"></a>&nbsp;
                                                    <a id='delete' class = 'fa fa-trash deleteVoucher' title='Delete' data-toggle='tooltip' delId ='{{$voucher->id}}' href = '#'></a>


                                                    <!-- <form class="inline-block-table" action="{{ route('admin.vouchers.destroy', $voucher->id)}}" method="post" id="delete-voucher-form">
                                                      @csrf
                                                      @method('DELETE')
                                                        <div class='col-sm-1'><a  href="javascript:{}" onclick="document.getElementById('delete-voucher-form').submit();" class='fa fa-trash' title='Delete' data-toggle='tooltip'></a></div>
                                                    </form> -->
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-voucher-delete">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this voucher</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="delete-voucher-confirmed">Yes</button>
        <button type="button" class="btn btn-outline-primary" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>
@push('scripts')
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
    $(function() {

    var voucherstable = $('#vouchers-table').DataTable({
        "language": {
        "search": ""
        },
        "columnDefs": [ {
            "targets": -1,
            "orderable": false
        } ],
        responsive:true,
        autoWitdh: false
    });
    new $.fn.dataTable.FixedHeader( voucherstable );
    $('#vouchers-table_filter input').addClass('datatable-default-serach');
    $('#vouchers-table_filter input').attr('placeholder', "Search");
    $('#vouchers-table_length select').addClass("form-control db-length-input");

    $(".delete-voucher").on('click', function(e) {
        var $form = $(this).closest('form');
        e.preventDefault();
        $('#confirm-voucher-delete').modal({
                backdrop: 'static',
                keyboard: false
        })
        .on('click', '#delete-voucher-confirmed', function(e) {
            $form.trigger('submit');
        });
    });
    $(document).on('click', '.deleteVoucher', function(e) {
        var delId = $(this).attr('delId');
        var thiss = $(this);
        swal({
            title: "<?php echo trans('messages.are_you_sure');?>",
            text: "<?php echo trans('messages.perform_action');?>",
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
        .then((isConfirm) => {

            if(isConfirm) {
                $.ajax({
                        url: '/admin/vouchers/destroy/'+delId,
                        type: 'GET',
                        data:{ },
                        success: function(ress){

                            if(ress) {

                                voucherstable
                                .row( thiss.parents('tr') )
                                .remove()
                                .draw(false);
                            }
                        }
                    });

            }
            else{
                e.preventDefault();
            }

        });
    });
    $('#modal-btn-no').click(function() {
        $('#confirm-voucher-delete').modal('hide');
    });

    $('#vouchers-table_filter input').addClass('datatable-default-serach');
    $('#vouchers-table_filter input').attr('placeholder', "Search");
    $('#vouchers-table_length select').addClass("form-control db-length-input");


});
</script>
@endpush
@endsection
