@extends('admins.master ')


@section('content')
<section class="section">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                    Hela Job
                </div>

                <div class="card card-primary">
                    <div class="card-header"><h4>Login</h4></div>

                    <div class="card-body">
                            <form action="{{ url(config('adminlte.login_url', 'login')) }}" id="admin-login" method="post">
                                {!! csrf_field() !!}
                                @if (Session::has('loginmessage'))
                                    <span class="help-block">
                                        <strong>{{ Session::get('loginmessage') }}</strong>
                                    </span>
                                @endif
                                @if (Session::has('message'))
                                    <span class="help-block">
                                        <span class="admin-loginform-error">{{ Session::get('message') }}</span>
                                    </span>
                                @endif
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="email" class="form-control" placeholder="Email" name="email" tabindex="1" autofocus>
                                <div class="invalid-feedback">
                                    Please fill in your email
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password" class="d-block">Password
                                    <div class="float-right">
                                        <a href="{{ url(config('adminlte.password_reset_url', 'admin/resetpassword')) }}">
                                            Forgot Password?
                                        </a>
                                    </div>
                                </label>
                                <input id="password" type="password" class="form-control" placeholder="Password" name="password" tabindex="2">
                                <div class="invalid-feedback">
                                    please fill in your password
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember">
                                    <label class="custom-control-label" for="remember">Remember Me</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-block" tabindex="4" value="Login">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="simple-footer">
                    Copyright &copy; Hela JOB 2019
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script>
    $(function () {
        $("#admin-login").validate({
          errorPlacement: function(error, element) {
                if(element.siblings('.glyphicon').length) {
                    error.insertAfter(element.siblings('.glyphicon'));
                } else {
                    error.insertAfter(element);
                }
          },
          rules: {
              email: {
                  required: true,
                  email:true
              },
              password: {
                  required: true,
              }
          },
          messages: {
              email  : {
                required : "<?php echo trans('messages.email_required');?>",
                email : "<?php echo trans('messages.invalid_email');?>"
              },
              password : {
                  required : "<?php echo trans('messages.password_required');?>",
              }

          }
      });

    });
</script>
@endpush
@endsection
