@extends('admins.page')

@section('content')

<section class="section">
    <h1 class="section-header">
        <div>Dashboard</div>
    </h1>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card card-sm-3">
                <a href="{{url('admin/services/servicesHistory')}}" >
                    <div class="card-icon bg-primary">
                        <i class="ion ion-settings"></i>
                    </div>
                </a>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Services</h4>
                    </div>
                    <div class="card-body">
                        {{$data['total_services']}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card card-sm-3">
                <a href="{{url('/admin/experts')}}">
                    <div class="card-icon bg-danger">
                        <i class="ion ion-ios-people"></i>
                    </div>
                </a>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Experts</h4>
                    </div>
                    <div class="card-body">
                        {{$data['total_experts']}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card card-sm-3">
                <a href="{{url('/admin/customers')}}">
                    <div class="card-icon bg-warning">
                        <i class="ion ion-ios-people"></i>
                    </div>
                </a>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Customers</h4>
                    </div>
                    <div class="card-body">
                        {{$data['total_customers']}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card card-sm-3">
                <a href="{{url('/admin/payouts')}}">
                    <div class="card-icon bg-success">
                        <i class="ion ion-cash"></i>
                    </div>
                </a>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Payouts</h4>
                    </div>
                    <div class="card-body">
                        £{{round($data['total_payouts'],2)}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h4>Service Request Response</h4>
                </div>
                <div class="card-body">
                    <div id="servicesAllReportChart"></div>
                    {!! $data['servicesAllChart'] !!}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h4>Payment Report</h4>
                </div>
                <div class="card-body">
                    <div id="paymentReportAllChart"></div>
                    {!! $data['paymentReportAllChart'] !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Payouts</h4>
                </div>
                <div class="card-body">
                    <div id="payoutChart"></div>
                    {!! $data['payoutChart'] !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>User Report</h4>
                </div>
                <div class="card-body">
                    <div id="userReportChart"></div>
                    {!! $data['usersChart'] !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @if(isset($data['paymentReportChart']))
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h4>Payment  Report</h4>
                </div>
                <div class="card-body">
                    <div id="paymentReportChart" ></div>
                    {!! $data['paymentReportChart'] !!}
                </div>
            </div>
        </div>
        @endif
        @if(isset($data['servicesChart']))
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h4> Service Report </div>
                    </div>
                    <div class="card-body">
                        <div id="servicesReportChart"></div>
                        {!! $data['servicesChart'] !!}
                    </div>
                </div>
            </div>
            @endif
        </div>
        @stop

        @section('css')
        <link rel="stylesheet" href="/css/admin_custom.css">
        @stop
