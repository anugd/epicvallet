@extends('admins.page')

@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Update Question</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Note: All fields marked with (<i class="text-danger">*</i>) are required.</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form method="post" action="{{ route('admin.faqs.update') }}" enctype="multipart/form-data" id="create_faq_form">
                            {{ csrf_field() }}
                            <div class="form-group col-md-8">
                                {!! Form::hidden('id', $faq->id) !!}
                                {!! htmlspecialchars_decode(Form::label('title', 'Question<span class="text-danger">*</span>')) !!}
                                {!! Form::text('title', $faq->title, ['class' => 'form-control','required'=>'true']) !!}
                            </div>
                            <div class="form-group col-md-6">
                                <label for="code"> Category <span class="text-danger">*</span></label>
                                <select name = "category" class="form-control">
                                <option value=''>-- Select Category --</option>
                                <?php 
                                foreach($category as $key=>$option) { ?>
                                    <option <?php echo ($key == $faq->category) ? 'selected' : '';?> value={{ $key }}>{{ $option }}</option>
                                <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                {!! htmlspecialchars_decode(Form::label('message', 'Answer<span class="text-danger">*</span>')) !!}
                                {!! Form::textarea('message', $faq->message, ['class' => 'form-control textarea','required'=>'true']) !!}
                            </div>
                            <div class="form-group col-md-12">
                                {!! Form::label('status', 'Status') !!}
                                {!! Form::checkbox('status', 1, true, ["class" => "minimal"]) !!}
                            </div>
                            <div class="form-group col-md-12">
                                {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script>
$(document).ready(function () {
    $('textarea').ckeditor();
});
</script>
<script>
$(function() {
    $('#create_faq_form').validate({
        errorPlacement: function(error, element) {
            if (element.hasClass('textarea')) {     
                error.insertAfter(element.next('div'));  // textarea
                element.next('div').addClass('error').removeClass('valid');
            } 
            else if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } 
            else{
                error.insertAfter(element);
            }
        },
        ignore: [],
        errorClass: "error",
        rules: {

            title: {
                required: true
            },
            category: {
                required: true
            },
            message: {
                required: true
            }
        },
        messages: {
            title: {
                required: '<?php echo trans('messages.faq_title_required');?>'
            },
            category: {
                required: "<?php echo trans('messages.faq_category_required');?>"
            },
            message: {
                required: '<?php echo trans('messages.faq_message_required');?>'
            }
        }
    });
});
</script>
@endpush
@endsection
