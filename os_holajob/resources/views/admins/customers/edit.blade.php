@extends('admins.page')
@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Edit Customer</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Note: All fields marked with (<i class="text-danger">*</i>) are required.</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                @if(session()->get('success'))
                                    <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                    </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </div>
                        </div>
                        <form method="post" action="{{ route('admin.customers.update') }}" enctype="multipart/form-data" id="update_customers_form">
                            {{ csrf_field() }}
                            {!! Form::hidden('id', $user->id) !!}
                            <div class="row form-row">
                                <input type="file" class="expertProfileImage" id="expertProfileImage" name="expertProfileImage"  />
                                <div class="col-md-12">
                                    <div class="profile-pic">
                                        @if (File::exists(public_path($user->profile_image)))
                                        @php $src = !empty($user) && !empty($user->profile_image) ? $user->profile_image : "img/download.jpeg"; @endphp
                                        @else
                                        @php $src = "img/download.jpeg"; @endphp
                                        @endif
                                        <img src="{{ asset($src)}}" width="75" height="75" class="expert-profileimage" alt="Profile Image"/>
                                        <a class="change-picture">Change Picture</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <h5>Basic Information</h5>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('first_name', 'First Name<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('first_name',isset($user->first_name)?$user->first_name:"", ['class' => 'form-control','required'=>'true']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('last_name', 'Last Name<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('last_name', isset($user->last_name)?$user->last_name:"", ['class' => 'form-control','required'=>'true']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('username', 'Username<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('username', isset($user->username)?$user->username:"", ['class' => 'form-control','required'=>'true','disabled'=>true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('mobile_number', 'Mobile Number<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('mobile_number', isset($user->mobile_number)?$user->mobile_number:"", ['class' => 'form-control','required'=>'true','disabled'=>true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('email', 'Email<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('email', isset($user->email)?$user->email:"", ['class' => 'form-control','required'=>'true','disabled'=>true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('city', 'City<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('city',isset( $address->city ) ? $address->city : "", ['class' => 'form-control','required'=>'true']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('state', 'State<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('state', isset( $address->state ) ?  $address->state : "", ['class' => 'form-control','required'=>'true']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('postal_code', 'Postal Code<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('postal_code', isset($address->postal_code) ?  $address->postal_code : "", ['class' => 'form-control','required'=>'true']) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('email', 'Address Line 1<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('address_line1', isset($address->address_line1) ?  $address->address_line1 : "", ['class' => 'form-control','required'=>'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row form-row">
                                <div class="col-md-12">
                                    <h5>Primary Address</h5>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! htmlspecialchars_decode(Form::label('address_line2', 'Address Line 2<span class="text-danger">*</span>')) !!}
                                        {!! Form::text('address_line2', isset($address->address_line2) ?  $address->address_line2 : "", ['class' => 'form-control','required'=>'true']) !!}
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row form-row">
                                <div class="col-md-12">
                                    <h5>Password</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                        <label for="password">New Password</label>
                                        <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password" tabindex="2">
                                        @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                        <label for="password_confirmation">Confirm Password</label>
                                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" tabindex="2">
                                        @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script>
function isPasswordPresent() {
    return $('#password').val().length > 0;
}
$(function() {
    
    $('#update_customers_form').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        errorClass: "error",
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            city : {
                required: true
            },
            state : {
                required: true,
            },
            postal_code : {
                required: true,
                alphanumeric: true,
                maxlength: 8
            },
            address_line1 : {
                required: true
            },
            address_line2 : {
                required: true
            },
            password :{
                
                minlength: 8
            },
            password_confirmation : {
                required: {
                    depends: isPasswordPresent,
                    required: true,
                },
                minlength: 8,
                equalTo : "#password"
            }
        },
        messages: {
            first_name: {
                required: "<?php echo trans('messages.customer_firstname_required');?>"
            },
            last_name: {
                required: "<?php echo trans('messages.customer_lastname_required');?>"
            },
            city : {
                required: "<?php echo trans('messages.customer_city_required');?>"
            },
            state : {
                required: "<?php echo trans('messages.customer_state_required');?>"
            },
            postal_code : {
                required: "<?php echo trans('messages.customer_postcode_required');?>",
                maxlength : "<?php echo trans('messages.customer_postcode_max_length_required');?>",
                alphanumeric :"<?php echo trans('messages.customer_postcode_alphanumeric_required');?>" 
            },
            address_line1 : {
                required: "<?php echo trans('messages.customer_address_required');?>"
            },address_line2 : {
                required: "<?php echo trans('messages.customer_address2_required');?>"
            },
            password :{
                minlength : "The password must be at least 8 characters",
                
            },
            password_confirmation : {
                minlength : "The password must be at least 8 characters",
                required : "<?php echo trans('messages.password_required');?>",
                equalTo : "<?php echo trans('messages.new_old_password_not_matched');?>",
            }
        }
    });

    $(".change-picture").click(function(){
       $("#expertProfileImage").click();
    });

    $("#expertProfileImage").change(function() {
      readURL(this);
    });

});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('.expert-profileimage').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
</script>
@endpush
@endsection
