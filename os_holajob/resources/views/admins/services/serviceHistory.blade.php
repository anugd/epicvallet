@extends('admins.page')
@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Service History</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}
                        </div><br />
                    @endif
                    <div class="card-header"></div>
                    <div class="card-body">
                        <div class="col-md-12 no_padd">
                          
                                <table class="table table-hover table-striped datatable" id="service-list">
                                    <thead>
                                        <tr>
                                            <th>Customer</th>
                                            <th>Service</th>
                                            <th>Expert</th>
                                            <th>Location</th>
                                            <th>Booking Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal right fade viewService-modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewServiceModalLabel">
    <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content viewServiceModal-content">
                <div class="modal-header viewServiceModal-header">
                    <h4 class="modal-title" id="viewServiceModalLabel">Service Detail</h4>
                    <button onclick="hideLoading()" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body viewServiceModal-body"></div>
            </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

@push('scripts')
<script>
$(document).ready(function () {
    var servicelist = $('#service-list').dataTable({
        responsive:true,
        autoWitdh: false,
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ route('admin.datatable.servicesHistoryData') }}',
            dataSrc: 'data'
        },
        columns: [
            {data: 'customer_fullname',defaultContent: "--"},
            {data: 'service.service',defaultContent: "--"},
            {data: 'expert_fullname',defaultContent: "--"},
            {data: 'address',defaultContent: "--"},
            {data: 'booking_date',defaultContent: "--"},
            {data: 'status',defaultContent: "--"},
            {data: 'action',defaultContent: "--", orderable : false,searchable : false}
        ]
    });
    new $.fn.dataTable.FixedHeader( servicelist );
    $('#service-list_filter input').addClass('datatable-default-serach');
    $('#service-list_filter input').attr('placeholder', "Search");
    $('#service-list_length select').addClass("form-control db-length-input");

});
   
 
$(document).on('click','.viewDetail',function() {
    var dataId = $(this).attr('dataId');
    viewDetail(dataId);
});

function viewDetail(dataId) {
    $('.modal-body').html('');
    $.ajax({
        type: "GET",
        url: "{{ route('admin.services.servicesHistoryDetailAdmin') }}",
        data: { id : dataId},
        success: function(data){

            $('.modal-body').html(data);
        }
    });

}
function hideLoading() {
        $("#viewModal").modal('hide');
    }
</script>
@endpush
@endsection
