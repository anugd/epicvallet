
@extends('admins.page')
@section('content')

<section class="section">
    <h1 class="section-header">
        <div>Dispatch Job</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Note: All fields marked with (<i class="text-danger">*</i>) are required.</h4>
                    </div>
                    <div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('admin.jobDispatchSave') }}" id="job_dispatch_form">
                        {{ csrf_field() }}
                        <div class="row">
                        <div class="form-group col-md-6">
                            <div class="form-group">
                                <label for="user_id">Customer<span class="text-danger">*</span></label>
                                <select id="user_id"  class="form-control select2" name="user_id">
                                    @foreach($data['customers'] as $customer)
                                    <option value="{{ $customer->id  }}" >
                                    <?php
                                    $name = '';
                                    if(!empty($customer->first_name) && !empty($customer->last_name)){
                                        $name = $customer->first_name.' '.$customer->last_name. ' - ';
                                    }
                                    echo ($customer->email ) ? $name.''.$customer->email : "---" ;
                                    ?>
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="form-group">
                                <label for="service_id">Service<span class="text-danger">*</span></label>
                                <select id="service_id"  class="form-control select2" name="service_id">
                                    @foreach($data['userservices'] as $userservice)
                                        <option value="{{ $userservice->service_id  }}" >
                                            {{ $userservice->service }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('expert_id', 'Job Seeker')) !!}
                            {!! Form::select('expert_id', [null=>'Please Select']+$data['experts'], null,['class' => 'form-control select2']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('booking_date', 'Time<span class="text-danger">*</span>')) !!}
                            {!! Form::text('booking_date', '', ['class' => 'form-control','required'=>'true']) !!}
                        </div>

                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('address', 'Location<span class="text-danger">*</span>')) !!}
                            {!! Form::text('address', '', ['id'=>'autocomplete','class' => 'form-control', 'required'=>'true']) !!}
                            <input type="hidden" id="latitude" name="latitude" />
                            <input type="hidden" id="longitude" name="longitude" />
                        </div>
                        </div>
                        <div class="form-group col-md-12 text-right">
                            {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo env("GOOGLE_API_KEY");?>&libraries=places&callback=initAutocomplete" async defer></script>
<script>
function initAutocomplete() {
    var input = document.getElementById('autocomplete');
    var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        document.getElementById('autocomplete').value = place.formatted_address;
        document.getElementById('latitude').value = place.geometry.location.lat();
        document.getElementById('longitude').value = place.geometry.location.lng();
    });
}

$(function() {

    $('#user_id, #expert_id, #service_id').select2();

    $("#service_id").select2({
        placeholder: 'Select Service',
        multiple :false,
        // ajax: {
        //     url: '{{ route("admin.expert.skills")}}',
        //     dataType: 'json',
        //     delay: 250,
        //     multi :true,
        //     processResults: function (data) {
        //         return {
        //             results: data
        //         };
        //     },
        //     cache: true
        // }
    });

    var date = new Date();
    date.setDate(date.getDate()+1);

    var maxD = new Date();
    maxD.setDate(maxD.getDate()+90);

    $('#booking_date').datetimepicker({
        format: "Y-MM-DD HH:mm",
        minDate : date,
        maxDate : maxD
    });
    // add valid and remove error classes on select2 element if valid
    $('.select2-hidden-accessible').on('change', function() {
        if($(this).valid()) {
            $(this).next('span').removeClass('error').addClass('valid');
        }
    });
    $('#job_dispatch_form').validate({
        errorPlacement: function(error, element) {
            if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            }
            else if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            }
            else{
                error.insertAfter(element);
            }
        },
        errorClass: "error",
        rules: {
            user_id: {
                required: true
            },
            // expert_id: {
            //     required: true
            // },
            service_id: {
                required: true
            },
            booking_date: {
                required: true
            },
            address: {
                required: true
            }
        },
        messages: {
            user_id: {required: "<?php echo trans('messages.job_dispatch_customer_required');?>"},
           // expert_id: {required: "<?php echo trans('messages.job_dispatch_expert_required');?>"},
            service_id: {required: "<?php echo trans('messages.job_dispatch_service_required');?>"},
            booking_date: {required: "<?php echo trans('messages.job_dispatch_datetime_required');?>"},
            address: {required: "<?php echo trans('messages.job_dispatch_address_required');?>"},
        }
    });
});

$(document).on('change','#service_id',function() {
    var service_id = $(this).val();
    $('#expert_id').html('');
    $.ajax({
        type: "POST",
        url: "{{ route('admin.getServicesUser') }}",
        data: { service_id : service_id},
        success: function(data){
            var data = jQuery.parseJSON(data);
            $('#expert_id').append("<option value=''>Please select</option>");
            $.each(data, function(index, element) {
                var name = '';
                if(element.user.first_name !=='' && element.user.last_name !=='') {
                    name =  element.user.first_name + " " + element.user.last_name +' - ';
                }
                $('#expert_id').append("<option value='"+ element.user_id +"'>" + name + ""+ element.user.email +  "</option>");
            });
        }
    });
});
</script>
@endpush
@endsection
