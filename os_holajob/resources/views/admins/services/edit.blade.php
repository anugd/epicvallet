@extends('admins.page')

@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Edit Service</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Note: All fields marked with (<i class="text-danger">*</i>) are required.</h4>
                    </div>
                    <div class="card-body">

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form method="post" action="{{ route('admin.services.update') }}" enctype="multipart/form-data" id="create_services_form">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::hidden('id', $service->id) !!}
                                    {!! Form::label('parent_id', 'Parent Service') !!}
                                    {!! Form::select('parent_id', $services, $service->parent_id, ['class' => 'form-control select2', 'placeholder' => '--Select Service--']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('language_id', 'Language<span class="text-danger">*</span>')) !!}
                                    {!! Form::select('language_id', $languages, $service->language_id, ['class' => 'form-control select2','required'=>'true']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('service', 'Service Name<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('service', $service->service, ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('price', 'Price<span class="text-danger">*</span>')) !!}
                                    {!! Form::text('price', $service->price, ['class' => 'form-control','required'=>'true']) !!}
                                </div>
                            </div>
                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    {!! htmlspecialchars_decode(Form::label('icon', 'Icon<span class="text-danger">*</span>')) !!}
                                    <div class="input-group">
                                        <input name="service_icon" data-placement="bottomRight" class="form-control icp icp-auto" value='{{$service->service_icon}}'
                                           type="text"/>
                                        <span class="input-group-addon"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="service_image_old" value="<?php echo $service->service_image;?>">
                                            {!! htmlspecialchars_decode(Form::label('service_image', 'Image<span class="text-danger">*</span>')) !!}
                                            {!! Form::file('service_image', ['class' => 'form-control']) !!}
                                            <p class="help-block">*jpg, jpeg, png, gif</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php if($service->service_image) { ?>
                                                <img src="{{ asset('service/' . $service->service_image) }}"  border = "0" width = "200" class = "img-rounded" align = "center"/>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('status', 'Status') !!}
                                    {!! Form::checkbox('status', 1, true, ["class" => "minimal"]) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                                </div>
                            </div>
                        </from>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="/fa/dist/js/fontawesome-iconpicker.js"></script>
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script>
$(function() {
    $('.select2').select2();

    $('.icp-auto').iconpicker();
    
    $('#create_services_form').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        errorClass: "error",
        rules: {

            language_id: {
                required: true
            },
            service: {
                required: true
            },
            price: {
                required: true,
                number: true
            },
            // service_icon: {
            //     required: true
            // }
        },
        messages: {
            language_id: {
                required: "<?php echo trans('messages.service_language_required');?>"
            },
            service: {
                required: "<?php echo trans('messages.service_name_required');?>"
            },
            price: {
                required: "<?php echo trans('messages.service_price_required');?>"
            },
            // service_icon: {
            //     required: "<?php echo trans('messages.service_icon_required');?>"
            // }
        }
    });
});
</script>
@endpush
@endsection
