@extends('admin.page')
@section('content')
<link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet">
<link href="/fa/dist/css/fontawesome-iconpicker.min.css" rel="stylesheet">


<div class="">
    <div class="row">
        <div class="col-md-12 col-md-offset">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Services - <small>Add Service</small>
                    <ol class="breadcrumb pull-right" style="padding:0;">
                    <li>
                        <a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a> 
                    </li>
                    <li>
                        <a href="{{ url('admin/services') }}">Services</a>        
                    </li>
                    <li class="active">Add Service</li>
                    </ol>
                </div>
                
                <div class="panel-body">
       
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <p style="margin-left:6px;"><strong>Note: All fields marked with (<i class="text-danger">*</i>) are required.</strong></p>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif

                        {!! Form::open(['route' => 'admin.services.store', 'class' => 'form','enctype'=>"multipart/form-data", 'id'=>'create_services_form']) !!}

                        <div class="form-group">
                            {!! Form::label('parent_id', 'Parent Service') !!}
                       
                            {!! Form::select('parent_id',  $services, null, ['class' => 'form-control select2', 'placeholder' => '--Select Service--']) !!}
                           
                        </div>
                        <div class="form-group">
                       
                            {!! htmlspecialchars_decode(Form::label("language_id", 'Language<span class="text-danger">*</span>')) !!}
                            {!! Form::select('language_id', $languages, null, ['class' => 'form-control select2','required'=>'true']) !!}
                           
                        </div>
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('service', 'Service Name<span class="text-danger">*</span>')) !!}
                            {!! Form::text('service', null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('price', 'Price<span class="text-danger">*</span>')) !!}
                            {!! Form::text('price', null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                        <!-- <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('icon', 'Icon<span class="text-danger">*</span>')) !!}
                            <div class="input-group">
                                <input name="service_icon" data-placement="bottomRight" class="form-control icp icp-auto" value="fas fa-archive"
                                   type="text"/>
                                <span class="input-group-addon"></span>
                            </div>

                        </div>
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('service_image', 'Image<span class="text-danger">*</span>')) !!}
                            {!! Form::file('service_image', null, ['class' => 'form-control','required'=>'true']) !!}
                            <p class="help-block">*jpg, jpeg, png, gif</p>
                        </div> -->

                        <div class="form-group">
                            {!! Form::label('status', 'Status') !!}
                            {!! Form::checkbox('status', 1, true, ["class" => "minimal"]) !!}
                        </div>

                        {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}

                        {!! Form::close() !!}
 
                        
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
</div>
@section('js')
<script src="/fa/dist/js/fontawesome-iconpicker.js"></script>
<script>
$(document).ready(function () {
    $('.select2').select2();
    $('.icp-auto').iconpicker();
});
</script>
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script>
$(function() {
    $('#create_services_form').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        errorClass: "error",
        rules: {
           
            language_id: {
                required: true
            },
            service: {
                required: true
            },
            price: {
                required: true,
                number: true
            },
            // service_icon: {
            //     required: true
            // },
            // service_image: {
            //     required: true
            // }
        },
        messages: {
            language_id: {
                required: "<?php echo trans('messages.service_language_required');?>"
            },
            service: {
                required: "<?php echo trans('messages.service_name_required');?>"
            },
            price: {
                required: "<?php echo trans('messages.service_price_required');?>"
            },
            // service_icon: {
            //     required: "<?php echo trans('messages.service_icon_required');?>"
            // },
            // service_image: {
            //     required: "<?php echo trans('messages.service_image_required');?>"
            // }
        }
    });
});
</script>
@stop
@endsection