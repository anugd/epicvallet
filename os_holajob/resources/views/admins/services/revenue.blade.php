@extends('admins.page')
@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Revenue</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}
                        </div><br />
                    @endif
                    <div class="card-header"></div>
                    <div class="card-body">
                        <div class="col-md-12 no_padd">
                            <table class="table table-hover table-striped datatable" id="revenue-list" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>Service</th>
                                        <th>Expert</th>
                                        <th>Duration (Hr)</th>
                                        <th>Rate / Hours</th>
                                        <th>Sub Total</th>
                                        <th>Fee</th>
                                        <th>Total Amount</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal right fade viewService-modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewServiceModalLabel">
    <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content viewServiceModal-content">
                <div class="modal-header viewServiceModal-header">
                    <h4 class="modal-title" id="viewServiceModalLabel">Revenue Detail</h4>
                    <button onclick="hideLoading()" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body viewServiceModal-body"></div>
            </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

@push('scripts')
<script>
$(document).ready(function () {
    var revenueList = $('#revenue-list').dataTable({
        responsive:true,
        autoWitdh: false,
        processing: true,
        serverSide: true,

        ajax: {
            url: '{{ route('admin.datatable.revenueData') }}',
            dataSrc: 'data'
        },
        columns: [
            {data: 'service',defaultContent: "--"},
            {data: 'expert.fullname',defaultContent: "--"},
            {data: 'job_duration',defaultContent: "--"},
            {data: 'per_hour_rate',defaultContent: "--"},
            {data: 'total_price',defaultContent: "--"},
            {data: 'service_fee',defaultContent: "--"},
            {data: 'sub_total',defaultContent: "--"},
            {data: 'payment_status',defaultContent: "--"},
            {data: 'action',defaultContent: "--", orderable : false,searchable : false}
        ]
    });
    new $.fn.dataTable.FixedHeader( revenueList );
    $('#revenue-list_filter input').addClass('datatable-default-serach');
    $('#revenue-list_filter input').attr('placeholder', "Search");
    $('#revenue-list_length select').addClass("form-control db-length-input");

});

$(document).on('click','.viewDetail',function() {
    var dataId = $(this).attr('dataId');
    viewDetail(dataId);
});

function viewDetail(dataId) {
    $('.modal-body').html('');
    $.ajax({
        type: "GET",
        url: "{{ route('admin.services.revenueDetail') }}",
        data: { id : dataId},
        success: function(data){
            $('.modal-body').html(data);
        }
    });

}
function hideLoading() {
        $("#viewModal").modal('hide');
    }
</script>
@endpush
@endsection
