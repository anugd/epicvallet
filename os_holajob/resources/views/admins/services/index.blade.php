@extends('admins.page')

@section('content')
<section class="section">
    <h1 class="section-header">
        <div>Categories</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}
                        </div><br />
                    @endif
                    <div class="card-header"></div>
                    <div class="card-body">
                        <div class="col-md-12 no_padd">
                           
                                <table class="table table-hover table-striped datatable" id="services-table">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="selectAll"></th>
                                            <th>Name</th>
                                            <!-- <th>Image</th>
                                            <th>Icon</th> -->
                                            <th>Price</th>
                                            <th>Language</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <span class="pull-left ">Actions:&nbsp; </span>
                                                        <select name="actions" class="actionAll form-control input-sm pull-left" 'style'=>'width:150px'>
                                                            <option value="">--Select--</option>
                                                            <!-- <option value="1">Delete</option> -->
                                                            <option value="2">Activate</option>
                                                            <option value="3">Deactivate</option>
                                                        </select>
                                                    </div>
                                                   
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('scripts')
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
$(document).ready(function () {
   
        servicestable = $('#services-table').DataTable({
        responsive:true,
        autoWitdh: false,
        processing: true,
        serverSide: true,
        
        ajax: {
            url: '{{ route('admin.datatable.getAllServices') }}',
            dataSrc: 'data'
        },
        columns: [
            {
            "data": function(data)
                {
                    return '<input type="checkbox" class="checkBoxClass" name="type" value="'+ data.id +'" />';
                }, "orderable": false, "searchable":false
            },
            {data: 'service',defaultContent: "--"},
            // {data: 'service_image'},
            // {data: 'service_icon'},
            {data: 'price',defaultContent: "--"},
            {data: 'language_id',defaultContent: "--"},
            {data: 'status',defaultContent: "--"},

            {data : 'action', orderable : false,searchable : false}
        ]
    });
    new $.fn.dataTable.FixedHeader( servicestable );
    $('#services-table_filter input').addClass('datatable-default-serach');
    $('#services-table_filter input').attr('placeholder', "Search");
    $('#services-table_length select').addClass("form-control db-length-input");


    $(document).on('click', '.deleteCat', function(e){
        var delId = $(this).attr('delId');
        var thiss = $(this);
        swal({
            title: "<?php echo trans('messages.are_you_sure');?>",
            text: "<?php echo trans('messages.perform_action');?>",
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
        .then((isConfirm) => {
            if(isConfirm) { 
                $.ajax({
                    url: '/admin/services/destroy/'+delId,
                    type: 'GET',
                    data:{ },
                    success: function(ress){
                        
                        if(ress) {
                            
                            servicestable
                            .row( thiss.parents('tr') )
                            .remove()
                            .draw(false);
                        }
                    }
                });
            
            }
            else{
                e.preventDefault();
            }
            
        });
    });
});
/* START: SELECT ALL CHECKBOX */

$(document).on('click','#selectAll',function () {
    var status = this.checked;
    $('.checkBoxClass').each(function(){
        this.checked = status;
    });
});

/* END */

/* START: Activate Deactivate service */
$(document).on('click','.updateStatus', function(e){

    var thiss = $(this)
    var id = $(this).attr('value')
    var status = $(this).attr('status');
    var status= (status==1)?0:1;

    checkedIds = new Array();
    checkedIds.push(id);//to keep sync with multi select

    updateStatus(status, checkedIds);

});
/* END */

//bulk action select box
$("select.actionAll").on('change', function() {

    if ($(this).val()) {
        checkedIds = new Array();
        $('.checkBoxClass:checked').each(function(){
            checkedIds.push($(this).val());
        });

        if (checkedIds.length !== 0) {
            if ($(this).val() == 2 || $(this).val()== 3){//2=active, 3=deactive
                var field = 'status';
                var status= ($(this).val()==2)?1:0;
                updateStatus(status, checkedIds);
            }
        }
        else{
            swal("Error", "<?php echo trans('messages.select_one_row');?>", "error");
        }
    }
});

//single/bulk update
function updateStatus(status, checkedIds) {
    var msgContent =  (status==1)?'activate':'deactivate';
    var msgContent_ed =  (status==1)?'activated':'deactivated';

    swal({
        title: "<?php echo trans('messages.are_you_sure');?>",
        text: "<?php echo trans('messages.perform_action');?>",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((isConfirm) => {

        if(isConfirm) {
            var tableId = '.datatable';
            $.ajax({
                    url: '{{ route('admin.services.updateStatus') }}',
                    type: 'POST',
                    data:{ 'id':checkedIds,'status':status },
                    success: function(data){

                        servicestable.draw(false);

                        $(".actionAll").val('');
                        $("input:checkbox").prop('checked', false);
                        swal("Success", "<?php echo trans('messages.multiselect_message');?> "+msgContent_ed+".", "success");
                    }
                });
            }
    });

    return false;
}

</script>
@endpush
@endsection
