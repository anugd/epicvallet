@extends('admins.page')
@section('content')

<section class="section">
    <h1 class="section-header">
        <div>Change Password</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Note: All fields marked with (<i class="text-danger">*</i>) are required.</h4>
                    </div>
                    <div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('admin.updatePassword') }}" id="change_password_form">
                        {{ csrf_field() }}
                        <div class="form-group col-sm-6">
                            {!! htmlspecialchars_decode(Form::label('current_password', 'Current Password<span class="text-danger">*</span>')) !!}
                            {!! Form::password('current_password',  ['class' => 'form-control ','required'=>'true']) !!}

                        </div>
                        <div class="form-group col-sm-6">
                            {!! htmlspecialchars_decode(Form::label('new_password', 'New Password<span class="text-danger">*</span>')) !!}
                            {!! Form::password('new_password',  ['class' => 'form-control ','required'=>'true']) !!}
                         </div>
                        <div class="form-group col-sm-6">
                            {!! htmlspecialchars_decode(Form::label('new_password_confirmation', 'New Password Confirm<span class="text-danger">*</span>')) !!}
                            {!! Form::password('new_password_confirmation',  ['class' => 'form-control ','required'=>'true']) !!}

                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script>
$(function() {
    $('#change_password_form').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        errorClass: "error",
        rules: {
            current_password: {
                required: true,
                remote: {
                    url: "{{ route('admin.checkOldPassword') }}",
                    type: "post",
                    data: {
                        current_password: function() {
                        return $("#current_password").val();
                        }
                    }
                }
            },
            new_password: {
                required: true,
                minlength: 6
            },
            new_password_confirmation: {
                required: true,
                equalTo : "#new_password",
                minlength: 6
            }

        },
        messages: {
            new_password: {required: "<?php echo trans('messages.new_password_required');?>"},
            new_password_confirmation: {
                required: "<?php echo trans('messages.new_password_confirmation_required');?>",
                equalTo: "<?php echo trans('messages.new_old_password_not_matched');?>"
                },
            current_password: {
                required: "<?php echo trans('messages.old_password_required');?>",
                remote: "<?php echo trans('messages.current_password_not_match');?>"
                }
            },
    });
});
</script>
@endpush
@endsection
