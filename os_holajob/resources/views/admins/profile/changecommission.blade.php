@extends('admins.page')
@section('content')

<section class="section">
    <h1 class="section-header">
        <div>Settings</div>
    </h1>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Note: All fields marked with (<i class="text-danger">*</i>) are required.</h4>
                    </div>
                    <div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('admin.updateCommission') }}" id="change_commission_form">
                        {{ csrf_field() }}
                        <div class="form-group col-sm-6">
                            {!! htmlspecialchars_decode(Form::label('commission_fees', 'Admin Commission(%)<span class="text-danger">*</span>')) !!}
                            {!! Form::text('commission_fees', $data['adminSetting']->value, ['class' => 'form-control ','required'=>'true']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="{{ asset('plugins/js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('plugins/js/additional-methods.min.js') }}" defer></script>
<script>
$(function() {
    $('#change_commission_form').validate({
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        errorClass: "error",
        rules: {
            commission_fees: {
                required: true,
                min: 0,
                max: 100,
                number: true
            }
        },
        messages: {
            commission_fees: {required: "<?php echo trans('messages.commission_fees_required');?>"}
        }
    });
});
</script>
@endpush
@endsection
