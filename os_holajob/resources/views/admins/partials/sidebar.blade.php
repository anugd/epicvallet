<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="/admin/dashboard">Hela Job</a>
        </div>
        <div class="sidebar-user">
            <div class="sidebar-user-picture">
                <img alt="image"   width="40" height="50"  src="{{ asset('img/admin.png')}}">
            </div>
            <div class="sidebar-user-details">
                <div class="user-name">{{ $auth = auth()->guard('admin')->user()->first_name }}</div>
                <div class="user-role">
                    Administrator
                </div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="{{ (request()->is('admin/dashboard')) ? 'active' : '' }}">
                <a href="/admin/dashboard"><i class="ion ion-speedometer"></i><span>Dashboard</span></a>
            </li>
            <li class="{{ (request()->is('admin/services/servicesHistory')) ? 'active' : '' }}">
                <a href="#" class="has-dropdown"><i class="ion ion-ios-albums-outline"></i><span>Reports</span></a>
                <ul class="menu-dropdown">
                    <li><a href="/admin/services/servicesHistory"> Service History</a></li>
                    <li><a href="/admin/services/revenue"> Revenue Listing</a></li>
                </ul>
            </li>
            <li class="{{ (request()->is('admin/experts') || request()->is('admin/payouts')) ? 'active' : '' }}">
                <a href="#" class="has-dropdown"><i class="ion ion-person"></i><span>Job Seeker</span></a>
                <ul class="menu-dropdown">
                    <li class="{{ (request()->is('admin/experts')) ? 'active' : '' }}"><a href="/admin/experts"> List Job Seeker</a></li>
                    <li class="{{ (request()->is('admin/payouts')) ? 'active' : '' }}"><a href="/admin/payouts"> Payout </a></li>
                </ul>
            </li>
            <li class="{{ (request()->is('admin/customers')) ? 'active' : '' }}">
                <a href="/admin/customers"><i class="ion ion-person"></i><span>Customers</span></a>
            </li>
            <!--<li>
                <a href="chartjs.html"><i class="ion ion-stats-bars"></i><span>Chart.js</span></a>
            </li>
            <li>
                <a href="simple.html"><i class="ion ion-ios-location-outline"></i><span>Google Maps</span></a>
            </li> -->
            <li class="{{ (request()->is('admin/changeCommission') || request()->is('admin/faqs') || request()->is('admin/cms') || request()->is('admin/services') || request()->is('admin/vouchers')) ? 'active' : '' }}">
                <a href="#" class="has-dropdown"><i class="ion ion-pricetag"></i><span>Platform Settings</span></a>
                <ul class="menu-dropdown">
                    <li class="{{ (request()->is('admin/changeCommission')) ? 'active' : '' }}"><a href="/admin/changeCommission"> Settings</a></li>
                    <li class="{{ (request()->is('admin/faqs')) ? 'active' : '' }}"><a href="/admin/faqs"> FAQ</a></li>
                    <li class="{{ (request()->is('admin/cms')) ? 'active' : '' }}"><a href="/admin/cms"> CMS</a></li>
                    <li class="{{ (request()->is('admin/services')) ? 'active' : '' }}"><a href="/admin/services"> Categories</a></li>
                    <li class="{{ (request()->is('admin/vouchers')) ? 'active' : '' }}"><a href="/admin/vouchers"> Vouchers</a></li>
                    
                </ul>
            </li>
            <li class="{{ (request()->is('admin/changeProfile') || request()->is('admin/changePassword')) ? 'active' : '' }}">
                <a href="#" class="has-dropdown"><i class="ion-android-cloud-circle"></i><span>Account Settings</span></a>
                <ul class="menu-dropdown">
                    <li class="{{ (request()->is('admin/changeProfile')) ? 'active' : '' }}"><a href="/admin/changeProfile"> Update Profile</a></li>
                    <li class="{{ (request()->is('admin/changePassword')) ? 'active' : '' }}"><a href="/admin/changePassword"> Change Password</a></li>
                </ul>
            </li>
            <li class="{{ (request()->is('admin/jobDispatch')) ? 'active' : '' }}"><a href="/admin/jobDispatch"> <i class="ion-compass"></i><span>Dispatch Job</span> </a></li>
            <!--
            <li class="menu-header">More</li>
            <li>
                <a href="#" class="has-dropdown"><i class="ion ion-ios-nutrition"></i> Click Me</a>
                <ul class="menu-dropdown">
                    <li><a href="#"><i class="ion ion-ios-circle-outline"></i> Menu 1</a></li>
                    <li><a href="#" class="has-dropdown"><i class="ion ion-ios-circle-outline"></i> Menu 2</a>
                        <ul class="menu-dropdown">
                            <li><a href="#"><i class="ion ion-ios-circle-outline"></i> Child Menu 1</a></li>
                            <li><a href="#"><i class="ion ion-ios-circle-outline"></i> Child Menu 2</a></li>
                            <li><a href="#" class="has-dropdown"><i class="ion ion-ios-circle-outline"></i> Child Menu 3</a>
                                <ul class="menu-dropdown">
                                    <li><a href="#"><i class="ion ion-ios-circle-outline"></i> Child Menu 1</a></li>
                                    <li><a href="#"><i class="ion ion-ios-circle-outline"></i> Child Menu 2</a></li>
                                    <li><a href="#"><i class="ion ion-ios-circle-outline"></i> Child Menu 3</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="ion ion-ios-circle-outline"></i> Child Menu 4</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="ion ion-heart"></i> Badges <div class="badge badge-primary">10</div></a>
            </li>
            <li>
                <a href="credits.html"><i class="ion ion-ios-information-outline"></i> Credits</a>
            </li> -->
        </ul>
        <!-- <div class="p-3 mt-4 mb-4">
            <a href="http://stisla.multinity.com/" class="btn btn-danger btn-shadow btn-round has-icon has-icon-nofloat btn-block">
                <i class="ion ion-help-buoy"></i> <div>Go PRO!</div>
            </a>
        </div> -->
    </aside>
</div>
