@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <div class="row">
                <div class="col-md-6">
                    <h4>Document</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('elements.account')
            <div class="tab-content">
                <div class="tab-pane fade show active" id="profile">
                    <h5 class="mb-4 mt-3">{{ trans('messages.documents') }}</h5>
                    <form method="post" enctype="multipart/form-data" action="{{ route('/user/submit-document') }}" id="expert-document-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 error-document">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if(Session::has('success'))
                                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="address_proof"> Identity Front Image :</label>
                                            @if(!empty($documents) && !empty($documents->front_image))
                                            <div class="col-md-6">
                                                <a href="{{asset($documents->front_image)}}" target="_blank">View Uploaded Document </a>
                                            </div>
                                            @else
                                            <input type="file" class="form-control expert-doc" id="address_proof" name="address_proof" value="" />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="identity_proof"> Indentity Back Image :</label>
                                            @if(!empty($documents) && !empty($documents->back_image))
                                            <div class="col-md-6">
                                                <a href="{{asset($documents->back_image)}}" target="_blank">View Uploaded Document </a>
                                            </div>
                                            @else
                                            <input type="file" class="form-control expert-doc" id="identity_proof" name="identity_proof" value="" />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="identity_face"> Identity Face :</label>
                                            @if(!empty($documents) && !empty($documents->face))
                                            <div class="col-md-6">
                                                <a href="{{asset($documents->face)}}" target="_blank">View Uploaded Document </a>
                                                <div>
                                                    @else
                                                    <input type="file" class="form-control expert-doc" id="identity_face" name="identity_face" value="" />
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exp_certificate"> Exp. Certificate :</label>
                                                    @if(!empty($documents) && !empty($documents->experience_certificate))
                                                    <div class="col-md-6">
                                                        <a href="{{asset($documents->experience_certificate)}}" target="_blank">View Uploaded Document </a>
                                                    </div>
                                                    @else
                                                    <input type="file" class="form-control expert-doc" id="exp_certificate" name="exp_certificate" value="" />
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="text-right">
                                                @if(empty($documents->experience_certificate) || empty($documents->front_image) || empty($documents->back_image) || empty($documents->face))
                                                <button type="submit" class="btn btn-primary btn-md mt-4">Save</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
        $(function() {


            $('#expert-document-form').validate({
                errorPlacement: function(error, element) {
                    error.appendTo($('.error-document'));
                },
                groups: {  // consolidate messages into one
                    names: "exp_certificate identity_proof exp_certificate address_proof identity_face"
                },
                rules: {
                    address_proof: {
                        require_from_group: [1, ".expert-doc"],
                        extension: "jpg|jpeg|png|pdf",
                        filesize : 2097152
                    },
                    identity_proof: {
                        require_from_group: [1, ".expert-doc"],
                        extension: "jpg|jpeg|png|pdf",
                        filesize : 2097152
                    },
                    identity_face: {
                        require_from_group: [1, ".expert-doc"],
                        extension: "jpg|jpeg|png|pdf",
                        filesize : 2097152
                    },
                    exp_certificate: {
                        require_from_group: [1, ".expert-doc"],
                        extension: "jpg|jpeg|png|pdf",
                        filesize : 2097152
                    }
                },
                messages: {
                    address_proof :{
                        require_from_group : "<?php echo trans('messages.document_upload_required');?>",
                        extension : " Only PNG, JPEG, JPD or PDF allowed to upload."
                    },
                    identity_proof : {
                        require_from_group : "<?php echo trans('messages.document_upload_required');?>",
                        extension : " Only PNG, JPEG, JPD or PDF allowed to upload."
                    },
                    identity_face : {
                        require_from_group : "<?php echo trans('messages.document_upload_required');?>",
                        extension : " Only PNG, JPEG, JPD or PDF allowed to upload."
                    },
                    exp_certificate : {
                        require_from_group : "<?php echo trans('messages.document_upload_required');?>",
                        extension : " Only PNG, JPEG, JPD or PDF allowed to upload."
                    }
                },
                submitHandler: function(form)
                {
                    $(form).find(':submit').attr("disabled", true);
                    $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
                    return true;
                }
            });

            $.validator.addMethod('filesize', function (value, element, param) {
                return this.optional(element) || (element.files[0].size <= param)
            }, 'File size must be less than 2 MB');

        });
    </script>
    @stop
