@extends('layouts.app')

@section('content')
    <div class="row">
		<div class="col-md-12">
    	 <div class="well profile">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <h2>{{ $userobj->fullname}}</h2>
                    <p>
                        <strong>About: </strong>
                        {{ $userobj->Profile->about}}
                    </p>
                    <p>
                        <strong>Languages i can speak: </strong>
                        @forelse($userobj->UserLanguages as $language)
                            <span class="tags">{{ $language->Language->language }}</span>
                        @empty
                            <span class="tags">No languages</span>
                        @endforelse
                    </p>
                    <p>
                        <strong>Skills: </strong>
                        @forelse($userobj->UserServices as $services)
                            <span class="tags">{{ $services->services->service }}</span>
                        @empty
                            <span class="tags">No skills</span>
                        @endforelse

                    </p>
                </div>
                <div class="col-xs-12 text-right col-md-4">
                    <figure>
                        @php
                            $src = !empty($userobj) && !empty($userobj->profile_image) ? $userobj->profile_image : "img/download.jpeg";
                        @endphp
                        <img src="{{ asset($src)}}" width="75" height="75" class="expert-profileimage" alt="Profile Image"/>
                        <figcaption class="ratings">
                            <p>Ratings
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                            <a href="#">
                                 <span class="fa fa-star-o"></span>
                            </a>
                            </p>
                        </figcaption>
                    </figure>
                </div>
            </div>
            <div class="row divider text-center">
                <div class="col-xs-12 col-sm-6 emphasis">
                    <h2><strong> 20,7K </strong></h2>
                    <p><small>Followers</small></p>
                    <button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span> Follow </button>
                </div>
                <div class="col-xs-12 col-sm-6 emphasis">
                    <h2><strong>245</strong></h2>
                    <p><small>Following</small></p>
                    <button class="btn btn-info btn-block"><span class="fa fa-user"></span> View Profile </button>
                </div>
            </div>
            <div class="row divider-history">
                <div class="col-md-12 text-center">
                    <h4> Service History </h4>
                </div>
                <div class="col-md-12">
                    @forelse($svs as $sv)
                        <div class="col-md-12 service-record">{{ $sv->Services["service"] }} <span class="service-date"> {{ date("d M y",strtotime($sv->created_at))}} </span></div>
                        <div class="col-md-12 service-record-to">
                            To :  <span class="fullname-to">{{ $sv->User->fullname }} </span>
                        </div>
                    @empty
                    <div class="col-md-12">
                        No Service yet
                    </div>
                    @endforelse
                </div>
            </div>
    	 </div>
		</div>
	</div>
@stop
