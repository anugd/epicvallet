@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <div class="row">
                <div class="col-md-6">
                    <h4>{{ trans('messages.my_account') }}</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('elements.account')
            <div class="tab-content">
                <div class="tab-pane fade show active" id="profile">
                    <h5 class="mb-4 mt-3">{{ trans('messages.change_password') }}</h5>
                    <form method="post" action="{{ route('/user/update-password') }}" id="user-passwordreset-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if(Session::has('success'))
                                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="name"> {{ trans('messages.old_password') }}</label>
                                    <input type="password" placeholder="{{ trans('messages.old_password') }}" required class="form-control" id="password" name="password" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="new-password">{{ trans('messages.new_password') }} </label>
                                    <input type="password" placeholder="{{ trans('messages.new_password') }}" required class="form-control" id="new-password" name="new-password" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="confirm-new-password">{{ trans('messages.confirm_new_password') }}</label>
                                    <input type="password" placeholder="{{ trans('messages.confirm_new_password') }}" required class="form-control" id="confirm-new-password" name="confirm-new-password" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">{{ trans('messages.change_password') }}</button>&nbsp;&nbsp;
                                <a href="{{ route('service-history.index')}}" class="btn btn-md btn-outline-primary"> {{ trans('messages.cancel_button') }} </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {

    $('#user-passwordreset-form').validate({
        rules: {
            password: {
                required: true,
                remote : "{{ route('/user/validate-old-password')}}"
            },
            'new-password': {
                required: true,
                minlength: 6
            },
            'confirm-new-password': {
                required: true,
                equalTo : "#new-password",
                minlength: 6
            }
        },
        messages: {
            password :{
                required : "<?php echo trans('messages.old_password_required');?>",
                remote: function() { return $.validator.format("<?php echo trans('messages.current_password_not_match');?>") }
            },
            'new-password' : {
                required : "<?php echo trans('messages.new_password_required');?>",
                minlength:  jQuery.validator.format("Enter at least {0} characters")
            },
            'confirm-new-password' : {
                required : "<?php echo trans('messages.new_password_confirmation_required');?>",
                equalTo : "<?php echo trans('messages.new_old_password_not_matched');?>",
                minlength:  jQuery.validator.format("Enter at least {0} characters")
            }
        },
        submitHandler: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            return true;
        }
    });

});
</script>
@stop
