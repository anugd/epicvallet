@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <h4>Payment Statistics </h4>
        </div>
        <div class="card-body">
            <div class="row tile_count">
                <div class="col-md-3 text-center col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-gbp"></i> Total Earning </span>
                    <div class="count">&pound;{{ isset($earnedMoney) && !empty($earnedMoney) ? number_format((float)$earnedMoney, 2, '.', '') : "0"}}</div>
                </div>
                <div class="col-md-3 text-center col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Payout </span>
                    <div class="count">&pound;{{ isset($totalpayout) && !empty($totalpayout) ? number_format((float)$totalpayout, 2, '.', '') : "0"}}</div>
                </div>
                <div class="col-md-3 text-center col-sm-3 col-xs-12 tile_stats_count">
                    <span class="count_top"><i class="fa fa-money"></i> Balance </span>

                    <div class="count green">&pound;
                    <?php
                        if(isset($balance) && !empty($balance)) {
                            echo $bal = number_format((float)$balance, 2, '.', '');
                        } else{
                            echo $bal = 0;
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="tab-pane margin-top-10">
    <div class="card shadow">
        <div class="card-header bg-white">
            <div class="row">
                <div class="col-md-6">
                    <h4>Payouts</h4>
                </div>
                <div class="col-md-6">
                    <?php if ($bal){ ?>
                        <button href="javascript:void(0);" type="button" class="btn btn-md btn-outline-primary float-right viewRequestPayout" data-toggle="modal" data-target="#viewRequestPayoutModal" sub_total="{{$bal}}">Request Payout</button>
                    <?php } ?>
                </a>
            </div>
        </div>
        <div class="card-body">
            <table id="payout-table" class="table">
                <thead>
                    <th>Request ID</th>
                    <th>Transection ID</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Date</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal right fade viewService-modal" id="viewPayoutModal" tabindex="-1" role="dialog" aria-labelledby="viewServiceModalLabel">
    <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content viewServiceModal-content">
                <div class="modal-header viewServiceModal-header">
                    <h4 class="modal-title" id="viewServiceModalLabel">Payout Detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body viewServiceModal-body"></div>
            </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->



<!-- Modal -->
<div id="viewRequestPayoutModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Request Payout</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label>Request Amount: </label>
                    <span class="amountContent">35</span>&nbsp;&nbsp;
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-small btn-primary payoutRequestBtn">Send Request</button>
            <button type="button" class="btn btn-default" data-dismiss = "modal">Close</button>
        </div>
    </div>
</div>
</div>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
    $(function() {
        $('#payout-table').dataTable({
            "language": {
                "search": ""
            },
            "columnDefs": [ {
                "targets": -1,
                "orderable": false
            } ],
            "bInfo" : false,
            "bLengthChange": false,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('payouts.list') }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'id',defaultContent: "--"},
                {data: 'stripe_txn_id',defaultContent: "--"},
                {data: 'amount',defaultContent: "--"},
                {data: 'status',defaultContent: "--"},
                {data: 'created_at',defaultContent: "--"}
            ]
        });
        $('#payout-table_filter input').addClass('custom-form-control');
        $('#payout-table_filter input').attr('placeholder', "Search");
        $('#payout-table_length select').addClass("form-control db-length-input");

        $(document).on('click','.viewDetail',function() {
            var dataId = $(this).attr('dataId');
            viewDetail(dataId);
        });
    });


function viewDetail(dataId) {
    $('.viewServiceModal-body').html('');
    $.ajax({
        type: "GET",
        url: "{{ route('payouts.payoutDetail') }}",
        data: { id : dataId},
        success: function(data){
            $('.viewServiceModal-body').html(data);
        }
    });
}

$(document).on('click','.viewRequestPayout',function() {
    var dataId = $(this).attr('dataId');
    var sub_total = $(this).attr('sub_total');
    var service_name = $(this).attr('service_name');
    $('.payoutRequestBtn').attr('serviceId',dataId);
    $('.payoutRequestBtn').attr('amount',sub_total);
    $('#viewRequestPayoutModal .service_name').html(service_name);
    $('#viewRequestPayoutModal .amountContent').html('&pound;'+sub_total);
});

$(document).on('click','.payoutRequestBtn',function() {
    var amount = $('.payoutRequestBtn').attr('amount');
    $.ajax({
        type: "POST",
        url: "{{ route('user.payoutRequest') }}",
        data: {amount:amount},
        success: function(data) {
            var data = jQuery.parseJSON(data);
            swal({text: data.message});
        }
    });
});
</script>
@stop
