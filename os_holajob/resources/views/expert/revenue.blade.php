@extends('layouts.app')
@section('content')

<!-- <div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <h4>Payment Statistics </h4>
        </div>
        <div class="card-body">
            <div class="row tile_count">
                <div class="col-md-3 text-center col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-gbp"></i> Total Earning </span>
                    <div class="count">&pound;{{ isset($totalpayout) && !empty($totalpayout) ? number_format((float)$totalpayout, 2, '.', '') : "0"}}</div>
                </div>

            </div>
        </div>
    </div>
</div> -->
<div class="tab-pane margin-top-10">
    <div class="card shadow">
        <div class="card-header bg-white">
            <h4>{{ trans('messages.earnings') }}</h4>
        </div>
        <div class="card-body">
            <table id="revenue-table" class="table mb-0" style="width:100%">
                <thead>
                    <th>Invoice ID</th>
                    <th>Date</th>
                    <th>Service</th>
                    <th>Service price/h</th>
                    <th>Duration</th>
                    <th>Cost</th>
                    <th>Discount</th>
                    <th>Total Paid</th>
                    <th>Commission</th>
                    <th>Expert Earning</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal right fade viewService-modal" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewServiceModalLabel">
    <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content viewServiceModal-content">
                <div class="modal-header viewServiceModal-header">
                    <h4 class="modal-title" id="viewServiceModalLabel">Revenue Detail</h4>
                    <button onclick="hideLoading()" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body viewServiceModal-body"></div>
            </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
<script>
    $(function() {

        $('#revenue-table').DataTable({
            "language": {
                "search": ""
            },
            "bInfo" : false,
            "bLengthChange": false,
            processing: true,
            serverSide: true,
            "bInfo" : false,
            "bLengthChange": false,
            ajax: {
                url: '{{ route('expert.revenueData') }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'id',defaultContent: "--"},
                {data: 'booking_date',defaultContent: "--"},
                {data: 'service_name',defaultContent: "--"},
                {data: 'per_hour_rate',defaultContent: "--"},
                {data: 'job_duration',defaultContent: "--"},
                {data: 'total_price',defaultContent: "--"},
                {data: 'voucher_discount',defaultContent: "--"},
                {data: 'total_paid',defaultContent: "--"},
                {data: 'service_fee',defaultContent: "--"},
                {data: 'sub_total',defaultContent: "--"}
            ]
        });

        $('#revenue-table_filter input').addClass('custom-form-control');
        $('#revenue-table_filter input').attr('placeholder', "Search");
        $('#revenue-table_length select').addClass("form-control db-length-input");

    });

    $(document).on('click','.viewDetail',function() {
        var dataId = $(this).attr('dataId');
        viewDetail(dataId);
    });

    function viewDetail(dataId) {
        $('.modal-body').html('');
        $.ajax({
            type: "GET",
            url: "{{ route('expert.revenueDetail') }}",
            data: { id : dataId},
            success: function(data){

                $('.modal-body').html(data);
            }
        });

    }
</script>
@stop
