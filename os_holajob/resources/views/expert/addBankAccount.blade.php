@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <div class="row">
                <div class="col-md-6">
                    <h4>{{ trans('messages.my_account') }}</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('elements.account')
            <div class="tab-content">
                <div class="tab-pane fade show active" id="profile">
                    <h5 class="mb-4 mt-3">{{ trans('messages.payment_info') }}</h5>
                    <form method="post" action="{{ route('/account/payments/updateBankAccount') }}" id="updateBankAccount-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if(Session::has('success'))
                                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="account_number"> {{ trans('messages.account_number') }}</label>
                                    <input type="text" placeholder="{{ trans('messages.account_number') }}" required class="form-control" id="account_number" name="account_number" value="" />
                                </div>
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="routing_number">{{ trans('messages.routing_number') }} </label>
                                    <input type="password" placeholder="{{ trans('messages.routing_number') }}" required class="form-control" id="routing_number" name="routing_number" value="" />
                                </div>
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="stripe_connect_id">{{ trans('messages.stripe_connect_id') }}</label>
                                    <input type="hidden" placeholder="{{ trans('messages.stripe_connect_id') }}"  class="form-control" id="stripe_connect_id" name="stripe_connect_id" value="<?php echo $profileDetail->stripe_account_id;?>" readonly="true"/>
                                    <p><?php echo $profileDetail->stripe_account_id;?></p>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="stripe_connect_status"> {{ trans('messages.stripe_connect_status') }} </label>
                                    <div>
                                        <label class="switch" for="stripe_connect_status">
                                            <input name="stripe_connect_status" id="stripe_connect_status" type="checkbox" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 text-right">
                                <button type="submit" 'id'='addBankAccount' class="btn btn-primary btn-md mt-4">{{ trans('messages.add_bank') }}</button>&nbsp;&nbsp;
                                <!-- <button href="{{ route('service-history.index')}}" class="btn btn-md btn-outline-primary baccount"> {{ trans('messages.cancel_button') }} </button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/sweetalert.js') }}"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
var $form = $('#updateBankAccount-form');
function stripeResponseHandler(status, response) {
    // Grab the form:
    var $form = $('#updateBankAccount-form');
    if (response.error) { // Problem!
	swal("Error", response.error.message, "error");
        $form.find('button.baccount').attr('disabled', false); // Re-enable submission
    } else { // Token created!
        // Get the token ID:
        var token = response.id;
        // Insert the token into the form so it gets submitted to the server:
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
        // Submit the form:
        submitBankAccount();
    }
}

function submitBankAccount() {
    var data = $form.serialize();
	$.ajax({
	type: 'POST',
	data: data,
	url: '{{ route("/account/payments/updateBankAccount")}}',
	success: function(response) {
	    var obj = jQuery.parseJSON(response);
	    swal(obj.requestStatus, obj.message, obj.requestStatus.toLowerCase());
	},
	error: function(xhr,textStatus,error){
	    swal("Error", error, "error");
	}
   });
}

$(document).ready(function() {

    Stripe.setPublishableKey('<?php echo config('app.STRIPE_TEST_PUBLISH_KEY');?>');

    jQuery.validator.addMethod("validateNullOrWhiteSpace", function (value, element) {
       return !isNullOrWhitespace(value);
    }, "Blanks are not allowed!");
    $("#updateBankAccount-form").validate({
        rules: {
            account_number: {
                required: true,
                validateNullOrWhiteSpace: true
            },
            routing_number: {
                required: true,
                validateNullOrWhiteSpace: true,
                number:true,
                minlength: 6,
                maxlength: 6
            }
        },
        messages: {
            account_number: {
                required : "<?php echo trans('messages.account_number_required');?>",
            },
            routing_number: {
                required : "<?php echo trans('messages.routing_number_required');?>",
            }
        },
        submitHandler: function(form) {
            addBankAccount(form);
        },
        errorElement: "div",
        errorPlacement: function ( error, element ) {
            error.addClass( "help-block" );
            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "div" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parent( "div" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parent( "div" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    });
});

//addBankAccount
function addBankAccount(form) {
    //Disable the submit button to prevent repeated clicks
    $('button.baccount').attr('disabled', true);
    Stripe.bankAccount.createToken({
	country: 'GB',
	currency: 'GBP',
	routing_number: $('#routing_number').val(),
	account_number: $('#account_number').val(),
	account_holder_type: 'individual'
    }, stripeResponseHandler);
    // Prevent the form from submitting with the default action
    return false;
}

function isNullOrWhitespace(input) {
    if (typeof input === 'undefined' || input == null)
        return true;
    return input.replace(/\s/g, '').length < 1;
}
</script>
@stop
