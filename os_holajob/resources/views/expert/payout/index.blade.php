@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <h4>Payouts</h4>
        </div>
        <div class="card-body">
            <table id="payout-table" class="table mb-0">
                <thead>
                    <th>Payot Request Id</th>
                    <th>Transaction id</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Date</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {

        $('#payout-table').dataTable({
            "language": {
              "search": ""
          },
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('payouts.list') }}',
                dataSrc: 'data'
            },
            columns: [
                {data: 'payout_request_id',defaultContent: "--"},
                {data: 'stripe_txn_id',defaultContent: "--"},
                {data: 'amount',defaultContent: "--"},
                {data: 'status',defaultContent: "--"},
                {data: 'created_at'},
            ]
        });

        $('#payout-table_filter input').addClass('custom-form-control');
        $('#payout-table_filter input').attr('placeholder', "Search");

    });
</script>
@stop
