@extends('layouts.app')

@section('content')
<div class="tab-pane">
    <div class="card shadow">
        <div class="card-header bg-white">
            <div class="row">
                <div class="col-md-6">
                    <h4>{{ trans('messages.my_account') }}</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('elements.account')
            <div class="tab-content">
                <div class="tab-pane fade show active" id="profile">
                    <h5 class="mb-4 mt-3">{{ trans('messages.work') }}</h5>
                    <form method="post" action="{{ route('/user/update-address') }}" id="user-address-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if(Session::has('success'))
                                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="address_line1">Address Line 1</label>
                                    <input id="address_line1" placeholder="Address Line 1" type="text" class="form-control" name="address_line1" value="{{ !empty($address) && !empty($address->address_line1) ? $address->address_line1 : ""}}" >
                                </div>
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="address_line2">Address Line 2</label>
                                    <input id="address_line2" placeholder="Address Line 2" type="text" class="form-control" name="address_line2" value="{{ !empty($address) && !empty($address->address_line2) ? $address->address_line2 : ""}}">
                                </div>
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="city">City</label>
                                    <input id="city" placeholder="City" type="text" class="form-control" name="city" value="{{ !empty($address) && !empty($address->city) ? $address->city : ""}}" >
                                </div>
                                <div class="form-group">
                                    <label class="text-primary font-weight-bold" for="postal_code">Postal Code</label>
                                    <input id="postal_code" placeholder="Postal Code" type="text" class="form-control" name="postal_code" value="{{ !empty($address) && !empty($address->postal_code) ? $address->postal_code : ""}}" >
                                </div>
                                <div class="form-group">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary btn-md mt-4">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {

    $('#user-address-form').validate({
        rules: {
            address_line1 : {
                required: true
            },
            city : {
                required: true
            },
            postal_code : {
                required: true,
                number: true,
                maxlength: 8
            }
        },
        messages: {
            address_line1 : {
                required: "<?php echo trans('messages.expert_address_required');?>"
            },
            city : {
                required: "<?php echo trans('messages.expert_city_required');?>"
            },
            postal_code : {
                required: "<?php echo trans('messages.expert_postcode_required');?>",
                maxlength : "<?php echo trans('messages.expert_postcode_max_length_required');?>",
                number : "<?php echo trans('messages.digit_allowed');?>"
            }
        },
        submitHandler: function(form)
        {
            $(form).find(':submit').attr("disabled", true);
            $(form).find(':submit').prepend("<i class='fa fa-spinner fa-spin'></i>   ");
            return true;
        }
    });

});
</script>
@stop
