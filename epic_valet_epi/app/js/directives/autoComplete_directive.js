angular.module("app").directive('autoComplete', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                      iElement.trigger('input');
                    }, 0);
                }
            });
        }
    };
});