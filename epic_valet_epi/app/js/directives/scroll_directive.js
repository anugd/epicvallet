angular.module("app").directive("scroll", function ($window) {
   return function(scope, element, attrs) {
            element.bind("DOMMouseScroll mousewheel onmousewheel", function(event) {
                   event.preventDefault();     
            });
        };
});