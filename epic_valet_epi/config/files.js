/* Exports a function which returns an object that overrides the default &
 *   plugin file patterns (used widely through the app configuration)
 *
 * To see the default definitions for Lineman's file paths and globs, see:
 *
 *   - https://github.com/linemanjs/lineman/blob/master/config/files.coffee
 */
module.exports = function(lineman) {
  return {
    js: {
      vendor: [
          "vendor/theme/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js",
          "vendor/theme/assets/plugins/jquery-1.11.min.js",
          "vendor/theme/assets/plugins/metrojs/metrojs.min.js",
          "vendor/theme/assets/plugins/bootstrap/bootstrap.min.js",
          "vendor/theme/assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js",
          "vendor/components/underscore/underscore.js",
          "vendor/components/toastr/toastr.js",
          "vendor/components/moment/moment.js",
          "vendor/components/moment-timezone/builds/moment-timezone.min.js",
          "vendor/components/moment-timezone/builds/moment-timezone-with-data.js",
          "vendor/components/bootstrap-datepicker/js/bootstrap-datepicker.js",
          "vendor/components/jquery-maskedinput/dist/jquery.maskedinput.js",
          "vendor/components/pdfjs-dist/build/pdf.js",
          "vendor/components/socket.io-client/socket.io.js",
          "vendor/components/angular/angular.js",
          "vendor/components/angular-sanitize/angular-sanitize.js",
          "vendor/components/angular-route/angular-route.js",
          "vendor/components/angular-resource/angular-resource.js",
          "vendor/components/angular-moment/angular-moment.js",
          "vendor/components/ngInfiniteScroll/build/ng-infinite-scroll.js",
          "vendor/components/angular-multi-select/angular-multi-select.js",
          "vendor/components/ng-file-upload/ng-file-upload-shim.js",
          "vendor/components/ng-file-upload/ng-file-upload.js",
          "vendor/components/xlsx/jszip.js",
          "vendor/components/xlsx/xlsx.js",
          "vendor/components/file-saver/FileSaver.min.js",
          "vendor/components/json-export-excel/dest/json-export-excel.min.js",
          "vendor/theme/assets/plugins/ang-file-upload/angular-file-upload.js",
          "vendor/theme/assets/plugins/jquery-migrate-1.2.1.min.js",
          "vendor/theme/assets/plugins/jquery-ui/jquery-ui-1.10.4.min.js",
          "vendor/theme/assets/plugins/mmenu/js/jquery.mmenu.min.all.js",
          "vendor/theme/assets/plugins/fullcalendar/moment.min.js",
          "vendor/components/fullcalendar/dist/fullcalendar.min.js",
          "vendor/theme/assets/plugins/datetimepicker/jquery.datetimepicker.js",
          "vendor/theme/assets/plugins/modal/js/classie.js",
          "vendor/theme/assets/plugins/modal/js/modalEffects.js",
          "vendor/theme/assets/plugins/charts-morris/raphael-2.1.0.min.js",
          "vendor/theme/assets/plugins/charts-morris/morris.min.js",
          "vendor/theme/assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js",
          "vendor/theme/assets/plugins/checklist-model.js",
          "vendor/theme/assets/js/ng-table.js",
          "vendor/theme/assets/js/angucomplete-alt.js",
          "vendor/theme/assets/plugins/colpick-jQuery-Color-Picker-master/js/colpick.js",
          "vendor/components/pdfviewer/ng-pdfviewer.js",
          "vendor/components/angular-google-places-autocomplete/src/autocomplete.js",
          "vendor/components/pdfjs-dist/build/pdf.worker.js",
          "vendor/components/pdfjs-dist/build/pdf.combined.js",
          "vendor/theme/assets/plugins/nprogress/nprogress.js",
          "vendor/theme/assets/plugins/breakpoints/breakpoints.js",
          "vendor/theme/assets/plugins/jquery.cookie.min.js",
          "vendor/theme/assets/js/application.js"
      ],
      app: [
        "app/js/app.js",
        "app/js/constants.js",
        "app/js/**/*.js"
      ],
      concatenated: "generated/js/app.js",
      concatenatedVendor: "generated/js/vendor.js",
      minifiedVendor: "generated/js/vendor.min.js",
      minifiedApp: "generated/js/app.min.js",
      minified: "dist/js/app.js"
    },

    css: {
      app: [
        "vendor/components/toastr/toastr.css",
        "vendor/components/angular-busy/dist/angular-busy.css",
        "vendor/components/jquery-ui/themes/base/jquery-ui.css",
        "vendor/components/angular-multi-select/angular-multi-select.css",
        "vendor/components/angular-google-places-autocomplete/src/autocomplete.css",
        "vendor/theme/assets/css/icons/icons.min.css",
        "vendor/theme/assets/css/bootstrap.min.css",
        "vendor/theme/assets/css/plugins.min.css",
        "vendor/theme/assets/css/style.css",
        "vendor/theme/assets/css/ng-table.css",
        "vendor/theme/assets/plugins/charts-morris/morris-0.4.3.min.css",
        "vendor/theme/assets/css/angular-multi-select.css",
        "vendor/components/fullcalendar/dist/fullcalendar.css",
        "vendor/theme/assets/plugins/metrojs/metrojs.css",
        "vendor/theme/assets/plugins/datetimepicker/jquery.datetimepicker.css",
        "vendor/theme/assets/plugins/modal/css/component.css",
        "vendor/theme/assets/css/angucomplete-alt.css",
        "vendor/theme/assets/plugins/colpick-jQuery-Color-Picker-master/css/colpick.css",
      ],
      concatenated: "generated/css/app.css",
      concatenatedTheme: "generated/assets/themecss.css",
      minifiedcss:"generated/css/app.min.css",
      minified: "dist/css/app.css"
    },

    less: {
      app: "app/css/app.less",
      watch: "app/css/**/*.less"
    },

    webfonts: {
      root: "fonts"
    }

  };
};
