var auth = require("./authentication"),
users = require("../controllers/users"),
clients = require("../controllers/clients"),
images = require("../controllers/images"),
events = require("../controllers/events"),
schedule = require("../controllers/schedule"),
availabilities = require("../controllers/availabilities"),
timeoffrequests = require("../controllers/timeoffrequests"),
ticketcounts = require("../controllers/ticketcounts"),
payrolls = require("../controllers/payrolls"),
notifications = require("../controllers/notifications"),
memos = require("../controllers/memos"),
documents = require("../controllers/documents"),
todos = require("../controllers/todos"),
whiteboards = require("../controllers/whiteboards"),
notes = require("../controllers/notes"),
exceptionrates = require("../controllers/exceptionrates"),
geolocations = require("../controllers/geolocations"),
questions = require("../controllers/questions"),
evalquestions = require("../controllers/evalquestions"),
initialzips = require("../controllers/initialzips"),
locations = require("../controllers/locations"),
shifttracklists = require("../controllers/shifttracklists"),
rankings = require("../controllers/rankings"),
mongoose = require("mongoose"),
encrypt = require("../utilities/encryption");
User = mongoose.model("User");
module.exports = function(app, config, io) {
  app.post("/api/login", auth.authenticate);
  app.post("/logout", function( req, res ) {
    req.logout();
    res.end();
  });

  app.post("/api/logout", function( req, res ) {
    User.findOneAndUpdate({_id: req.body.loggedid}, {logged_in:false}, function(err, response){
      if (err) {
        console.log(err);
      }
      else{
         req.logout();
         res.end();
      }
    });
  });

  // Upload
  app.post('/api/fileUpload', auth.requireAPILogin, images.fileUpload);
  app.post('/api/imageupload', auth.requireAPILogin, images.upload);

  //Ticket Counts System
  app.post('/api/saveticketCount', auth.requiresRole("adminManagerHr"), ticketcounts.saveticketcount);
  app.post('/api/gettickets', auth.requiresRole("adminManagerHr"), ticketcounts.gettickets);

  //TimeOff Request
  app.post('/api/savetimeoffrequests', auth.requireAPILogin, function(req, res) {timeoffrequests.savetimeoff(req, res, io);}); //Save time Off requests
  app.post('/api/gettimeoff', auth.requireAPILogin, timeoffrequests.gettimeoff);  //Get Timeoff Requests
  app.post('/api/updatetimeoff', auth.requiresRole("adminManagerHr"), timeoffrequests.updatetimeoffRequest); //Update Timeoff Requests
  app.get('/api/gettimeoff/:timeoffid', auth.requireAPILogin, timeoffrequests.gettimeoff); //Get Employee Timeoff Schedule

  //Payroll Section
  app.post('/api/addPayroll', auth.requireAPILogin, payrolls.addpayroll);
  app.post('/api/getPayrolls', auth.requireAPILogin, payrolls.getpayrolls);

  // Users Section
  app.get("/api/users", auth.requireAPILogin, users.getUsers);
  app.post("/api/availableusers", auth.requireAPILogin, users.availableusers);
  app.get("/api/users/:id", auth.requireAPILogin, users.getUserById);
  app.post("/api/users", auth.requireAPILogin, users.createUser);
  app.put("/api/users", auth.requireAPILogin, users.updateUser);
  app.post('/api/delempl',auth.requiresRole("adminManagerHr"), users.delempl); //to delete user
  app.post('/api/delmanagerempl',auth.requiresRole("adminManagerHr"), users.delempl); //to delete manager user
  app.post('/api/chk_random', auth.requiresRole("adminManagerHr"), users.chkrandom);
  app.post('/api/chk_random_edit', auth.requiresRole("adminManagerHr"), users.chkrandomedit);
  app.post('/api/chk_random_account', auth.requiresRole("adminManagerHr"), events.chkrandomAccount);
  app.post('/api/chk_random_account_edit', auth.requiresRole("adminManagerHr"), events.chkrandomAccountEdit);
  app.post('/api/enableemployees',auth.requiresRole("adminManagerHr"), users.enabledisableemp);
  app.post('/api/enableNotifications',auth.requiresRole("adminManagerHr"), users.enableNotifications);
  app.post('/api/update_status',auth.requiresRole("adminManagerHr"), users.update_status);
  app.post('/api/update_notification',auth.requiresRole("adminManagerHr"), users.update_notification);
  app.post('/api/getemployees',auth.requireAPILogin, users.getEmployeesList); // For paging
  app.get('/api/admnemplist', auth.requiresRole("adminManagerHr"), users.getAdminEmp); 
  app.post("/api/getmanagerempl", auth.requiresRole("adminManagerHr"), users.getManagerEmp);
  app.post("/api/getempl", auth.requireAPILogin, users.getAdminEmp);
  app.post("/api/getemplAndManagers", auth.requireAPILogin, users.getemplAndManagers);
  app.post("/api/getAllUsers", auth.requireAPILogin, users.getAllUsers);
  app.post("/api/getAdminEmpActiveInactive", auth.requireAPILogin, users.getAdminEmpActiveInactive);
  app.post("/api/getAllManagers", auth.requireAPILogin, users.getAllManagers);
  app.post('/api/changepassword', auth.requireAPILogin, users.changepassword);
  app.put("/api/users/:id", auth.requireAPILogin, users.updateUserInfo);
  app.post('/api/forgetpassword', users.forgotPassword);
  app.post('/api/getEmployeeCount', auth.requireAPILogin, users.getEmployeeCount);
  app.post('/api/uniqueEmail', auth.requireAPILogin, users.uniqueEmail);
  app.post('/api/uniqueEmailEdit', auth.requireAPILogin, users.uniqueEmailEdit);
  app.post('/api/restoreEmployee', auth.requireAPILogin, users.restoreEmployee);
  app.post('/api/fetchUserTime', auth.requiresRole("adminManagerHr"), users.fetchUserTime);
  
  app.post('/api/checkloggedin', auth.requireAPILogin, users.checkLoggedin); //Check Logged In Employee List
  app.post('/api/checkPwd', auth.requireAPILogin, users.checkPwd);//checklogin for single
  app.post('/api/dailyWorkHours', auth.requireAPILogin, users.getDailyWorkHours); //Get Hours worked on daily basis

  /* Events */
  app.post('/api/events', auth.requiresRole("adminManagerHr"), events.createEvent);
  app.post('/api/eventlist', auth.requiresRole("adminManagerHr"), events.getEventlist);
  app.get('/api/getadminevents', auth.requiresRole("adminManagerHr"), events.getadminEvents);
  app.post('/api/managerevents', auth.requiresRole("adminManagerHr"), events.getManagerEvents);
  app.post('/api/manageraccounts', auth.requiresRole("adminManagerHr"), events.getManagerAccounts);
  app.post('/api/getManagerEventAccounts', auth.requiresRole("adminManagerHr"), events.getManagerEventAccounts);
  app.get('/api/events/:id', auth.requiresRole("adminManagerHr"), events.getEventById);
  app.put('/api/events', auth.requiresRole("adminManagerHr"), events.updateEvent);
  app.post('/api/removeshifts', auth.requireAPILogin, events.removeshifts);
  app.post('/api/removeshiftfromTradeBoard', auth.requireAPILogin, events.removeshiftfromTradeBoard);
  app.post('/api/removeshiftfromTradeBoardAfterSchedule', auth.requireAPILogin, events.removeshiftfromTradeBoardAfterSchedule);
  app.post('/api/removeAllExtrashiftfromTradeBoard', auth.requireAPILogin, events.removeAllExtrashiftfromTradeBoard);
  app.post('/api/getweeklyevents', auth.requireAPILogin, events.getweeklyevents);
  app.post('/api/getschedulecount', auth.requireAPILogin, events.getschedulecount);//Get Schedules Counts for Dashboard

  app.post('/api/eventsByLocation', auth.requireAPILogin, events.getEvents);
  app.post('/api/accountsByLocation', auth.requireAPILogin, events.getAccounts);

  //app.get('/api/getEventAccounts', auth.requireAPILogin, events.getEventAccounts);
  app.post('/api/getEventAccounts', auth.requireAPILogin, events.getEventAccounts);
  app.post('/api/getAllEventAccounts', auth.requireAPILogin, events.getAllEventAccounts);
  app.post('/api/getEventAccountsWithTicketControl', auth.requireAPILogin, events.getEventAccountsWithTicketControl);
  app.post('/api/update_event_status', auth.requireAPILogin, events.update_event_status);
  app.post('/api/enableevents', auth.requireAPILogin, events.enabledisableevent);
  app.post('/api/addEventAccountQuestions', auth.requireAPILogin, events.addEventAccountQuestions);
  app.post('/api/fetchTotalShiftsInWeek', auth.requiresRole("adminManagerHr"), events.fetchTotalShiftsInWeek);
  app.post('/api/fetchTotalExtraShiftsInWeek', auth.requiresRole("adminManagerHr"), events.fetchTotalExtraShiftsInWeek);
  app.post('/api/fetchScheduledShifts', auth.requiresRole("adminManagerHr"), events.fetchScheduledShifts);
  app.post('/api/getemployeeCurrentWeekSchedule', auth.requireAPILogin, events.getemployeeCurrentWeekSchedule);
  app.post('/api/fetchScheduledShiftsData', auth.requiresRole("adminManagerHr"), events.fetchScheduledShiftsData);
  app.post('/api/fetchScheduledShiftsInEvent', auth.requiresRole("adminManagerHr"), events.fetchScheduledShiftsInEvent);
  app.post('/api/addAccountNoteInList', auth.requiresRole("adminManagerHr"), notes.addAccountNoteInList);
  app.post('/api/accountNotesList', auth.requiresRole("adminManagerHr"), notes.accountNotesList);
  app.post('/api/removeAccountNoteInList', auth.requiresRole("adminManagerHr"), notes.removeAccountNoteInList);
  app.post('/api/removeWholeAccountNoteList', auth.requiresRole("adminManagerHr"), notes.removeWholeAccountNoteList);
  app.post('/api/extraShiftsOnAcc', auth.requiresRole("adminManagerHr"), events.addExtraShifts);
  app.post('/api/removeExtrashiftInList', auth.requiresRole("adminManagerHr"), events.removeExtrashiftInList);
  app.post('/api/removeWholeExtrashiftList', auth.requiresRole("adminManagerHr"), events.removeExtrashiftList);

  /* Schedule Management */
  app.post('/api/saveschedule', auth.requiresRole("adminManagerHr"), function(req, res) {schedule.saveschedule(req, res, io);}); // Save schedule
  app.post('/api/schedulereport', auth.requiresRole("adminManagerHr"), schedule.schedulereport);
  app.post('/api/schedulePdfReport', auth.requiresRole("adminManagerHr"), schedule.schedulePdfReport);
  app.post('/api/schedulereportList', auth.requiresRole("adminManagerHr"), schedule.schedulereportList);
  app.post('/api/checkSchedule', auth.requireAPILogin, schedule.scheduleDetails); // for acknowledge
  app.post('/api/checkScheduleDetails', auth.requireAPILogin, schedule.viewscheduleDetails);// just for view
  app.post('/api/empSchedules', auth.requiresRole("adminManagerHr"), schedule.empSchedules);// just for view
  app.post('/api/getEmpDaySchedules', auth.requiresRole("adminManagerHr"), schedule.getEmpDaySchedules);
  app.post('/api/checkoverlappings', auth.requiresRole("adminManagerHr"), schedule.checkoverlappings);
  app.post('/api/markOverlapReport', auth.requiresRole("adminManagerHr"), schedule.markOverlapReport);
  app.post('/api/checkScreenLock', auth.requiresRole("adminManagerHr"), schedule.checkScreenLock);
  app.post('/api/screenLock', auth.requiresRole("adminManagerHr"), schedule.screenLock);
  app.post('/api/screenUnLock', auth.requiresRole("adminManagerHr"), schedule.screenUnLock);

  app.post('/api/getAvailabilitylist', auth.requiresRole("adminManagerHr"), schedule.getAvailabilitylist);// just for view
  app.post('/api/getSchedulelist', auth.requiresRole("adminManagerHr"), schedule.getSchedulelist);// just for view
  app.post('/api/getAvailabilitylistEmpWise', auth.requiresRole("adminManagerHr"), schedule.getAvailabilitylistEmpWise);// just for view
  app.post('/api/getSchedulelistEmpWise', auth.requiresRole("adminManagerHr"), schedule.getSchedulelistEmpWise);// just for view
  app.post('/api/dayScheduleForAllEmp', auth.requireAPILogin, schedule.dayScheduleForAllEmp);// just for view
  app.post('/api/postOnBoard', auth.requireAPILogin, schedule.postOnBoard);
  app.post('/api/fetchCalendarShifts', auth.requireAPILogin, schedule.fetchCalendarShifts);
  app.post('/api/fetchTradeShiftsInfo', auth.requireAPILogin, schedule.fetchTradeShiftsInfo);
  app.post('/api/checkonTradestatus', auth.requireAPILogin, schedule.checkonTradestatus);
  app.post('/api/takeFromTrade', auth.requireAPILogin, schedule.takeFromTrade);
  app.post('/api/checkDateShift', auth.requireAPILogin, schedule.checkDateShift);
  app.post('/api/updateUserTradeSubscription', auth.requireAPILogin, users.update_trade_subscribe);

  app.post('/api/fetchTradeRequests', auth.requireAPILogin, schedule.fetchTradeRequests);
  app.post('/api/fetchAminTradeRequests', auth.requireAPILogin, schedule.fetchAminTradeRequests);
  app.post('/api/fetchAminRejectedTradeRequests', auth.requireAPILogin, schedule.fetchAminRejectedTradeRequests);
  app.get('/api/gettrade/:tradeid', auth.requireAPILogin, schedule.gettrade); //Get Employee Timeoff Schedule
  app.get('/api/getrejectedtrade/:tradeid', auth.requireAPILogin, schedule.getrejectedtrade); //Get Employee Timeoff Schedule
  app.post('/api/updatetrade', auth.requireAPILogin, function(req, res) {schedule.updatetrade(req, res, io);}); //Update Timeoff Requests
  app.post('/api/selectTradeShift', auth.requireAPILogin, function(req, res) {schedule.selectTradeShift(req, res, io);}); //Update Timeoff Requests
  app.post('/api/getCoworkerEmplData', auth.requireAPILogin, schedule.getCoworkerEmplData); //Get Employee Timeoff Schedule
  app.post('/api/fetchSchedulesbyIds', auth.requireAPILogin, schedule.fetchSchedulesbyIds); 
  app.post('/api/saveschNote', auth.requireAPILogin, schedule.saveschNote); 

  /* Availability */
  app.post('/api/weekavailability', auth.requireAPILogin, availabilities.saveWeekAvailability);// Mark availability by My availability section on employee end.
  app.post('/api/dayavailability', auth.requireAPILogin, availabilities.dayAvailability); // fetch day wise availability.
  app.get('/api/availabilityReminder', availabilities.availabilityReminder); //Availability Reminders
  app.post('/api/getshiftcount', auth.requireAPILogin, availabilities.getshiftcount);
  app.post('/api/getextrashiftcount', auth.requireAPILogin, availabilities.getextrashiftcount);
  app.post('/api/getnocount', availabilities.getnocount);
  app.post('/api/getYesMaybeCount', availabilities.getYesMaybeCount);
  app.post('/api/getYesMaybeScrewedCount', availabilities.getYesMaybeScrewedCount);
  app.post('/api/getempschedule', auth.requireAPILogin, availabilities.getempschedule);
  app.post('/api/getempschedulelist', auth.requireAPILogin, availabilities.getempschedulelist);
  app.post('/api/otherEmpOnEventsNoShift', auth.requireAPILogin, availabilities.otherEmpOnEventsNoShift);
  app.post('/api/getreport',auth.requireAPILogin, availabilities.getReport); // Fetch Reports data
  app.post('/api/getCommentReport',auth.requireAPILogin, availabilities.getCommentReport); // Fetch Reports data
  app.post('/api/weeklyreport', auth.requireAPILogin, availabilities.saveWeeklyComment); // Save Weekly Report Section
  app.post('/api/getweeklyreport', auth.requireAPILogin, availabilities.getWeeklyComment); // Get Weekly Report Section
  app.post('/api/updateAvailability', auth.requireAPILogin, availabilities.updateAvailability);// Update employee availability by manager
  app.post('/api/checkmarkavailability', auth.requireAPILogin, availabilities.checkMarkAvailability); // Dashboard Who don't mark their availability of current week. 
  app.get('/api/getemployeeSideSchedule/:employee_id/:location', auth.requireAPILogin, availabilities.getScrollEmployeeSchedules); // My Schedule on employee login.
  app.get('/api/getemployeeSchedule/:employee_id/:location', auth.requireAPILogin, availabilities.getEmployeeSchedules); // My Schedule on employee login.

  app.get('/api/getshifts', availabilities.getshifts); // get shifts for employee
  app.post('/api/checkedshifts',availabilities.checkedshifts); // For Schedule Socket notification on employee end
  //app.get('/api/getshiftsacknowledgement/:alignedBy_id/:page', availabilities.getshiftsacknowledgement);
  app.post('/api/getshiftsacknowledgement', availabilities.getshiftsacknowledgement);
  app.post('/api/getshiftsacknowledgementForDashboard', availabilities.getshiftsacknowledgementForDashboard);
  app.post('/api/otherEmpOnThisEventAndDate', auth.requireAPILogin, availabilities.otherEmpOnThisEventAndDate);
  app.post('/api/dayAvailabilityForAllEmp', auth.requireAPILogin, availabilities.dayAvailabilityForAllEmp); // fetch day wise availability.

  /* API FOR MEMOS */
  app.post('/api/getmemos', auth.requireAPILogin, memos.getMemos); // RETRIEVE all memos
  app.post('/api/getschMsg', auth.requireAPILogin, memos.getSchTexts); // RETRIEVE all memos
  app.post('/api/memos', auth.requiresRole("adminManagerHr"), memos.createMemo); // CREATE new memo
  app.get('/api/memos/:id', auth.requireAPILogin, memos.getMemo); // GET memo
  app.post('/api/delmemo', auth.requiresRole("adminManagerHr"), memos.delmemo); //to delete
  app.post('/api/updmemo', auth.requiresRole("adminManagerHr"), memos.updmemo); //to update
  app.post('/api/markread', auth.requireAPILogin, memos.markMemo); // MARK MEMO AS READ
  app.post('/api/memoupload', auth.requiresRole("adminManagerHr"), memos.memoupload);
  app.post('/api/memodownload',auth.requireAPILogin, memos.memodownload);
  app.post('/api/createMemoNotification',auth.requireAPILogin, function(req, res) {notifications.createNotification(req, res, io);});
  app.post('/api/markReadNotification', auth.requireAPILogin, function(req, res) {notifications.markNotifications(req, res, io);});
  app.post('/api/createMemoGroup', auth.requiresRole("adminManagerHr"), memos.createMemoGroup); // CREATE new memo group
  app.post('/api/fetchGroups', auth.requiresRole("adminManagerHr"), memos.fetchGroups); // Fetch memoGroup List
  app.post('/api/removeMemoGroup', auth.requiresRole("adminManagerHr"), memos.removeMemoGroup); //  Remove memo group
  
  //Time Slot
  app.get('/api/timeslot', auth.requireAPILogin, users.getTimeSlot);
  app.post('/api/timeslot', auth.requiresRole("adminHr"), users.saveTimeSlot);

  //Time Slot Buffer
  app.post('/api/timeslotbuffer', auth.requireAPILogin, users.getTimeSlotBuffer);// Fetch Time Slot Buffer
  app.post('/api/savetimeslotbuffer', auth.requiresRole("adminHr"), users.saveTimeSlotBuffer); // Save Time Slot Buffer Data

  //Document Library
  app.post('/api/folders', auth.requiresRole("adminManagerHr"), documents.createFolder); // CREATE new folder
  app.get('/api/folders/:folderid', auth.requireAPILogin, documents.getFolderById); // CREATE new folder
  app.post('/api/getfolders', auth.requireAPILogin, documents.getFolder); // get all folders
  app.post('/api/docupload', auth.requiresRole("adminManagerHr"), documents.docUploadInDirectory); // Upload document
  app.post('/api/savedocument', auth.requiresRole("adminManagerHr"), documents.saveDocument); // Upload document
  app.post('/api/docdownload', auth.requireAPILogin, documents.docDownload); // Upload document
  app.get('/api/viewfile/:fileId', auth.requireAPILogin, documents.viewDoc); // Upload document
  app.post('/api/renameFilename', auth.requiresRole("adminManagerHr"), documents.renameFile);
  app.post('/api/renameFoldername', auth.requiresRole("adminManagerHr"), documents.renameFolder);
  app.post('/api/removeFile', auth.requiresRole("adminManagerHr"), documents.removeFile);
  app.post('/api/removeFolder', auth.requiresRole("adminManagerHr"), documents.removeFolder);
  app.post('/api/removeLink', auth.requiresRole("adminManagerHr"), documents.removeLink);
  app.post('/api/link', auth.requiresRole("adminManagerHr"), documents.createLink);
  app.post('/api/renameLink', auth.requiresRole("adminManagerHr"), documents.renameLink);
  app.post('/api/checkOnTopDocument', auth.requiresRole("adminManagerHr"), documents.checkOnTopDocument);

  //Todo's Library
  app.post('/api/savetodolist', auth.requiresRole("adminManagerHr"), todos.saveTodoList); // Add Todo
  app.post('/api/gettodolist', auth.requireAPILogin, todos.getTodoList); // get all Todos
  app.post('/api/dashboardtodolist', auth.requireAPILogin, todos.dashboardtodolist);
  app.post('/api/removeItemsInList', auth.requiresRole("adminManagerHr"), function(req, res) {todos.removeItemsInList(req, res, io);}); // get all Todos
  app.post('/api/removeAllItemsInList', auth.requiresRole("adminManagerHr"), function(req, res) {todos.removeAllItemsInList(req, res, io);}); // get all Todos
  app.post('/api/addItemsInList', auth.requiresRole("adminManagerHr"), function(req, res) {todos.addItemsInList(req, res, io);});// get all Todos
  app.post('/api/deletetoDoList', auth.requiresRole("adminManagerHr"), todos.deletetoDoList);
  app.post('/api/completeTaskInList',auth.requireAPILogin, todos.completeTaskInList);
  app.post('/api/todoListForCalendar',auth.requiresRole("adminManagerHr"), todos.todoListForCalendar);
  app.post('/api/todoListForEmployeeCalendar',auth.requireAPILogin, todos.todoListForCalendar);
  app.post('/api/employeeToDoForThisDate', auth.requireAPILogin, todos.employeeToDoForThisDate);

  //Notes Library
  app.post('/api/addNoteInList', auth.requiresRole("adminManagerHr"), notes.addNoteInList);
  app.post('/api/empNotesList', auth.requiresRole("adminManagerHr"), notes.empNotesList);
  app.post('/api/removeNoteInList', auth.requiresRole("adminManagerHr"), notes.removeNoteInList);
  app.post('/api/removeWholeNoteList', auth.requiresRole("adminManagerHr"), notes.removeWholeNoteList);

  //Whiteboard 
  app.post('/api/addPostInWhiteBoard', auth.requireAPILogin, function(req, res) {whiteboards.addPostInWhiteBoard(req, res, io);});
  app.post('/api/whiteboardListing', auth.requireAPILogin, whiteboards.whiteboardListing);
  app.post('/api/WhiteboardtopicList', auth.requireAPILogin, whiteboards.WhiteboardtopicList);
  app.post('/api/addTopic', auth.requireAPILogin, whiteboards.addTopic);
  app.post('/api/clearWhiteboard', auth.requiresRole("adminManagerHr"), whiteboards.clearWhiteboard);
  app.post('/api/clearWhiteboardByTopic', auth.requiresRole("adminManagerHr"), whiteboards.clearWhiteboardByTopic);
  app.post('/api/clearWhiteboardbyIds', auth.requiresRole("adminManagerHr"), whiteboards.clearWhiteboardbyIds);
  app.post('/api/clearWhiteboardItem', auth.requiresRole("adminManagerHr"), whiteboards.clearWhiteboardItem);

  //Exception Rate
  app.post('/api/addExceptionInList', auth.requiresRole("adminManagerHr"), exceptionrates.addExceptionInList);
  app.post('/api/empExceptionsList', auth.requiresRole("adminManagerHr"), exceptionrates.empExceptionsList);
  app.post('/api/removeExceptionInList', auth.requiresRole("adminManagerHr"), exceptionrates.removeExceptionInList);
  app.post('/api/removeWholeExceptionList', auth.requiresRole("adminManagerHr"), exceptionrates.removeWholeExceptionList); 
  app.post('/api/fetchExceptionRate', auth.requireAPILogin, exceptionrates.fetchExceptionRate); 

  //GeoLocation Module
  app.post('/api/GeoAddRadius', auth.requiresRole("adminManagerHr"), geolocations.saveRadius); 
  app.post('/api/checkExistingRadius', auth.requiresRole("adminManagerHr"), geolocations.checkExistingRadius); 
  app.get('/api/GetGeoRadius', auth.requiresRole("adminManagerHr"), geolocations.GetGeoRadius); 
  app.get('/api/GetGeoRadiusList', auth.requiresRole("adminManagerHr"), geolocations.GetGeoRadiusList); 
  app.post('/api/removeGeoEntry', auth.requiresRole("adminManagerHr"), geolocations.removeGeoEntry); 
  app.post('/api/checkGeoFencingArea', auth.requireAPILogin, geolocations.checkGeoFencingArea);
  app.post('/api/clockIn', auth.requireAPILogin, geolocations.clockIn);
  app.post('/api/clockOut', auth.requireAPILogin, geolocations.clockOut);
  app.post('/api/updateClockInfo', auth.requireAPILogin, geolocations.updateClockInfo);
  app.post('/api/updateClockFullInfo', auth.requireAPILogin, geolocations.updateClockFullInfo);
  app.post('/api/removeClockEntry', auth.requireAPILogin, geolocations.removeClockEntry);
  app.post('/api/clockOutByAdmin', auth.requiresRole("adminManagerHr"), geolocations.clockOutByAdmin);
  app.post('/api/clockIncheck', auth.requireAPILogin, geolocations.clockInCheck);
  app.post('/api/startBreak', auth.requireAPILogin, geolocations.startBreakTime);
  app.post('/api/stopBreak', auth.requireAPILogin, geolocations.stopBreakTime);
  app.post('/api/totalBreak', auth.requireAPILogin, geolocations.totalBreak);
  app.post('/api/gettimesheet', auth.requireAPILogin, geolocations.getTimeSheet);
  app.post('/api/getTimesheetDataByIds', auth.requireAPILogin, geolocations.getTimesheetDataByIds);
  app.post('/api/scheduleReminder', auth.requireAPILogin, geolocations.scheduleReminder);
  app.post('/api/scheduleReminderInFuture', auth.requireAPILogin, geolocations.scheduleReminderInFuture);
  app.post('/api/scheduleReminderOnWeekDay', auth.requireAPILogin, geolocations.scheduleReminderOnWeekDay);
  app.post('/api/activeInactiveCron', auth.requireAPILogin, geolocations.activeInactiveCron);
  app.post('/api/employeeReminderInFuture', auth.requireAPILogin, geolocations.employeeReminderInFuture);
  app.post('/api/saveOpeningClosure', auth.requireAPILogin, geolocations.saveOpeningClosure);
  app.post('/api/saveTicketControl', auth.requireAPILogin, geolocations.saveTicketControl);
  app.post('/api/saveTicketControlReport', auth.requireAPILogin, geolocations.saveTicketControlReport);
  app.post('/api/saveTicketControlReportOnEdit', auth.requireAPILogin, geolocations.saveTicketControlReportOnEdit);
  app.post('/api/updateTicketControlIntimesheet', auth.requireAPILogin, geolocations.updateTicketControlIntimesheet);
  app.post('/api/updateTicketControlReport', auth.requireAPILogin, geolocations.updateTicketControlReport);
  app.post('/api/updateTicketControlReportbyId', auth.requireAPILogin, geolocations.updateTicketControlReportbyId);
  app.post('/api/viewTicketControl', auth.requireAPILogin, geolocations.viewTicketControl);
  app.post('/api/getAvailabilityHaveNotMarked', auth.requireAPILogin, schedule.getAvailabilityHaveNotMarked);
  
  app.post('/api/removereportControls', auth.requireAPILogin, geolocations.removereportControls);
  app.post('/api/getTicketSystemReport', auth.requireAPILogin, geolocations.getTicketSystemReport);
  app.post('/api/getTicketSystemCommentReport', auth.requireAPILogin, geolocations.getTicketSystemCommentReport);
  app.post('/api/getTicketNotes', auth.requireAPILogin, geolocations.getTicketNotes);
  app.post('/api/addShiftTrackInList', auth.requireAPILogin, shifttracklists.addShiftTrackInList);
  app.post('/api/fetchTrackList', auth.requireAPILogin, shifttracklists.fetchTrackList);
  app.post('/api/removeTrackInList', auth.requireAPILogin, shifttracklists.removeTrackInList);
  app.post('/api/removeWholeTrackList', auth.requireAPILogin, shifttracklists.removeWholeTrackList); 
  app.post('/api/getmissedpunchreport',auth.requiresRole("adminManagerHr"), function(req, res) {geolocations.getmissedpunchreport(req, res, io);}); // Fetch Reports data

  //Questions Library
  app.post('/api/addQuestion', auth.requiresRole("adminManagerHr"), questions.addQuestion); 
  app.post('/api/getquestionlist', auth.requiresRole("adminManagerHr"), questions.getQuestionlist); 
  app.post('/api/update_Question_status', auth.requiresRole("adminManagerHr"), questions.update_Question_status);
  app.post('/api/enablequestions', auth.requiresRole("adminManagerHr"), questions.enable_disable_questions);
  app.post('/api/deleteQuestList', auth.requiresRole("adminManagerHr"), questions.deleteQuestList);
  app.get('/api/getAllQuestions', auth.requiresRole("adminManagerHr"), questions.getAllQuestions);
  app.post('/api/editQuestion', auth.requiresRole("adminManagerHr"), questions.editQuestion);
  app.post('/api/IdBasedQuestion', auth.requireAPILogin, questions.IdBasedQuestion);

  //Questions Library
  app.post('/api/Eval/addQuestion', auth.requiresRole("adminManagerHr"), evalquestions.addQuestion); 
  app.post('/api/Eval/getquestionlist', auth.requiresRole("adminManagerHr"), evalquestions.getQuestionlist); 
  app.post('/api/Eval/update_Question_status', auth.requiresRole("adminManagerHr"), evalquestions.update_Question_status);
  app.post('/api/Eval/enablequestions', auth.requiresRole("adminManagerHr"), evalquestions.enable_disable_questions);
  app.post('/api/Eval/deleteQuestList', auth.requiresRole("adminManagerHr"), evalquestions.deleteQuestList);
  app.get('/api/Eval/getAllQuestions', auth.requiresRole("adminManagerHr"), evalquestions.getAllQuestions);
  app.post('/api/Eval/editQuestion', auth.requiresRole("adminManagerHr"), evalquestions.editQuestion);
  app.post('/api/Eval/IdBasedQuestion', auth.requireAPILogin, evalquestions.IdBasedQuestion);
  app.post('/api/Eval/getAllQuestions', auth.requireAPILogin, evalquestions.getAllQuestions);
  app.post('/api/saveEvaluation', auth.requireAPILogin, evalquestions.saveEvaluation);
  app.post('/api/removeEval', auth.requireAPILogin, evalquestions.removeEval);
  app.post('/api/fetchEvaluation', auth.requireAPILogin, evalquestions.fetchEvaluation);

  //ProfitLoss Module
  app.post('/api/getProfitLossReport', auth.requiresRole("adminHr"), geolocations.getProfitLossReport);
  app.post('/api/getTotalHoursOfDay', auth.requireAPILogin, geolocations.getTotalHoursOfDay);
  app.post('/api/getCashDepositControl', auth.requiresRole("adminHr"), geolocations.getCashDepositControl);
  app.post('/api/getHoursworkedReport', auth.requiresRole("adminManagerHr"), geolocations.getHoursworkedReport);
  app.post('/api/getTotalHoursReport', auth.requiresRole("adminManagerHr"), geolocations.getTotalHoursReport);
  app.post('/api/getClockingReport', auth.requiresRole("adminManagerHr"), geolocations.getClockingReport);
  app.post('/api/getGrossPayrollDetails', auth.requiresRole("adminManagerHr"), geolocations.getGrossPayrollDetails);
  app.post('/api/getpayrollreport', auth.requiresRole("adminManagerHr"), geolocations.getpayrollreport);

  //Logs
  app.post('/api/getEmployeeLogs', auth.requireAPILogin, notifications.getEmployeeLogs);
  //app.post('/api/getAdminManagerLogs', auth.requireAPILogin, notifications.getAdminManagerLogs);

  //Ranking Recognition Writeups
  app.post('/api/saveWriteUp', auth.requiresRole("adminManagerHr"), rankings.saveWriteUp);
  app.post('/api/fetchWriteUps', auth.requireAPILogin, rankings.fetchWriteUps);
  app.post('/api/clearWriteupsbyIds', auth.requiresRole("adminManagerHr"), rankings.clearWriteupsbyIds);
  app.post('/api/clearWriteups', auth.requiresRole("adminManagerHr"), rankings.clearWriteups);
  app.post('/api/getAllRankings', auth.requireAPILogin, rankings.getAllRankings);

  //Initials Zip Codes
  app.post('/api/addInitialzip', auth.requiresRole("adminManagerHr"), initialzips.addInitialzip); 
  app.post('/api/getInitalziplist', auth.requiresRole("adminManagerHr"), initialzips.getInitalziplist); 
  app.post('/api/update_Initalzip_status', auth.requiresRole("adminManagerHr"), initialzips.update_Initalzip_status);
  app.post('/api/enable_disable_Initalzips', auth.requiresRole("adminManagerHr"), initialzips.enable_disable_Initalzips);
  app.post('/api/deleteZipList', auth.requiresRole("adminManagerHr"), initialzips.deleteZipList);
  app.post('/api/editInitalzip', auth.requiresRole("adminManagerHr"), initialzips.editInitalzip);
  app.post('/api/IdBasedInitalzip', auth.requireAPILogin, initialzips.IdBasedInitalzip);
  app.get('/api/getAllInitalzips', auth.requireAPILogin, initialzips.getAllInitalzips);

  //Locations
  app.post('/api/addlocation', auth.requiresRole("adminManagerHr"), locations.addlocation); 
  app.post('/api/getLocationlist', auth.requiresRole("adminManagerHr"), locations.getLocationlist); 
  app.post('/api/update_location_status', auth.requiresRole("adminManagerHr"), locations.update_location_status);
  app.post('/api/enable_disable_Locations', auth.requiresRole("adminManagerHr"), locations.enable_disable_Locations);
  app.post('/api/deleteLocationList', auth.requiresRole("adminManagerHr"), locations.deleteLocationList);
  app.post('/api/IdBasedLocation', auth.requireAPILogin, locations.IdBasedLocation);
  app.get('/api/getAllLocations', auth.requireAPILogin, locations.getAllLocations);
  
  io.on('connection', function (socket) {
    socket.emit('connection', "Connection Created.");
    socket.on('get notifications', function(data){
      notifications.getNotifications(data, function(response){
          socket.emit('notification data', response.data);
      });
    });

    socket.on('get trade notifications', function(data){
      schedule.getTradeNotifications(data, function(count){
          socket.emit('trade notification data',count.data);
      });
    });

    socket.on('get availability notification', function(data){
      availabilities.getnotificationicon(data, function(count){
        socket.emit('notification icon data',count.data);
      });
    });

    socket.on('getTodoNotifications',function(data){
      todos.getnotificationicon(data, function(count){
        socket.emit('todoNotificationIconCount',count.data);
      });
    });

    socket.on('getTimeOffNotifications',function(data){
      timeoffrequests.gettimeOffnotificationicon(data, function(count){
        socket.emit('timeOffNotification',count.data);
      });
    });

    socket.on('getAdminTradeNotifications',function(data){
      schedule.getAdminTradeNotifications(data, function(count){
        socket.emit('AdminTradeNotifications',count.data);
      });
    });

  });   
};