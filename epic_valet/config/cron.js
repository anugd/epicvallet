var CronJob = require('cron').CronJob;
var availabilities = require("../controllers/availabilities");
var schedule = require("../controllers/schedule");
var geolocations = require("../controllers/geolocations");
var fs = require('fs');
module.exports = function(app, config) {
    var logData = function(cronNo, error, success){
        var now = new Date();
        var month = now.getMonth() + 1;
        var logfile_name = 'logs/cron-' + now.getFullYear() + "-"+ month + "-" + now.getDate() +'.txt'
        var writeData = "";
        writeData += "[" + new Date() + "]";
        writeData += "[Cron No: "+cronNo+"]\t";
        writeData += "[Error: " + JSON.stringify(error) + "]\t\n";
        writeData += "[Success: " + JSON.stringify(success) + "]\t\n";
        writeData += "==================================================================================================================";
        writeData += "\n\n";
        fs.appendFile(logfile_name, writeData, function(err) {
          if(err) throw err;
        });    
    };
    // Cron1 : This is a reminder to complete your availability by midnight if you have not completed that already
    new CronJob('0 12 * * 3', function() { // Run at noon and only on Wednesday
        logData("Cron1");
       /* availabilities.availabilityReminder(function(err, success) {
            console.log('err', err, 'success', success);
        });*/
    }, null, true, 'America/Phoenix');

    /*..Cron2 : SMS Reminder before 24 hours of a shift to employee..*/ 
    
    new CronJob('0 02 03 * * *', function() { // Run one time in a day at 6PM
       /* schedule.sendMorningReminder(function(err, success) {
            logData("Cron2", err, success);
        });*/
    }, null, true, 'America/Phoenix');

    new CronJob('0 30 18 * * *', function() { // Run one time in a day at 6:30PM
       /* schedule.sendAfternoonReminder(function(err, success) {
            logData("Cron2", err, success);
        });*/
    }, null, true, 'America/Phoenix');

    new CronJob('0 00 19 * * *', function() { // Run one time in a day at 7PM
        /*schedule.sendNightReminder(function(err, success) {
            logData("Cron2", err, success);
        });*/
    }, null, true, 'America/Phoenix');

    new CronJob('0 30 19 * * *', function() { // Run one time in a day at 7:30PM
       /* schedule.sendLatenightReminder(function(err, success) {
            logData("Cron2", err, success);
        });*/
    }, null, true, 'America/Phoenix');

    /*..Cron3 : Send notification to alert Admin if an employee doesn't clock in for their shift..*/
    new CronJob('*/1 * * * *', function() { // Run every minute
       // logData("Cron3");
        /*schedule.sendAlertToAdminForAmericaPhoenix(function(err, success) {

            console.log('err', err, 'success', success);
        });*/
    }, null, true, 'America/Phoenix'); 

    new CronJob('0 20 7 * * *', function() { // Run every 7:30 morning
        logData("Cron4");
        /*geolocations.removeMissedPunch(function(err, success) {
            console.log('err', err, 'success', success);
        });*/
    }, null, true, 'America/Phoenix');

    /*...Cron4 : Enter missed punchouts records in database...*/
    new CronJob('0 30 7 * * *', function() { // Run every 7:30 morning
        logData("Cron4");
        /*geolocations.saveMissedPunchInDb(function(err, success) {
            console.log('err', err, 'success', success);
        });*/
    }, null, true, 'America/Phoenix');


    ////America - Los_Angeles
    new CronJob('*/1 * * * *', function() { // Run every minute
        console.log('executed');
       console.log('2324');
      /*  schedule.sendAlertToAdminForLosAngeles(function(err, success) {
            console.log('err', err, 'success', success);
        });*/
    }, null, true, 'America/Los_Angeles');
}
