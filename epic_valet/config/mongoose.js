var mongoose = require("mongoose"),
    userModel = require("../models/user"),
    clientModel = require("../models/client"),
    eventModel = require("../models/event"),
    timeslotModel = require("../models/timeslot"),
    timeslotbufferModel = require("../models/timeslotbuffer"),
    availabilityModel = require("../models/availability"),
    notificationModel = require("../models/notification"),
    reportModel = require("../models/report"),
    employeebackupModel = require("../models/employeebackup"),
    memoModel = require("../models/memo");
    memoGroup = require("../models/memogroup");
    documentModel = require("../models/document");
    todoModel = require("../models/todo");
    whiteboardModel = require("../models/whiteboard");
    whiteboardtopicModel = require("../models/whiteboardtopic");
    noteModel = require("../models/note");
    accountnoteModel = require("../models/accountnote");
    Timeoffrequest = require("../models/timeoffrequest");
    Ticketcount = require("../models/ticketcount");
    Payroll = require("../models/payroll");
    Exceptionrate = require("../models/exceptionrate");
    Geolocation = require("../models/geolocation");
    Timesheet = require("../models/timesheet"),
    Ticketcontrol = require("../models/ticketcontrol"),
    Schedule = require("../models/schedule");
    questionModel = require("../models/question");
    evalquestionModel = require("../models/evalquestion");
    employeeevalModel = require("../models/employeeeval");
    tradeModel = require("../models/trade");
    rejecttradeModel = require("../models/rejectedtrade");
    usertimeModel = require("../models/usertime");
    shifttracklistModel = require("../models/shifttracklist");
    rankingModel = require("../models/ranking");
    initialzipModel = require("../models/initialzip");
    locationModel = require("../models/location");
    missedpunchModel = require("../models/missedpunch");
    scheduletextModel = require("../models/scheduletext");
    
module.exports = function(config) {
  mongoose.connect(config.db);
  var db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error..."));
  db.once("open", function callback() {
    console.log("event-manager db opened");
  });
};

