var passport = require("passport");
var User = require("mongoose").model("User");
exports.authenticate = function( req, res, next ) {
  req.body.email = req.body.email.toLowerCase();
  var auth = passport.authenticate('local', function(err, user) {
    if (err) { return next(err); }
    if (!user) { res.send({ success: false, message:"Wrong Username or Password","status_code": "500"});  }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      if ( true === user.active ){
        User.findOneAndUpdate({_id: user._id}, {logged_in:true, last_login:new Date()}, function(err, response){
          if (err) {
            return next(err);
          }
          else{
             req.session.user = user;
             res.send({ success: true, user: user });
          }
        });
      }else{
        res.send({ success: true , message:"Inactive User","status_code": "500", user: user});
      }
    });
  });
  auth( req, res, next );
};

exports.requireAPILogin = function( req, res, next ) {
  if (!req.isAuthenticated()) {
    res.status(403);
    res.end();
  } else {
    next();
  }
};

exports.requiresRole = function( role ) {
  return function(req, res, next) {
    if(role == 'adminManager'){
      if (!req.isAuthenticated() || req.user.roles.indexOf('admin') !== -1) {
        next();
      }else if (!req.isAuthenticated() || req.user.roles.indexOf('manager') !== -1) {
        next();
      }
      else {
        res.status(403);
        res.end();
      }
    }

    if(role == 'adminHr'){
      if (!req.isAuthenticated() || req.user.roles.indexOf('admin') !== -1) {
        next();
      }else if (!req.isAuthenticated() || req.user.roles.indexOf('hr') !== -1) {
        next();
      }
      else {
        res.status(403);
        res.end();
      }
    }

    if(role == 'adminManagerHr'){
      if (!req.isAuthenticated() || req.user.roles.indexOf('admin') !== -1) {
        next();
      }else if (!req.isAuthenticated() || req.user.roles.indexOf('manager') !== -1) {
        next();
      }
      else if (!req.isAuthenticated() || req.user.roles.indexOf('hr') !== -1) {
        next();
      }
      else {
        res.status(403);
        res.end();
      }
    }

    if(role == 'admin'){
      if (!req.isAuthenticated() || req.user.roles.indexOf('admin') === -1) {
        res.status(403);
        res.end();
      } else {
        next();
      }
    }
  };
};