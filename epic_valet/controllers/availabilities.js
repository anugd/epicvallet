var Availability = require("mongoose").model("Availability"),
    Timeslot = require("mongoose").model("Timeslot"),
    User = require("mongoose").model("User"),
    Event = require("mongoose").model("Event");
    Notification = require("mongoose").model("Notification");
var Schedule = require("mongoose").model("Schedule");
Report = require("mongoose").model("Report");
var mongoose = require('mongoose');
var _ = require('lodash');
var moment = require('moment');
var nodemailer = require("nodemailer");
var path = require('path');
var root = process.cwd();
var constantObj = require(path.resolve(root, 'constants.js'));

exports.dayAvailability = function(req, res) {
    var empID = req.body._id;
    var weekDay = req.body.weekDay;
    Availability.findOne({
        employee_id: empID,
        currDate: weekDay
    }, function(err, response) {
        if (null !== response) {
            res.json(response);
        } else {
            res.json({
                "currDate": weekDay,
                "error": err
            });
        }
    });
};

exports.assignEvent = function(req, res) {
    Availability.findOne({
        employee_id: req.body.employee_id,
        currDate: req.body.assignDate
    }, function(err, response) {
        if (null !== response) {
            if (req.body.assignShift == 'morning') {
                response.manager_schedule.morning_schedule_details.is_morning_scheduled = true;
                response.manager_schedule.morning_schedule_details.event = req.body.event_id;
            } else if (req.body.assignShift == 'afternoon') {
                response.manager_schedule.afternoon_schedule_details.is_afternoon_scheduled = true;
                response.manager_schedule.afternoon_schedule_details.event = req.body.event_id;
            } else if (req.body.assignShift == 'night') {
                response.manager_schedule.night_schedule_details.is_night_scheduled = true;
                response.manager_schedule.night_schedule_details.event = req.body.event_id;
            } else if (req.body.assignShift == 'latenight') {
                response.manager_schedule.late_night_schedule_details.is_late_night_scheduled = true;
                response.manager_schedule.late_night_schedule_details.event = req.body.event_id;
            }
            response.manager_schedule.modified_date = req.body.modified_date;
            response.manager_schedule.modified_by = req.body.modified_by;
            response.save(function(err, updateResponse) {
                if (!err) {
                    res.json({
                        "message": "success",
                        "status_code": "200"
                    });
                }
            });
        } else {
            var assignedData = {};
            assignedData.manager_schedule = {};
            assignedData.manager_schedule.morning_schedule_details = {};
            assignedData.manager_schedule.afternoon_schedule_details = {};
            assignedData.manager_schedule.night_schedule_details = {};
            assignedData.manager_schedule.late_night_schedule_details = {};
            assignedData.employee_id = req.body.employee_id;
            assignedData.currDate = req.body.assignDate;
            if (req.body.assignShift == 'morning') {
                assignedData.manager_schedule.morning_schedule_details.is_morning_scheduled = true;
                assignedData.manager_schedule.morning_schedule_details.event = req.body.event_id;
            } else if (req.body.assignShift == 'afternoon') {
                assignedData.manager_schedule.afternoon_schedule_details.is_afternoon_scheduled = true;
                assignedData.manager_schedule.afternoon_schedule_details.event = req.body.event_id;
            } else if (req.body.assignShift == 'night') {
                assignedData.manager_schedule.night_schedule_details.is_night_scheduled = true;
                assignedData.manager_schedule.night_schedule_details.event = req.body.event_id;
            } else if (req.body.assignShift == 'latenight') {
                assignedData.manager_schedule.late_night_schedule_details.is_late_night_scheduled = true;
                assignedData.manager_schedule.late_night_schedule_details.event = req.body.event_id;
            }
            assignedData.manager_schedule.modified_date = req.body.modified_date;
            assignedData.manager_schedule.modified_by = req.body.modified_by;
            Availability(assignedData).save(function(err, saveResponse) {
                if (!err) {
                    res.json({
                        "message": "success",
                        "status_code": "200"
                    });
                }
            })
        }

        User.findOne({
            _id: req.body.employee_id
        }).exec(function(err, user) {
            //For event info
            Event.findOne({
                _id: req.body.event_id
            }).exec(function(err, event) {
                var email = req.body.email;
                // setup e-mail data with unicode symbols
                var mailOptions = {
                    from: constantObj.emailSettings.fromWords, // sender address
                    to: user.email, // list of receivers
                    subject: "Event Assignment", // Subject line                
                    html: "Hello " + user.first_name + ", <p style='padding-left:10px'>A new event has been assigned to you for <b>" + req.body.assignShift + "</b> shift. Following are the details:<br><br> Event Name :<b>" + event.name + "</b><br><br> Event Start Date :<b>" + event.start_date + "</b><br><br> Event Start Date :<b>" + event.end_date + "</b><br><br>Note :- Kindly login to your EMS Account and see the full details of your assignment.</p>" + constantObj.emailSettings.signature // html body
                };
                // send mail with defined transport object
                constantObj.transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Message sent: ' + info.response);
                    }
                });
            });
        });

    });
};

exports.updateAvailability = function(req, res) {
    var notificationData = {};
    notificationData.receiver_id = req.body.employee_id;
    notificationData.message = "Availability test";
    notificationData.sender_id = req.body.modified_by;
    notificationData.sent_date = req.body.modified_date;
    notificationData.notification_type = "Availability";
    Availability.findOne({
        employee_id: req.body.employee_id,
        currDate: new Date(req.body.assignDate)
    }, function(err, response) {
        if ((response)  && (null !== response)) {
            if (req.body.assignShift == 'morning') {
                response.is_morning_scheduled = ('yes' == req.body.employee_availability || 'maybe' == req.body.employee_availability)? true : false;
                response.is_morning_scheduled_type = req.body.employee_availability;
                response.is_morning_scheduled_force = false;
            } else if (req.body.assignShift == 'afternoon') {
                response.is_afternoon_scheduled = ('yes' == req.body.employee_availability || 'maybe' == req.body.employee_availability)? true : false;
                response.is_afternoon_scheduled_type = req.body.employee_availability;
                response.is_afternoon_scheduled_force = false;
            } else if (req.body.assignShift == 'night') {
                response.is_night_scheduled = ('yes' == req.body.employee_availability || 'maybe' == req.body.employee_availability)? true : false;
                response.is_night_scheduled_type = req.body.employee_availability;
                response.is_night_scheduled_force = false;
            } else if (req.body.assignShift == 'latenight') {
                response.is_late_night_scheduled = ('yes' == req.body.employee_availability || 'maybe' == req.body.employee_availability)? true : false;
                response.is_late_night_scheduled_type = req.body.employee_availability;
                response.is_late_night_scheduled_force = false;

            }
            response.modified_date = req.body.modified_date;
            response.modified_by = req.body.modified_by;
            response.save(function(err, updateResponse) {
                if (!err) {
                    Notification(notificationData).save(function(error,response){
                        if (error){
                            res.status("500").json({"message": "Something went wrong!", "err": err});
                        }else{
                             res.json({
                                "message": "success",
                                "status_code": "200"
                            });
                        }
                    });    
                }
            });
        } else {
            var assignedData = {};
            if (req.body.assignShift == 'morning') {
                assignedData.is_morning_scheduled = ('yes' == req.body.employee_availability || 'maybe' == req.body.employee_availability)? true : false;
                assignedData.is_morning_scheduled_type = req.body.employee_availability;
                assignedData.is_morning_scheduled_force = false;
            } else if (req.body.assignShift == 'afternoon') {
                assignedData.is_afternoon_scheduled = ('yes' == req.body.employee_availability || 'maybe' == req.body.employee_availability)? true : false;
                assignedData.is_afternoon_scheduled_type = req.body.employee_availability;
                assignedData.is_afternoon_scheduled_force = false;
            } else if (req.body.assignShift == 'night') {
                assignedData.is_night_scheduled = ('yes' == req.body.employee_availability || 'maybe' == req.body.employee_availability)? true : false;
                assignedData.is_night_scheduled_type = req.body.employee_availability;
                assignedData.is_night_scheduled_force = false;
            } else if (req.body.assignShift == 'latenight') {
                assignedData.is_late_night_scheduled = ('yes' == req.body.employee_availability || 'maybe' == req.body.employee_availability)? true : false;
                assignedData.is_late_night_scheduled_type = req.body.employee_availability;
                assignedData.is_late_night_scheduled_force = false;
            }
            assignedData.employee_id = req.body.employee_id;
            assignedData.currDate = req.body.assignDate;
            assignedData.modified_date = req.body.modified_date;
            assignedData.modified_by = req.body.modified_by;
            Availability(assignedData).save(function(err, response) {
                if (!err) {
                    Notification(notificationData).save(function(error,response){
                        if (error){
                            res.status("500").json({"message": "Something went wrong!", "err": err});
                        }else{
                             res.json({
                                "message": "success",
                                "status_code": "200"
                            });
                        }
                    }); 
                }
            })
        }
    });
}

exports.removeAssignEvent = function(req, res) {
    Availability.findOne({
        employee_id: req.body.employee_id,
        currDate: req.body.assignDate
    }, function(err, response) {
        console.log(err);
        if (null !== response) {
            if (req.body.assignShift == 'morning') {
                response.manager_schedule.morning_schedule_details.is_morning_scheduled = false;
                response.manager_schedule.morning_schedule_details.event = null;
            } else if (req.body.assignShift == 'afternoon') {
                response.manager_schedule.afternoon_schedule_details.is_afternoon_scheduled = false;
                response.manager_schedule.afternoon_schedule_details.event = null;
            } else if (req.body.assignShift == 'night') {
                response.manager_schedule.night_schedule_details.is_night_scheduled = false;
                response.manager_schedule.night_schedule_details.event = null;
            } else if (req.body.assignShift == 'latenight') {
                response.manager_schedule.late_night_schedule_details.is_late_night_scheduled = false;
                response.manager_schedule.late_night_schedule_details.event = null;
            }
            console.log(response);
            response.save(function(err, updateResponse) {
                if (!err) {
                    res.json({
                        "message": "success",
                        "status_code": "200"
                    });
                }
            });
        }
    });
}

exports.getEmployeeSchedules = function(req, res) {
    var empID = req.params.employee_id;
    var location = req.params.location;
    var fields = {employee_id: mongoose.Types.ObjectId(empID), "userInfo.location" : mongoose.Types.ObjectId(location), "eventInfo.location" : mongoose.Types.ObjectId(location), "eventInfo.is_active" : true};
    Schedule.aggregate([
        {
            $lookup: {
              from: "users",
              localField: "employee_id",
              foreignField: "_id",
              as: "userInfo"
            }
        },
        {
            $lookup: {
              from: "events",
              localField: "event_account_id",
              foreignField: "_id",
              as: "eventInfo"
            }
        },
        { $match: fields }
    ]).exec(function(error, schedules){
        schedules.map(function(item) {
          item.employee_id = item.userInfo[0];  
          item.event_account_id = item.eventInfo[0];  
        })
        if (error) {
            res.status(500).json(error);
        } else {
            res.status(200).json(schedules);
        }
    });
}

exports.getScrollEmployeeSchedules = function(req, res) {
    var empID = req.params.employee_id;
    var location = req.params.location;
    var today = moment.utc().tz(constantObj.Timezones.AP);
    var currdate = today._d;
    var currdate = new Date(currdate.setHours(0, 0, 0, 0));
    var fields = {employee_id: mongoose.Types.ObjectId(empID), scheduleDate:{ $gte : currdate}, "userInfo.location" : mongoose.Types.ObjectId(location), "eventInfo.location" : mongoose.Types.ObjectId(location), "userInfo.active" : true, "eventInfo.is_active" : true};
    Schedule.aggregate([
        {
            $lookup: {
              from: "users",
              localField: "employee_id",
              foreignField: "_id",
              as: "userInfo"
            }
        },
        {
            $lookup: {
              from: "events",
              localField: "event_account_id",
              foreignField: "_id",
              as: "eventInfo"
            }
        },
        { $match: fields },
        {
            $sort: {scheduleDate : 1}
        }
    ]).exec(function(error, schedules){
        schedules.map(function(item) {
          item.employee_id = item.userInfo[0];  
          item.event_account_id = item.eventInfo[0];  
        })
        if (error) {
            res.status(500).json(error);
        } else {
            res.status(200).json(schedules);
        }
    });
}

exports.otherEmpOnThisEventAndDate = function(req, res) {
    var fields = {
        event_account_id: mongoose.Types.ObjectId(req.body.event),
        scheduleDate: new Date(req.body.scheduleDate),
        employee_id: {
            $ne : mongoose.Types.ObjectId(req.body.curremployee)
        },
        'userInfo.active' : true,
        'userInfo.location' : mongoose.Types.ObjectId(req.body.locationval)
    }; 

    Schedule.aggregate([
        {
            $lookup: {
              from: "users",
              localField: "employee_id",
              foreignField: "_id",
              as: "userInfo"
            }
        },
        { $match: fields }
    ]).exec(function(err, response){
        response.map(function(item) {
          item.employee_id = item.userInfo[0];  
        })
        if (err) {
            res.json({
                "message": "error",
                "data": err,
                "status_code": "500"
            });
        } else {
            res.json({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.getEmployeeAvailabilityStatus = function(req, res) {
    var loginId = req.body._id;
    User.find(condition).populate('employee_id').exec(function(err, response) {
        if (err) {
            res.json({
                "message": "error",
                "data": err,
                "status_code": "500"
            });
        } else {
            res.json({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.checkMarkAvailability = function(req, res) {
    var empID = req.body.empids;
    var startWeek = req.body.startWeek;
    var endWeek = req.body.endWeek;
    var mongoose = require("mongoose");
    Availability.aggregate([
        {$match: {
                employee_id: {
                    $in: empID.map(
                        function(id) {
                            return mongoose.Types.ObjectId(id);
                        })
                },
                currDate: {
                    $lte: new Date(endWeek),
                    $gte: new Date(startWeek)
                },
                $and: [{
                    'is_morning_scheduled_type': {$exists:true}
                }, {
                    'is_morning_scheduled_type': {$ne:''}
                }, {
                    'is_afternoon_scheduled_type': {$exists:true}
                }, {
                    'is_afternoon_scheduled_type': {$ne:''}
                }, {
                    'is_night_scheduled_type': {$exists:true}
                }, {
                    'is_night_scheduled_type': {$ne:''}
                }, {
                    'is_late_night_scheduled_type': {$exists:true}
                }, {
                    'is_late_night_scheduled_type': {$ne:''}
                }]
            }
        },
        {$group: {
                _id: "$employee_id",
                count: {
                    $sum: 1
                }
            }
        }
    ]).exec(function(err, response) {
        if (response !== undefined && response.length > 0) {
            var x = 0;
            for (var i = 0; i < response.length; i++) {
                if (response[i].count == 7) {
                    var index = empID.indexOf(response[i]._id.toString());
                    if (index > -1) {
                        empID.splice(index, 1);
                    }
                }
                if (x === response.length - 1) {
                    User.find({
                        _id: {
                            $in: empID
                        }
                    }, function(err, users) {
                        res.json({
                            "message": "success",
                            "data": users,
                            "status_code": "200"
                        });
                    });
                }
                x++;
            }
        } else {
            User.find({
                _id: {
                    $in: empID
                }
            }, function(err, users) {
                res.json({
                    "message": "success",
                    "data": users,
                    "status_code": "200"
                });
            });
        }
    });
};

exports.otherEmpOnEventsNoShift = function(req, res) {
    var condition = {
        $or: [{
            'manager_schedule.morning_schedule_details.event': req.body.event
        }, {
            'manager_schedule.afternoon_schedule_details.event': req.body.event
        }, {
            'manager_schedule.night_schedule_details.event': req.body.event
        }, {
            'manager_schedule.late_night_schedule_details.event': req.body.event
        }]
    };
    Availability.find(condition, {
        employee_id: 1
    }).populate('employee_id').exec(function(err, response) {
        if (err) {
            console.log(err);
            res.json({
                "message": "error",
                "data": err,
                "status_code": "500"
            });
        } else {
            res.json({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

//function to get shifts
exports.getshifts = function(req, res) {
    var fields = {};
    fields.employee_id = req.session.user._id;
    Availability.find(fields, function(err, data) {
        if (err) {
            console.log(err);
        } else {
            console.log(data);
            res.json({
                "message": "success",
                "data": data,
                "status_code": "200"
            });

        }
    }).populate("manager_schedule.morning_schedule_details.event manager_schedule.late_night_schedule_details.event manager_schedule.afternoon_schedule_details.event manager_schedule.night_schedule_details.event");
}


exports.checkedshifts = function(req, res) {
    Availability.findOne({
        _id: req.body.ID
    }, function(err, data) {
        if (!err) {
            if (req.body.case == 'Morning') {
                data.manager_schedule.morning_schedule_details.is_approve = true;
                data.save(function(err, data1) {
                    if (err)
                        throw err;
                });
                res.json({
                    "message": "success",
                    "status_code": "200"
                });
            } else if (req.body.case == 'LateNyt') {
                data.manager_schedule.late_night_schedule_details.is_approve = true;
                data.save();
                res.json({
                    "message": "success",
                    "status_code": "200"
                });
            } else if (req.body.case == 'Afternoon') {
                data.manager_schedule.afternoon_schedule_details.is_approve = true;
                data.save();
                res.json({
                    "message": "success",
                    "status_code": "200"
                });
            } else if (req.body.case == 'Night') {
                data.manager_schedule.night_schedule_details.is_approve = true;
                data.save();
                res.json({
                    "message": "success",
                    "status_code": "200"
                });
            }
        } else {
            res.json(err);
        }
    });
}

exports.getshiftsacknowledgement = function(req, res) {
    var alignedID = req.body.alignedBy_id;
    var today = moment.utc().tz(constantObj.Timezones.AP);
    var currdate = today._d;
    var currdate = new Date(currdate.setHours(0, 0, 0, 0));
    var page  = req.body.page || 1;
    count = req.body.count || 50;
    var skipNo = (page - 1) * count;
    var searchStr = req.body.search;
    var fields = {scheduleDate:{ $gte : currdate}, $or:[{is_approve:{$exists:false}},{is_approve:false}], "userInfo.active" : true, "eventInfo.is_active" : true};
    if(searchStr && searchStr.eventId){
        fields.event_account_id = mongoose.Types.ObjectId(searchStr.eventId);
    }
    if(searchStr && searchStr.empId){
        fields.employee_id = mongoose.Types.ObjectId(searchStr.empId);
    }
    Schedule.aggregate([
        {
            $lookup: {
              from: "users",
              localField: "employee_id",
              foreignField: "_id",
              as: "userInfo"
            }
        },
        {
            $lookup: {
              from: "events",
              localField: "event_account_id",
              foreignField: "_id",
              as: "eventInfo"
            }
        },
        { $match: fields },
        {
            $sort: {scheduleDate : 1}
        }
    ]).exec(function(err, totalSchedules){
        if(err){
            res.send({"message": "Something went wrong!", "err": err, "status": "500"});
        }else{
            Schedule.aggregate([
            {
                $lookup: {
                  from: "users",
                  localField: "employee_id",
                  foreignField: "_id",
                  as: "userInfo"
                }
            },
            {
                $lookup: {
                  from: "events",
                  localField: "event_account_id",
                  foreignField: "_id",
                  as: "eventInfo"
                }
            },
            { $match: fields },
            {
                $sort: {scheduleDate : 1}
            },
            {
                $skip: skipNo
            }, {
                $limit: count
            }
            ]).exec(function(error, schedules){
                schedules.map(function(item) {
                  item.employee_id = item.userInfo[0];  
                  item.event_account_id = item.eventInfo[0];  
                })
                if (error) {
                   res.send({"message": "Something went wrong!", "err": error, "status": "500"});
                } else {
                    res.send({"message": "success", "data": schedules,"total": totalSchedules.length, "status_code": "200"});
                }
            });
        }
    });
}

exports.getshiftsacknowledgementForDashboard = function(req, res) {
    var alignedID = req.body.alignedBy_id;
    var today = moment.utc().tz(constantObj.Timezones.AP);
    var currdate = today._d;
    var currdate = new Date(currdate.setHours(0, 0, 0, 0));
    var page  = req.body.page || 1;
    count = req.body.count || 50;
    var skipNo = (page - 1) * count;
    var searchStr = req.body.search;
    var fields = {scheduleDate:{ $gte : currdate}, $or:[{is_approve:{$exists:false}},{is_approve:false}], "userInfo.active" : true, "eventInfo.is_active" : true};
    if(searchStr && searchStr.eventId){
        fields.event_account_id = mongoose.Types.ObjectId(searchStr.eventId);
    }
    if(searchStr && searchStr.empId){
        fields.employee_id = mongoose.Types.ObjectId(searchStr.empId);
    }
    Schedule.aggregate([
    {
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "userInfo"
        }
    },
    {
        $lookup: {
          from: "events",
          localField: "event_account_id",
          foreignField: "_id",
          as: "eventInfo"
        }
    },
    { $match: fields },
    {
      $sort: {scheduleDate : 1}
    },
    {
        $skip: skipNo
    }, {
        $limit: count
    }
    ]).exec(function(error, schedules){
        schedules.map(function(item) {
          item.employee_id = item.userInfo[0];  
          item.event_account_id = item.eventInfo[0];  
        })
        if (error) {
           res.send({"message": "Something went wrong!", "err": error, "status": "500"});
        } else {
            res.send({"message": "success", "data": schedules,"total": schedules.length, "status_code": "200"});
        }
    });
       
}

exports.getnotificationicon = function(id, callback) {
    var currdate = new Date();
    var currdate = new Date(currdate.setHours(0, 0, 0, 0));
    var fields = {"userInfo.location" : mongoose.Types.ObjectId(id.location)};
    fields.employee_id = id.user_id;
    fields.is_approve = { $exists: false };
    fields.scheduleDate = { $gte : currdate};   
    Schedule.aggregate([
        {
            $lookup: {
              from: "users",
              localField: "employee_id",
              foreignField: "_id",
              as: "userInfo"
            }
        },
        { $match: fields }
    ]).exec(function(err, schedules){
        if (err) {
            callback({
                "message": "error",
                "data": err,
                "status_code": "500"
            });
        } else {
            callback({
                "message": "success",
                "data": schedules.length,
                "status_code": "200"
            });
        }
    });
}

//overlapscheduled
exports.overlapschedule = function(req, res) {
    var case1 = req.body.case;
    console.log(req.body.case);
    if (case1 == 'afternoon_night') {
        Availability.find({
                is_afternoon_scheduled: true,
                is_night_scheduled: true
            }, function(err, data) {
                if (err) {
                    res.json(err);
                } else {
                    res.json({
                        "message": "success",
                        "data": data,
                        "status_code": "200"
                    });
                }
            }).populate("employee_id")
            .populate("manager_schedule.morning_schedule_details.event manager_schedule.late_night_schedule_details.event manager_schedule.afternoon_schedule_details.event manager_schedule.night_schedule_details.event");
    } else if (case1 == 'morning_afternoon') {
        Availability.find({
                is_morning_scheduled: true,
                is_afternoon_scheduled: true
            }, function(err, data) {
                if (err) {
                    res.json(err);
                } else {
                    res.json({
                        "message": "success",
                        "data": data,
                        "status_code": "200"
                    });
                }
            }).populate("employee_id")
            .populate("manager_schedule.morning_schedule_details.event manager_schedule.late_night_schedule_details.event manager_schedule.afternoon_schedule_details.event manager_schedule.night_schedule_details.event");
    } else if (case1 == 'morning_night') {
        Availability.find({
                is_morning_scheduled: true,
                is_night_scheduled: true
            }, function(err, data) {
                if (err) {
                    res.json(err);
                } else {
                    res.json({
                        "message": "success",
                        "data": data,
                        "status_code": "200"
                    });
                }
            }).populate("employee_id")
            .populate("manager_schedule.morning_schedule_details.event manager_schedule.late_night_schedule_details.event manager_schedule.afternoon_schedule_details.event manager_schedule.night_schedule_details.event");
    } else if (case1 == 'morning_latenight') {
        Availability.find({
                is_morning_scheduled: true,
                is_late_night_scheduled: true
            }, function(err, data) {
                if (err) {
                    res.json(err);
                } else {
                    res.json({
                        "message": "success",
                        "data": data,
                        "status_code": "200"
                    });
                }
            }).populate("employee_id")
            .populate("manager_schedule.morning_schedule_details.event manager_schedule.late_night_schedule_details.event manager_schedule.afternoon_schedule_details.event manager_schedule.night_schedule_details.event");
    } else if (case1 == 'afternoon_latenight') {
        Availability.find({
                is_afternoon_scheduled: true,
                is_late_night_scheduled: true
            }, function(err, data) {
                if (err) {
                    res.json(err);
                } else {
                    res.json({
                        "message": "success",
                        "data": data,
                        "status_code": "200"
                    });
                }
            }).populate("employee_id")
            .populate("manager_schedule.morning_schedule_details.event manager_schedule.late_night_schedule_details.event manager_schedule.afternoon_schedule_details.event manager_schedule.night_schedule_details.event");
    } else if (case1 == 'night_latenight') {
        Availability.find({
                is_night_scheduled: true,
                is_late_night_scheduled: true
            }, function(err, data) {
                if (err) {
                    res.json(err);
                } else {
                    res.json({
                        "message": "success",
                        "data": data,
                        "status_code": "200"
                    });
                }
            }).populate("employee_id")
            .populate("manager_schedule.morning_schedule_details.event manager_schedule.late_night_schedule_details.event manager_schedule.afternoon_schedule_details.event manager_schedule.night_schedule_details.event");
    } else if (case1 == 'latenight_morning') {
        Availability.find({
                is_late_night_scheduled: true,
                is_morning_scheduled: true
            }, function(err, data) {
                if (err) {
                    res.json(err);
                } else {
                    res.json({
                        "message": "success",
                        "data": data,
                        "status_code": "200"
                    });
                }
            }).populate("employee_id")
            .populate("manager_schedule.morning_schedule_details.event manager_schedule.late_night_schedule_details.event manager_schedule.afternoon_schedule_details.event manager_schedule.night_schedule_details.event");
    } else if (case1 == 'latenight_afternoon') {
        Availability.find({
                is_late_night_scheduled: true,
                is_afternoon_scheduled: true
            }, function(err, data) {
                if (err) {
                    res.json(err);
                } else {
                    res.json({
                        "message": "success",
                        "data": data,
                        "status_code": "200"
                    });
                }
            }).populate("employee_id")
            .populate("manager_schedule.morning_schedule_details.event manager_schedule.late_night_schedule_details.event manager_schedule.afternoon_schedule_details.event manager_schedule.night_schedule_details.event");

    }
}

//get overlappingshifts    
exports.overlappingshifts = function(req, res) {
    Timeslot.find({}).exec(function(err, data) {
        if (data[0].morning_start_period != "AM") {
            var mor_start = data[0].morning_start_time + 12;
        } else {
            var mor_start = data[0].morning_start_time;
        }

        if (data[0].afternoon_start_period != "AM") {
            var aftnon_start = data[0].afternoon_start_time + 12;
        } else {
            var aftnon_start = data[0].afternoon_start_time;
        }

        if (data[0].night_start_period != "AM") {
            var night_start = data[0].night_start_time + 12;
        } else {
            var night_start = data[0].night_start_time;
        }

        if (data[0].late_night_start_period != "AM") {
            var ltnt_start = data[0].late_night_start_time + 12;
        } else {
            var ltnt_start = data[0].late_night_start_time;
        }

        if (data[0].morning_end_period != "AM") {
            var mor_end = data[0].morning_end_time + 12;
        } else {
            var mor_end = data[0].morning_end_time;
        }

        if (data[0].afternoon_end_period != "AM") {
            var aftnon_end = data[0].afternoon_end_time + 12;
        } else {
            var aftnon_end = data[0].afternoon_end_time;
        }

        if (data[0].night_end_period != "AM") {
            var night_end = data[0].night_end_time + 12;
        } else {
            var night_end = data[0].night_end_time;
        }

        if (data[0].late_night_end_period != "AM") {
            var ltnt_end = data[0].late_night_end_time + 12;
        } else {
            var ltnt_end = data[0].late_night_end_time;
        }
        var overlapping_Shifts = [];

        if (mor_end > aftnon_start) {
            overlapping_Shifts.push({
                shift: 'morning shift and afternoon shift',
                case: 'morning_afternoon'
            })
        }
        if (mor_end > night_start) {
            overlapping_Shifts.push({
                shift: 'morning shift and night shift',
                case: 'morning_night'
            })
        }
        if (mor_end > ltnt_start) {
            overlapping_Shifts.push({
                shift: 'morning shift and latenight shift',
                case: 'morning_latenight'
            })
        }
        if (aftnon_end > night_start) {
            overlapping_Shifts.push({
                shift: 'afternoon shift and night shift',
                case: 'afternoon_night'
            })
        }
        if (aftnon_end > ltnt_start) {
            overlapping_Shifts.push({
                shift: 'afternoon shift and latenight shift',
                case: 'afternoon_latenight'
            })
        }
       
        if (night_end > ltnt_start) {
            overlapping_Shifts.push({
                shift: 'night shift and latenight shift',
                case: 'night_latenight'
            })
        }
       
        if (ltnt_end > mor_start) {
            overlapping_Shifts.push({
                shift: 'latenight shift and morning shift',
                case: 'latenight_morning'
            })
        }
        if (ltnt_end > aftnon_start) {
            overlapping_Shifts.push({
                shift: 'latenight shift and afternoon shift',
                case: 'latenight_afternoon'
            })
        }
        
        res.json({
            "message": "success",
            "data": overlapping_Shifts,
            "status_code": "200"
        });

    });

}
exports.assignmentToMultiple = function(req, res) {
    empdata = req.body.employee_id;
    var x = 0;
    for (var i = 0; i < empdata.length; i++) {
        (function(i) {
            Availability.findOne({
                employee_id: empdata[i].employee_id,
                currDate: req.body.assignDate
            }, function(err, response) {
                console.log(err);
                if (null !== response) {
                    if (req.body.assignShift == 'morning') {
                        response.manager_schedule.morning_schedule_details.is_morning_scheduled = true;
                        response.manager_schedule.morning_schedule_details.event = req.body.event_id;
                        response.is_morning_scheduled = true;
                    } else if (req.body.assignShift == 'afternoon') {
                        response.manager_schedule.afternoon_schedule_details.is_afternoon_scheduled = true;
                        response.manager_schedule.afternoon_schedule_details.event = req.body.event_id;
                        response.is_afternoon_scheduled = true;
                    } else if (req.body.assignShift == 'night') {
                        response.manager_schedule.night_schedule_details.is_night_scheduled = true;
                        response.manager_schedule.night_schedule_details.event = req.body.event_id;
                        response.is_night_scheduled = true;
                    } else if (req.body.assignShift == 'latenight') {
                        response.manager_schedule.late_night_schedule_details.is_late_night_scheduled = true;
                        response.manager_schedule.late_night_schedule_details.event = req.body.event_id;
                        response.is_late_night_scheduled = true;
                    }
                    response.manager_schedule.modified_date = req.body.modified_date;
                    response.manager_schedule.modified_by = req.body.modified_by;
                    response.save(function(err, updateResponse) {
                        if (!err) {
                            res.json({
                                "message": "success",
                                "status_code": "200"
                            });
                        }
                        if (x == empdata.length - 1) {
                            res.json({
                                "message": "success",
                                "status_code": "200"
                            });
                        }
                    });
                } else {
                    var assignedData = {};
                    assignedData.manager_schedule = {};
                    assignedData.manager_schedule.morning_schedule_details = {};
                    assignedData.manager_schedule.afternoon_schedule_details = {};
                    assignedData.manager_schedule.night_schedule_details = {};
                    assignedData.manager_schedule.late_night_schedule_details = {};
                    assignedData.employee_id = req.body.employee_id;
                    assignedData.currDate = req.body.assignDate;
                    if (req.body.assignShift == 'morning') {
                        assignedData.manager_schedule.morning_schedule_details.is_morning_scheduled = true;
                        assignedData.manager_schedule.morning_schedule_details.event = req.body.event_id;
                        assignedData.is_morning_scheduled = true;
                    } else if (req.body.assignShift == 'afternoon') {
                        assignedData.manager_schedule.afternoon_schedule_details.is_afternoon_scheduled = true;
                        assignedData.manager_schedule.afternoon_schedule_details.event = req.body.event_id;
                        assignedData.is_afternoon_scheduled = true;
                    } else if (req.body.assignShift == 'night') {
                        assignedData.manager_schedule.night_schedule_details.is_night_scheduled = true;
                        assignedData.manager_schedule.night_schedule_details.event = req.body.event_id;
                        assignedData.is_night_scheduled = true;
                    } else if (req.body.assignShift == 'latenight') {
                        assignedData.manager_schedule.late_night_schedule_details.is_late_night_scheduled = true;
                        assignedData.manager_schedule.late_night_schedule_details.event = req.body.event_id;
                        assignedData.is_late_night_scheduled = true;
                    }
                    assignedData.manager_schedule.modified_date = req.body.modified_date;
                    assignedData.manager_schedule.modified_by = req.body.modified_by;
                    Availability(assignedData).save(function(err, saveResponse) {
                        if (!err) {
                            res.json({
                                "message": "success",
                                "status_code": "200"
                            });
                        }

                        if (x == empdata.length - 1) {
                            res.json({
                                "message": "success",
                                "status_code": "200"
                            });
                        }
                    })
                }

                User.findOne({
                    _id: empdata[i].employee_id
                }).exec(function(err, user) {
                    //For event info
                    Event.findOne({
                        _id: req.body.event_id
                    }).exec(function(err, event) {
                        var email = user.email;
                        // setup e-mail data with unicode symbols
                        var mailOptions = {
                            from: constantObj.emailSettings.fromWords, // sender address
                            to: user.email, // list of receivers
                            subject: "Event Assignment", // Subject line                
                            html: "Hello " + user.first_name.toUpperCase() + ", <p style='padding-left:10px'>A new event has been assigned to you for <b>" + req.body.assignShift + "</b> shift. Following are the details:<br><br> Event Name :<b>" + event.name + "</b><br><br> Event Start Date :<b>" + event.start_date + "</b><br><br> Event Start Date :<b>" + event.end_date + "</b><br><br>Note :- Kindly login to your EMS Account and see the full details of your assignment.</p>" + constantObj.emailSettings.signature // html body
                        };
                        // send mail with defined transport object
                        constantObj.transporter.sendMail(mailOptions, function(error, info) {
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Message sent: ' + info.response);
                            }
                        });
                    });
                });
                x++;
            });

        })(i);
    }
}

exports.saveWeeklyComment = function(req, res) {
    var empid = req.body.emp_id;
    var startWeek = req.body.startWeek;
    var endWeek = req.body.endWeek;
    var report = req.body.report;
    var dataToUpdate = {
        employee_id: empid,
        startWeekDate: startWeek,
        endWeekDate: endWeek,
        comment: report
    };
    Report.findOneAndUpdate({
        employee_id: empid,
        startWeekDate: startWeek,
        endWeekDate: endWeek
    }, dataToUpdate, {
        upsert: true, new : true
    }, function(err, report) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            res.send({
                "message": "success",
                "data": report,
                "status_code": "200"
            });
        }
    });
}
exports.getWeeklyComment = function(req, res){
    var empid = req.body.emp_id;
    var startWeek = req.body.startWeek;
    var endWeek = req.body.endWeek;
    Report.findOne({
        employee_id: empid,
        startWeekDate: startWeek,
        endWeekDate: endWeek
    }).exec(function(err, report) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            res.send({
                "message": "success",
                "data": report,
                "status_code": "200"
            });
        }
    });
}

exports.getReport = function(req, res) {
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var ownerId = mongoose.Types.ObjectId(req.body.owner_id);
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var startdateStr = new Date(req.body.dateRange.start_date);
    var endDatestr = new Date(req.body.dateRange.end_date);
    var query = {  $or: [{
            "endWeekDate": {
                $lte: endDatestr,
                $gte: startdateStr
            }
        }, {
            "startWeekDate": {
                $lte: endDatestr,
                $gte: startdateStr
            }
        }],
        comment: {
            $exists: true
        },
        "employee.location" : mongoose.Types.ObjectId(req.body.location)
    };
   
    Report.aggregate([{
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "employee"
        }
      }, {
        $match: query
      }]).exec(function(err, total) {
    if (err) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      Report.aggregate([{
         $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "employee"
        }
      }, {
        $match: query
      }, {
        $sort: sortObject
      },{
        $skip: skipNo
      }, {
        $limit: count
      }]).exec(function(err, reports) {
        if (!err) {
          res.send({
            "message": "success",
            "data": reports,
            "total": total.length,
            "status_code": "200"
          });
        } else {
          res.send({
            "message": "success",
            "data": err,
            "total": total.length,
            "status_code": "200"
          });
        }
      });
    }
  });
};

exports.getCommentReport = function(req, res) {
    var startWeek = req.body.from;
    var endWeek = req.body.to;
    var search = req.body.search || "";
    var query = {};
    var startdateStr = new Date(startWeek);
    var endDatestr = new Date(endWeek);
    var query = { 
        startWeekDate: startdateStr,
        endWeekDate: endDatestr,
        comment: {
            $exists: true
        },
        "employee.location" : mongoose.Types.ObjectId(req.body.location)
    };
    if(search){
        query.employee_id = mongoose.Types.ObjectId(search);
    }
    Report.aggregate([{
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "employee"
        }
      }, {
        $match: query
      }]).exec(function(err, report) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            res.send({
                "message": "success",
                "data": report,
                "status_code": "200"
            });
        }
      });
};

/*end*/
/*get count of NO filled by employees*/
exports.getnocount = function(req, res) {
    var to = new Date(req.body.data[6]);
    var from = new Date(req.body.data[0]);
    var data = req.body.data;
    var query = {};
    var query = {'userInfo.location' : mongoose.Types.ObjectId(req.body.location), 'userInfo.active' : true, $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
    query.currDate = {
        $gte : from,
        $lte : to
    }

    Availability.aggregate([
    {
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "userInfo"
        }
    },
    { $match: query }]).exec(function(err, data) {
        data.map(function(item) {
          item.employee_id = item.userInfo[0];  
        })
        var result = _.chain(data)
            .groupBy("currDate")
            .pairs()
            .map(function(currentItem) {
                return _.object(_.zip(["date", "result"], currentItem));
            })
            .value();
        var tmpArr = [];
        result.forEach(function(value, key) {
            var obj = {};
            obj.date = value.date;
            obj.shifts = {};
            obj.shifts.morning = 0;
            obj.shifts.afternoon = 0;
            obj.shifts.night = 0;
            obj.shifts.late_night = 0;
            value.result.forEach(function(value1, key1) {
                if (value1.is_morning_scheduled_type == 'no') {
                    obj.shifts.morning = obj.shifts.morning + 1;
                }
                if (value1.is_afternoon_scheduled_type == 'no') {
                    obj.shifts.afternoon = obj.shifts.afternoon + 1;
                }
                if (value1.is_night_scheduled_type == 'no') {
                    obj.shifts.night = obj.shifts.night + 1;
                }
                if (value1.is_night_scheduled_type == 'no') {
                    obj.shifts.late_night = obj.shifts.late_night + 1;
                }
            })
            tmpArr.push(obj);
        })
        res.send({
            "message": "success",
            "data": tmpArr,
            "status_code": "200"
        });
    })
}
    /*end*/

/*added by suman to get count of shifts*/
exports.getshiftcount = function(req, res) {
        var to = new Date(req.body.data[6]);
        var from = new Date(req.body.data[0]);
        var data = req.body.data;
        var startweek = from;
        var endweek = to;
        var firstand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}}]};
        var secondand = {$and:[{end_date:{$gte:startweek}},{end_date:{$lte:endweek}}]};
        var thirdand = {$and:[{start_date:{$lte:startweek}},{end_date:{$gte:endweek}}]};
        var fourdand = {$and:[{start_date:{$lte:startweek}},{is_repeat : true}]};
        var fivedand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}},{is_repeat : true}]};
        var conditions = {is_active: true, location : req.body.location, $or:[firstand,secondand,thirdand,fourdand,fivedand]};
        Event.find(conditions).exec(function(err, result) {
            if (err) {
                res.send({
                    "message": "Something went wrong!",
                    "err": err,
                    "status_code": "500"
                });
            } else {
                var a = from;
                var b = to;
                var diff = moment(b, "MM-DD-YYYY").diff(moment(a, "MM-DD-YYYY"), 'days');
                var daysArr = []
                for (var i = 0; i <= diff; i++) {
                    var obj = {}
                    obj.day = moment(a).add(i, 'days').format("dddd");
                    obj.date = moment(a).add(i, 'days').format("MM-DD-YYYY");
                    daysArr.push(obj);
                }

                var finalRes = {};
                result.forEach(function(value, key) {
                    var a1 = moment(value.start_date, "MM-DD-YYYY");
                    var a2 = moment(value.end_date, "MM-DD-YYYY");
                    daysArr.forEach(function(dayData, dayKey) {
                        if (!finalRes[dayData.day]) {
                            finalRes[dayData.day] = {
                                morning: 0,
                                afternoon: 0,
                                night: 0,
                                latenight: 0
                            }
                        }
                        value.shift_template_id.forEach(function(value1, key1) {
                            if (dayData.day == value1.day) {
                                if (value1.timeslot == 'Morning') {
                                    finalRes[dayData.day].morning = finalRes[dayData.day].morning + value1.shifts.length;
                                }
                                if (value1.timeslot == 'Afternoon') {
                                    finalRes[dayData.day].afternoon = finalRes[dayData.day].afternoon + value1.shifts.length;
                                }
                                if (value1.timeslot == 'Night') {
                                    finalRes[dayData.day].night = finalRes[dayData.day].night + value1.shifts.length;
                                }
                                if (value1.timeslot == 'Late Night') {
                                    finalRes[dayData.day].latenight = finalRes[dayData.day].latenight + value1.shifts.length;
                                }
                            }
                        })
                    })
                })
                res.send({
                    "message": "success",
                    "data": finalRes,
                    "status_code": "200"
                });
            }
        })
}

exports.getYesMaybeCount = function(req, res){
    var to = new Date(req.body.to);
    var from = new Date(req.body.from);
    var data = req.body.data;
    var query = {};
    var query = {'userInfo.location' : mongoose.Types.ObjectId(req.body.location), 'userInfo.active' : true, $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
    query.currDate = {
        $gte: from,
        $lte: to
    }
    Availability.aggregate([
    {
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "userInfo"
        }
    },
    { $match: query }]).exec(function(err, response) {
        response.map(function(item) {
          item.employee_id = item.userInfo[0];  
        })
        var result = _.chain(response)
            .groupBy("currDate")
            .pairs()
            .map(function(currentItem) {
                return _.object(_.zip(["date", "result"], currentItem));
            })
            .value();
        var tmpArr = [];
        result.forEach(function(value, key) {
            var obj = {};
            obj.date = value.date;
            obj.shifts = {};
            obj.shifts.morning = 0;
            obj.shifts.afternoon = 0;
            obj.shifts.night = 0;
            obj.shifts.late_night = 0;
            value.result.forEach(function(value1, key1) {
                if (value1.is_morning_scheduled_type == 'yes' || value1.is_morning_scheduled_type == 'maybe') {
                    obj.shifts.morning = obj.shifts.morning + 1;
                }
                if (value1.is_afternoon_scheduled_type == 'yes' || value1.is_afternoon_scheduled_type == 'maybe') {
                    obj.shifts.afternoon = obj.shifts.afternoon + 1;
                }
                if (value1.is_night_scheduled_type == 'yes' || value1.is_night_scheduled_type == 'maybe') {
                    obj.shifts.night = obj.shifts.night + 1;
                }
                if (value1.is_night_scheduled_type == 'yes' || value1.is_night_scheduled_type == 'maybe') {
                    obj.shifts.late_night = obj.shifts.late_night + 1;
                }
            })
            tmpArr.push(obj);
        })
        res.send({
            "message": "success",
            "data": tmpArr,
            "status_code": "200"
        });
    })
}

exports.getYesMaybeScrewedCount = function(req, res){
    var to = new Date(req.body.to);
    var from = new Date(req.body.from);
    var data = req.body.data;
    var query = {};
    var query = {'userInfo.location' : mongoose.Types.ObjectId(req.body.location), 'userInfo.active' : true, $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
    query.currDate = {
        $gte: from,
        $lte: to
    }
    Availability.aggregate([
    {
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "userInfo"
        }
    },
    { $match: query }]).exec(function(err, response) {
        response.map(function(item) {
          item.employee_id = item.userInfo[0];  
        })
        var result = _.chain(response)
            .groupBy("currDate")
            .pairs()
            .map(function(currentItem) {
                return _.object(_.zip(["date", "result"], currentItem));
            })
            .value();
        var tmpArr = [];
        result.forEach(function(value, key) {
            var obj = {};
            obj.date = value.date;
            obj.shifts = {};
            obj.shifts.morning = 0;
            obj.shifts.afternoon = 0;
            obj.shifts.night = 0;
            obj.shifts.late_night = 0;
            value.result.forEach(function(value1, key1) {
                if (value1.is_morning_scheduled_type == 'yes' || value1.is_morning_scheduled_type == 'maybe' || value1.is_morning_scheduled_force == true) {
                    obj.shifts.morning = obj.shifts.morning + 1;
                }
                if (value1.is_afternoon_scheduled_type == 'yes' || value1.is_afternoon_scheduled_type == 'maybe' || value1.is_afternoon_scheduled_force == true) {
                    obj.shifts.afternoon = obj.shifts.afternoon + 1;
                }
                if (value1.is_night_scheduled_type == 'yes' || value1.is_night_scheduled_type == 'maybe' || value1.is_night_scheduled_force == true) {
                    obj.shifts.night = obj.shifts.night + 1;
                }
                if (value1.is_night_scheduled_type == 'yes' || value1.is_night_scheduled_type == 'maybe' || value1.is_late_night_scheduled_force == true) {
                    obj.shifts.late_night = obj.shifts.late_night + 1;
                }
            })
            tmpArr.push(obj);
        })
        res.send({
            "message": "success",
            "data": tmpArr,
            "status_code": "200"
        });
    })
}

exports.getextrashiftcount = function(req, res) {
        var to = new Date(req.body.data[6]);
        var from = new Date(req.body.data[0]);
        var data = req.body.data;
        var startweek = from;
        var endweek = to;
        var firstand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}}]};
        var secondand = {$and:[{end_date:{$gte:startweek}},{end_date:{$lte:endweek}}]};
        var thirdand = {$and:[{start_date:{$lte:startweek}},{end_date:{$gte:endweek}}]};
        var fourdand = {$and:[{start_date:{$lte:startweek}},{is_repeat : true}]};
        var fivedand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}},{is_repeat : true}]};
        var conditions = {is_active: true, location : req.body.location, $or:[firstand,secondand,thirdand,fourdand,fivedand], "extrashifts": { $not: { $size: 0 } }};
        Event.find(conditions).exec(function(err, result) {
            if (err) {
                res.send({
                    "message": "Something went wrong!",
                    "err": err,
                    "status_code": "500"
                });
            } else {
                var a = from;
                var b = to;
                var diff = moment(b, "MM-DD-YYYY").diff(moment(a, "MM-DD-YYYY"), 'days')
                var daysArr = []
                for (var i = 0; i <= diff; i++) {
                    var obj = {}
                    obj.day = moment(a).add(i, 'days').format("dddd");
                    obj.date = moment(a).add(i, 'days').format("MM-DD-YYYY");
                    daysArr.push(obj);
                }

                var finalRes = {};
                result.forEach(function(value, key) {
                    var a1 = moment(value.start_date, "MM-DD-YYYY");
                    var a2 = moment(value.end_date, "MM-DD-YYYY");
                    daysArr.forEach(function(dayData, dayKey) {
                        if (!finalRes[dayData.day]) {
                            finalRes[dayData.day] = {
                                morning: 0,
                                afternoon: 0,
                                night: 0,
                                latenight: 0
                            }
                        }
                        value.extrashifts.forEach(function(value1, key1) {
                            if (dayData.day == value1.day) {
                                if (value1.timeslot == 'Morning') {
                                    finalRes[dayData.day].morning = finalRes[dayData.day].morning + value1.shifts.length;
                                }
                                if (value1.timeslot == 'Afternoon') {
                                    finalRes[dayData.day].afternoon = finalRes[dayData.day].afternoon + value1.shifts.length;
                                }
                                if (value1.timeslot == 'Night') {
                                    finalRes[dayData.day].night = finalRes[dayData.day].night + value1.shifts.length;
                                }
                                if (value1.timeslot == 'Late Night') {
                                    finalRes[dayData.day].latenight = finalRes[dayData.day].latenight + value1.shifts.length;
                                }
                            }
                        })
                    })
                })
                res.send({
                    "message": "success",
                    "data": finalRes,
                    "status_code": "200"
                });
            }
        })
}

// For employee availability, we have a cut off time of midnight the Wednesday 11 days in advance.  For example, the availability for the week of 7/31 would need to be submitted by 11:59 pm on 7/20
exports.availabilityReminder = function(req, res) {
    var today = new Date(new Date().setHours(0, 0, 0, 0));
    var day = today.getDay();
    var date = today.getDate() - day;
    var StartDate = new Date(today.setDate(date));
    var date = StartDate.getDate();
    var EndDate = new Date(today.setDate(date + 14)); // For getting start date of nextnextweek
    var currentDate = moment(new Date());
    var nextnextweekdate = moment(EndDate);
    var duration = nextnextweekdate.diff(currentDate, 'days') + 1;
    if (duration >= 11) {
        var query = {
            currDate: EndDate
        };
        Availability.find(query, {
            "employee_id": 1
        }).exec(function(err, data) {
            var employeeIdsWhoMarkAvailbility = _.chain(data)
            .map(function(currentItem) {
                return currentItem.employee_id;
            })
            .value();
            User.find({
                roles: ["employee"],
                _id: {
                    $nin: employeeIdsWhoMarkAvailbility
                },
                active: true,
                is_deleted:false
            }, function(err, users) {
                var empNeedReminder = _.chain(users)
                    .map(function(currentItem) {
                        return currentItem;
                    })
                    .value();
                var phone_number = _.chain(empNeedReminder)
                    .map(function(currentItem) {
                        if (currentItem.phone) {
                            return currentItem.phone;
                        }
                    }).filter(function(obj) {
                        if (obj) {
                            return true
                        } else {
                            return false
                        }
                    })
                    .value();
                exports.send_sms(req, res, 0, phone_number);
            });
        });
    } 
    else {
        console.log("Something wrong,check time");
    }
}
    /*end*/
exports.send_sms = function(req, res, i, phone_number) {
    var client = require('twilio')(constantObj.twilio.accountSid, constantObj.twilio.authToken);
    client.messages.create({
        to: phone_number[i],
        from: constantObj.twilio.twilio_number,//test number of client registered in twilio
        body: "This is a reminder to complete your availability by midnight if you have not completed that already.\n\n"+ constantObj.twilio.twilio_signature 
    }, function(err, message) {
        if (i + 1 == phone_number.length) {
            console.log("Sent to all");
        } else {
            exports.send_sms(req, res, i + 1, phone_number);
        }
    });
}
/*Schedule Employee Module*/
exports.getempschedulelist = function(req, res) {
    var to = new Date(req.body.to);
    var from = new Date(req.body.from);
    var data = req.body.data;
    var searchEmployeeId = req.body.searchEmpId;
    var query = {};
    var query = {'userInfo.location' : mongoose.Types.ObjectId(req.body.location), 'userInfo.active' : true, $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
    query.currDate = {
        $gte: from,
        $lte: to
    }
    if(searchEmployeeId){
        query.employee_id = mongoose.Types.ObjectId(searchEmployeeId);
    }
    Availability.aggregate([
    {
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "userInfo"
        }
    },
    { $match: query }]).exec(function(err, response) {
        response.map(function(item) {
          item.employee_id = item.userInfo[0];  
        })
        var result = _.chain(response)
        .groupBy("currDate")
        .pairs()
        .map(function(currentItem) {
            return _.object(_.zip(["date", "result"], currentItem));
        })
        .value();
        res.send({
            "message": "success",
            "data": result,
            "status_code": "200"
        });
    })
}

exports.getempschedule = function(req, res){
    var to = new Date(req.body.to);
    var from = new Date(req.body.from);
    var data = req.body.data;
    var query = {};
    var query = {'userInfo.location' : mongoose.Types.ObjectId(req.body.location), 'userInfo.active' : true, $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
    query.currDate = {
        $gte: from,
        $lte: to
    }
    Availability.aggregate([
    {
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "userInfo"
        }
    },
    { $match: query }]).exec(function(err, response) {
        response.map(function(item) {
          item.employee_id = item.userInfo[0];  
        })
        var result = _.chain(response)
        .groupBy("currDate")
        .pairs()
        .map(function(currentItem) {
            return _.object(_.zip(["date", "result"], currentItem));
        })
        .value();
        res.send({
            "message": "success",
            "data": result,
            "status_code": "200"
        });
    })
}

exports.dayAvailabilityForAllEmp = function(req, res) {
    var weekDay = new Date(req.body.weekDay);
    var query = {};
    var query = {currDate: weekDay, 'userInfo.location' : mongoose.Types.ObjectId(req.body.location), 'userInfo.active' : true, $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
    Availability.aggregate([
    {
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "userInfo"
        }
    },
    { $match: query }]).exec(function(err, response) {
        response.map(function(item) {
          item.employee_id = item.userInfo[0];  
        })
        if (err) {
            res.status(500).jsonp({"currDate": weekDay, "error": err });
        } else {
            res.status(200).jsonp({"data" :response});
        }
    });
}

exports.saveWeekAvailItration = function(req, res, i, weekData){
    var resultArr = [];
    if(weekData.length){
        Availability.findOne({employee_id : weekData[i].employee_id, currDate: new Date(weekData[i].currDate)},
            function(err, response) {
                if (null !== response) {
                    if(weekData[i].is_morning_scheduled_type){
                        response.is_morning_scheduled = weekData[i].is_morning_scheduled;    
                        response.is_morning_scheduled_type = weekData[i].is_morning_scheduled_type;
                        response.is_morning_scheduled_force = weekData[i].is_morning_scheduled_force;
                    }

                    if(weekData[i].is_afternoon_scheduled_type){
                        response.is_afternoon_scheduled = weekData[i].is_afternoon_scheduled;
                        response.is_afternoon_scheduled_type = weekData[i].is_afternoon_scheduled_type;
                        response.is_afternoon_scheduled_force = weekData[i].is_afternoon_scheduled_force;
                    }

                    if(weekData[i].is_night_scheduled_type){
                        response.is_night_scheduled = weekData[i].is_night_scheduled;
                        response.is_night_scheduled_type = weekData[i].is_night_scheduled_type;
                        response.is_night_scheduled_force = weekData[i].is_night_scheduled_force;
                    }

                    if(weekData[i].is_late_night_scheduled_type){
                        response.is_late_night_scheduled = weekData[i].is_late_night_scheduled;
                        response.is_late_night_scheduled_type = weekData[i].is_late_night_scheduled_type;
                        response.is_late_night_scheduled_force = weekData[i].is_late_night_scheduled_force;
                    }

                    
                    response.save(function(err, updateResponse) {
                        if (!err) {
                            resultArr.push(response);
                        }
                        var lengthcheck = weekData.length - 1;
                        if (i == lengthcheck) {
                            resultArr.sort(function(a, b) {
                                var dateA = new Date(a.currDate);
                                var dateB = new Date(b.currDate);
                                return dateA - dateB;
                            });
                            res.json({"message": "success","data": resultArr,"status_code": "200"});
                        }
                        else{
                            exports.saveWeekAvailItration(req, res, i + 1, weekData);
                        }
                    });
                } else {
                    Availability(weekData[i]).save(function(err, saveResponse) {
                        if (!err) {
                            resultArr.push(saveResponse);
                        }
                        if (i == weekData.length - 1) {
                            res.json({
                                "message": "success",
                                "data": resultArr,
                                "status_code": "200"
                            });
                        }else{
                            exports.saveWeekAvailItration(req, res, i + 1, weekData);
                        }
                    })
                }
            }
        );
    }
}
exports.saveWeekAvailability = function(req, res) {
    weekData = req.body;
    weekData.sort(function(a, b) {
        var dateA = new Date(a.currDate);
        var dateB = new Date(b.currDate);
        return dateA - dateB;
    });
    var x = 0;
    exports.saveWeekAvailItration(req, res, 0, weekData);
};

/*end*/