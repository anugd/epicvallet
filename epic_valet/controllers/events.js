var Event = require("mongoose").model("Event");
var Schedule = require("mongoose").model("Schedule");
var Trade = require("mongoose").model("Trade");
var mongoose = require('mongoose');
var path = require('path');
var fs = require('fs');
  exports.createEvent = function( req, res, next ) {
    console.log("--CREATE EVENT--");
    var eventData = req.body;
    eventData.name = req.body.name.substring(0,1).toLocaleUpperCase() + req.body.name.substring(1);
    eventData.event_data.created_at = new Date();
    if (true === eventData.is_repeat) {
      var dat = eventData.recurring_event_data;
      eventData.recurring_event_data = [];
      eventData.recurring_event_data[0] = dat;
    }
    eventData.location_coordinates = [eventData.location_address.lng,eventData.location_address.lat];
    Event.create(eventData, function(err, event) {
      if (!err) {
        if ( typeof eventData.shiftData !== 'undefined' && eventData.shiftData.length > 0 ) {
          eventData.shift_template_id = eventData.shiftData;
          Event.findOneAndUpdate({_id: event._id}, {shift_template_id: eventData.shift_template_id}, function(err, event){
            if (err) {
              res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            }
            else {
              res.send({"message": "success", "data": event, "status_code": "200"});
            }
          });
        }
        else {
          res.send({"status_code": "200", "event": event,"message": "Added Successfully"});
        }  
      }
      else{
        res.send({ reason: err.toString() });      
      }
    });
  };
  
  exports.getEvents = function( req, res, next ) {
      Event.find({is_repeat:{$ne:true}, is_active:true, location : req.body.location}, null,{ sort: {"event_data.created_at": -1 } },function(err, events){
        res.send({"status_code": "200", "events": events, "message": "All Event List"});
      });
  };

  exports.getAccounts = function( req, res, next ) {
      Event.find({is_repeat:true, is_active:true, location : req.body.location}, null,{ sort: {"event_data.created_at": -1 } },function(err, events){
        res.send({"status_code": "200", "events": events, "message": "All Event List"});
      });
  };

  exports.getEventAccounts = function( req, res, next ) {
      Event.find({is_active:true, location : req.body.location}, null,{ sort: {"name": 1 } },function(err, events){
        res.send({"status_code": "200", "events": events, "message": "All Event List"});
      });
  };
  exports.getAllEventAccounts = function( req, res, next ) {
      Event.find({location : req.body.location}, null,{ sort: {"name": 1 } },function(err, events){
        res.send({"status_code": "200", "events": events, "message": "All Event List"});
      });
  };
  exports.getEventAccountsWithTicketControl = function( req, res, next ) {
      Event.find({is_active:true, location : req.body.location, ticket_control:true}, null,{ sort: {"event_data.created_at": -1 } },function(err, events){
        res.send({"status_code": "200", "events": events, "message": "All Event List"});
      });
  };
  
  //getadminEvents
  exports.getadminEvents = function( req, res, next ) {
      Event.find({is_active:true}, null,{ sort: {"event_data.created_at": -1 } },function(err, events){
        res.send({"status_code": "200", "events": events, "message": "All Event List"});
      });
  }
  
  exports.getManagerEvents = function( req, res, next ) {
      Event.find({set_up_person: req.body.manager_id,is_active:true, is_repeat:{$ne:true}}, null,{ sort: {"event_data.created_at": -1 } },function(err, events){
        res.send({"status_code": "200", "events": events, "message": "All Event List"});
      });
  };

  exports.getManagerAccounts = function( req, res, next ) {
      Event.find({is_repeat:true, set_up_person: req.body.manager_id,is_active:true}, null,{ sort: {"event_data.created_at": -1 } },function(err, events){
        res.send({"status_code": "200", "events": events, "message": "All Event List"});
      });
  };

  exports.getManagerEventAccounts = function( req, res, next ) {
      Event.find({is_active:true, set_up_person: req.body.manager_id}, null,{ sort: {"event_data.created_at": -1 } },function(err, events){
        res.send({"status_code": "200", "events": events, "message": "All Event List"});
      });
  };
  
  exports.getEventById = function(req, res){
    if (req.params.id) {
      Event.findById(req.params.id, function(err, event){
        if (!err) {
          res.send({"status_code": "200", "data": event});
        }
        else{
          res.send({"status_code": "500", "data": err});
        }
      });
    }
  };
  
  // Update Event For An _id
  exports.updateEvent = function( req, res ) {
    var updateData = req.body;
    updateData.name = req.body.name.substring(0,1).toLocaleUpperCase() + req.body.name.substring(1);
    var updateId = req.body._id;
    if (true === updateData.is_repeat) {
      var dat = updateData.recurring_event_data;
      updateData.recurring_event_data=[];
      updateData.recurring_event_data[0]=dat;
    }
    
    if (typeof updateData.shiftData !== 'undefined' && updateData.shiftData.length > 0 ) {
      updateData.shift_template_id = updateData.shiftData;
    } 
    delete updateData._id;
    if(updateData.location_address){
      updateData.location_coordinates = [updateData.location_address.lng,updateData.location_address.lat];
    }
    Event.findOneAndUpdate({_id: updateId}, updateData, function(err, event){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else {
        res.send({"message": "success", "data": event, "status_code": "200"});
      }
    });
  };
  
  exports.getschedulecount = function(req,res){
      var currdate = req.body.currDate;
      var loginuserid = req.body._id;
      var f_type = req.body.fetchtype;
      if (f_type == 'today') {
        var firstand = {$and:[{start_date:{$lte:currdate}},{end_date:{$gte:currdate}}]};
        var secondand = {$and:[{start_date:{$lte:currdate}},{is_repeat : true}]};
        var conditions = {location:req.body.location, is_active: true, $or:[firstand,secondand]};
      }
      if (f_type == 'week') {
        var startweek = req.body.startWeek;
        var endweek = req.body.endWeek;
        var firstand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}}]};
        var secondand = {$and:[{end_date:{$gte:startweek}},{end_date:{$lte:endweek}}]};
        var thirdand = {$and:[{start_date:{$lte:startweek}},{end_date:{$gte:endweek}}]};
        var fourdand = {$and:[{start_date:{$lte:startweek}},{is_repeat : true}]};
        var fivedand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}},{is_repeat : true}]};
        var conditions = {location:req.body.location, is_active: true, $or:[firstand,secondand,thirdand,fourdand,fivedand]};
      }
      if (f_type == 'month') {
        var currMonthStartDate = req.body.currMonthStartDate;
        var currMonthEndDate = req.body.currMonthEndDate;
        var firstand = {$and:[{start_date:{$gte:currMonthStartDate}},{start_date:{$lte:currMonthEndDate}}]};
        var secondand = {$and:[{end_date:{$gte:currMonthStartDate}},{end_date:{$lte:currMonthEndDate}}]};
        var thirdand = {$and:[{start_date:{$lte:currMonthStartDate}},{end_date:{$gte:currMonthEndDate}}]};
        var fourdand = {$and:[{start_date:{$lte:currMonthStartDate}},{is_repeat : true}]};
        var fivedand = {$and:[{start_date:{$gte:currMonthStartDate}},{start_date:{$lte:currMonthEndDate}},{is_repeat : true}]};
        var conditions = {location:req.body.location, is_active: true, $or:[firstand,secondand,thirdand,fourdand,fivedand]};
      }
      Event.find(conditions, {'event_data.created_at' : 1, is_repeat : 1, name : 1, start_date : 1, end_date : 1, showendtime : 1},{ sort: {"event_data.created_at": -1 } }).exec(function(err, events) {
        res.send({"status_code": "200", "data": events});
      });
  }

  //get reoccurring account event
  exports.reoccurringaccount=function(req,res){
    Event.find({is_repeat:true},null,{ sort: {"event_data.created_at": -1 } } ,function(err, events){
      if(!err){
          res.send({"status_code": "200", "events": events, "message": "reoccurring events"});
              }
      else {
          res.send({"status_code": "400", "message": "error"});
           }
    });
  }

  //view employee for events
  exports.viewempforevents=function(req,res){
    var date= new Date(req.body.date);
    Event.find({start_date:{$lte:date},end_date:{$gte:date}},{name:1},function(err,data){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else {
        res.send({"message": "success", "data": data, "status_code": "200"});
      }
    })
  }

  /*added by suman api to get weekly events*/
  exports.getweeklyevents = function(req, res) {
      var page = req.body.page || 1,
      count = req.body.count || 1;
      var skipNo = (page - 1) * count;
      var to = new Date(req.body.to);
      var from = new Date(req.body.from);
      var search = req.body.search || "";
      var query = {};
      var startweek = from;
      var endweek = to;
      var firstand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}}]};
      var secondand = {$and:[{end_date:{$gte:startweek}},{end_date:{$lte:endweek}}]};
      var thirdand = {$and:[{start_date:{$lte:startweek}},{end_date:{$gte:endweek}}]};
      var fourdand = {$and:[{start_date:{$lte:startweek}},{is_repeat : true}]};
      var fivedand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}},{is_repeat : true}]};
      var conditions = {is_active: true, location : req.body.location, $or:[firstand,secondand,thirdand,fourdand,fivedand]};
      if (search) {
        var conditions = {is_active: true, location : req.body.location, $or:[firstand,secondand,thirdand,fourdand], name: new RegExp(search,'i')};
      }
      Event.count(conditions).exec(function(err, total) {
        if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        } else {
          Event.find(conditions, {billing_address : 0, location_address : 0, event_data : 0, location_coordinates : 0, parking_prices : 0, questions : 0, }).sort({"start_date" : 1}).skip(skipNo).limit(count).exec(function(err, events) {
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
            res.send({"message": "success", "data": events,"total": total, "status_code": "200"});
            }
          })
        }
      })
  }
  /*end*/
    
  exports.getEventlist = function(req, res) {
    if (req.body.eventType == "oneTime" ) {
      var query = {is_repeat:{$ne:true}};
    }
    else if (req.body.eventType == "reoccur") {
      var query = {is_repeat:true};
    } 
    var page = req.body.page || 1,
      count = req.body.count || 50;
    var skipNo = (page - 1) * count;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var search = req.body.search || "";
    if(req.body.status == 'active'){
      query.is_active = true
    }
    else if(req.body.status == 'inactive' ){
      query.is_active = false
    }
    query.location = req.body.location;
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    if (search) {
      query['$or'] = [];
      query['$or'].push({name: new RegExp(search,'i')})
      query['$or'].push({'event_data.contact_name': new RegExp(search,'i')})
      query['$or'].push({'event_data.contact_phone': new RegExp(search,'i')})
      //query['$or'].push({'event_data.contact_email': new RegExp(search,'i')})
    }
    Event.count(query).exec(function(err, total) {
      if (err) {
          res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } else {
        Event.find(query).sort(sortObject).skip(skipNo).limit(count).exec(function(err, events) {
          if (err) {
           res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
          } else {
          res.send({"message": "success", "data": events,"total": total, "status_code": "200"});
          }
        })
      }
  
    })
  }
  /*enable disable event*/
  exports.enabledisableevent = function(req, res) {
    var enabled = req.body.enabled;
    var selAll = req.body.allChecked;
    var condition = {};
    if (req.body.eventType == "oneTime" ) {
      var condition = {is_repeat:{$ne:true}};
    }
    else if (req.body.eventType == "reoccur") {
      var condition = {is_repeat:true};
    }

    var query = {};
    var fields = {};
    query._id = {
      $in: req.body.event
    };
    if (enabled == true) {
      fields.is_active = true;
    } else {
      fields.is_active = false;
    }
    Event.update(query, fields, {
      multi: true
    }).exec(function(error, events) {
      if (error) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
      }
      Event.find(condition, function(err, events) {
        if (error) {
          res.send({
            "message": "Something went wrong!",
            "err": err,
            "status_code": "500"
          });
        } else {
          res.send({
            "message": "success",
            "data": events,
            "status_code": "200"
          });
        }
      })
    })
  }

exports.update_event_status = function(req, res) {
  var update = {};
  var data = req.body.data;
  update.is_active = (data.is_active) ? false : true;
  Event.update({
    _id: data._id
  }, update).exec(function(error, users) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": users,
        "status_code": "200"
      });
    }
  })
}

exports.addEventAccountQuestions = function(req, res){
  var update = {};
  var data = req.body;
  update.questions = data.questions;
  Event.update({
    _id: data.event_account_Id
  }, update).exec(function(error, users) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": users,
        "status_code": "200"
      });
    }
  })
}

exports.fetchTotalShiftsInWeek = function(req, res){
      var to = new Date(req.body.to);
      var from = new Date(req.body.from);
      var query = {};
      var startweek = from;
      var endweek = to;
      var search = req.body.search || "";
      var firstand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}}]};
      var secondand = {$and:[{end_date:{$gte:startweek}},{end_date:{$lte:endweek}}]};
      var thirdand = {$and:[{start_date:{$lte:startweek}},{end_date:{$gte:endweek}}]};
      var fourdand = {$and:[{start_date:{$lte:startweek}},{is_repeat : true}]};
      var fivedand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}},{is_repeat : true}]};
      var conditions = {is_active: true, location : req.body.location, $or:[firstand,secondand,thirdand,fourdand,fivedand]};
      if (search) {
        var conditions = {is_active: true, location : req.body.location, $or:[firstand,secondand,thirdand,fourdand,fivedand], name: new RegExp(search,'i')};
      }
      if (req.body.role == "manager") {
        conditions.set_up_person = req.body.owner_id;
      }
      Event.find(conditions,{'shift_template_id' : 1, 'shift_template_id.shifts' : 1}).exec(function(err, shiftResponse){
        var shiftslen = 0;
        if(shiftResponse.length){
          var i = 0;
          shiftResponse.forEach(function(elem, key){
              elem.shift_template_id.forEach(function(v, k){
                shiftslen = parseInt(shiftslen) + parseInt(v.shifts.length);
              })
              if (i == shiftResponse.length - 1){
                  res.send({"message": "success", "totalShifts": shiftslen, "status_code": "200"});
              }
              i++;
          });
        }
        else if(err){
          res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        }
        else{
          res.send({"message": "success", "totalShifts": shiftslen, "status_code": "200"});
        }
      })
}

exports.fetchTotalExtraShiftsInWeek = function(req, res){
      var to = new Date(req.body.to);
      var from = new Date(req.body.from);
      var query = {};
      var startweek = from;
      var endweek = to;
      var search = req.body.search || "";
      var firstand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}}]};
      var secondand = {$and:[{end_date:{$gte:startweek}},{end_date:{$lte:endweek}}]};
      var thirdand = {$and:[{start_date:{$lte:startweek}},{end_date:{$gte:endweek}}]};
      var fourdand = {$and:[{start_date:{$lte:startweek}},{is_repeat : true}]};
      var fivedand = {$and:[{start_date:{$gte:startweek}},{start_date:{$lte:endweek}},{is_repeat : true}]};
      var conditions = {is_active: true, location : req.body.location, $or:[firstand,secondand,thirdand,fourdand,fivedand], "extrashifts": { $not: { $size: 0 } }};
      if (search) {
        var conditions = {is_active: true, location : req.body.location, $or:[firstand,secondand,thirdand,fourdand,fivedand], "extrashifts": { $not: { $size: 0 } }};
      }
      if (req.body.role == "manager") {
        conditions.set_up_person = req.body.owner_id;
      }
      Event.find(conditions,{'extrashifts' :1}).exec(function(err, shiftResponse){
        var extrashiftslen = 0;
        if(shiftResponse.length){
              var i = 0;
              shiftResponse.forEach(function(elem, key){
                  if(elem.extrashifts.length){
                    elem.extrashifts.forEach(function(vi, ki){
                      if((new Date(vi.shiftdate).getTime() <= to.getTime()) && (new Date(vi.shiftdate).getTime() >= from.getTime())){
                        extrashiftslen = parseInt(extrashiftslen) + parseInt(vi.shifts.length);
                      }
                    });
                  }
                  if (i == shiftResponse.length - 1){
                      res.send({"message": "success", "extrashifts" : extrashiftslen, "status_code": "200"});
                  }
                  i++;
              });
        }
        else if(err){
          res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        }
        else{
          res.send({"message": "success", "extrashifts" : extrashiftslen, "status_code": "200"});
        }
      })
}

exports.fetchScheduledShifts = function(req, res){
  var condition = {"week_of" : new Date(req.body.from), "userInfo.location" : mongoose.Types.ObjectId(req.body.location)};
  Schedule.aggregate([
  {
    $lookup: {
      from: "users",
      localField: "employee_id",
      foreignField: "_id",
      as: "userInfo"
    }
  },
  {$match: condition},
  {$project: {employee_id:1, _id : 1, day_of_week : 1, shiftReminderTime : 1, shiftEndTime : 1, timeslot : 1, scheduleDate : 1, event_account_id : 1, week_of : 1}}]).exec(function(err, totalScheduled) {
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }else{
        res.send({"status_code": "200", "data": totalScheduled.length});
      }
  });
}

exports.fetchScheduledShiftsInEvent = function(req, res){
  var condition = {week_of : req.body.from, event_account_id : req.body.event_account};
  Schedule.count(condition).exec(function(err, totalScheduled) {
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }else{
        res.send({"status_code": "200", "data": totalScheduled});
      }
  });
}

exports.fetchScheduledShiftsData = function(req, res){
  var condition = {"week_of" : new Date(req.body.from), "userInfo.location" : mongoose.Types.ObjectId(req.body.location), "userInfo.active" : true, $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
  Schedule.aggregate([
  {
    $lookup: {
      from: "users",
      localField: "employee_id",
      foreignField: "_id",
      as: "userInfo"
    }
  },
  {$match: condition}]).exec(function(err, Scheduleddata) {
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }else{
        res.send({"status_code": "200", "data": Scheduleddata});
      }
  });
}

exports.chkrandomAccount=function(req,res){
  var num=req.body.num;
  Event.find({accountId:num}).exec(function(err, response){
      if (err){
        res.json({"message": "error", "data": err, "status_code": "500"});
      }
      else{
        if(response.length >0){
          res.json({"message": "duplicate value", "data": response, "status_code": "500"});
        }
        else{
          res.json({"message": "success", "data": response, "status_code": "200"});
        }
      }
  });
}

exports.chkrandomAccountEdit=function(req,res){
  var num=req.body.num;
  Event.find({accountId:num, _id:{$ne:mongoose.Types.ObjectId(req.body.accountId)}}).exec(function(err, response){
      if (err){
        res.json({"message": "error", "data": err, "status_code": "500"});
      }
      else{
        if(response.length >0){
          res.json({"message": "duplicate value", "data": response, "status_code": "500"});
        }
        else{
          res.json({"message": "success", "data": response, "status_code": "200"});
        }
      }
  });
}

exports.addExtraShifts = function(req, res){
  var eventData = req.body;
  if ( typeof eventData.shiftData !== 'undefined' && eventData.shiftData.length > 0 ) {
    Event.findOneAndUpdate({_id: req.body.eventAccountId}, {extrashifts: eventData.shiftData},{new:true}, function(err, event){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else {
        res.send({"message": "success", "data": event, "status_code": "200"});
      }
    });
  }
  else {
    res.send({"status_code": "200", "event": event,"message": "Added Successfully"});
  }  
}

exports.removeExtrashiftInList = function(req,res){
  Event.update({_id: req.body.itemMainId, 'extrashifts._id':req.body.removeExtraObjectId},{$pull:{'extrashifts.$.shifts':{_id:req.body.removeExtrashiftId}}}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        Event.findOne({_id: req.body.itemMainId}).exec(function(err, event) {    
          if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
          } 
          else if(event){
            res.send({"message": "success", "data": event, "status_code": "200"});
          }
          else {
            res.send({"message": "success", "data": [], "status_code": "400"});
          }
        });
    }
  });
}

exports.removeExtrashiftList = function(req,res){
  Event.update({_id:req.body.itemMainId}, {$unset:{extrashifts:""}}, function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        res.send({"message": "success", "status_code": "200"}); 
    }
  });
}

exports.getemployeeCurrentWeekSchedule = function(req, res){
  var fields = {employee_id: mongoose.Types.ObjectId(req.body.userId), week_of : new Date(req.body.from), "userInfo.active" : true, "eventInfo.is_active" : true};
  Schedule.aggregate([
      {
          $lookup: {
            from: "users",
            localField: "employee_id",
            foreignField: "_id",
            as: "userInfo"
          }
      },
      {
          $lookup: {
            from: "events",
            localField: "event_account_id",
            foreignField: "_id",
            as: "eventInfo"
          }
      },
      { $match: fields },
      {
          $sort: {scheduleDate : -1}
      }
  ]).exec(function(error, schedules){
      schedules.map(function(item) {
        item.employee_id = item.userInfo[0];  
        item.event_account_id = item.eventInfo[0];  
      })
      if (error) {
          res.send({"message": "Something went wrong!", "err": error, "status_code": "500"});
      } else {
          res.send({"status_code": "200", "data": schedules});
      }
  });
}

exports.removeshifts = function(req, res){
  var outputJSON = {};
  var scheduledata = req.body.removeShifts;
  var orArray = [];
  var orTradeArray = [];
  var x = 0;
  for (var i = 0; i < scheduledata.length; i++) {
      orArray.push({
        event_account_id: mongoose.Types.ObjectId(scheduledata[i].eventId),
        shift_id: mongoose.Types.ObjectId(scheduledata[i].shift._id),
        day_of_week: scheduledata[i].week,
        timeslot: scheduledata[i].tmsl
      });

      orTradeArray.push({
        eventId: mongoose.Types.ObjectId(scheduledata[i].eventId),
        shiftId: mongoose.Types.ObjectId(scheduledata[i].shift._id),
        timeslot: scheduledata[i].tmsl
      });

      if(x === scheduledata.length - 1){
        Schedule.remove({$or : orArray}).exec(function(err, results) {
          if(err){
            console.error(err);
          }
          else{
            Trade.remove({$or : orTradeArray}).exec(function(error, traderesults) {
                if(error){
                  console.error(error);
                }else{
                  outputJSON = {'status': 'success', 'messageId':200, 'message':'successfully'};
                  res.send(outputJSON); 
                }
            });
          }
        });
      }
      x++;
  }
}

exports.removeshiftfromTradeBoard = function(req, res){
  var outputJSON = {};
  var scheduledata = req.body.removeShifts;
  var orArray = [];
  var orTradeArray = [];
  var x = 0;
  for (var i = 0; i < scheduledata.length; i++) {
      if(scheduledata[i].tmsl){
        orArray.push({
          event_account_id: mongoose.Types.ObjectId(scheduledata[i].eventId),
          shift_id: mongoose.Types.ObjectId(scheduledata[i].shift),
          timeslot: scheduledata[i].tmsl
        });
        orTradeArray.push({
          eventId: mongoose.Types.ObjectId(scheduledata[i].eventId),
          shiftId: mongoose.Types.ObjectId(scheduledata[i].shift),
          timeslot: scheduledata[i].tmsl
        });
      }else{
        orArray.push({
          event_account_id: mongoose.Types.ObjectId(scheduledata[i].eventId),
          shift_id: mongoose.Types.ObjectId(scheduledata[i].shift),
        });
        orTradeArray.push({
          eventId: mongoose.Types.ObjectId(scheduledata[i].eventId),
          shiftId: mongoose.Types.ObjectId(scheduledata[i].shift)
        });
      }
      if(x === scheduledata.length - 1){
        Schedule.remove({$or : orArray}).exec(function(err, results) {
          if(err){
            console.error(err);
          }
          else{
            Trade.remove({$or : orTradeArray}).exec(function(error, traderesults) {
                if(error){
                  console.error(error);
                }else{
                  outputJSON = {'status': 'success', 'messageId':200, 'message':'successfully'};
                  res.send(outputJSON); 
                }
            });
          }
        });
      }
      x++;
  }
}

exports.removeshiftfromTradeBoardAfterSchedule = function(req, res){
  var outputJSON = {};
  var scheduledata = req.body.removeShifts;
  var orTradeArray = [];
  var x = 0;
  for (var i = 0; i < scheduledata.length; i++) {
      orTradeArray.push({
        eventId: mongoose.Types.ObjectId(scheduledata[i].eventId),
        shiftId: mongoose.Types.ObjectId(scheduledata[i].shift),
        timeslot: scheduledata[i].tmsl,
        status : {$ne:"Approve"}
      });
      if(x === scheduledata.length - 1){
        Trade.remove({$or : orTradeArray}).exec(function(error, traderesults) {
            if(error){
              console.error(error);
            }else{
              outputJSON = {'status': 'success', 'messageId':200, 'message':'successfully'};
              res.send(outputJSON); 
            }
        });
      }
      x++;
  }
}

exports.removeAllExtrashiftfromTradeBoard = function(req, res){
  var outputJSON = {};
  var eventId = req.body.itemMainId;
  var orArray = [];
  var orTradeArray = [];
  var query = {_id : mongoose.Types.ObjectId(eventId)};
  Event.aggregate([{$match: query },
      {
        $unwind : "$extrashifts"
      },
      {$project:{
        _id:1,
        timeslot : "$extrashifts.timeslot",
        shift_id : "$extrashifts.shifts._id"
      }}
      ]
  ).exec(function(error, result){
    var x = 0;
    result.forEach(function(elem, key){
        orTradeArray.push({
          eventId: mongoose.Types.ObjectId(elem._id),
          shiftId: mongoose.Types.ObjectId(elem.shift_id[0]),
          timeslot: elem.timeslot
        });

        orArray.push({
          event_account_id: mongoose.Types.ObjectId(elem._id),
          shift_id : mongoose.Types.ObjectId(elem.shift_id[0]),
          timeslot : elem.timeslot
        });
        if(x === result.length - 1){
            Schedule.remove({$or : orArray}).exec(function(err, results) {
              if(err){
                console.error(err);
              }
              else{
                Trade.remove({$or : orTradeArray}).exec(function(error, traderesults) {
                    if(error){
                      console.error(error);
                    }else{
                      res.send({"message": "success", "status_code": "200"}); 
                    }
                });
              }
            });
        }
        x++;
    });
  })
}



  