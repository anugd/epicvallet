var Exceptionrate = require("mongoose").model("Exceptionrate");
var mongoose = require('mongoose');
exports.addExceptionInList = function(req, res){
  if(req.body.exceptionrateslistCount){
    //Exceptionrate.findOneAndUpdate({employee: req.body.employee, created_by : req.body.created_by, event_account : req.body.event_account}).exec(function(err, exceptionratelist) {    
    Exceptionrate.findOneAndUpdate({employee: req.body.employee, created_by : req.body.created_by, event_account : req.body.event_account},{$pull:{rates:{effective_date:req.body.exceptionratesEntry.effective_date}}},{new:true}, function(err, exceptionratelist) {    
      if(!err){
        var itemData = req.body.exceptionratesEntry;
        exceptionratelist.rates.push(itemData);
        exceptionratelist.save(function(err,response){
          res.send({"message": "success", "status_code": "200"});    
        }); 
      }
    });
  }
  else{
    Exceptionrate.findOneAndUpdate({employee: req.body.employee, created_by : req.body.created_by, event_account : req.body.event_account},{upsert:true}).exec(function(error, response){
      if(response){
        var itemData = req.body.exceptionratesEntry;
        response.rates.push(itemData);
        response.save(function(err,response){
          res.send({"message": "success", "status_code": "200"});    
        }); 
      }else{
        var exceptionratesArr = [];
        exceptionratesArr.push(req.body.exceptionratesEntry);
        req.body.rates = exceptionratesArr;
        Exceptionrate(req.body).save(function(err,response){
          if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
          } else {
            res.send({"message": "success", "status_code": "200"});
          }
        });
      }
    })
  }
}

exports.empExceptionsList = function(req,res){
  Exceptionrate.findOne({employee: req.body.employee, created_by : req.body.created_by, event_account : req.body.event_account}).populate('event_account').exec(function(err, exceptionratelist) {    
    if (err) {
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } 
    else if(exceptionratelist){
      res.send({"message": "success", "data": exceptionratelist, "status_code": "200"});
    }
    else {
      res.send({"message": "success", "data": [], "status_code": "400"});
    }
  });
}

exports.fetchExceptionRate = function(req,res){
  var query = {'employee': mongoose.Types.ObjectId(req.body.employee), 'event_account' : mongoose.Types.ObjectId(req.body.eventId), 'rates.effective_date' : {$lte : new Date(req.body.fetchExceptionDate)}};
  Exceptionrate.aggregate({$unwind:"$rates"},{$match:query},{$sort:{'rates.effective_date':1}}).exec(function(err,exceptionrate){  
    if (err) {
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } 
    else if(exceptionrate){
      res.send({"message": "success", "data": exceptionrate, "status_code": "200"});
    }
    else {
      res.send({"message": "success", "data": [], "status_code": "400"});
    }
  });
}

exports.removeExceptionInList = function(req,res){
  Exceptionrate.update({_id: req.body.exceptionMainId},{$pull:{rates:{_id:req.body.removeExceptionId}}}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        Exceptionrate.findOne({employee: req.body.employee, created_by : req.body.created_by, event_account:req.body.eventAccId}).populate('rates.event_account').exec(function(err, exceptionratelist) {    
          if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
          } 
          else if(exceptionratelist){
            res.send({"message": "success", "data": exceptionratelist, "status_code": "200"});
          }
          else {
            res.send({"message": "success", "data": [], "status_code": "400"});
          }
        });
    }
  });
}

exports.removeWholeExceptionList = function(req,res){
  Exceptionrate.remove({_id: req.body.exceptionMainId}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        res.send({"message": "success", "status_code": "200"}); 
    }
  });
}

