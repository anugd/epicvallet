var Geolocation = require("mongoose").model("Geolocation");
var Event = require("mongoose").model("Event");
var Schedule = require("mongoose").model("Schedule");
var Scheduletext = require("mongoose").model("Scheduletext");
var Timesheet = require("mongoose").model("Timesheet");
var Ticketcontrol = require("mongoose").model("Ticketcontrol");
var Missedpunch = require("mongoose").model("Missedpunch");
var mongoose = require('mongoose');
var moment = require('moment');
var _ = require('lodash');
var path = require('path');
var root = process.cwd();
var constantObj = require(path.resolve(root, 'constants.js'));
var CronJob = require('cron').CronJob;
var weekdays  = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
exports.checkExistingRadius = function(req, res){
    Geolocation.findOne({startTime:req.body.startTimeMS, endTime:req.body.endTimeMS}).exec(function(error, resp){
        if(resp){
            res.send({
                "message": "Radius is already exists in this time range.",
                "data": resp,
                "status_code": "208"
            }); 
        }
        else if(error){
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        }
        else{
            res.send({
                "message": "Not Exists",
                "status_code": "200"
            }); 
        }
    });
}

exports.saveRadius = function(req, res){
   req.body.startTime = req.body.startTimeMS;
   req.body.endTime = req.body.endTimeMS;
   if(req.body.existId){
    var condition = {_id:req.body.existId};
   }
   if (!req.body.existId) {
    var condition = {_id:new mongoose.mongo.ObjectID()};
    }
   Geolocation.findOneAndUpdate(condition, req.body, {
        upsert: true, new:true}, function(err, response) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.twentyfour_to_twelve = function(ti){
    ti = ti.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [ti];
    if (ti.length > 1) { // If time format correct
        ti = ti.slice (1);  // Remove full string match value
        ti[5] = +ti[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
        ti[0] = +ti[0] % 12 || 12; // Adjust hours
    }
    return ti.join (''); // return adjusted time or original string
}

exports.GetGeoRadius = function(req,res){
  var today = moment.utc().tz(constantObj.Timezones.AP);
  var currentTime = today.format("HH:mm");
  var timeParts = currentTime.split(":");
  var msStr = (timeParts[0] * (60000 * 60)) + (+timeParts[1] * 60000);
  var query = {};
  query.startTime = {$lte: msStr};
  query.endTime = {$gte: msStr};
  Geolocation.find(query).exec(function(err,response){
    if (err) {
        res.send({
            "message": "Something went wrong!",
            "err": err,
            "status_code": "500"
        });
    }else if(!response){
        res.send({
            "message": "success",
            "status_code": "400"
        });
    }

    else {
        res.send({
            "message": "success",
            "data": response,
            "status_code": "200"
        });
    }
  })
}

exports.GetGeoRadiusList = function(req,res){
  var query = {};
  Geolocation.find(query).exec(function(err,response){
    if (err) {
        res.send({
            "message": "Something went wrong!",
            "err": err,
            "status_code": "500"
        });
    }else if(!response){
        res.send({
            "message": "success",
            "status_code": "400"
        });
    }
    else {
        res.send({
            "message": "success",
            "data": response,
            "status_code": "200"
        });
    }
  })
}

exports.removeGeoEntry = function(req,res){
    var recordId = req.body.radiusId;
    Geolocation.remove({_id: req.body.radiusId}).exec(function(err, response) {
        if (err) {
            res.send({
              "message": "Something went wrong!",
              "err": err,
              "status_code": "500"
            });
        } else{
            res.send({"message": "success", "status_code": "200", "data": response}); 
        }
    });
}

exports.checkGeoFencingArea = function(req,res){
    var currDate = req.body.currDate;
    var lng = req.body.longitude;
    var lat = req.body.latitude;
    var firstand = {$and:[{start_date:{$lte:currDate}},{end_date:{$gte:currDate}}]};
    var secondand = {$and:[{start_date:{$lte:currDate}},{is_repeat : true}]};
    var today = moment.utc().tz(constantObj.Timezones.AP);
    var currentTime = today.format("HH:mm");
    var timeParts = currentTime.split(":");
    var msStr = (timeParts[0] * (60000 * 60)) + (+timeParts[1] * 60000);
    var query = {};
    query.startTime = {$lte: msStr};
    query.endTime = {$gte: msStr};
    Geolocation.find(query).exec(function(err,response){
        if(response.length){
            radius = (response[0].radius / 5280).toFixed(3);
        }else{
            var defaultVal = constantObj.defaultradius.radius;
            var radius = (defaultVal / 5280).toFixed(3);
        }
        var radius = parseFloat(radius);
        Event.find({"location_coordinates":{
        $geoWithin: {
            $centerSphere: [
                [lng, lat],
                radius / 3963.2
            ]
        }
        }, "is_active" : true, "location" : req.body.location, $or:[firstand,secondand]}).exec(function(error,resp){
            if (error) {
                res.send({
                    "message": "Something went wrong!",
                    "err": error,
                    "status_code": "500"
                });
            } else {
                res.send({
                    "message": "success",
                    "data": resp,
                    "status_code": "200"
                });
            }
        });   
    });  
}
exports.clockIn = function(req,res){
    var timesheetInfo = req.body;
    var condition = {"scheduleDate": new Date(timesheetInfo.created_date), "timeslot" : timesheetInfo.shift, "event_account_id" : timesheetInfo.eventId, "employee_id" : timesheetInfo.userId};
    Schedule.findOne(condition).exec(function(error, scheduleinfo){
        if(scheduleinfo){
            timesheetInfo.alignScheduleTime = scheduleinfo.shiftReminderTime;
        }else{
            timesheetInfo.alignScheduleTime = '';
        }
        Timesheet.create(timesheetInfo, function(err, details) {
            if (err) {
                res.send({
                    "message": "Something went wrong!",
                    "err": err,
                    "status_code": "500"
                });
            } else {
                Timesheet.findOne({_id:details._id}).populate('eventId').exec(function(error,eventInfo){
                    if (err) {
                        res.send({
                            "message": "Something went wrong!",
                            "err": error,
                            "status_code": "500"
                        });
                    }else{
                        res.send({
                            "message": "success",
                            "data": eventInfo,
                            "status_code": "200"
                        });
                    }
                });
            }
        });  
    });   
}

exports.clockOut = function(req,res) {
    var timesheetInfo = req.body;
    timesheetInfo.clockOutDateTime = moment.utc().tz(timesheetInfo.locationtimezone).toISOString();
   
    var then = timesheetInfo.clockInTime;
    var nowtime = timesheetInfo.clockOutDateTime;
    var ms = moment(nowtime).diff(moment(then)); // Millisecond
    var timeWithoutBreak = ms - timesheetInfo.totalBreakTime;
    var d = moment.duration(timeWithoutBreak);
    var s = d.asHours().toFixed(2);
    timesheetInfo.totalTimeDiff = timeWithoutBreak;// Total Time - Break time 
    timesheetInfo.payroll = parseFloat(s*timesheetInfo.payrate).toFixed(2);

    var lng = req.body.longitude;
    var lat = req.body.latitude;
    var today = moment.utc().tz(constantObj.Timezones.AP);
    var currentTime = today.format("HH:mm");
    var timeParts = currentTime.split(":");
    var msStr = (timeParts[0] * (60000 * 60)) + (+timeParts[1] * 60000);
    var query = {};
    query.startTime = {$lte: msStr};
    query.endTime = {$gte: msStr};
    Geolocation.find(query).exec(function(err,response){
        if(response.length){
            var radius = (response[0].radius / 5280).toFixed(3);
        }else{
            var defaultVal = constantObj.defaultradius.radius;
            var radius = (defaultVal / 5280).toFixed(3);
        }
        var radius = parseFloat(radius);
        Timesheet.findOneAndUpdate({"location_coordinates":{
            $geoWithin: {
                $centerSphere: [
                    [lng, lat],
                    radius / 3963.2
                ]
            }
            },_id:timesheetInfo.clockInId}, {$set:{clockOutDateTime:timesheetInfo.clockOutDateTime,totalTimeDiff:timesheetInfo.totalTimeDiff,payrate:timesheetInfo.payrate, payroll:timesheetInfo.payroll, totalBreakTime:timesheetInfo.totalBreakTime}}, {},function(err,data){
            if (err) {
                res.send({
                    "message": "Something went wrong!",
                    "err": err,
                    "status_code": "500"
                });
            } 
            else if(!data){
                res.send({
                    "message": "You are not in range of location to clock out.",
                    "err": err,
                    "status_code": "201"
                }); 
            }
            else {
                Missedpunch.findOneAndUpdate({'missedPunchTimesheets.timesheetId' : timesheetInfo.clockInId},{$pull:{missedPunchTimesheets:{timesheetId:timesheetInfo.clockInId}}}).exec(function(error, missedpunchData){
                    if(error){
                       res.send({
                            "message": "Something went wrong!",
                            "err": error,
                            "status_code": "500"
                        });  
                    }else{
                        res.send({
                            "message": "success",
                            "data": data,
                            "status_code": "200"
                        });
                    }
                });   
            }
        });
    });
}

exports.clockInCheck = function(req,res){
    var checkInInfo = req.body;
    Timesheet.findOne({userId:checkInInfo.userId, $or:[{clockOutDateTime:{$exists:false}},{clockOutDateTime:''}]}).populate('eventId').exec(function(error,eventInfo){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": eventInfo,
                "status_code": "200"
            });
        }
    });
}

exports.startBreakTime = function(req,res){
    var breakInfo = req.body;
    Timesheet.findOne({_id:breakInfo.clockId}).exec(function(error,clockInfo){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            clockInfo.breaks.push({startTime : breakInfo.startTime});
            clockInfo.save(function(err,response){
                if (err) {
                    res.send({
                        "message": "Something went wrong!",
                        "err": err,
                        "status_code": "500"
                    });
                }else{
                    res.send({
                        "message": "success",
                        "data": response,
                        "status_code": "200"
                    });
                }
            });
        }
    });
}

exports.stopBreakTime = function(req,res){
    var breakInfo = req.body;
    Timesheet.findOneAndUpdate({'_id':breakInfo.clockId,'breaks._id':breakInfo.breakId},{$set:{'breaks.$.endTime':breakInfo.endTime,'breaks.$.timediff':breakInfo.timediff}}).exec(function(error,response){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.clockOutByAdmin = function(req,res){
    var timesheetInfo = req.body;
    Timesheet.findOneAndUpdate({_id:timesheetInfo.clockInId}, {$set:{clockOutDateTime:timesheetInfo.clockOutDateTime, totalTimeDiff:timesheetInfo.totalTimeDiff, 'breaks.0':{'timediff':timesheetInfo.breaktime}, payrate:timesheetInfo.payrate, payroll:timesheetInfo.payroll, totalBreakTime:timesheetInfo.totalBreakTime}}, {},function(err,data){
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            Missedpunch.findOneAndUpdate({'missedPunchTimesheets.timesheetId' : timesheetInfo.clockInId},{$pull:{missedPunchTimesheets:{timesheetId:timesheetInfo.clockInId}}}).exec(function(error, missedpunchData){
                if(error){
                   res.send({
                        "message": "Something went wrong!",
                        "err": error,
                        "status_code": "500"
                    });  
                }else{
                    res.send({
                        "message": "success",
                        "data": data,
                        "status_code": "200"
                    });
                }
            });   
        }
    });
}

exports.getTimeSheet = function(req,res){
    var query = {};
    var userId = req.body.user;
    query.userId = userId;
    var from = new Date(req.body.from);
    var to = new Date(req.body.to);
    query.clockInDateTime = {
        $gte: from,
        $lte: to
    }
    query.location = req.body.location;
    Timesheet.find(query).populate('eventId').exec(function(error, response){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.saveOpeningClosure = function(req,res){
    Timesheet.findOneAndUpdate({'_id':req.body.clockId},{$set:{'questions':req.body.questionArr}}).exec(function(error,response){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.saveTicketControl = function(req,res){
    Timesheet.findOneAndUpdate({'_id':req.body.clockId},{$set:{'ticketControl':req.body.ticketData}}).exec(function(error,response){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.saveTicketControlReport = function(req,res){
    Ticketcontrol.count({eventId:req.body.eventId, dateOfReport:req.body.dateOfReport, shift:req.body.shift}).exec(function(err, response) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            if(!response){
                Ticketcontrol.create(req.body, function(err, response) {
                    if (err) {
                        res.send({
                            "message": "Something went wrong!",
                            "err": err,
                            "status_code": "500"
                        });
                    } else {
                        res.send({
                            "message": "success",
                            "data": response,
                            "status_code": "200"
                        });
                    }
                });
            }else{
                res.send({
                    "message": "Already entered",
                    "err": response,
                    "status_code": "202"
                });
            }
        }
    });
}


exports.saveTicketControlReportOnEdit = function(req,res){
    req.body.eventId = mongoose.Types.ObjectId(req.body.eventId);
    req.body.created_by = mongoose.Types.ObjectId(req.body.created_by);
    Ticketcontrol.count({eventId:req.body.eventId, dateOfReport:req.body.dateOfReport, shift:req.body.shift}).exec(function(err, response) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            if(!response){
                Ticketcontrol.findOneAndUpdate({_id:req.body._id}, req.body, function(err, response) {
                    if (err) {
                        res.send({
                            "message": "Something went wrong!",
                            "err": err,
                            "status_code": "500"
                        });
                    } else {
                        res.send({
                            "message": "success",
                            "data": response,
                            "status_code": "200"
                        });
                    }
                });
            }else{
                res.send({
                    "message": "Already entered",
                    "err": response,
                    "status_code": "202"
                });
            }
        }
    });
}

exports.updateTicketControlReport = function(req,res){
   Ticketcontrol.findOneAndUpdate({eventId:req.body.eventId, dateOfReport:req.body.dateOfReport, shift:req.body.shift}, req.body, function(err, response) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.updateTicketControlReportbyId = function(req,res){
   Ticketcontrol.findOneAndUpdate({_id:req.body.ticketcontrolId}, req.body, function(err, response) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.viewTicketControl = function(req, res){
    Ticketcontrol.findOne({_id:req.body.ticketId}).populate('eventId created_by').exec(function(err, response) {
        if (err) {
            res.status("500").jsonp({
                "message": "Something went wrong!",
                "err": err
            });
        } else {
            res.status("200").jsonp({
                "message": "success",
                "data": response
            });
        }
    });
}

exports.getTicketNotes = function(req, res) {
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var ownerId = mongoose.Types.ObjectId(req.body.owner_id);
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var query = { 
            "clockOutDateTime": {
                $lte: new Date(req.body.dateRange.end_date),
                $gte: new Date(req.body.dateRange.start_date)
            },
            "ticketControl.notes": {
                $exists: true
            }
    };
    if (search) {
        query['$or'] = [];
        query['$or'].push({
          'event.name': new RegExp(search, 'i')
        })
    }
    Timesheet.aggregate([{
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "employee"
        }
      },{
        $lookup: {
          from: "events",
          localField: "eventId",
          foreignField: "_id",
          as: "event"
        }
      }, {
        $match: query
      }]).exec(function(err, total) {
    if (err) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      Timesheet.aggregate([{
         $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "employee"
        }
      },{
        $lookup: {
          from: "events",
          localField: "eventId",
          foreignField: "_id",
          as: "event"
        }
      }, {
        $match: query
      }, {
        $sort: sortObject
      },{
        $skip: skipNo
      }, {
        $limit: count
      }]).exec(function(err, reports) {
        if (!err) {
          res.send({
            "message": "success",
            "data": reports,
            "total": total.length,
            "status_code": "200"
          });
        } else {
          res.send({
            "message": "success",
            "data": err,
            "total": total.length,
            "status_code": "200"
          });
        }
      });
    }
  });
};

exports.totalBreak = function(req,res){
    var timesheetInfo = req.body;
    Timesheet.find({_id:timesheetInfo.clockInId}).exec(function(error, response){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.getProfitLossReport = function(req,res){
    var start_date = req.body.dateRange.start_date;
    var end_date = req.body.dateRange.end_date;
    var event_account = mongoose.Types.ObjectId(req.body.event_account_id);
    Timesheet.aggregate([
        {$match: {
                "clockInDateTime": {
                    $gte: new Date(start_date), 
                    $lte: new Date(end_date)
                },
                "eventId" : event_account,
                "clockOutDateTime":{
                    $exists:true
                }
            }
        },
        {"$project":{
                "month": {"$month":"$created_date"},
                "year": {"$year": "$created_date"},
                "day": {"$dayOfMonth": "$created_date"},
                "userId": 1,
                "eventId": 1, 
                "payroll": 1,
                "totalTimeDiff" : 1,
                "created_date" : 1,
                "revenueType" : 1,
                "revenueRate" : 1,
                "revenueFixed" : 1,
                "payrate" : 1
            }
        },
        {$group: {
                "_id": {"eventId":"$eventId", "year":"$year","month":"$month","day":"$day", "created_date":"$created_date","revenueType" :"$revenueType", "revenueRate" : "$revenueRate","revenueFixed" : "$revenueFixed", "payrate":"$payrate"},
                "payroll": {$sum: "$payroll"},
                "totalTimeDiff" : {$sum : "$totalTimeDiff"}
            }
        }
    ]).exec(function(err, reports) {
        if (!err) {
          res.status("200").jsonp({
            "message": "success",
            "data": reports,
          });
        } else {
          res.status("500").jsonp({
            "message": "error",
            "data": err,
          });
        }
    });
}

exports.getTotalHoursOfDay = function(req, res){
    var selectDay = req.body.selectDay;
    var event_account = mongoose.Types.ObjectId(req.body.event_account_id);
    Timesheet.aggregate([
        {$match: {
                "created_date": new Date(selectDay),
                "eventId" : event_account,
                "clockOutDateTime":{
                    $exists:true
                }
            }
        },
        {"$project":{
                "month": {"$month":"$created_date"},
                "year": {"$year": "$created_date"},
                "day": {"$dayOfMonth": "$created_date"},
                "eventId": 1, 
                "totalTimeDiff" : 1,
                "created_date" : 1
            }
        },
        {$group: {
                "_id": {"eventId":"$eventId", "year":"$year","month":"$month","day":"$day", "created_date":"$created_date"},
                "totalTimeDiff" : {$sum : "$totalTimeDiff"}
            }
        }
    ]).exec(function(err, reports) {
        if (!err) {
          res.status("200").jsonp({
            "message": "success",
            "data": reports,
          });
        } else {
          res.status("500").jsonp({
            "message": "error",
            "data": err,
          });
        }
    });
}

exports.getCashDepositControl = function(req,res){
    var start_date = req.body.dateRange.start_date;
    var end_date = req.body.dateRange.end_date;
    var event_account = mongoose.Types.ObjectId(req.body.event_account_id);
    Ticketcontrol.aggregate([
        {$match: {
                "dateOfReport": {
                    $lte: new Date(req.body.dateRange.end_date),
                    $gte: new Date(req.body.dateRange.start_date)
                },
                "eventId" : event_account
            }
        },
        {"$project":{
                "month": {"$month":"$dateOfReport"},
                "year": {"$year": "$dateOfReport"},
                "day": {"$dayOfMonth": "$dateOfReport"},
                "created_by": 1,
                "eventId": 1, 
                "cashDeposit": 1,
                "revenue": 1,
                "revenueType" : 1,
                "revenueRate" : 1
            }
        },
        {$group: {
                "_id": {"eventId":"$eventId", "year":"$year","month":"$month","day":"$day", "revenueType" :"$revenueType", "revenueRate" : "$revenueRate"},
                "cashDeposit": {$sum: "$cashDeposit"},
                "revenue": {$sum: "$revenue"},
            }
        }
    ]).exec(function(err, reports) {
        if (!err) {
          res.status("200").jsonp({
            "message": "success",
            "data": reports
          });
        } else {
          res.status("500").jsonp({
            "message": "error",
            "data": err
          });
        }
    });
}

exports.getGrossPayrollDetails = function(req,res){
    var selecteddate = req.body.selecteddate;
    var event_account = mongoose.Types.ObjectId(req.body.event_account_id);
    Timesheet.aggregate([{
            $lookup: {
              from: "users",
              localField: "userId",
              foreignField: "_id",
              as: "userinfo"
            }
        }, 
        {$match: {
                "created_date": new Date(selecteddate),
                "eventId" : event_account,
                "clockOutDateTime":{
                    $exists:true
                }
            }
        },
        {"$project":{
                "month": {"$month":"$created_date"},
                "year": {"$year": "$created_date"},
                "day": {"$dayOfMonth": "$created_date"},
                "userId": 1,
                "userfname" : "$userinfo.first_name",
                "userlname" : "$userinfo.last_name",
                "eventId": 1, 
                "payroll": 1,
                "totalTimeDiff" : 1
            }
        }
    ]).exec(function(err, reports) {
        if (!err) {
          res.status("200").jsonp({
            "message": "success",
            "data": reports,
          });
        } else {
          res.status("500").jsonp({
            "message": "error",
            "data": err,
          });
        }
    });
}

exports.getHoursworkedReport = function(req,res){
    var start_date = req.body.dateRange.start_date;
    var end_date = req.body.dateRange.end_date;
    var event_account = mongoose.Types.ObjectId(req.body.eventId);
    var query = {};
    var query = {
        "clockInDateTime": {
            $gte: new Date(start_date), 
            $lte: new Date(end_date)
        },
        "eventId" : event_account,
        "clockOutDateTime":{
            $exists:true
        }
    };
    if(req.body.empId){
        var employeeId = mongoose.Types.ObjectId(req.body.empId);
        query.userId = employeeId;
    }

    Timesheet.aggregate([
        {$sort: {"created_date": 1}},
        {$lookup: {from: "users", localField: "userId", foreignField: "_id", as: "userinfo"}},
        {$lookup: {from: "events", localField: "eventId", foreignField: "_id", as: "eventinfo"}},
        {$match: query},
        {"$project":{
                "month": {"$month":"$created_date"},
                "year": {"$year": "$created_date"},
                "day": {"$dayOfMonth": "$created_date"},
                "eventId": 1, 
                "totalBreakTime" : 1,
                "totalTimeDiff" : 1,
                "created_date" : 1,
                "userId" : 1,
                "first_name" : "$userinfo.first_name",
                "last_name" : "$userinfo.last_name",
                "employeeid" : "$userinfo.employeeid",
                "accountName" : "$eventinfo.name",
                "clockInDateTime" : 1,
                "clockOutDateTime" : 1
            }
        },
        {$group: {
                "_id": {"year":"$year","month":"$month","day":"$day"},
                "Timesheets": { $push: "$$ROOT" },
                "totalTimeDiffSum": { $sum: "$totalTimeDiff" } 
            }
        }
    ]).exec(function(err, reports) {
        if (!err) {
          res.status("200").jsonp({
            "message": "success",
            "data": reports,
          });
        } else {
          res.status("500").jsonp({
            "message": "error",
            "data": err,
          });
        }
    });
}

exports.getTotalHoursReport = function(req,res){
    var start_date = req.body.dateRange.start_date;
    var end_date = req.body.dateRange.end_date;
    var query = {};
    var query = {
        "clockInDateTime": {
            $gte: new Date(start_date), 
            $lte: new Date(end_date)
        },
        "clockOutDateTime":{
            $exists:true
        },
        "userinfo.location" : mongoose.Types.ObjectId(req.body.location),
        "eventinfo.location" : mongoose.Types.ObjectId(req.body.location)
    };
    if(req.body.empId){
        var employeeId = mongoose.Types.ObjectId(req.body.empId);
        query.userId = employeeId;
    }

    Timesheet.aggregate([
        {$sort: {"created_date": 1}},
        {$lookup: {from: "users", localField: "userId", foreignField: "_id", as: "userinfo"}},
        {$lookup: {from: "events", localField: "eventId", foreignField: "_id", as: "eventinfo"}},
        {$match: query},
        {"$project":{
                "month": {"$month":"$clockInDateTime"},
                "year": {"$year": "$clockInDateTime"},
                "day": {"$dayOfMonth": "$clockInDateTime"},
                "eventId": 1, 
                "totalBreakTime" : 1,
                "totalTimeDiff" : 1,
                "created_date" : 1,
                "userId" : 1,
                "first_name" : "$userinfo.first_name",
                "last_name" : "$userinfo.last_name",
                "zipcode" : "$userinfo.address.zip",
                "phone" : "$userinfo.phone",
                "employeeid" : "$userinfo.employeeid",
                "accountName":"$eventinfo.name",
                "clockInDateTime" : 1,
                "clockOutDateTime" : 1
            }
        },
        {$group: {
                "_id": "$userId",
                "Timesheets": { $push: "$$ROOT" },
                "totalTimeDiffSum": { $sum: "$totalTimeDiff" } 
            }
        }
    ]).exec(function(err, reports) {
        reports.sort(function compare(a, b) {
          var A = a.totalTimeDiffSum;
          var B = b.totalTimeDiffSum;
          return A - B;
        });
        reports.reverse(); 
        if (!err) {
          res.status("200").jsonp({
            "message": "success",
            "data": reports,
          });
        } else {
          res.status("500").jsonp({
            "message": "error",
            "data": err,
          });
        }
    });
}

exports.saveMissedPunchInDb = function(req, res, socket){
    var todayDate = new Date();
    var d = new Date();
    var yestDate = new Date(d.setDate(d.getDate() - 1));
    var yestIsoStr = new Date(yestDate.setHours(0, 0, 0, 0));
    var yestDatestr = yestDate.getDate();
    var yestDatemonth = yestDate.getMonth() + 1;
    var yestDateyear = yestDate.getFullYear();
    Timesheet.aggregate([
      {$project: {_id : 1, clockOutDateTime : 1, clockInDateTime : 1, location : 1, created_date :1, day: { $cond: ["$created_date", { $dayOfMonth: "$created_date" }, -1] }, year :{ $cond: ["$created_date", { $year: "$created_date" }, -1] }, month:{ $cond: ["$created_date", { $month: "$created_date" }, -1] }}},
      {$match: {day: yestDatestr, month : yestDatemonth, year : yestDateyear, "clockOutDateTime":null}}
    ]).exec(function(err, result){
        if (!err && result.length) {
            var dataNew = _.chain(result)
            .groupBy("created_date")
            .value();
            var keysvalue  = _.keys(dataNew);
            var datavalues = _.values(dataNew);
            var i = 0;
            TimesheetsList = [];
            datavalues[0].forEach(function(element, key) {
                TimesheetsList.push({'timesheetId':element._id, 'clockInDateTime':element.clockInDateTime, 'location' : element.location});
                i++;
            })
            if(i == datavalues[0].length){
              var insertedJson = {};
              insertedJson.missedPunchOut = TimesheetsList.length;
              insertedJson.missedPunchTimesheets = TimesheetsList;
              insertedJson.ClockInDate = yestIsoStr;
              Missedpunch(insertedJson).save(function(error, saveResponse){
                if(!error){
                  console.log('Punch out entry is saved in missedPunch table');
                }else{
                  console.log(error);
                }
              }) 
            }       
        } else {
          console.log(err);
        }
    })
}

exports.updateClockInfo = function(req, res){
    Timesheet.findOneAndUpdate({_id:req.body.clockInid}, {$set:{'clockInDateTime':req.body.clockInDateTime, 'created_by' : req.body.created_by}}).exec(function(error,info){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": info,
                "status_code": "200"
            });
        }
    });
}

exports.updateClockFullInfo = function(req, res){
    Timesheet.findOneAndUpdate({_id:req.body.clockInid}, {$set:{'clockOutDateTime':req.body.clockOutDateTime, 'clockInDateTime':req.body.clockInDateTime, 'created_by' : req.body.created_by, 'totalTimeDiff':req.body.totalTimeDiff, 'payroll':req.body.payroll, 'payrate' : req.body.payrate, 'eventId' : req.body.eventId, 'shift':req.body.shift, 'totalBreakTime' : req.body.totalBreakTime}}).exec(function(error,info){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": info,
                "status_code": "200"
            });
        }
    });
}

exports.removeClockEntry = function(req, res){
    Timesheet.remove({_id: req.body.clockInid}).exec(function(err, response) {
        if (err) {
            res.send({
              "message": "Something went wrong!",
              "err": err,
              "status_code": "500"
            });
        } else{
            res.send({"message": "success", "status_code": "200", "data": response}); 
        }
    });
}

exports.getmissedpunchreport = function(req, res) {
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var skipNo = (page - 1) * count;
    var query = { 
        "ClockInDate": {
            $lte: new Date(req.body.dateRange.end_date),
            $gte: new Date(req.body.dateRange.start_date)
        },
        //"missedPunchTimesheets": { $not: { $size: 0 } } 
        "missedPunchTimesheets.location" : mongoose.Types.ObjectId(req.body.location)
    };
    Missedpunch.aggregate([{$match : {"missedPunchTimesheets": { $not: { $size: 0 } } }}, {$unwind : "$missedPunchTimesheets"}, {$match : query}, 
    {$group: {
        _id: "$_id",
        missedPunchOut:{$push : "$missedPunchOut"},
        ClockInDate: {$push : "$ClockInDate"},
        missedPunchTimesheets: {$push: "$missedPunchTimesheets"},
        created_date : {$push : "$created_date"}
    }},
    {
        $skip: skipNo
    }, {
        $limit: count
    }
    ]).exec(function(err, missedPunch) {

      missedPunch.map(function(item) {
          item.missedPunchOut = item.missedPunchOut[0];  
          item.ClockInDate = item.ClockInDate[0];
          item.created_date = item.created_date[0];
      })
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } else {
        res.send({"message": "success", "data": missedPunch,"total": missedPunch.length, "status_code": "200"});
      }
    })
}

exports.getpayrollreport = function(req, res){
    var start_date = req.body.dateRange.start_date;
    var end_date = req.body.dateRange.end_date;
    var query = {
        "userinfo.location" : mongoose.Types.ObjectId(req.body.location),
        "eventinfo.location" : mongoose.Types.ObjectId(req.body.location)
    };
    query.clockInDateTime = {
        $gte: new Date(start_date), 
        $lte: new Date(end_date)
    }
   
    if(req.body.reportselect == "complete"){
        query.clockOutDateTime = {
            $exists:true
        }
    }
    if(req.body.reportselect == "incomplete"){
        query.clockOutDateTime = {
            $exists:false
        }
    }
   
    if(req.body.eventId){
        var event_account = mongoose.Types.ObjectId(req.body.eventId);
        query.eventId = event_account;
    }
    if(req.body.empId){
        var employeeId = mongoose.Types.ObjectId(req.body.empId);
        query.userId = employeeId;
    }

    Timesheet.aggregate([
        {$lookup: {from: "users", localField: "userId", foreignField: "_id", as: "userinfo"}},
        {$lookup: {from: "events", localField: "eventId", foreignField: "_id", as: "eventinfo"}},
        {$match: query},
        {$sort: {"clockInDateTime" : 1}}
    ]).exec(function(error, response){
        if(response){
            response.map(function(item) {
              item.userId = item.userinfo[0];  
              item.eventId = item.eventinfo[0];
            })
        }
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.scheduleReminderInFuture = function(req, res){
    var timestr = new Date(req.body.schRemindertime);
    var secs = timestr.getSeconds();
    var mins = timestr.getMinutes();
    var hours = timestr.getHours();
    var dayofmonth = timestr.getDate();
    var monthIncron = timestr.getMonth();
    var month = timestr.getMonth() + 1;
    var dayofweek = timestr.getDay();
    var year = timestr.getFullYear();
    var hourMin = ("0" + hours).slice(-2)+':'+("0" + mins).slice(-2); 
    var currDate = new Date();
    if(currDate.getFullYear() === year){
        var cronTimestr = secs+' '+mins+' '+hours+' '+dayofmonth+' '+monthIncron+' '+dayofweek;
        var cronTimestrToSave = month+'/'+dayofmonth+'/'+year+' | '+exports.twentyfour_to_twelve(hourMin)+' | '+ weekdays[dayofweek];
        var datatoSave   = {};
        datatoSave.title = req.body.title;
        datatoSave.description  = req.body.description;
        datatoSave.created_date = req.body.created_date;
        datatoSave.created_by_name = req.body.creator_name;
        datatoSave.cronTimestr = cronTimestrToSave;
        datatoSave.cronExestr = cronTimestr;
        datatoSave.schDate = timestr;
        Scheduletext(datatoSave).save(function(error, saveResponse){
            new CronJob(cronTimestr, function() {
                Scheduletext.findOneAndUpdate({_id:saveResponse._id},{CronStatus : true}).exec(
                    function(err, result){
                        if(result.status){
                             var eventId = mongoose.Types.ObjectId(req.body.eventid);
                            var title = req.body.title;
                            var description = req.body.description;
                            var created_by = req.body.created_by;
                            var role = req.body.role;
                            var created_date = req.body.created_date;
                            var creator_name = req.body.creator_name;
                            var timetosend = req.body.timetosend;
                            var query = {};
                            var d = new Date();
                            var currentDate = new Date(d.setDate(d.getDate()));
                            var todayDatestr = currentDate.getDate();
                            var todayYearstr = currentDate.getFullYear();
                            var todayMonthstr = currentDate.getMonth() + 1;
                            Timesheet.aggregate([
                              {$lookup: {from: "users", localField: "userId", foreignField: "_id", as: "userinfo"}},
                              {$project: {eventId: 1, day: {$dayOfMonth: '$clockInDateTime'}, year : {$year: '$clockInDateTime'}, month : {$month: '$clockInDateTime'}, userphone : "$userinfo.phone", userId : 1}},
                              {$match: {day: todayDatestr, year : todayYearstr, month : todayMonthstr, "clockOutDateTime":null, "eventId" : eventId}}
                            ]).exec(function(err, result){
                                var phoneNoList = [];
                                var i = 0;
                                result.forEach(function(element, key) {
                                    phoneNoList.push(element.userphone[0]);
                                    i++;
                                })
                                if(i == result.length){
                                    exports.send_sms_reminder(req, res, 0, phoneNoList, title, description);
                                }
                            });
                        }
                    }
                );
               
            }, null, true, 'America/Phoenix');
            res.send({"message": "success", "status_code": "200"});
        });
    }
}

exports.scheduleReminderOnWeekDay = function(req, res){
    var timestr = req.body.schRepeattime;
    var timestrArr = timestr.split(":");
    var secs = 00;
    var mins = timestrArr[1];
    var hours = timestrArr[0];
    var dayofweek = req.body.weekdy;
    var currDate = new Date();
    var cronTimestr = secs+' '+mins+' '+hours+' * * '+dayofweek;
    var hourMin = ("0" + hours).slice(-2)+':'+("0" + mins).slice(-2); 
    var cronTimestrToSave = exports.twentyfour_to_twelve(hourMin) +' | DayOfWeek : '+ weekdays[dayofweek];
    var datatoSave   = {};
    datatoSave.title = req.body.title;
    datatoSave.description  = req.body.description;
    datatoSave.created_date = req.body.created_date;
    datatoSave.created_by_name = req.body.creator_name;
    datatoSave.cronTimestr = cronTimestrToSave;
    datatoSave.cronExestr = cronTimestr;
    Scheduletext(datatoSave).save(function(error, saveResponse){
        new CronJob(cronTimestr, function() {
            Scheduletext.findOneAndUpdate({_id:saveResponse._id},{CronStatus : true}).exec(
                function(err, result){
                    if(result.status){
                        console.log('checkingCronOfWeekDay');
                        var eventId = mongoose.Types.ObjectId(req.body.eventid);
                        var title = req.body.title;
                        var description = req.body.description;
                        var created_by = req.body.created_by;
                        var role = req.body.role;
                        var created_date = req.body.created_date;
                        var creator_name = req.body.creator_name;
                        var timetosend = req.body.timetosend;
                        var query = {};
                        var d = new Date();
                        var currentDate = new Date(d.setDate(d.getDate()));
                        var todayDatestr = currentDate.getDate();
                        var todayYearstr = currentDate.getFullYear();
                        var todayMonthstr = currentDate.getMonth() + 1;
                        Timesheet.aggregate([
                          {$lookup: {from: "users",localField: "userId",foreignField: "_id",as: "userinfo"}},
                          {$project: {eventId: 1, day: {$dayOfMonth: '$clockInDateTime'}, year : {$year: '$clockInDateTime'}, month : {$month: '$clockInDateTime'}, userphone : "$userinfo.phone", userId : 1}},
                          {$match: {day: todayDatestr, year : todayYearstr, month : todayMonthstr, "clockOutDateTime":null, "eventId" : eventId}}
                        ]).exec(function(err, result){
                            var phoneNoList = [];
                            var i = 0;
                            result.forEach(function(element, key) {
                                phoneNoList.push(element.userphone[0]);
                                i++;
                            })
                            if(i == result.length){
                                exports.send_sms_reminder(req, res, 0, phoneNoList, title, description);
                            }
                        });
                    }
                }
            );
            
        }, null, true, 'America/Phoenix');
        res.send({"message": "success", "status_code": "200"});
    });
}

exports.employeeReminderInFuture = function(req, res){
    var timestr = new Date(req.body.schRemindertime);
    var secs = timestr.getSeconds();
    var mins = timestr.getMinutes();
    var hours = timestr.getHours();
    var dayofmonth = timestr.getDate();
    var monthIncron = timestr.getMonth();
    var month = timestr.getMonth() + 1;
    var dayofweek = timestr.getDay();
    var year = timestr.getFullYear();
    var hourMin = ("0" + hours).slice(-2)+':'+("0" + mins).slice(-2); 
    var currDate = new Date();
    if(currDate.getFullYear() === year){
        var cronTimestr = secs+' '+mins+' '+hours+' '+dayofmonth+' '+monthIncron+' '+dayofweek;
        var cronTimestrToSave = month+'/'+dayofmonth+'/'+year+' | '+exports.twentyfour_to_twelve(hourMin)+' | '+ weekdays[dayofweek];
        var datatoSave   = {};
        datatoSave.title = req.body.title;
        datatoSave.description  = req.body.description;
        datatoSave.created_date = req.body.created_date;
        datatoSave.created_by_name = req.body.creator_name;
        datatoSave.cronTimestr = cronTimestrToSave;
        datatoSave.cronExestr = cronTimestr;
        datatoSave.schDate = timestr;
        Scheduletext(datatoSave).save(function(error, saveResponse){
            new CronJob(cronTimestr, function() {
                Scheduletext.findOneAndUpdate({_id:saveResponse._id},{CronStatus : true}).exec(
                    function(err, result){
                        if(result.status){
                            var memoData = req.body;
                            var created_by = req.body.created_by;
                            var role = req.body.role;
                            var created_date = req.body.created_date;
                            var creator_name = req.body.creator_name;
                            var timetosend = req.body.timetosend;
                            var query = {};
                            var d = new Date();
                            var currentDate = new Date(d.setDate(d.getDate()));
                            var todayDatestr = currentDate.getDate();
                            var todayYearstr = currentDate.getFullYear();
                            var todayMonthstr = currentDate.getMonth() + 1;
                            exports.send_sms_reminder(req, res, 0, memoData.phoneNumberList, memoData.title,memoData.description);
                        }
                    }
                );
            }, null, true, 'America/Phoenix');
            res.send({"message": "success", "status_code": "200"});
        });
    }        
}

exports.scheduleReminder = function(req, res){
    var eventId = mongoose.Types.ObjectId(req.body.eventid);
    var title = req.body.title;
    var description = req.body.description;
    var created_by = req.body.created_by;
    var role = req.body.role;
    var created_date = req.body.created_date;
    var creator_name = req.body.creator_name;
    var timetosend = req.body.timetosend;
    var query = {};
    var today = moment.utc().tz(constantObj.Timezones.AP);
    var todayDatestr = Number(today.format('D'));
    var todayMonthstr = Number(today.format('M'));
    var todayYearstr = Number(today.format('YYYY'));
    var cronTimestrToSave = new Date();
    var datatoSave   = {};
    datatoSave.title = req.body.title;
    datatoSave.description  = req.body.description;
    datatoSave.created_date = req.body.created_date;
    datatoSave.created_by_name = req.body.creator_name;
    datatoSave.cronTimestr = cronTimestrToSave;
    datatoSave.cronExestr = cronTimestrToSave;
    datatoSave.schDate = new Date();
    Scheduletext(datatoSave).save(function(error, saveResponse){
        Timesheet.aggregate([
          {$lookup: {from: "users",localField: "userId",foreignField: "_id",as: "userinfo"}},
          {$project: {eventId: 1, day: {$dayOfMonth: '$clockInDateTime'}, year : {$year: '$clockInDateTime'}, month : {$month: '$clockInDateTime'}, userphone : "$userinfo.phone", userId : 1}},
          {$match: {day: todayDatestr, year : todayYearstr, month : todayMonthstr, "clockOutDateTime":null, "eventId" : eventId}}
        ]).exec(function(err, result){
            Scheduletext.findOneAndUpdate({_id:saveResponse._id},{CronStatus : true}).exec(
                function(error, innerresult){
                    if(!err){
                        var phoneNoList = [];
                        var i = 0;
                        if(result.length){
                            result.forEach(function(element, key) {
                                phoneNoList.push(element.userphone[0]);
                                i++;
                            })
                        }
                        if(i == result.length){

                            exports.send_sms_reminder(req, res, 0, phoneNoList, title, description);
                            res.send({"message": "success", "data": result, "status_code": "200"});
                        }
                    }
                }
            );
        });
    });
}

exports.send_sms_reminder = function(req, res, i, numberList,title, description) {
    if (numberList.length) {
        var client = require('twilio')(constantObj.twilio.accountSid, constantObj.twilio.authToken);
        client.messages.create({
            to: constantObj.twilio.countrycode+numberList[i],
            from: constantObj.twilio.twilio_number, //test number of client registered in twilio
            body: title+' : '+description+'\n\n'+ constantObj.twilio.twilio_signature
        }, function(err, message) {
            console.log(err);
            if (i + 1 == numberList.length) {
                console.log("Sent to all");
            } else {
                //mention name of route because this function is calling in cron also.
                exports.send_sms_reminder(req, res, i + 1, numberList, title, description);
            }
        });
    } else {
        console.log("no data");
    }
}

exports.getTimesheetDataByIds = function(req, res){
    Timesheet.find({_id:{$in:req.body.timesheetIdsArr}}).populate('eventId userId').exec(function(error, response){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.getTicketSystemReport = function(req, res) {
    var ownerId = mongoose.Types.ObjectId(req.body.owner_id);
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var query = { 
            "dateOfReport": {
                $lte: new Date(req.body.dateRange.end_date),
                $gte: new Date(req.body.dateRange.start_date)
            },
            "event.location" : mongoose.Types.ObjectId(req.body.location)
    };
    if (req.body.shift) {
       query.shift = req.body.shift;
    }
    if (req.body.eventId) {
       query.eventId = mongoose.Types.ObjectId(req.body.eventId);
    }
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
     Ticketcontrol.aggregate([{
        $lookup: {
          from: "users",
          localField: "created_by",
          foreignField: "_id",
          as: "userInfo"
        }
      },{
        $lookup: {
          from: "events",
          localField: "eventId",
          foreignField: "_id",
          as: "event"
        }
      }, {
        $match: query
      }, {
        $sort: sortObject
      }]).exec(function(error, countreports) {
        if(error){
            res.send({"message": "Something went wrong!", "err": error, "status_code": "500"});
        }else{
            Ticketcontrol.aggregate([{
                $lookup: {
                  from: "users",
                  localField: "created_by",
                  foreignField: "_id",
                  as: "userInfo"
                }
              },{
                $lookup: {
                  from: "events",
                  localField: "eventId",
                  foreignField: "_id",
                  as: "event"
                }
              }, {
                $match: query
              }, {
                $sort: sortObject
              },{
                $skip: skipNo
              }, {
                $limit: count
              }]).exec(function(err, reports) {
                    reports.map(function(item) {
                      item.created_by = item.userInfo[0];  
                      item.eventId = item.event[0];
                    })
                    if (err) {
                        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
                    } else {
                        res.send({"message": "success", "data": reports,"total": countreports.length, "status_code": "200"});
                    }
              });
        }
      });
}

exports.getTicketSystemCommentReport = function(req, res) {
    var ownerId = mongoose.Types.ObjectId(req.body.owner_id);
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var query = { 
            "dateOfReport": {
                $lte: new Date(req.body.dateRange.end_date),
                $gte: new Date(req.body.dateRange.start_date)
            },
            "event.location" : mongoose.Types.ObjectId(req.body.location)
    };
    if (req.body.eventId) {
       query.eventId = mongoose.Types.ObjectId(req.body.eventId);
    }

    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    Ticketcontrol.aggregate([{
        $lookup: {
          from: "users",
          localField: "created_by",
          foreignField: "_id",
          as: "userInfo"
        }
      },{
        $lookup: {
          from: "events",
          localField: "eventId",
          foreignField: "_id",
          as: "event"
        }
      }, {
        $match: query
      }, {
        $sort: sortObject
      }]).exec(function(error, countreports) {
            if (error) {
                res.send({"message": "Something went wrong!", "err": error, "status_code": "500"});
            }else{
                Ticketcontrol.aggregate([{
                    $lookup: {
                      from: "users",
                      localField: "created_by",
                      foreignField: "_id",
                      as: "userInfo"
                    }
                  },{
                    $lookup: {
                      from: "events",
                      localField: "eventId",
                      foreignField: "_id",
                      as: "event"
                    }
                  }, {
                    $match: query
                  }, {
                    $sort: sortObject
                  },{
                    $skip: skipNo
                  }, {
                    $limit: count
                  }]).exec(function(err, reports) {
                        reports.map(function(item) {
                          item.created_by = item.userInfo[0];  
                          item.eventId = item.event[0];
                        })
                        if (err) {
                         res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
                        } else {
                         res.send({"message": "success", "data": reports,"total": countreports.length, "status_code": "200"});
                        }
                  });
            }   
      });
}

exports.removereportControls = function(req,res){
  Ticketcontrol.remove({_id: {$in:req.body.controlId}}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        res.send({"message": "success", "status_code": "200", "data": response}); 
    }
  });
}

exports.removeMissedPunch = function(req,res){
  Missedpunch.remove({ "missedPunchTimesheets": { $exists: true, $size: 0 } }).exec(function(err, datares) {
    if (err) {
       console.log('Error is coming in clearing empty array from missed punches');
    } else{
        console.log('Empty array of timesheets is removed from Missed punch table.');
    }
  });
}

exports.updateTicketControlIntimesheet = function(req, res){
    Timesheet.findOneAndUpdate({_id:req.body.clockInId}, {$set:{'ticketControl':true}}).exec(function(error,info){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": info,
                "status_code": "200"
            });
        }
    });
}

exports.getClockingReport = function(req, res){
    var start_date = req.body.dateRange.start_date;
    var end_date = req.body.dateRange.end_date;
    var event_account = req.body.eventId;
    var query = {};
    var query = {
        "clockInDateTime": {
            $gte: new Date(start_date), 
            $lte: new Date(end_date)
        },
        "eventId" : event_account
    };
    if(req.body.empId){
        var employeeId = mongoose.Types.ObjectId(req.body.empId);
        query.userId = employeeId;
    }
    Timesheet.find(query).populate('eventId userId').exec(function(error, response){
        if (error) {
            res.send({
                "message": "Something went wrong!",
                "err": error,
                "status_code": "500"
            });
        }else{
            res.send({
                "message": "success",
                "data": response,
                "status_code": "200"
            });
        }
    });
}

exports.activeInactiveCron = function(req, res){
  var update = {};
  var data = req.body
  update.status = (data.status) ? false : true;
  Scheduletext.update({
    _id: data.scheduletextId
  }, update).exec(function(error, schedules) {
    if (error) {
        res.send({
            "message": "Something went wrong!",
            "err": error,
            "status_code": "500"
        });
    } else {
        res.send({
          "message": "success",
          "data": schedules,
          "status_code": "200"
        }); 
    }
  })
}