var Question = require("mongoose").model("Question");
var mongoose = require('mongoose');
exports.addQuestion = function(req,res){
  Question(req.body).save(function(err,response){
    if(!err) {
      res.json({"message": "success", "status_code": "200"});
    }else{
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    }
  });    
};

exports.getQuestionlist = function(req, res){
    var page = req.body.page || 1,
    count = req.body.count || 10;
    var role = req.body.role;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var query = {'is_deleted':false};  
    if (search) {
    query['$or'] = [];
    query['$or'].push({title: new RegExp(search,'i')});
    }
    Question.count(query).exec(function(err, total) {
        if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        } 
        else{
          Question.find(query).sort(sortObject).skip(skipNo).limit(count).exec(function(err, questionlists) {
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
            res.send({"message": "success", "data": questionlists,"total": total, "status_code": "200"});
            }
          })
        }
    });
};

exports.getAllQuestions = function(req, res){
  Question.find({'is_deleted':false, 'active':true}).exec(function(err,questions){
    if (err) {
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      res.send({"message": "success", "data": questions,"status_code": "200"});
    }
  });
}

exports.update_Question_status = function(req, res) {
  var update = {};
  var data = req.body.data
  update.active = (data.active) ? false : true;
  Question.findOneAndUpdate({
    _id: data._id
  }, update, {new:true}).exec(function(error, questions) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": questions,
        "status_code": "200"
      });
    }
  })
}

exports.editQuestion = function(req, res) {
  var update = {};
  var data = req.body;
  update.title = data.questtitle;
  Question.update({
    _id: data.questid
  }, update).exec(function(error, questions) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": questions,
        "status_code": "200"
      });
    }
  })
}

/*enable/disable questions*/
exports.enable_disable_questions = function(req, res) {
  var enabled = req.body.enabled;
  var selAll = req.body.allChecked;
  var query = {};
  var fields = {};
  query._id = {
      $in: req.body.questions
  };
  if (enabled == true) {
      fields.active = true;
  } else {
      fields.active = false;
  }
  Question.update(query, fields, {
    multi: true
  }).exec(function(error, questions) {
    if (error) {
      return res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    }
    Question.find({is_deleted:false}, function(err, questions) {
        if (error) {
          res.send({
            "message": "Something went wrong!",
            "err": err,
            "status_code": "500"
          });
        } else {
          res.send({
            "message": "success",
            "data": questions,
            "status_code": "200"
          });
        }
      })
  })
}



exports.deleteQuestList=function(req,res){
        var query = {};
        var fields = {};
        query._id = {
            $in: req.body.questid
        };
        fields.is_deleted = true;
        Question.update(query, fields, {
          multi: true
        }).exec(function(error, questions) {
          if (error) {
            return res.send({
              "message": "Something went wrong!",
              "err": err,
              "status_code": "500"
            });
          }
          Question.find({}, function(err, questions) {
              if (error) {
                res.send({
                  "message": "Something went wrong!",
                  "err": err,
                  "status_code": "500"
                });
              } else {
                res.send({
                  "message": "success",
                  "data": questions,
                  "status_code": "200"
                });
              }
            })
        })
}
exports.IdBasedQuestion = function(req, res){
  Question.find({'is_deleted':false, 'active':true, '_id':{$in:req.body.questionIds}}).exec(function(err,questions){
    if (err) {
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      res.send({"message": "success", "data": questions,"status_code": "200"});
    }
  });
}


