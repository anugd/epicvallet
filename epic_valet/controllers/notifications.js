var Notification = require("mongoose").model("Notification");
var mongoose = require('mongoose');

exports.getNotifications = function(data, callback) {
    var userId = data.user_id;
    Notification.find({receiver_id: userId, is_marked:false}, function(err, response){
        if (err)
            callback({"message": "error", "data": err, "status_code": "500"});
        else
            callback({"message": "success", "data": response, "status_code": "200"});
    });  
};

exports.createNotification = function(req, res, socket){
    console.log("-- CREATE NOTIFICATIONS --");
    var notificationData = req.body;
    var x = 0;
    for(var i=0; i< notificationData.length; i++) {
        Notification(notificationData[i]).save(function(err,response){
            if (err){
                res.status("500").json({"message": "Something went wrong!", "err": err});
            }
        });    
        if (x === notificationData.length - 1) {
            res.status("200").json({"message": "success"});
            socket.emit('MemoNotification', 'NewMemo');
        }
        x++;
    }
};

exports.markNotifications = function(req, res,socket) {
    var notificationData = req.body;
    Notification.findOneAndUpdate({memo_id:notificationData.memo_id, receiver_id:notificationData.userId},{is_marked:true}).exec(function(err,response){
         res.status("200").json({"message": "success"});
         socket.emit('MemoNotification', 'updateMemo');
    });
};

exports.getEmployeeLogs = function(data, callback) {
    var userId = data.user_id;
    Notification.find({receiver_id: userId, is_marked:false}, function(err, response){
        if (err)
            callback({"message": "error", "data": err, "status_code": "500"});
        else
            callback({"message": "success", "data": response, "status_code": "200"});
    });  
};

exports.getEmployeeLogs = function(req, res) {
  var page = req.body.page || 1,
  count = req.body.count || 50;
  var sort = req.body.sortOrder;
  var sort_field =  req.body.field;
  var skipNo = (page - 1) * count;
  var search = req.body.search || "";
  var query = {};
  var query = {notification_type: {$in:["Availability"]}};
  var sortObject = {};
  var stype = sort_field;
  var sdir = sort;
  sortObject[stype] = sdir;
  if (search) {
    query['$or'] = [];
  }
  Notification.count(query).exec(function(err, total) {
    if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      Notification.find(query).populate('sender_id').sort(sortObject).skip(skipNo).limit(count).exec(function(err, notifications) {
        if (err) {
         res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        } else {
        res.send({"message": "success", "data": notifications,"total": total, "status_code": "200"});
        }
      })
    }
  })
}