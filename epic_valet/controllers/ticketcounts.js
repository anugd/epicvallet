var Ticketcount = require("mongoose").model("Ticketcount");
var mongoose = require('mongoose');
exports.saveticketcount = function(req,res){
    Ticketcount(req.body).save(function(err,response){
       if (!err) {
        res.json({"message": "success", "status_code": "200"});
       }
    });
};

exports.gettickets = function(req, res) {
    var page = req.body.page || 1,count = req.body.count || 50;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var ownerId = req.body.owner_id;
    var query = {owner_id: mongoose.Types.ObjectId(ownerId)};
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
  if (search) {
    query['$or'] = [];
    query['$or'].push({
      'shift': new RegExp(search, 'i')
    })
    query['$or'].push({
      'name_of_event.name': new RegExp(search, 'i')
    })
  }
  Ticketcount.aggregate([{
        $lookup: {
          from: "events",
          localField: "event",
          foreignField: "_id",
          as: "name_of_event"
        }
      }, {
        $match: query
      }]).exec(function(err, totaltickets) {
    if (err) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      Ticketcount.aggregate([{
        $lookup: {
          from: "events",
          localField: "event",
          foreignField: "_id",
          as: "name_of_event"
        }
      }, {
        $match: query
      }, {
        $sort: sortObject
      },{
        $skip: skipNo
      }, {
        $limit: count
      }]).exec(function(err, tickets) {
        if (!err) {
          res.send({
            "message": "success",
            "data": tickets,
            "total": totaltickets.length,
            "status_code": "200"
          });
        } else {
          res.send({
            "message": "success",
            "data": err,
            "total": totaltickets.length,
            "status_code": "200"
          });
        }
      });
    }
  });
}
