var Ranking = require("mongoose").model("Ranking");
var Employeeeval = require("mongoose").model("Employeeeval");
var mongoose = require('mongoose');

exports.saveWriteUp = function(req, res){
	var rankingData = {};
	rankingData.employee = req.body.employee;
	rankingData.event_account = req.body.event_account;
	rankingData.manager = req.body.manager;
	rankingData.reason = req.body.reason;
	rankingData.value = req.body.value;
	rankingData.currDate = req.body.currDate;
  rankingData.created_date = req.body.created_date;
	rankingData.writeup_type = req.body.writeup_type;
  rankingData.writeupDate = req.body.writeupDate;
  rankingData.location = req.body.location;
	var query = {_id: req.body._id};
	if (!query._id) {
	    query._id = new mongoose.mongo.ObjectID();
	}
	Ranking.findOneAndUpdate(query, rankingData, {upsert:true}, function(err,response){
    if(err){
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      res.send({"message": "success", "status_code": "200"});
    }
  });
}

exports.fetchWriteUps = function(req, res) {
  var query = {};
  var page = req.body.page || 1,
  count = req.body.count || 50;
  var skipNo = (page - 1) * count;
  var role = req.body.userrole;
  var loggedId = req.body.loggedInId;
  var sort = req.body.sortOrder;
  var sort_field =  req.body.field;
  var search = req.body.search || "";
  var accountsearch = req.body.accountsearch || "";
  var managersearch = req.body.managersearch || "";
  var sortObject = {};
  var stype = sort_field;
  var sdir = sort;
  sortObject[stype] = sdir;
  if(role == "employee"){
      var query = {'employee':mongoose.Types.ObjectId(loggedId)};  
  }
  query.location = mongoose.Types.ObjectId(req.body.location);
  if (search) {
        query.employee = mongoose.Types.ObjectId(search);
  }

  if (accountsearch) {
        query.event_account = mongoose.Types.ObjectId(accountsearch);
  }

  if (managersearch) {
        query.manager = mongoose.Types.ObjectId(managersearch);
  }
  Ranking.count(query).exec(function(err, total) {
    if (err) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      Ranking.aggregate([{
        $lookup: {
          from: "events",
          localField: "event_account",
          foreignField: "_id",
          as: "eventInfo"
        }
      },
      {
        $lookup: {
          from: "users",
          localField: "manager",
          foreignField: "_id",
          as: "managerInfo"
        }
      },
      {
        $lookup: {
          from: "users",
          localField: "employee",
          foreignField: "_id",
          as: "empInfo"
        }
      }, {
        $match: query
      }, {
        $sort: sortObject
      },{
        $skip: skipNo
      }, {
        $limit: count
      }]).exec(function(err, writeups) {
        if (!err) {
          res.send({
            "message": "success",
            "data": writeups,
            "total": total,
            "status_code": "200"
          });
        } else {
          res.send({
            "message": "success",
            "data": err,
            "total": total,
            "status_code": "200"
          });
        }
      });
    }
  });
}

exports.clearWriteups = function(req, res){
  var query = {};
  if(req.body.searchemp){
    query.employee = mongoose.Types.ObjectId(req.body.searchemp);

  }
  if(req.body.searchAcc){
    query.event_account = mongoose.Types.ObjectId(req.body.searchAcc);
  }
  Ranking.remove(query).exec(function(err,response){
      if (err) {
        res.status("500").json({"message": "Something went wrong!", "err": err});
      } else {
        res.status("200").json({"message": "success"});
      }
  });
}

exports.clearWriteupsbyIds = function(req, res){
  Ranking.remove({'_id':{$in:req.body.removeIds}}).exec(function(err,response){
      if (err) {
        res.status("500").json({"message": "Something went wrong!", "err": err});
      } else {
        res.status("200").json({"message": "success"});
      }
  });
}

exports.getAllRankings = function(req, res){
  var datebeffourmonth = new Date(req.body.datebeforefourMonth);
  if(req.body.searchbyemp){
    var query = { $or:[ { "rankingdata.created_date" : {$gte : datebeffourmonth} }, {"active" : true , "employee" : mongoose.Types.ObjectId(req.body.searchbyemp), "userinfo.location" : mongoose.Types.ObjectId(req.body.location), "userinfo.active" : true}]};

   // var query = {"rankingdata.created_date" : {$gte : datebeffourmonth}, "active" : true , "employee" : mongoose.Types.ObjectId(req.body.searchbyemp), "userinfo.location" : mongoose.Types.ObjectId(req.body.location), "userinfo.active" : true};
  }else{
    var query = { $or:[ { "rankingdata.created_date" : {$gte : datebeffourmonth} }, {"active" : true , "userinfo.location" : mongoose.Types.ObjectId(req.body.location), "userinfo.active" : true}]};
   // var query = {"rankingdata.created_date" : {$gte : datebeffourmonth}, "active" : true , "userinfo.location" : mongoose.Types.ObjectId(req.body.location), "userinfo.active" : true};
  }
  Employeeeval.aggregate([ 
        {$lookup: {from: "rankings", localField: "employee",foreignField: "employee", as: "rankingdata"}},
        {$lookup: {from: "users", localField: "employee", foreignField: "_id", as: "userinfo"}},
        {$match:  query},
        {$sort: {"created_date" : -1}},
        {$project : {"rankcreated" : "$rankingdata.created_date", "userfname" : "$userinfo.first_name","userlname" : "$userinfo.last_name","employeeId" : "$userinfo.employeeid", "hiring_date" : "$userinfo.date_of_hiring", "employee" : 1, "evaluation" :1, "rankvalue":"$rankingdata.value"}},
        {$group : { _id : {"rankcreated" : "$rankcreated", "employeeId": "$employee", "empfname" : "$userfname", "emplname": "$userlname", "empId":"$employeeId", "hiringDate" : "$hiring_date", "rankval" : {$sum:"$rankvalue"}}, evaluationval : {$avg : "$evaluation"}}} 
  ]).exec(function(err, response) {
      if(err){
        res.status("500").json({"message": "Something went wrong!", "err": err});
      }else{console.log(response);
        res.status("200").json({"message": "success", "data" :response});
      }
  });
  
}