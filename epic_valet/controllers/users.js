var User = require("mongoose").model("User"),
    Availability = require("mongoose").model("Availability"),
    Usertime = require("mongoose").model("Usertime"),
    Employeebackup = require("mongoose").model("Employeebackup"),
    encrypt = require("../utilities/encryption"),
    Timeslot = require("mongoose").model("Timeslot"),
    Timeslotbuffer = require("mongoose").model("Timeslotbuffer"),
    formidable = require('formidable');
    var nodemailer = require("nodemailer");
    var path = require('path');
    var root = process.cwd();
    var fs = require("fs");
    var constantObj = require(path.resolve(root, 'constants.js'));
    var mongoose = require('mongoose');
    
exports.getUsers = function( req, res ) {
  User.find({}).exec(function(err, collection) {
    res.send(collection);
  });
};

exports.del_usr_img = function(req, res,user) {
    User.findOneAndUpdate({_id:req.body._id},{$set:{prof_image:null}},{},function(err,data){
      if(!err){
        var form = new formidable.IncomingForm();
        form.uploadDir = path.join(__dirname + './../uploads/' + user.prof_image);
        fs.unlink(form.uploadDir, function(err, s){
          if (err) {
              res.status(400);
              res.json({
                  'success': false
              });
          } else {
              res.status(200);
              res.json({
                  'success': true,
              });
          }
        });
      }else{
        res.status(400);
        res.json({
            'success': false
        });
      }
    })
}

exports.createUser = function( req, res, next ) {
  var origpass = req.body.password;
  var userData={};
  userData = req.body;
  userData.first_name = req.body.first_name.substring(0,1).toLocaleUpperCase() + req.body.first_name.substring(1);
  userData.last_name = req.body.last_name.substring(0,1).toLocaleUpperCase() + req.body.last_name.substring(1); 
  userData.email    = userData.email.toLowerCase();
  userData.salt     = encrypt.createSalt();
  userData.password = encrypt.hashPassword(userData.salt, userData.password);
  User.create(userData, function(err, user) {
      if(err){
        if (err.toString().indexOf('E11000') > -1) {
          err = new Error("That email is already part of this system.");
        }
        res.send({"message": "Email already exists", "status_code": "500", "reason": err.toString()});
      }else{
          var employeeIdArr = [];
          employeeIdArr.push(userData._id);
          updateUsertimeToSelectedEmp(employeeIdArr, userData.active, 0,function(err,success){
            console.log("Selected Employees Status Time period is updated");    
          });
          res.send({"status_code": "200","data":user});
          var email = req.body.email;
          var mailOptions = {
                from: constantObj.emailSettings.fromWords, // sender address
                to: userData.email, // list of receivers
                subject: "Epic Valet Account", // Subject line 
                html: "Hello "+userData.first_name.toUpperCase() + ", <p style='padding-left:10px'>Your EMS Account has been created. Following are the details.<br><br> EMS Url :<b>"+req.protocol + "://"+req.get('host')+"</b><br><br> EMS Id :<b>"+userData.email+"</b><br><br> EMS Password :<b>"+origpass+"</b><br><br>Note :- Kindly login to your EMS Account and fill in your personal details and update your password.</p>"+ constantObj.emailSettings.signature // html body
          };  
          constantObj.transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  console.log(error);
              }else{
                  console.log('Message sent: ' + info.response);
              }
          });
      }
  });
};

exports.updateUser = function(req, res) {
    var user = req.body;
    var updated_user = {}
    updated_user.first_name = user.first_name.substring(0,1).toLocaleUpperCase() + user.first_name.substring(1);
    updated_user.last_name = user.last_name.substring(0,1).toLocaleUpperCase() + user.last_name.substring(1);
    updated_user.phone = user.phone;
    updated_user.email = user.email;
    updated_user.employeeid = user.employeeid;
    updated_user.prof_image = (user.prof_image) ? user.prof_image : '';
    updated_user.address = {};
    updated_user.address.line1 = (user.address.line1) ? user.address.line1 : '';
    updated_user.address.line2 = (user.address.line2) ? user.address.line2 : '';
    updated_user.address.city = (user.address.city) ? user.address.city : '';
    updated_user.address.state = (user.address.state) ? user.address.state : '';
    updated_user.driver_license = (user.driver_license) ? user.driver_license : '';
    updated_user.date_of_birth = (user.date_of_birth) ? user.date_of_birth : '';
    updated_user.date_of_hiring = (user.date_of_hiring) ? user.date_of_hiring : '';
    updated_user.address.zip = (user.address.zip) ? user.address.zip : '';
    updated_user.roles = (user.roles) ? user.roles : '';
    updated_user.emergency_contact_name = (user.emergency_contact_name) ? user.emergency_contact_name : '';
    updated_user.emergencyphone = (user.emergencyphone) ? user.emergencyphone : '';
    updated_user.active = user.active;
    updated_user.location = user.location;
    User.findOneAndUpdate({
      _id: req.body._id
    }, updated_user,{new:true}, function(err, updateduserInfo) {
      if (err) {
        if (err.toString().indexOf('E11000') > -1) {
          err = new Error("That email is already part of this system.");
          res.send({"message": "email already exists", "status_code": "503", "reason": err.toString(), "err": err});
        }else{
          res.send({"status_code": "500", "reason": err.toString(), "err": err});
        }
      }
      else {
        var employeeIdArr = [];
        employeeIdArr.push(req.body._id);
        updateUsertimeToSelectedEmp(employeeIdArr, user.active, 0,function(err,success){
          console.log("Selected Employees Status Time period is updated");    
        });
        if(user.changeAddress){
          
          exports.updateAddressEmail(req, res, user);//send email to admin/hr/manager if address changed
        }
        if(req.body.del_profile_pic){
          exports.del_usr_img(req,res,updateduserInfo);
        }else{
          res.send({
            "message": "success",
            "data": updateduserInfo,
            "status_code": "200"
          });
        }

        
      }
    });
};

exports.updateAddressEmail = function(req, res, user) {
    var emails = [];
    User.find(
      {
        $and : [
        {$or: [{roles: {$in:["admin","hr"]}}, {roles: {$in:["manager"]}, location :user.locationID }]},
        {$or:[{is_deleted:{$exists:false}},{is_deleted:false}]}
        ], 
        active:true
      },
      {email:1, notificationalert:1, location : 1, is_deleted : 1, active : 1, roles : 1}, function(err, users){
        if (!err) {
          
          for(i=0; i<users.length;i++) {
            emails.push(users[i].email);
          }
        }
        exports.sendupdateAddressEmail(emails,user);
      });
}
exports.sendupdateAddressEmail = function(emails,user){

  var mailOptions = {
    from: constantObj.emailSettings.fromWords, // sender address
    to: emails, // list of receivers
    subject: "Updated Profile Info", // Subject line 
    html: "<p>Hello</p><br>" + "This is an informatory mail. User <b>" + user.first_name + " "+ user.last_name +"</b>, has updated his/her profile. Profile Details are as follows :<br /> <table> <tr> <th style = 'width:200px; text-align:left' > Address </th><td>" + user.address.line1 + '<br>' + user.address.city + '<br>' + user.address.state + '<br>' + user.address.zip + '' + "</td></tr>" + "<tr><th style = 'width:200px; text-align:left' >Contact No.</th><td>" + user.phone + "</td></tr>" + "</table>" + "<br><p> Warmly</p><p> Epic Valet Website</p>" // html body 

  };
	constantObj.transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log("email error", error);
    } else {
      console.log('Message sent: ' + info.response);
    }
  });
}
exports.getUserById = function( req, res ) {
  req.params.id = req.params.id.substring(1);
  User.findOne({ _id:req.params.id }).exec(function(err, user) {
    res.send(user);
  });
};

exports.updateUserInfo = function(req, res) {
  var data = req.body;
  data.first_name = data.first_name.substring(0,1).toLocaleUpperCase() + data.first_name.substring(1);
  data.last_name = data.last_name.substring(0,1).toLocaleUpperCase() + data.last_name.substring(1);
  delete data['password'];
  var dataid = data._id
  delete data._id;
  User.update({
    _id: dataid
  }, data, {
    upsert: true
  }, function(err, result) {
    if (err) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": data,
        "status_code": "200"
      });
    }
  })
};

// Get Time Slot Data
exports.getTimeSlot = function(req, res){
  Timeslot.find({location : mongoose.Types.ObjectId(req.query.locationId)}).exec(function(err, collection) {
    res.send(collection);
  });
};

exports.saveTimeSlot = function(req, res){
  if (!req.body._id) {
    Timeslot.create(req.body, function(err, slot) {
      if (err) {
        res.send({"err": err});
      }
      else {
        res.send({"message": "success", "data": slot, "status_code": "200"});
      }
    });
  }
  else if (req.body._id) 
  {
    var id=req.body._id;
    delete req.body._id;
    Timeslot.findOneAndUpdate({_id:id}, req.body, {new:true}, function(err, slot){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else{
        res.send({"message": "success", "data": slot, "status_code": "200"});
      }
    });
  }
  else {
    res.send({"message": "error", "data": "error", "status_code": "500"});
  }
};

exports.getDailyWorkHours = function(req,res){
  var empId = req.body._id;
  var currdate = req.body.currDate;
  Availability.find({employee_id:empId, currDate:{"$lt" : currdate }},{manager_schedule:1, currDate:1},function(err,response){
    res.send({"data": response});
  });
};

exports.getEmployeeAvailabilityStatus = function(req,res){
    var loginId = req.body._id;
    var startWeek = req.body.startWeek;
    var endWeek = req.body.endWeek;
    User.find({created_by:loginId,roles: ["employee"]}).exec(function(err, response){
        if (err){
            res.json({"message": "error", "data": err, "status_code": "500"});
        }
        else{
            res.json({"message": "success", "data": response, "status_code": "200"});
        }
    });
}
exports.checkLoggedin = function(req,res){
    var condition = {_id:{$in:req.body.empids}};
    User.find(condition).exec(function(err, response){
        if (err){
            res.json({"message": "error", "data": err, "status_code": "500"});
        }
        else{
            res.json({"message": "success", "data": response, "status_code": "200"});
        }
    }); 
}
exports.checkPwd = function(req,res){
    var condition = {_id:req.body.empid};
    User.findOne(condition).exec(function(err, user){
        if (err){
            console.log(err);
            res.json({"message": "error", "data": err, "status_code": "500"});
        }
        else if (user && user.authenticate(req.body.password)) {
          res.json({"message": "success", "data": user, "status_code": "200"});
        } else {
          res.json({"message": "user not found", "status_code": "404"});
        }
    }); 
}
//delete User
exports.delempl=function(req,res){
    var query = {};
    var fields = {};
    query._id = {
        $in: req.body.empid
    };
    fields.is_deleted = true;
    User.update(query, fields, {
      multi: true
    }).exec(function(error, users) {
      if (error) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
      }
      User.find({}, function(err, users) {
          if (error) {
            res.send({
              "message": "Something went wrong!",
              "err": err,
              "status_code": "500"
            });
          } else {
            res.send({
              "message": "success",
              "data": users,
              "status_code": "200"
            });
          }
        })
    })
}

// check random
exports.chkrandom=function(req,res){
  var num=req.body.num;
  User.find({employeeid:num}).exec(function(err, response){
      if (err){
        console.log(err);
        res.json({"message": "error", "data": err, "status_code": "500"});
      }
      else{
        if(response.length >0){
          res.json({"message": "duplicate value", "data": response, "status_code": "500"});
        }
        else{
          res.json({"message": "success", "data": response, "status_code": "200"});
        }
      }
  });
}

exports.chkrandomedit=function(req,res){
  var num=req.body.num;
  User.find({employeeid:num, _id:{$ne:mongoose.Types.ObjectId(req.body.editempId)}}).exec(function(err, response){
      if (err){
          res.json({"message": "error", "data": err, "status_code": "500"});
      }
      else{
        if(response.length >0){
          res.json({"message": "duplicate value", "data": response, "status_code": "500"});
        }
        else{
          res.json({"message": "success", "data": response, "status_code": "200"});
        }
      }
  });
}

//check availableusers
exports.availableusers=function(req,res){
    var shift=req.body.shift;
    var currdate= new Date(req.body.currdate);
     if(shift== 'morning shift'){
        var condition = {is_morning_scheduled:true,currDate: currdate};
     }
     else if(shift== 'afternoon shift'){
         var condition = {is_afternoon_scheduled:true,currDate: currdate};
     }
     else if(shift== 'night shift'){
         var condition = {is_night_scheduled:true,currDate: currdate};
     }
     else if(shift== 'late night shift'){
         var condition = {is_late_night_scheduled:true,currDate: currdate};
     }
     Availability.find(condition,{employee_id:1},function(err,data){
        if(!err){
          var emp=[];
            for(i in data){
              emp.push(data[i].employee_id)
            }
          User.find({roles:["employee"], _id:{$nin:emp}}, null, {sort: {created_date: -1}}, function(err, users){
            if (err) {
              res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            }
            else {
              res.send({"message": "success", "data": users, "status_code": "200"});
            }
          });  
        }
        else{
          res.json(err);
        }
     })   
}
//viewempnameforevents
exports.viewempnameforevents=function(req,res){
  var date=new Date(req.body.date);
  var date2=new Date(req.body.date);
  date2.setDate(date2.getDate()+ 1);
  var event= req.body.event;
  Availability.find({currDate:{$gte:date,$lt:new Date(date2)},
    $or:[{"manager_schedule.late_night_schedule_details.event":event},
    {"manager_schedule.night_schedule_details.event":event},
    {"manager_schedule.afternoon_schedule_details.event":event},
    {"manager_schedule.morning_schedule_details.event":event}]},function(err,data){
    if(err){
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    }
    else{
        res.send({"message": "success", "data": data, "status_code": "200"});
    }
  }).populate("employee_id");
}

// Get Time Slot Buffer Data
exports.getTimeSlotBuffer = function(req, res){
  Timeslotbuffer.find({location:req.body.locationId}).exec(function(err, collection) {
     res.json({"message": "success", "data": collection, "status_code": "200"});
  });
};


// Save Time Slot Buffer Data
exports.saveTimeSlotBuffer = function(req, res){
    var errorMessage = "";
    var outputJSON = {};
    Timeslotbuffer.remove({location : req.body.location},function(error, response){
        if (error) {
            outputJSON = {'status': 'success', 'messageId':500, 'message':'Invalid Operation'};
        }
        else{
            var inputData = req.body.data;
            Timeslotbuffer.findOneAndUpdate({location:req.body.location}, {buffervalues: inputData, location:req.body.location},{upsert: true, new : true}, function(err, event){
              if (err) {
                res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
              }
              else {
                res.send({'status': 'success', 'messageId':200, 'message':'successfully'});
              }
            });
        }
    });
};

//Get employee by ownerId
exports.getEmployeeCount = function(req, res){
  var query = {roles: {$in:["employee","manager","hr"]}, active:true, is_deleted:false, location : req.body.location};
  User.count(query).exec(function(error, empcount){
      if (error){
          res.json({"message": "error", "data": error, "status_code": "500"});
      }else{
          res.json({"message": "success", "data": empcount, "status_code": "200"});
      }
  }); 
}

/*added by suman to send mail onupdate profile*/
exports.updatesendmail = function(user, req, res) {
  var mail_email = ''
  User.findOne({
    _id: user.created_by
  }).exec(function(err, response) {
    console.log("response",response);
    if (response.roles.indexOf('admin') > -1) {
      var query = {
        roles: ["employee"]
      };
    }
    if (response.roles.indexOf('manager') > -1) {
      var query = {
        created_by: user.created_by,
        roles: ["employee"]
      };
    }
    User.find(query).exec(function(err, admres) {
      if (err) {
        res.json({
          "message": "error",
          "data": err,
          "status_code": "500"
        });
      } else {
        admres.forEach(function(val, index) {
          mail_email = mail_email + (mail_email == '' ? val.email : ',' + val.email);
        })

        var mailOptions = {
          from: constantObj.emailSettings.fromWords, // sender address
          to: mail_email, // list of receivers
          subject: "Updated Profile Info", // Subject line
          html: "Hello " + "This is an informatory mail. User " + user.first_name + ", has updated his/her profile. Profile Details are as follows :<br /> <table> <tr> <th style = 'width:200px; text-align:left' > Address </th><td>" + user.address.line1 + '\n' + user.address.line2 + '\n' + user.address.city + '\n' + user.address.state + '\n' + user.address.zip + '\n' + "</td></tr>" + "<tr><th style = 'width:200px; text-align:left' >Contact No.</th><td>" + user.phone + "</td></tr>" + "</table>" + " Thanks Epic Valet" // html body 

        };
        console.log("mailOptions", mailOptions);
        /*constantObj.transporter.sendMail(mailOptions, function(error, info) {
          if (error) {
            console.log("email error", error);
          } else {
            console.log('Message sent: ' + info.response);
          }
        });*/
      }
    })
  });
}
  /*end*/
/* to get employee listing*/
exports.getEmployeesList = function(req, res) {
  var page = req.body.page || 1,
  count = req.body.count || 50;
  var sort = req.body.sortOrder;
  var sort_field =  req.body.field;
  var skipNo = (page - 1) * count;
  var search = req.body.search || "";
  var rolesArr = ["employee","manager","hr"];
  if(req.body.searchByRole == "employee"){
    var rolesArr = ["employee"];
  }
  else if(req.body.searchByRole == "manager"){
    var rolesArr = ["manager"];
  }
  else if(req.body.searchByRole == "hr"){
    var rolesArr = ["hr"];
  }
  var query = {roles: {$in:rolesArr},is_deleted:false};
  query.location = req.body.location;
  if(req.body.status == 'active'){
    query.active = true
  }
  else if(req.body.status == 'inactive' ){
    query.active = false
  }
  var sortObject = {};
  var stype = sort_field;
  var sdir = sort;
  sortObject[stype] = sdir;
  if (search) {
    query['$or'] = [];
    query['$or'].push({first_name: new RegExp(search,'i')})
    query['$or'].push({last_name: new RegExp(search,'i')})
    query['$or'].push({email: new RegExp(search,'i')}) 
    //query['$or'].push({employeeid: search})
  }
  User.count(query).exec(function(err, total) {
    if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      User.find(query).sort(sortObject).skip(skipNo).limit(count).exec(function(err, users) {
        if (err) {
         res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        } else {
        res.send({"message": "success", "data": users,"total": total, "status_code": "200"});
        }
      })
    }
  })
}
/*end*/


var updateUsertimeToSelectedEmp = function(empIds, status, i, cb){
    if(i == empIds.length){
        return cb(null,'success');
    }else{      
      if(status){
        Usertime.findOne({"employee_id":empIds[i], "ActiveFrom":null}).exec(function(err, userTimeEntry){
          if(userTimeEntry){
            userTimeEntry.ActiveFrom = new Date();
            userTimeEntry.save(function(err, user) {
              i = i + 1;
              updateUsertimeToSelectedEmp(empIds, status, i, cb);  
            });
          }else{
            Usertime.create({employee_id:empIds[i], InActiveFrom:new Date()}, function(err,usr){
               i = i + 1;
               updateUsertimeToSelectedEmp(empIds, status, i, cb);  
            });
          }
        })
      }else{
        Usertime.create({employee_id:empIds[i], InActiveFrom:new Date()}, function(err,usr){
           i = i + 1;
           updateUsertimeToSelectedEmp(empIds, status, i, cb);  
        });
      }
    }
}

/*enable/disable employee*/
exports.enabledisableemp = function(req, res) {
  var enabled = req.body.enabled;
  var selAll = req.body.allChecked;
  var query = {};
  var fields = {};
  query._id = {
      $in: req.body.employee
  };
  if (enabled == true) {
      fields.active = true;
  } else {
      fields.active = false;
  }
  User.update(query, fields, {
    multi: true
  }).exec(function(error, users) {
      if (error) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
      }
      // Update uer time of active days
      updateUsertimeToSelectedEmp(req.body.employee, req.body.enabled, 0,function(err,success){
        console.log("Selected Employees Status Time period is updated");    
      });
      // End of update user time of active days

      User.find({roles: {$in:["employee"]},is_deleted:false}, function(err, users) {
          if (error) {
            res.send({
              "message": "Something went wrong!",
              "err": err,
              "status_code": "500"
            });
          } else {
            res.send({
              "message": "success",
              "data": users,
              "status_code": "200"
            });
          }
      })
  })
}
/*end*/

/*enable/disable Notifications*/
exports.enableNotifications = function(req, res) {
  var notificationalert = req.body.notificationalert;
  var selAll = req.body.allChecked;
  var query  = {};
  var fields = {};
  query._id = {
      $in: req.body.employee
  };
  if (notificationalert == true) {
      fields.notificationalert = true;
  } else {
      fields.notificationalert = false;
  }
  User.update(query, fields, {
    multi: true
  }).exec(function(error, users) {
      if (error) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
      }

      User.find({roles: {$in:["employee"]},is_deleted:false}, function(err, users) {
          if (error) {
            res.send({
              "message": "Something went wrong!",
              "err": err,
              "status_code": "500"
            });
          } else {
            res.send({
              "message": "success",
              "data": users,
              "status_code": "200"
            });
          }
      })
  })
}
/*end*/

exports.update_status = function(req, res) {
  var update = {};
  var data = req.body.data
  update.active = (data.active) ? false : true;
  User.update({
    _id: data._id
  }, update).exec(function(error, users) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": error,
        "status_code": "500"
      });
    } else {
      if(update.active){
        Usertime.findOne({"employee_id":data._id, "ActiveFrom":null}).exec(function(err, userTimeEntry){
          if(userTimeEntry){
            userTimeEntry.ActiveFrom = new Date();
            userTimeEntry.save(function(err, user) {
              if (err) {
                res.send({
                  "message": "Something went wrong!",
                  "err": err,
                  "status_code": "500"
                });
              }else{
                res.send({
                  "message": "success",
                  "data": user,
                  "status_code": "200"
                });
              }
            });
          }
        })
      }else{
        Usertime.create({employee_id:data._id, InActiveFrom:new Date()}, function(err,usr){
          if(usr){
            res.send({
              "message": "success",
              "data": usr,
              "status_code": "200"
            });
          }
        });
      }
    }
  })
}

exports.update_notification = function(req, res) {
  var update = {};
  var data = req.body.data
  update.notificationalert = (data.notificationalert) ? false : true;
  User.update({
    _id: data._id
  }, update).exec(function(error, users) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": error,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": users,
        "status_code": "200"
      });
    }
  })
}

exports.changepassword = function(req, res){
    var newPassword = req.body.new_pass;
    var currentPassword = req.body.curr_pass;
    var userEmail = req.body.email;
    var userId = req.body._id;
    if ((req.user._id == userId) && (req.user.email == userEmail)  ) {
      User.findOne({email: userEmail}, function(err, user){
        if (err) {
          res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        }
        else{
          if(user && user.authenticate(currentPassword)) {
            user.salt     = encrypt.createSalt();  
            user.password = encrypt.hashPassword(user.salt, newPassword);
            user.password_change = true;
            user.save(function(err) {
              if (err)
                  res.send({"message": "error", "data": err, "status_code": "500"});
              else
                  res.send({"message": "success", "data": user, "status_code": "200"});
            });
          }else{
            res.send({"message": "Current password is not correct.", "data": err, "status_code": "400"});
          }
        }
      });
    }
    else {
      res.send({"message": "Something went wrong!", "err": "Not Authorized", "status_code": "500"});
    } 
}

exports.forgotPassword = function(req, res){
    var userEmail = req.body.email;
    var randomPass = Math.random().toString(36).substring(7, 15);
    User.findOne({email: userEmail}, function(err, user){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else{
        if(user) {
          user.salt     = encrypt.createSalt();        
          user.password = encrypt.hashPassword(user.salt, randomPass);
          user.save(function(err){
            if (err) return handleError(err);
            else {                       
              // setup e-mail data with unicode symbols
              var mailOptions = {
                  from: constantObj.emailSettings.fromWords, // sender address
                  to: userEmail, // list of receivers
                  subject: "Password Change Request", // Subject line                
                  html : "Hello " + user.first_name.toUpperCase() + ", <p style='padding-left:10px'>Your password changed successfully. Your new password is <b>"+randomPass+"</b>.</p>"+ constantObj.emailSettings.signature
              };
              // send mail with defined transport object
              constantObj.transporter.sendMail(mailOptions, function(error, info){
                  if(error){
                      console.log(error);
                  }else{
                      console.log('Message sent: ' + info.response);
                  }
              });
              return res.send({"message": "success", "data": user, "status_code": "200"});
            }
          });
        }else{
          res.send({"message": "This Email Address is not Exist.", "data": err, "status_code": "202"});
        }
      }
    });
}

exports.getAdminEmp = function(req, res){
    User.find({location : req.body.location, roles: {$in:["employee","manager","hr"]}, active : true, $or:[{is_deleted:{$exists:false}},{is_deleted:false}]}, {first_name : 1, last_name : 1, employeeid : 1, email : 1, phone :1, active : 1,  is_deleted : 1, roles : 1, prof_image : 1}, {sort: {first_name: 1}}, function(err, users){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else {
        res.send({"message": "success", "data": users, "status_code": "200"});
      }
    })
}

exports.getAdminEmpActiveInactive = function(req, res){
    User.find({location : req.body.location, roles: {$in:["employee","manager","hr"]}, $or:[{is_deleted:{$exists:false}},{is_deleted:false}]}, null, {sort: {first_name: 1}}, function(err, users){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else {
        res.send({"message": "success", "data": users, "status_code": "200"});
      }
    })
}

exports.getemplAndManagers = function(req, res){
    User.find({roles: {$in:["employee","manager","hr"]}, active : true, location : req.body.location, $or:[{is_deleted:{$exists:false}},{is_deleted:false}]}, {first_name : 1, last_name : 1, employeeid : 1, email : 1, phone :1, active : 1,  is_deleted : 1, roles : 1}, {sort: {first_name: 1}}, function(err, users){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else {
        res.send({"message": "success", "data": users, "status_code": "200"});
      }
    })
}

exports.getAllUsers = function(req, res){
    User.find({location : req.body.location, active : true, $or:[{is_deleted:{$exists:false}},{is_deleted:false}]}, null, {sort: {first_name: 1}}, function(err, users){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else {
        res.send({"message": "success", "data": users, "status_code": "200"});
      }
    })
}

exports.getAllManagers = function(req, res){
    User.find({location : req.body.location, roles: {$in:["manager","hr"]}, active : true, $or:[{is_deleted:{$exists:false}},{is_deleted:false}]}, null, {sort: {first_name: 1}}, function(err, users){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else {
        res.send({"message": "success", "data": users, "status_code": "200"});
      }
    })
}

exports.getManagerEmp = function(req, res){
    var manager_id = req.body.manager_id;
    User.find({created_by: manager_id, active : true, $or:[{is_deleted:{$exists:false}},{is_deleted:false}]}, null, {sort: {first_name: 1}}, function(err, users){
      if (err) {
        console.log(err);
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
      else{
        console.log(users);
        res.send({"message": "success", "data": users, "status_code": "200"});
      }
    });
}

exports.uniqueEmail=function(req,res){
  var mail1=req.body.email;
  User.findOne({email:mail1},function(err,user){
    if(err){
      res.status(400).jsonp({"message":"Error"})
    }
    else{
      if(user){
        res.status(200).jsonp({"message":"already existed","data":user})
      }
      else{
        res.status(200).jsonp({"message":"valid email","data":user})
      } 
    }
  })
};

exports.uniqueEmailEdit=function(req,res){
  var mail1=req.body.email;
  User.findOne({email:mail1, _id:{$ne:mongoose.Types.ObjectId(req.body.editempId)}},function(err,user){
    if(err){
      res.status(400).jsonp({"message":"Error"})
    }
    else{
      if(user){
        res.status(200).jsonp({"message":"already existed","data":user})
      }
      else{
        res.status(200).jsonp({"message":"valid email","data":user})
      } 
    }
  })
};

exports.restoreEmployee = function(req,res)
{
  var mail1 = req.body.email;
  User.update({email:mail1},{$set:{is_deleted:false}},function(err,usr){
    if(err){
      res.status(400).jsonp({"message":"error"})
    }
    else{
      if(usr){
        res.status(200).jsonp({"message":"updated successfullly"})
      }
      else{
        res.status(200).jsonp({"message":"not updated"}) 
      }
    }
  })
}

exports.fetchUserTime = function(req, res){
  Usertime.find({employee_id: req.body.employee_id}).populate('employee_id').exec(function(err, userTimeList) {
      if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
      } else {
        res.send({
          "message": "success",
          "data": userTimeList,
          "status_code": "200"
        });
      }
  })
}

exports.update_trade_subscribe = function(req, res) {
  var update = {};
  var data = req.body;
  update.tradesubscribe = data.tradesubscribe;
  User.findOneAndUpdate({
    _id: data._id
  }, update,{new:true}).exec(function(error, users) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": error,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": users,
        "status_code": "200"
      });
    }
  })
}
