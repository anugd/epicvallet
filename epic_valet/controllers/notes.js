var Note = require("mongoose").model("Note");
var Accountnote = require("mongoose").model("Accountnote");
var mongoose = require('mongoose');
exports.addNoteInList = function(req, res){
  if(req.body.noteslistCount || req.body.noteItemListId){
    Note.findOne({employee: req.body.employee, created_by : req.body.created_by}).exec(function(err, notelist) {    
      if(!err){
        var itemData = req.body.notesEntry;
        notelist.notes.push(itemData);
        notelist.save(function(err,response){
          res.send({"message": "success", "status_code": "200", "data":response});    
        });
      }
    });
  }
  else{
    var notesArr = [];
    notesArr.push(req.body.notesEntry);
    req.body.notes = notesArr;
    Note(req.body).save(function(err,response){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } else {
        res.send({"message": "success", "status_code": "200", "data":response});
      }
    });
  }
}

exports.empNotesList = function(req,res){
  Note.findOne({employee: req.body.employee}).populate('notes.added_by').exec(function(err, notelist) {    
  //Note.findOne({employee: req.body.employee, created_by : req.body.createdby}).populate('notes.added_by').exec(function(err, notelist) {    
    if (err) {
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } 
    else if(notelist){
      res.send({"message": "success", "data": notelist, "status_code": "200"});
    }
    else {
      res.send({"message": "success", "data": [], "status_code": "400"});
    }
  });
}

exports.removeNoteInList = function(req,res){
  Note.update({_id: req.body.noteMainId},{$pull:{notes:{_id:req.body.removeNoteId}}}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        Note.findOne({employee: req.body.employee, created_by : req.body.created_by}).exec(function(err, notelist) {    
          if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
          } 
          else if(notelist){
            res.send({"message": "success", "data": notelist, "status_code": "200"});
          }
          else {
            res.send({"message": "success", "data": [], "status_code": "200"});
          }
        });
    }
  });
}

exports.removeWholeNoteList = function(req,res){
  Note.remove({_id: req.body.noteMainId}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        res.send({"message": "success", "status_code": "200"}); 
    }
  });
}

// Event/Account
exports.addAccountNoteInList = function(req, res){
  if(req.body.noteslistCount){
    Accountnote.findOne({event_account: req.body.event_account, created_by : req.body.created_by}).exec(function(err, notelist) {    
      if(!err){
        var itemData = req.body.notesEntry;
        notelist.notes.push(itemData);
        notelist.save(function(err,response){
          res.send({"message": "success", "status_code": "200", "data":response});    
        }); 
      }
    });
  }
  else{
    var notesArr = [];
    notesArr.push(req.body.notesEntry);
    req.body.notes = notesArr;
    Accountnote(req.body).save(function(err,response){
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } else {
        res.send({"message": "success", "status_code": "200", "data":response});
      }
    });
  }
}

exports.accountNotesList = function(req,res){
  Accountnote.findOne({event_account: req.body.event_account, created_by : req.body.createdby}).exec(function(err, notelist) {    
    if (err) {
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } 
    else if(notelist){
      res.send({"message": "success", "data": notelist, "status_code": "200"});
    }
    else {
      res.send({"message": "success", "data": [], "status_code": "400"});
    }
  });
}

exports.removeAccountNoteInList = function(req,res){
  Accountnote.update({_id: req.body.noteMainId},{$pull:{notes:{_id:req.body.removeNoteId}}}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        Accountnote.findOne({event_account: req.body.event_account, created_by : req.body.created_by}).exec(function(err, notelist) {    
          if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
          } 
          else if(notelist){
            res.send({"message": "success", "data": notelist, "status_code": "200"});
          }
          else {
            res.send({"message": "success", "data": [], "status_code": "400"});
          }
        });
    }
  });
}

exports.removeWholeAccountNoteList = function(req,res){
  Accountnote.remove({_id: req.body.noteMainId}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        res.send({"message": "success", "status_code": "200"}); 
    }
  });
}

