var ShiftTrackList = require("mongoose").model("ShiftTrackList");
var mongoose = require('mongoose');
exports.addShiftTrackInList = function(req, res){
    ShiftTrackList.findOne({clockTimesheetId: mongoose.Types.ObjectId(req.body.clockTimesheetId)}).exec(function(error, response){
      if(response){
        console.log('I am in update');
        var itemData = req.body.ShiftTrackListsEntry;
        response.trackList.push(itemData);
        response.save(function(err,response){
          res.send({"message": "success", "status_code": "200"});    
        }); 
      }else{
        var ShiftTrackListsArr = [];
        ShiftTrackListsArr.push(req.body.ShiftTrackListsEntry);
        req.body.trackList = ShiftTrackListsArr;
        ShiftTrackList(req.body).save(function(err,response){
          if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
          } else {
            res.send({"message": "success", "status_code": "200"});
          }
        });
      }
    })
}

exports.fetchTrackList = function(req,res){
  ShiftTrackList.findOne({clockTimesheetId: req.body.clockTimesheetId}).exec(function(err, ShiftTrackListlist) {    
    if (err) {
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } 
    else if(ShiftTrackListlist){
      res.send({"message": "success", "data": ShiftTrackListlist, "status_code": "200"});
    }
    else {
      res.send({"message": "success", "data": [], "status_code": "400"});
    }
  });
}

exports.removeTrackInList = function(req,res){
  ShiftTrackList.update({_id: req.body.trackMainId},{$pull:{trackList:{_id:req.body.tracksubMainId}}}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        ShiftTrackList.findOne({clockTimesheetId: req.body.clockTimesheetId}).exec(function(err, ShiftTrackListlist) {    
          if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
          } 
          else if(ShiftTrackListlist){
            res.send({"message": "success", "data": ShiftTrackListlist, "status_code": "200"});
          }
          else {
            res.send({"message": "success", "data": [], "status_code": "400"});
          }
        });
    }
  });
}

exports.removeWholeTrackList = function(req,res){
  ShiftTrackList.remove({clockTimesheetId: req.body.clockTimesheetId}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        res.send({"message": "success", "status_code": "200"}); 
    }
  });
}

