var mongoose = require('mongoose');
var Schedule = mongoose.model("Schedule");
var User = mongoose.model("User");
var Location = require("mongoose").model("Location");
var Availability = mongoose.model("Availability");
var Trade = mongoose.model("Trade");
var Rejectedtrade = mongoose.model("Rejectedtrade");
var Timesheet = require("mongoose").model("Timesheet");
var Report = require("mongoose").model("Report");
var moment = require('moment');
var moment = require('moment-timezone');
var _ = require('lodash');
var path = require('path');
var root = process.cwd();
var fs = require('fs');
var conversion = require("phantom-html-to-pdf")();
var constantObj = require(path.resolve(root, 'constants.js'));
var weekdays  = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
exports.saveschedule = function(req, res, socket) {
	var outputJSON = {};
	var scheduledata = req.body.data;
	schedulebulk = Schedule.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < scheduledata.length; i++) {
		if(scheduledata[i].action){
			schedulebulk.find({
				event_account_id: mongoose.Types.ObjectId(scheduledata[i].event_id),
				shift_id: mongoose.Types.ObjectId(scheduledata[i].shift_id),
				employee_id: mongoose.Types.ObjectId(scheduledata[i].employeeId),
				day_of_week: scheduledata[i].day,
				timeslot: scheduledata[i].timeslot,
				//aligned_by: mongoose.Types.ObjectId(scheduledata[i].aligned_by), // creating issue when removing by other one 
				week_of: new Date(scheduledata[i].week_of),
				shiftReminderTime : scheduledata[i].shiftReminderTime,
				shiftEndTime : scheduledata[i].endshiftTime
			}).remove();
		}else{
			schedulebulk.find({
				event_account_id: mongoose.Types.ObjectId(scheduledata[i].event_id),
				shift_id: mongoose.Types.ObjectId(scheduledata[i].shift_id),
				day_of_week: scheduledata[i].day,
				timeslot: scheduledata[i].timeslot,
				//aligned_by: mongoose.Types.ObjectId(scheduledata[i].aligned_by),
				week_of: new Date(scheduledata[i].week_of),
				shiftReminderTime : scheduledata[i].shiftReminderTime,
				shiftEndTime : scheduledata[i].endshiftTime
			}).upsert().update({$set:{
				event_account_id: mongoose.Types.ObjectId(scheduledata[i].event_id),
				shift_id: mongoose.Types.ObjectId(scheduledata[i].shift_id),
				employee_id: mongoose.Types.ObjectId(scheduledata[i].employeeId),
				day_of_week: scheduledata[i].day,
				timeslot: scheduledata[i].timeslot,
				aligned_by: mongoose.Types.ObjectId(scheduledata[i].aligned_by),
				week_of: new Date(scheduledata[i].week_of),
				scheduleDate : new Date(scheduledata[i].scheduleDate),
				current_date : new Date(),
				shiftReminderTime : scheduledata[i].shiftReminderTime,
				shiftEndTime : scheduledata[i].endshiftTime,
				is_approve : false,
				created_date : new Date(),
				checkInOverlap : false
			}});
		}
	}
	if(scheduledata.length){
		schedulebulk.execute(function (error, data) {
			outputJSON = {'status': 'success', 'messageId':200, 'message':'successfully'};
			//socket.emit('scheduleNotification', {user_id: scheduledata[0].aligned_by});
			res.send(outputJSON); 
		});
	}else{
		outputJSON = {'status': 'success', 'messageId':200, 'message':'successfully'};
		res.send(outputJSON); 
	}
}
exports.sendMorningReminder = function(req, res){
	exports.sendReminder(req, res, 'Morning');
}
exports.sendAfternoonReminder = function(req, res){
	exports.sendReminder(req, res, 'Afternoon');
}
exports.sendNightReminder = function(req, res){
	exports.sendReminder(req, res, 'Night');
}
exports.sendLatenightReminder = function(req, res){
	exports.sendReminder(req, res, 'Late Night');
}

exports.sendReminder = function(req, res, timeslot) {
	var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]; //Added sunday again to get Sunday reminder in the list.
    var today = moment.utc().tz(constantObj.Timezones.AP);
	var dayName = weekdays[today.day() + 1];
	var day = today.day();
	var timeslotval = timeslot;
	if(day == 6){
		var date = today.add(1, 'days');
	}else{
		var date = today.subtract(day, 'days');
	}
	var week_of_start = moment(date);
	var WeekOfDatestr = Number(week_of_start.format('D'));
    var WeekOfDatemonth = Number(week_of_start.format('M'));
    var WeekOfDateyear = Number(week_of_start.format('YYYY'));
	Schedule.aggregate([
	  {$lookup: {from: "users", localField: "employee_id", foreignField: "_id", as: "userinfo"}},
	  {$lookup: {from: "events", localField: "event_account_id", foreignField: "_id", as: "eventAccountinfo"}},
      {$project: {_id : 1, day_of_week : 1, shiftReminderTime : 1, timeslot : 1, scheduleDate : 1, employee_id : 1, event_account_id : 1, week_of : 1, day: {$dayOfMonth: '$week_of'}, year :{$year: '$week_of'}, month:{$month: '$week_of'}, userphone : "$userinfo.phone", useractive : "$userinfo.active", userDeleted : "$userinfo.is_deleted", eventAccountName:"$eventAccountinfo.name", eventAccounttextReminder : "$eventAccountinfo.text_reminder"}},
      {$match: {day: WeekOfDatestr, month : WeekOfDatemonth, year : WeekOfDateyear, day_of_week: dayName, eventAccounttextReminder : true, timeslot: timeslotval}}
    ]).exec(function(error, schedules) {
		var phone_number = _.chain(schedules)
			.map(function(currentItem) {
				if (currentItem.userphone[0] && currentItem.useractive[0] && !currentItem.userDeleted[0]) {
					var retObj = {'phoneNumber':currentItem.userphone[0],'account':currentItem.eventAccountName[0], 'startTime':currentItem.shiftReminderTime, 'schDate':currentItem.scheduleDate};
					return retObj;
				}
			}).filter(function(obj) {
				if (obj) {
					return true
				} else {
					return false
				}
			})
			.value();
		var end_result = []
		phone_number.map(function(no) {
			if (end_result.length == 0) {
				end_result.push(no);
			} else if (end_result.indexOf(no) == -1) {
				end_result.push(no);
			}
		})
		exports.send_sms_reminder(req, res, 0, end_result);
	});
}

exports.twentyfour_to_twelve = function(ti){
	ti = ti.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [ti];
	if (ti.length > 1) { // If time format correct
		ti = ti.slice (1);  // Remove full string match value
		ti[5] = +ti[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
		ti[0] = +ti[0] % 12 || 12; // Adjust hours
	}
	return ti.join (''); // return adjusted time or original string
}

exports.send_sms_reminder = function(req, res, i, end_result) {
	if(end_result.length){
		var client = require('twilio')(constantObj.twilio.accountSid, constantObj.twilio.authToken);
		var schDte = new Date(end_result[i].schDate).toLocaleDateString();
		client.messages.create({
			to: end_result[i].phoneNumber,
			from: constantObj.twilio.twilio_number, //test number of client registered in twilio
			body: "This is a reminder for your shift tomorrow " +schDte+ " at " +end_result[i].account+ " starting at " +exports.twentyfour_to_twelve(end_result[i].startTime)+". Thank you and have a great shift! \n\n"+ constantObj.twilio.twilio_signature
		}, function(err, message) {
			if (i + 1 == end_result.length) {
				console.log("Sent to all");
			} else {
				exports.send_sms_reminder(req, res, i + 1, end_result);
			}
		});
	} else {
		console.log("no data");
	}
}

exports.sendTradeSmsReminder = function(req, res, i, phonenumber, message) {
    if (phonenumber) {
        var client = require('twilio')(constantObj.twilio.accountSid, constantObj.twilio.authToken);
        client.messages.create({
            to: phonenumber,
            from: constantObj.twilio.twilio_number, //test number of client registered in twilio
            body: "Trade Response : "+message+'\n\n'+ constantObj.twilio.twilio_signature
        }, function(err, message) {
            if(err){
	            console.log(err);
	        }else{
	            console.log('Trade request response SMS Sent');
	        }
        });
    } else {
        console.log("no data");
    }
}

exports.schedulereport = function(req, res){
	var condition = {'userInfo.location' : mongoose.Types.ObjectId(req.body.location),'userInfo.active' : true, week_of : new Date(req.body.from), $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
	if(req.body.shift){
		condition.timeslot = req.body.shift;
	}
	
	Schedule.aggregate([
    {
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "userInfo"
        }
  	},
  	{ $match: condition }]).exec(function(error, schedules){
  			schedules.map(function(item) {
	          item.employee_id = item.userInfo[0];  
	        })
			res.send({"status_code": "200", "data": schedules});
    });

}

exports.scheduleDetails = function(req,res){
	var schid = req.body.schId;
	var approveDate = req.body.acknowledgeDate;
	Schedule.findOneAndUpdate({_id:schid},{is_approve:true, is_approve_date:approveDate}, {new:true}).populate("event_account_id employee_id").exec(function(error, scheduleinfo){
		res.send({"status_code": "200", "data": scheduleinfo});
	});
}

exports.viewscheduleDetails = function(req,res){
	var schid = req.body.schId;
	Schedule.findOne({_id:schid}).populate("event_account_id employee_id").exec(function(error, scheduleinfo){
		res.send({"status_code": "200", "data": scheduleinfo});
	});
}

exports.schedulereportList = function(req, res){
	var eventId = req.body.eventId;
	var week_of_start = moment(req.body.from);
	var WeekOfDatestr = Number(week_of_start.format('D'));
    var WeekOfDatemonth = Number(week_of_start.format('M'));
    var WeekOfDateyear = Number(week_of_start.format('YYYY'));
	if(req.body.shift){
		var condition = {userlocation : mongoose.Types.ObjectId(req.body.location), useractive : true, userDeleted : false, day: WeekOfDatestr, month : WeekOfDatemonth, year : WeekOfDateyear, timeslot : req.body.shift};
	}else{
		var condition = {userlocation : mongoose.Types.ObjectId(req.body.location), useractive : true, userDeleted : false, day: WeekOfDatestr, month : WeekOfDatemonth, year : WeekOfDateyear};
	}
	if(eventId){
		condition.event_account_id = mongoose.Types.ObjectId(eventId);
	}
	Schedule.aggregate([
    {
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "userinfo"
        }
  	},
  	{
        $lookup: {
          from: "events",
          localField: "event_account_id",
          foreignField: "_id",
          as: "eventAccountinfo"
        }
  	},
  	{$project: {_id : 1, day_of_week : 1, shiftReminderTime : 1, shiftEndTime : 1, timeslot : 1, scheduleDate : 1, employee_id : 1, event_account_id : 1, week_of : 1, day: {$dayOfMonth: '$week_of'}, year :{$year: '$week_of'}, month:{$month: '$week_of'}, userphone : "$userinfo.phone", userfname: "$userinfo.first_name", zipcode : "$userinfo.address.zip", employeeid:"$userinfo.employeeid", userlname : "$userinfo.last_name", useractive : "$userinfo.active", userDeleted : "$userinfo.is_deleted", userlocation : "$userinfo.location", eventAccountName:"$eventAccountinfo.name", eventAccountNumber:"$eventAccountinfo.accountId"}},
  	{$match: condition},
  	{$sort: {eventAccountNumber : 1}},
  	{"$group" : {_id:"$event_account_id", entries: { $push: "$$ROOT" }}}
	]).exec(function(error, schedules){
		if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
    	}else{
    		res.send({"status_code": "200", "data": schedules});
    	}
	});
}

exports.sendAlertToAdminForAmericaPhoenix = function(req, res){
	Location.findOne({"locationtimezone" : "America/Phoenix"}).exec(function(err, locationInfo) {
      if (err) {
       res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } else {
      	exports.sendAlertToAdmin(req, res, locationInfo);
      }
    })
}

exports.sendAlertToAdminForLosAngeles = function(req, res){
	Location.findOne({"locationtimezone" : "America/Los_Angeles"}).exec(function(err, locationInfo) {
      if (err) {
       res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } else {
      	exports.sendAlertToAdmin(req, res, locationInfo);
      }
    })
}
exports.sendAlertToAdmin = function(req, res, locationinfo){
	var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var today = moment.utc().tz(locationinfo.locationtimezone);
	var dayName = weekdays[today.day()];
	var day = today.day();
	var date = today.subtract(day, 'days');
	var week_of_start = moment(date);
	var WeekOfDatestr = Number(week_of_start.format('D'));
    var WeekOfDatemonth = Number(week_of_start.format('M'));
    var WeekOfDateyear = Number(week_of_start.format('YYYY'));
	var currentHour = today.format('HH:mm'); 
	var locationObjectId = mongoose.Types.ObjectId(locationinfo._id);
	//User.find({$or: [{roles: {$in:["admin","hr"]}}, {roles: {$in:["manager"]}, location : locationinfo._id}], active:true, $or:[{is_deleted:{$exists:false}},{is_deleted:false}]}, {email:1, phone:1, notificationalert:1, location : 1, is_deleted : 1, active : 1, roles : 1}, function(err, users){
	User.find(
	{
		$and : [
		{$or: [{roles: {$in:["admin","hr"]}}, {roles: {$in:["manager"]}, location : locationinfo._id}]},
		{$or:[{is_deleted:{$exists:false}},{is_deleted:false}]}
		], 
		active:true
	},
	{email:1, phone:1, notificationalert:1, location : 1, is_deleted : 1, active : 1, roles : 1}, function(err, users){
		if (err){
       	 	console.log("Manager or admin is not found to send notification.");
	    }
	    else{
	    	var phones = [];
	    	var emails = [];
	    	var y = 0;
	    	for(i=0; i<users.length;i++){
	    		if(users[i].notificationalert){
	    			phones.push(users[i].phone);
	    		}
	    		emails.push(users[i].email);
	    		if(y == users.length-1){
	    			Schedule.aggregate([
					  {$lookup: {from: "users", localField: "employee_id", foreignField: "_id", as: "userinfo"}},
					  {$lookup: {from: "users", localField: "aligned_by", foreignField: "_id", as: "alignedByInfo"}},
					  {$lookup: {from: "events", localField: "event_account_id", foreignField: "_id", as: "eventAccountinfo"}},
				      {$project: {_id : 1, day_of_week : 1, shiftReminderTime : 1, scheduleDate : 1, employee_id : 1, event_account_id : 1, week_of : 1, day: {$dayOfMonth: '$week_of'}, year :{$year: '$week_of'}, month:{$month: '$week_of'}, userphone : "$userinfo.phone", userFname : "$userinfo.first_name", userLname : "$userinfo.last_name", userEmpId : "$userinfo.employeeid", useractive : "$userinfo.active", userDeleted : "$userinfo.is_deleted", userlocation : "$userinfo.location", eventAccountName:"$eventAccountinfo.name", eventAccountLocation:"$eventAccountinfo.location", eventActive:"$eventAccountinfo.is_active",  eventOnCall : "$eventAccountinfo.event_data.on_call",alignnedByEmail : "$alignedByInfo.email", alignnedByPhone : "$alignedByInfo.phone"}},
				      {$match: {day: WeekOfDatestr, month : WeekOfDatemonth, year : WeekOfDateyear, day_of_week: dayName, shiftReminderTime: currentHour, userlocation : locationObjectId, eventAccountLocation : locationObjectId, useractive : true, userDeleted : false, eventActive : true}}
				    ])
	    			.exec(function(error, schedules) {
					var x = 0;
					for (var i = 0; i < schedules.length; i++) {
						(function(i) {
							var employeeId = schedules[i].employee_id;
							var eventAccountId = schedules[i].event_account_id;
							var alignedByEmail = schedules[i].alignnedByEmail[0];
							var alignedByNumber = schedules[i].alignnedByPhone[0];
							var schEmployeelocation = schedules[i].userlocation[0];
							var employeename = schedules[i].userEmpId[0]+'-'+schedules[i].userFname[0]+' '+schedules[i].userLname[0]+' '+schedules[i].userphone[0] + ' ' +locationinfo.title;
							var eventtitle = schedules[i].eventAccountName[0];
							if(!schedules[i].eventOnCall[0]){
								Timesheet.findOne({userId:employeeId, eventId: eventAccountId, $or:[{clockOutDateTime:{$exists:false}},{clockOutDateTime:''}]}).populate('userId eventId').exec(function(error,eventInfo){
							        if (!eventInfo) {
							        	var msg = employeename + " doesn't clock in for "+ exports.twentyfour_to_twelve(currentHour) +" of " + eventtitle;
							            var client = require('twilio')(constantObj.twilio.accountSid, constantObj.twilio.authToken);
							            phones.map(function(currentnumber) {
							            	client.messages.create({
													to: currentnumber,
													from: constantObj.twilio.twilio_number, //test number of client registered in twilio
													body: msg+'\n\n'+ constantObj.twilio.twilio_signature
												}, function(err, message) {console.log(err);console.log(message);}
											);
							            });
										exports.sendMissedPunchEmail(emails, msg);
										//Start of entry in Logs
										var writeData = "";
									    writeData += "[" + new Date() + "]";
									    writeData += "Alert admin if an employee doesn't clock in for their shift";
									    writeData += "[RESULT: " + msg + "]\t\n";
									    writeData += "\n\n";
										var LOG_FILE_NAME = path.join(__dirname + './../logs/cron.log');
										fs.appendFile(LOG_FILE_NAME, writeData, function (error) {
								            if (error) 
								                console.log(error);
								       	});
							        }
						    	});
							}
					    	if (x == schedules.length - 1) {
					    		console.log("All Alerts are sent.");
					    	}
					    	x++;
						})(i);
					}
				});
	    		}
	    		y++;
	    	}
		}
	});
}

exports.empSchedules = function(req, res){
	var condition = {week_of : req.body.from, employee_id:req.body.empId};
	Schedule.find(condition).exec(function(err, schedules){
		if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        }else{
			res.send({"status_code": "200", "data": schedules});
		}
	});
}

exports.getEmpDaySchedules = function(req, res){
	var today = moment.utc().tz(constantObj.Timezones.AP);
	var WeekOfDatestr = Number(today.format('D'));
    var WeekOfDatemonth = Number(today.format('M'));
    var WeekOfDateyear = Number(today.format('YYYY'));
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var currentDate = new Date(req.body.currDate);
    var query = {userlocation : mongoose.Types.ObjectId(req.body.location), scheduleDate : {$gte : currentDate}, useractive : true, userDeleted : false};
    if (search) {
        query.employee_id = mongoose.Types.ObjectId(search);
    }
	Schedule.aggregate([
        {
	        $lookup: {
	          from: "users",
	          localField: "employee_id",
	          foreignField: "_id",
	          as: "userinfo"
	        }
      	},
      	{
	        $lookup: {
	          from: "events",
	          localField: "event_account_id",
	          foreignField: "_id",
	          as: "eventAccountinfo"
	        }
      	},
      	{$project: {employee_id:1, _id : 1, day_of_week : 1, scheduleDate : 1, shiftReminderTime : 1, shiftEndTime : 1, timeslot : 1, scheduleDate : 1, employee_id : 1, event_account_id : 1, week_of : 1, day: {$dayOfMonth: '$scheduleDate'}, year :{$year: '$scheduleDate'}, month:{$month: '$scheduleDate'}, userphone : "$userinfo.phone", userlocation : "$userinfo.location", userfname: "$userinfo.first_name", userlname : "$userinfo.last_name", useractive : "$userinfo.active", userDeleted : "$userinfo.is_deleted", eventAccountName:"$eventAccountinfo.name", checkInOverlap : 1}},
      	{$match: query}      	
	]).exec(function(err, total){
		if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        }else{
        	Schedule.aggregate([
	        {
		        $lookup: {
		          from: "users",
		          localField: "employee_id",
		          foreignField: "_id",
		          as: "userinfo"
		        }
	      	},
	      	{
		        $lookup: {
		          from: "events",
		          localField: "event_account_id",
		          foreignField: "_id",
		          as: "eventAccountinfo"
		        }
	      	},
	      	{$project: {employee_id:1, _id : 1, day_of_week : 1, scheduleDate : 1, shiftReminderTime : 1, shiftEndTime : 1, timeslot : 1, scheduleDate : 1, employee_id : 1, event_account_id : 1, week_of : 1, day: {$dayOfMonth: '$scheduleDate'}, year :{$year: '$scheduleDate'}, month:{$month: '$scheduleDate'}, userphone : "$userinfo.phone", userlocation : "$userinfo.location", userfname: "$userinfo.first_name", userlname : "$userinfo.last_name", useractive : "$userinfo.active", userDeleted : "$userinfo.is_deleted", eventAccountName:"$eventAccountinfo.name", checkInOverlap : 1}},
	      	{$match: query},
	      	{"$group" : {_id:{'employee_id':"$employee_id",'scheduleDate':"$scheduleDate"}, count:{$sum:1}, entries: { $push: "$$ROOT" }}}
			]).exec(function(err, schedules){
				if (err) {
		          	res.send({
			            "message": "Something wrong",
			            "data": err,
			            "status_code": "400"
			        });
		        } else {
		          	var i = 1;
		          	var newschedules = [];
					if(schedules.length){
			          	schedules.sort(function compare(a, b) {
						  var dateA = new Date(a._id.scheduleDate);
						  var dateB = new Date(b._id.scheduleDate);
						  return dateA - dateB;
						});
						schedules.forEach(function(elem, key){
							if(elem.count > 1){
								newschedules.push(elem);
							}
							if(i === schedules.length){
								res.send({
						            "message": "success",
						            "data": newschedules,
						            "total": newschedules.length,
						            "status_code": "200"
						        });
							}
							i++;
						});
					}else{
						res.send({
				            "message": "success",
				            "data": newschedules,
				            "total": newschedules.length,
				            "status_code": "200"
				        });
					}
		        }
			});
		}
	});
}

exports.getAvailabilitylist = function(req, res){
	  var to = new Date(req.body.to);
	  var searchEmployeeId = req.body.searchEmpId;
	  var from = new Date(req.body.from);
	  var query = {'userInfo.location' : mongoose.Types.ObjectId(req.body.location),'userInfo.active' : true, $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
	  if(searchEmployeeId){
            query.employee_id = mongoose.Types.ObjectId(searchEmployeeId);
      }
	  query.currDate = {
	    $gte: from,
	    $lte: to
	  }
	  Availability.aggregate([
	    {
	        $lookup: {
	          from: "users",
	          localField: "employee_id",
	          foreignField: "_id",
	          as: "userInfo"
	        }
	  	},
	  	{ $match: query },
	  	{$project:{
	        _id:1,
	        currDate:1,
	        employee_id:1,
	        is_morning_scheduled:1,
	        is_afternoon_scheduled:1,
	        is_night_scheduled:1,
	        is_late_night_scheduled:1,
	        is_morning_scheduled_type:1,
	        is_afternoon_scheduled_type:1,
	        is_night_scheduled_type:1,
	        is_late_night_scheduled_type:1,
	        is_morning_scheduled_force:1,
	        is_afternoon_scheduled_force:1,
	        is_night_scheduled_force:1,
	        is_late_night_scheduled_force:1,
	        empFirstName:"$userInfo.first_name",
	        empLastName:"$userInfo.last_name",
	        employeeid:"$userInfo.employeeid",
	        zipcode:"$userInfo.address.zip"
	  	}}
	  ]).exec(function(err, schedules){
	  	var result = _.chain(schedules)
	            .groupBy("currDate")
	            .pairs()
	            .map(function(currentItem) {
	                return _.object(_.zip(["date", "result"], currentItem));
	            })
	            .value();
		if (err) {
	        res.status(500).jsonp({"message": "Something went wrong!", "err": err});
	    }else{
			res.status(200).jsonp({"Availabilitydata": result});
		}
	 });
}

exports.getSchedulelist = function(req, res){
      var to = new Date(req.body.to);
      var from = new Date(req.body.from);
      var searchEmployeeId = req.body.searchEmpId;
      var query = {'userInfo.location' : mongoose.Types.ObjectId(req.body.location),'userInfo.active' : true, $or:[{'userInfo.is_deleted':{$exists:false}},{'userInfo.is_deleted':false}]};
	  if(searchEmployeeId){
            query.employee_id = mongoose.Types.ObjectId(searchEmployeeId);
      }
      query.scheduleDate = {
        $gte: from,
        $lte: to
      }
      Schedule.aggregate([
        {
	        $lookup: {
	          from: "users",
	          localField: "employee_id",
	          foreignField: "_id",
	          as: "userInfo"
	        }
      	},
      	{ $match: query },
      	{ $project:{
	        _id:1,
	        scheduleDate:1,
	        day_of_week:1,
	        timeslot:1,
	        employee_id:1,
	        empFirstName:"$userInfo.first_name",
	        empLastName:"$userInfo.last_name",
	        employeeid:"$userInfo.employeeid"
      	}
      	}
	  ]).exec(function(err, schedules){
	  	var result = _.chain(schedules)
                .groupBy("scheduleDate")
                .pairs()
                .map(function(currentItem) {
                    return _.object(_.zip(["date", "result"], currentItem));
                })
                .value();
		if (err) {
            res.status(500).jsonp({"message": "Something went wrong!", "err": err});
        }else{
			res.status(200).jsonp({"scheduledata": result});
		}
	  });
}

exports.dayScheduleForAllEmp = function(req, res){
	var condition = {scheduleDate : new Date(req.body.weekDay),"userInfo.location" : mongoose.Types.ObjectId(req.body.location), "userInfo.active" : true};
	Schedule.aggregate([
        {
	        $lookup: {
	          from: "users",
	          localField: "employee_id",
	          foreignField: "_id",
	          as: "userInfo"
	        }
      	},
      	{
	        $lookup: {
	          from: "events",
	          localField: "event_account_id",
	          foreignField: "_id",
	          as: "eventInfo"
	        }
      	},
      	{ $match: condition }]).exec(function(err, schedules){
      		schedules.map(function(item) {
	          item.employee_id = item.userInfo[0];  
	          item.event_account_id = item.eventInfo[0];  
	        })
			if (err) {
				res.status(500).jsonp({"message": "Something went wrong!", "err": err});
	        }else{
	        	res.status(200).jsonp({"data": schedules});
			}
		});
}

exports.postOnBoard = function(req, res){
	var eventAccountName = req.body.eventName;
	if(req.body.scheduleId){
		var condition = {location:req.body.location, scheduleId : req.body.scheduleId, userId : req.body.userId, status : {$ne:'Approve'}};	
	}
	if(req.body.shiftId && !req.body.scheduleId){
		var condition = {location:req.body.location, shiftId : req.body.shiftId, userId : req.body.userId, status : {$ne:'Approve'}};		
	}
	Trade.count(condition).exec(function(error, tradeExist){
		if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
		}
		else if(tradeExist){
			res.status(208).jsonp({"message": "Already Exists", "err": error});
		}
		else{
			Trade(req.body).save(function(err, response){
				if (err) {
					res.status(500).jsonp({"message": "Something went wrong!", "err": err});
		        }else{
		        	if(req.body.tradePosttype == "onBoard"){
		        		User.find({roles: {$in:["employee"]}, location:req.body.location, active:true, $or:[{is_deleted:{$exists:false}},{is_deleted:false}], tradesubscribe : true}, {email:1, phone:1, notificationalert:1, location : 1}, function(err, users){
							if (err){
					       	 	console.log("No employee is found to send trade notification.");
						    }
						    else{
						    	var emails = [];
						    	var y = 0;
						    	if(users.length){
						    		for(i=0; i<users.length;i++){
							    		emails.push(users[i].email);
							    		if(y === users.length-1) {
											if(req.body.comment) {
												isComment = "<p><b> Notes: </b>"+req.body.comment+"</p>";
											}else {
												isComment = '';
											}
							    			var mailOptions = {
											    from: constantObj.emailSettings.fromWords, 
											    to : "info@epicvalet.com",
											    bcc: emails, 
											    subject: "Trade Board Notification",              
												html : "Hello <p style='padding-left:10px'>A new shift is now available on the trade board.(<b>"+new Date(response.eventScheduleDate).toLocaleDateString()+"</b>) on "+eventAccountName+" posted by "+req.body.postedBy+".</p>"+isComment+ constantObj.emailSettings.signature																			    
											};  
											constantObj.transporter.sendMail(mailOptions, function(error, info){
											      if(error){
											          console.log(error);
											      }else{
											          console.log('Message sent: ' + info.response);
											      }
											});
											res.status(200).jsonp({"data": response});
							    		}
							    		y++;
							    	}
						    	}else{
						    		res.status(200).jsonp({"data": response});
						    	}
							}							
		        		});
					}else{
						res.status(200).jsonp({"data": response});
					}
				}
			});
		}
	});
}

exports.fetchCalendarShifts = function(req, res){
	var currMonthStartDate = new Date(req.body.currMonthStartDate);
    var currMonthEndDate = new Date(req.body.currMonthEndDate);
    Trade.aggregate([
	    {$match:{'location' : mongoose.Types.ObjectId(req.body.location), 'eventScheduleDate':{$gte:currMonthStartDate, $lte:currMonthEndDate}, $or:[{status:{$exists:false}},{status:""}], 'coWorkerUserId':{$exists:false}}},
	 	{"$group" : {_id:"$eventScheduleDate", count:{$sum:1}}}
	]).exec(function(error, trades){
    	if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
        }else{
        	res.status(200).jsonp({"data": trades});
		}
    });
}

exports.fetchTradeShiftsInfo = function(req, res){
	var selectedDate = new Date(req.body.selectedDate);
	Trade.find({location : req.body.location, eventScheduleDate : selectedDate,  $or:[{status:{$exists:false}},{status:""}], 'coWorkerUserId':{$exists:false}}).populate('eventId userId coWorkerUserId').exec(function(error, trades){
		if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
        }else{
        	res.status(200).jsonp({"data": trades});
		}
	});
}

exports.fetchTradeRequests = function(req, res){
	var coWorkerUserId = req.body.coWorkerUserId;
	var page = req.body.page || 1,
	count = req.body.count || 50;
	var skipNo = (page - 1) * count;
	Trade.count({location:req.body.location, coWorkerUserId : coWorkerUserId}).exec(function(err, total){
	    if (err) {
	        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
	    } else {
	      Trade.find({location:req.body.location, coWorkerUserId : coWorkerUserId}).sort({
	        'tradeDateTime': -1
	      }).skip(skipNo).limit(count).populate('eventId userId coWorkerUserId exchangeEventAccId exchangeSchId').exec(function(error, tradeReq) {
	        if(error){
				res.status(500).jsonp({"message": "Something went wrong!", "err": error});
	        }else{
	        	res.status(200).jsonp({"data": tradeReq, "total": total});
			}
	      })
	    }
	})
}

exports.fetchAminTradeRequests = function(req, res){
  var page = req.body.page || 1,
  count = req.body.count || 50;
  var skipNo = (page - 1) * count;
  var condition = {location:req.body.location, coWorkerUserId:{$ne:null}, status:{$ne:null}};
  if(req.body.searchByRole == "Approve"){
	condition.status = "Approve";
  }
  else if(req.body.searchByRole == "Reject"){
	condition.status = "Reject";
  }
  else if(req.body.searchByRole == "Waiting"){
	condition.status = "Waiting";
  }
  Trade.count(condition).exec(function(err, total) {
    if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      Trade.find(condition).sort({
        'tradeDateTime': -1
      }).skip(skipNo).limit(count).populate('eventId userId coWorkerUserId exchangeEventAccId').exec(function(error, tradeReq){
        if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
        }else{
        	res.status(200).jsonp({"data": tradeReq, "total": total});
		}
      })
    }
  })
}

exports.fetchAminRejectedTradeRequests = function(req, res){
  var page = req.body.page || 1,
  count = req.body.count || 50;
  var skipNo = (page - 1) * count;
  Rejectedtrade.count().exec(function(err, total) {
    if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      Rejectedtrade.find().sort({
        'tradeDateTime': -1
      }).skip(skipNo).limit(count).populate('eventId userId coWorkerUserId exchangeEventAccId').exec(function(error, tradeReq){
        if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
        }else{
        	res.status(200).jsonp({"data": tradeReq, "total": total});
		}
      })
    }
  })
}


exports.gettrade = function(req, res){
	var condition = {_id: mongoose.Types.ObjectId(req.params.tradeid)};
	Trade.findOne(condition).populate('eventId userId coWorkerUserId exchangeEventAccId approveRejectBy exchangeSchId').exec(function(error, tradeReq) {
        if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
        }else{
        	res.status(200).jsonp({"data": tradeReq});
		}
    });
}
exports.getrejectedtrade = function(req, res){
	var condition = {_id: mongoose.Types.ObjectId(req.params.tradeid)};
	Rejectedtrade.findOne(condition).populate('eventId userId coWorkerUserId exchangeEventAccId approveRejectBy exchangeSchId').exec(function(error, tradeReq) {
        if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
        }else{
        	res.status(200).jsonp({"data": tradeReq});
		}
    });
}

exports.updatetrade = function(req,res){
  var updated_by = req.body.approveRejectBy;
  Trade.findOne({_id:req.body.tradeId}).populate('coWorkerUserId userId').exec(function(err,response){
      if (null!==response) {
        response.status = req.body.status;
        response.approveRejectBy = updated_by;
        response.manager_comment = req.body.manager_comment;
        var coWorkerUserName = response.coWorkerUserId.first_name+' '+response.coWorkerUserId.last_name;
        if(req.body.status == 'Waiting'){
        	Trade.update({_id:req.body.tradeId}, {$set:{status:"Waiting", approveRejectBy:updated_by}}, function(error, innerresponse) { 
        		if (!error){
        			if(response.status == 'Waiting'){
			          var responsestatus = 'accepted by '+coWorkerUserName+' , now it is waiting for admin approval.';
			        }
        			var message = 'Trade request for ' +new Date(response.eventScheduleDate).toLocaleDateString()+' | '+response.shiftTime+' is '+responsestatus;
        			exports.sendTradeSmsReminder(req, res, 0, response.userId.phone, message);
        			exports.sendEmail(response.userId.email, message);
        			res.status(200).jsonp({"data": innerresponse, "message":"success"});
        		}
        	})
        }
        if(req.body.status == 'Reject'){
        	var datatoSave = {};
        	datatoSave.status = "rejected";
        	datatoSave.approveRejectBy = updated_by;
        	datatoSave.manager_comment = req.body.manager_comment;
        	datatoSave.userId = response.userId;
        	datatoSave.eventId = response.eventId;
        	datatoSave.scheduleId = response.scheduleId;
        	datatoSave.shiftId = response.shiftId;
        	datatoSave.shiftTime = response.shiftTime;
        	datatoSave.endshiftTime = response.endshiftTime;
        	datatoSave.day_of_week = response.day_of_week;
        	datatoSave.week_of = response.week_of;
        	datatoSave.tradePosttype = response.tradePosttype;
        	datatoSave.coWorkerUserId = response.coWorkerUserId;
        	datatoSave.approveRejectBy = response.approveRejectBy;
        	datatoSave.location = response.location;
        	datatoSave.updatedAt = response.updatedAt;
        	datatoSave.tradeId = response._id;
        	datatoSave.eventScheduleDate = response.eventScheduleDate;
        	datatoSave.timeslot = response.timeslot;
        	datatoSave.tradeDateTime = response.tradeDateTime;
        	datatoSave.exchangeshift = response.exchangeshift;
        	datatoSave.exchangeScheduleDate = response.exchangeScheduleDate;
        	datatoSave.exchangeSchId = response.exchangeSchId;
        	datatoSave.exchangeEventAccId = response.exchangeEventAccId;
        	datatoSave.exchangeScheduleDate = response.exchangeScheduleDate;
        	datatoSave.comment = response.comment;

			Rejectedtrade(datatoSave).save(function(error, saveResponse){
				if(!error){
					Trade.update({_id:req.body.tradeId}, {$unset:{coWorkerUserId:"", status:"", exchangeshift:"", exchangeScheduleDate:"", exchangeSchId:"", approveRejectBy : ""}, $set:{tradePosttype:"onBoard"}}, function(error, innerresponse) { 
		        		if (!error){
		        			if(response.status == 'Approve'){
					          var responsestatus = 'approved by admin';
					        }
					        if(response.status == 'Reject'){
					          var responsestatus = 'rejected by admin';
					        }
		        			var message = 'Your selection from trade board for ' +new Date(response.eventScheduleDate).toLocaleDateString()+' | '+response.shiftTime+' is '+responsestatus + '. Comment : '+req.body.manager_comment;
		        			var tradeInitiatormessage = 'Trade request for ' +new Date(response.eventScheduleDate).toLocaleDateString()+' | '+response.shiftTime+' is '+responsestatus + '. Comment : '+req.body.manager_comment;        			
		        			exports.sendTradeSmsReminder(req, res, 0, response.coWorkerUserId.phone, message);
		        			exports.sendTradeSmsReminder(req, res, 0, response.userId.phone, tradeInitiatormessage);
		        			exports.sendEmail(response.coWorkerUserId.email, message);
		        			exports.sendEmail(response.userId.email, tradeInitiatormessage);
		        			res.status(200).jsonp({"data": innerresponse, "message":"success"});
		        		}
		        	})
				}
			});
			
        }
        if(req.body.status == 'Approve'){
        	response.save(function(err, updateResponse){
        		if(!err){
        			if(response.tradePosttype == "onBoard"){
        				if(response.scheduleId){
        					Schedule.findOneAndUpdate({_id:response.scheduleId},{employee_id:response.coWorkerUserId._id, is_approve:true, is_approve_date : req.body.tradeapproveDate}).exec(function(error, scheduleinfo){
								res.status(200).jsonp({"data": scheduleinfo, "message":"success"});
								if(response.status == 'Approve'){
						          var responsestatus = 'approved by admin';
						        }
						        if(response.status == 'Reject'){
						          var responsestatus = 'rejected by admin';
						        }
								var message = 'Your selection from trade board for ' +new Date(response.eventScheduleDate).toLocaleDateString()+' | '+response.shiftTime+' is '+responsestatus+'. Comment : '+req.body.manager_comment;
								var tradeInitiatormessage = 'Trade request for ' +new Date(response.eventScheduleDate).toLocaleDateString()+' | '+response.shiftTime+' is '+responsestatus+'. Comment : '+req.body.manager_comment;
			        			exports.sendTradeSmsReminder(req, res, 0, response.coWorkerUserId.phone, message);
			        			exports.sendTradeSmsReminder(req, res, 0, response.userId.phone, tradeInitiatormessage);
			        			exports.sendEmail(response.coWorkerUserId.email, message);
			        			exports.sendEmail(response.userId.email, tradeInitiatormessage);
							});
        				}
        				if(!response.scheduleId){
        					var schData = {};
        					schData.event_account_id =  mongoose.Types.ObjectId(response.eventId);
							schData.shift_id =  mongoose.Types.ObjectId(response.shiftId);
							schData.employee_id =   mongoose.Types.ObjectId(response.coWorkerUserId._id);
							schData.day_of_week =  response.day_of_week;
							schData.timeslot =   response.timeslot;
							schData.aligned_by =   mongoose.Types.ObjectId(response.userId._id);
							schData.week_of =   new Date(response.week_of);
							schData.scheduleDate =   response.eventScheduleDate;
							schData.current_date =   new Date();
							schData.shiftReminderTime =   response.shiftTime;
							schData.shiftEndTime =   response.endshiftTime;
							schData.is_approve =   true;
							schData.is_approve_date =   req.body.tradeapproveDate;
							schData.checkInOverlap =   false;

							Schedule(schData).save(function(error, scheduleinfo){
								res.status(200).jsonp({"data": scheduleinfo, "message":"success"});
								if(response.status == 'Approve'){
						          var responsestatus = 'approved by admin';
						        }
						        if(response.status == 'Reject'){
						          var responsestatus = 'rejected by admin';
						        }
								var message = 'Your selection from trade board for ' +new Date(response.eventScheduleDate).toLocaleDateString()+' | '+response.shiftTime+' is '+responsestatus+'. Comment : '+req.body.manager_comment;
								var tradeInitiatormessage = 'Trade request for ' +new Date(response.eventScheduleDate).toLocaleDateString()+' | '+response.shiftTime+' is '+responsestatus+'. Comment : '+req.body.manager_comment;
			        			exports.sendTradeSmsReminder(req, res, 0, response.coWorkerUserId.phone, message);
			        			exports.sendTradeSmsReminder(req, res, 0, response.userId.phone, tradeInitiatormessage);
			        			exports.sendEmail(response.coWorkerUserId.email, message);
			        			exports.sendEmail(response.userId.email, tradeInitiatormessage);
							});
        				}
	        		}
	        		else if(response.tradePosttype == "coworker"){
	        			Schedule.findOneAndUpdate({_id:response.scheduleId},{employee_id:response.coWorkerUserId._id}).exec(function(error, scheduleinfo){
	        				if(error){
								res.status(500).jsonp({"message": "Something went wrong!", "err": error});
	        				}else{
								//Schedule.remove({_id:response.exchangeSchId}).exec(function(error,removeResponse){
								Schedule.findOneAndUpdate({_id:response.exchangeSchId}, {employee_id:response.userId._id}).exec(function(error,removeResponse){									
									if(error){
										res.status(500).jsonp({"message": "Something went wrong!", "err": error});
			        				}else{
										if(response.status == 'Approve'){
								          var responsestatus = 'approved by admin';
								        }
								        if(response.status == 'Reject'){
								          var responsestatus = 'rejected by admin';
								        }
										var message = 'Your selection from trade board for ' +new Date(response.eventScheduleDate).toLocaleDateString()+' | '+response.shiftTime+' is '+responsestatus+'. Comment : '+req.body.manager_comment;
										var tradeInitiatormessage = 'Trade request for ' +new Date(response.eventScheduleDate).toLocaleDateString()+' | '+response.shiftTime+' is '+responsestatus+'. Comment : '+req.body.manager_comment;
					        			exports.sendTradeSmsReminder(req, res, 0, response.coWorkerUserId.phone, message);
					        			exports.sendTradeSmsReminder(req, res, 0, response.userId.phone, tradeInitiatormessage);
					        			exports.sendEmail(response.coWorkerUserId.email, message);
					        			exports.sendEmail(response.userId.email, tradeInitiatormessage);
					        			res.status(200).jsonp({"message":"success"});
			        				}
								});
	        				}
						});
	        		}
        		}
        	});
        }
      }
  });    
};

exports.sendEmail = function(coWorkerUserIdemail, message){
	var mailOptions = {
	    from: constantObj.emailSettings.fromWords, 
	    to: coWorkerUserIdemail, 
	    subject: "TradeResponse",              
	    html: "Hello <p style='padding-left:10px'>Trade Response : "+message+"</p>" + constantObj.emailSettings.signature
	};  
	constantObj.transporter.sendMail(mailOptions, function(error, info){
	      if(error){
	          console.log(error);
	      }else{
	          console.log('Message sent: ' + info.response);
	      }
	});
}

exports.sendMissedPunchEmail = function(coWorkerUserIdemail, message){
	var mailOptions = {
	    from: constantObj.emailSettings.fromWords, 
	    to: coWorkerUserIdemail, 
	    subject: "Missed Clock In",              
	    html: "Hello <p style='padding-left:10px'> Missed Clock In : "+message+"</p>" + constantObj.emailSettings.signature
	};  
	constantObj.transporter.sendMail(mailOptions, function(error, info){
        if(error){
           console.log(error);
        }else{
           console.log('Message sent: ' + info.response);
        }
	});
}

exports.selectTradeShift = function(req,res, socket){
	Trade.findOne({_id:req.body.tradeId}).populate('coWorkerUserId').exec(function(err,response){
      	if (null!==response) {
	        response.status = req.body.status;
	        response.coWorkerUserId = req.body.coWorkerUserId;
	        response.save(function(err, updateResponse){
	        	if(err){
					res.status(500).jsonp({"message": "Something went wrong!", "err": err});
		        }else{
		        	res.status(200).jsonp({"data": updateResponse});
		        	socket.emit('adminTradeNotification', 'NewNotification');
				}
	        });
    	}
    });
}
exports.getCoworkerEmplData = function(req, res){
	var startDate = new Date(req.body.startDate);
    var endDate = new Date(req.body.endDate);
    Schedule.aggregate([
	    {
	      $lookup:
	        {
	          from: "users",
	          localField: "employee_id",
	          foreignField: "_id",
	          as: "employeeInfo"
	        }
	   },
	   {$match:{'employeeInfo.location' : mongoose.Types.ObjectId(req.body.location), 'scheduleDate':{$gte:startDate, $lte:endDate}}},
	   {$project:{scheduleDate:1,employee_id:1,shiftReminderTime:1,shiftEndTime:1,empfirstname:"$employeeInfo.first_name", emplastname:"$employeeInfo.last_name", empId:"$employeeInfo.employeeid"}},
	   {"$group" : {_id:{employee_id:"$employee_id", empFname:"$empfirstname", empLname:"$emplastname", empId : "$empId"}, shiftReminderTime:{$push:{ shiftId: "$_id"}}}}
	]).exec(function(error, trades){
    	if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
        }else{
        	res.status(200).jsonp({"data": trades});
		}
    });
}

exports.fetchSchedulesbyIds = function(req, res){
	var scheduleIds = req.body.schIds;
	Schedule.aggregate([
      	{
	        $lookup: {
	          from: "events",
	          localField: "event_account_id",
	          foreignField: "_id",
	          as: "eventInfo"
	        }
      	},
      	{$match: {
                _id: {
                    $in: scheduleIds.map(
                        function(id) {
                            return mongoose.Types.ObjectId(id);
                        })
                },
                "eventInfo.is_active" : true
            }
        }
    ]).exec(function(err, schedules){
      		schedules.map(function(item) {
	          item.event_account_id = item.eventInfo[0];  
	        })
			if (err) {
				res.status(500).jsonp({"message": "Something went wrong!", "err": err});
	        }else{
	        	res.status(200).jsonp({"data": schedules});
			}
	});
}

exports.scheduleReportSlotWise = function(sevenWeekDayArr, htmldata, schedules, scheduleIds, timeslotToWrite, k, cb){
	if(k == 4){
        return cb(htmldata);
    }else{  	
		scheduleIdsUnique = _.uniq(scheduleIds[k], 'EventAccountName');
		var htmldata = htmldata + "<h5 style='text-align:center'>"+timeslotToWrite[k]+"</h5>";
	    var htmldata = htmldata + "<table class='table table-bordered'>";
	    htmldata = htmldata + "<tr>";
	    var x = 0;
	    sevenWeekDayArr.forEach(function(el, ke){
	    	htmldata = htmldata + "<th style='width:14%'>"+moment(el).format('ddd, MM/DD/YY')+"</th>";
	    	if(x == sevenWeekDayArr.length-1){
	    		htmldata = htmldata + "</tr>";
	    		if(schedules[k].length){
	    			htmldata = htmldata + "<tr>";
	    			htmldata = htmldata + "<td colspan='7'>";
	    			scheduleIdsUnique.forEach(function(scelem, sckey){
	        			htmldata = htmldata + "<table class='table' style='border :1px outset lightgray;margin-bottom:10px;background-color:#F5F5F5;width:100%;'>";
	        			htmldata = htmldata + "<tr>";
	        			htmldata = htmldata + "<td class='unbreakable' colspan='7', style='border-bottom:1px solid #ddd; padding:3px;vertical-align:top;'>";
	        			htmldata = htmldata + "<h2 style='color:#2a6496;font-weight:300 !important;font-size:10px;'>"+scelem.EventAccountName+"</h2>";
	        			htmldata = htmldata + "</td>";
	        			htmldata = htmldata + "</tr>";
	        			htmldata = htmldata + "<tr>";
	        			weekdays.forEach(function(weel, weke){
	            			htmldata = htmldata + "<td class='unbreakable' style='padding:0px !important;padding-right:2px !important;width:14%;vertical-align:top;'>";
	            			htmldata = htmldata + "<ul class='unbreakable' style='list-style-type:none;margin:0;padding:0'>";
							schedules[k].forEach(function(schel, schke){
								if((schel.day_of_week === weel) && (JSON.stringify(schel.eventAccountId[0]) == JSON.stringify(scelem.eventAccountId))){
		                			htmldata = htmldata + "<li class='m-b-5 p-5' style='background-color:#EEEEEE;border:1px solid lightgray;min-height:50px;'>";
		                			htmldata = htmldata + "<span style='display:block;color:#2a6496;font-weight:700;font-size:9px;'>"+exports.twentyfour_to_twelve(schel.shiftReminderTime)+"-"+exports.twentyfour_to_twelve(schel.shiftEndTime)+" : "+schel.empFirstName+" "+schel.empLastName+"- ("+schel.employeeid+") </span>";
		                			htmldata = htmldata + "</li>";
	                			}
	            			});
	            			htmldata = htmldata + "</ul>";
	            			htmldata = htmldata + "</td>";
	        			})
	        			htmldata = htmldata + "</tr>";
	        			htmldata = htmldata + "</table>";
					})
	    			htmldata = htmldata + "</td>";
	    			htmldata = htmldata + "</tr>";
	    		}
	    		htmldata = htmldata + "</table>";
	    		k = k + 1;
	    		exports.scheduleReportSlotWise(sevenWeekDayArr, htmldata, schedules, scheduleIds, timeslotToWrite, k, cb);
	    	}
	    	x++;
	    })
	}
}

exports.schedulePdfReport = function(req, res){
	var week_of_start = moment(req.body.from);
	var StDa = new Date(req.body.from);
    var EnDa = new Date(req.body.to);
	var morScheduleIds = [];
	var morSchedules = [];
	var allschedules = [];
	var allscheduleIds = [];
	var afterScheduleIds = [];
	var afterSchedules = [];
	var nightScheduleIds = [];
	var nightSchedules = [];
	var ltNtScheduleIds = [];
	var ltNtSchedules = [];
	var timeslotToWrite = [];
	var WeekOfDatestr = Number(week_of_start.format('D'));
    var WeekOfDatemonth = Number(week_of_start.format('M'));
    var WeekOfDateyear = Number(week_of_start.format('YYYY'));
	var condition = {userlocation : mongoose.Types.ObjectId(req.body.location), day: WeekOfDatestr, month : WeekOfDatemonth, year : WeekOfDateyear, useractive : true, userDeleted : false};
	if (StDa && EnDa) {
	    var diff = (new Date(EnDa.getTime()) - new Date(StDa.getTime()))/86400000;
	}
	var sevenWeekDayArr = [];
	for(var i=0; i<= diff; i++) {
	    sevenWeekDayArr[i] = new Date(StDa.getTime() + i*86400000);
	    sevenWeekDayArr[i].dayName = weekdays[sevenWeekDayArr[i].getDay()];
	    sevenWeekDayArr[i].differ = Math.floor((new Date(StDa.getTime() + i*86400000) - new Date().getTime())/86400000);
	}
    Schedule.aggregate([
        {
	        $lookup: {
	          from: "users",
	          localField: "employee_id",
	          foreignField: "_id",
	          as: "userInfo"
	        }
      	},
      	{
	        $lookup: {
	          from: "events",
	          localField: "event_account_id",
	          foreignField: "_id",
	          as: "event"
	        }
	    },
      	{$project:{
      		 _id:1,
      		 day_of_week:1,
      		 shiftReminderTime:1,
      		 shiftEndTime:1,
      		 timeslot:1,
      		 created_date : 1,
      		 eventAccountId:"$event._id",
      		 eventAccountName:"$event.name",
      		 emp_id:"$userInfo._id",
      		 employeeid:"$userInfo.employeeid",
      		 empFirstName:"$userInfo.first_name",
	         empLastName:"$userInfo.last_name",
	         userlocation:"$userInfo.location",
	         scheduleDate : 1, 
	         employee_id : 1,
	         event_account_id : 1,
	         week_of : 1, 
	         day: {$dayOfMonth: '$week_of'}, 
	         year :{$year: '$week_of'},
	         month:{$month: '$week_of'},
	         useractive : "$userInfo.active", 
	         userDeleted : "$userInfo.is_deleted"
      	}},
      	{$match: condition}
	  ]).exec(function(error, schedules){
	    	if(error){
				res.status(500).jsonp({"message": "Something went wrong!", "err": error});
        	}
        	else if(!schedules.length){
        		res.send({"status_code": "404"});
        	}
        	else{
        		var z = 0;
        		schedules.forEach(function(elem, key){
                    elem.eventAccId = elem.eventAccountId;
                    if(elem.timeslot == "Morning"){
                    	morScheduleIds.push({'eventAccountId' : elem.eventAccountId[0], 'EventAccountName':elem.eventAccountName[0]});
                    	morSchedules.push(elem);
                    }else if(elem.timeslot == "Afternoon"){
                    	afterScheduleIds.push({'eventAccountId' : elem.eventAccountId[0], 'EventAccountName':elem.eventAccountName[0]});
                    	afterSchedules.push(elem);
                    }else if(elem.timeslot == "Night"){
                    	nightScheduleIds.push({'eventAccountId' : elem.eventAccountId[0], 'EventAccountName':elem.eventAccountName[0]});
                    	nightSchedules.push(elem);
                    }else if(elem.timeslot == "Late Night"){
                    	ltNtScheduleIds.push({'eventAccountId' : elem.eventAccountId[0], 'EventAccountName':elem.eventAccountName[0]});
                    	ltNtSchedules.push(elem);
                    }
                    if(z == schedules.length -1){
                    	var style = "<style>td{min-width:85px;};.table-bordered{border: 1px solid #ddd;};.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {border: 1px solid #ddd;}.table {margin-bottom: 20px;background-color:transparent;width:100%;}.m-b-5{margin-bottom:5px;}.p-5{padding:5px;}.unbreakable{display:inline-block;}.unbreakable:after{display:block;height:0px; visibility: hidden;}</style>";	                
		                var htmldata = "";
		                allschedules.push(morSchedules, afterSchedules, nightSchedules, ltNtSchedules);
						allscheduleIds.push(morScheduleIds, afterScheduleIds, nightScheduleIds, ltNtScheduleIds);
						timeslotToWrite.push("Morning Shifts", "Afternoon Shifts", "Night Shifts", "Late Night Shifts");
		                exports.scheduleReportSlotWise(sevenWeekDayArr, htmldata, allschedules, allscheduleIds, timeslotToWrite, 0, function(response){
		                	var htmldata = response;
		                	htmldata = htmldata +  style;
			        		htmlfinaldata = "<html><head><style>body{padding:10px;}table{font-size:10px;}</style></head><body>" + htmldata + "</body></html>";
			        		var LOG_FILE_NAME = path.join(__dirname + '/../uploads/test.html');
									fs.appendFile(LOG_FILE_NAME, htmlfinaldata, function (error) {
							            if (error) 
							                console.log(error);
							       	});
			                conversion({html: htmlfinaldata}, function(err, pdf) {
							    var output = fs.createWriteStream(__dirname + "/../uploads/schReport.pdf");
							    pdf.stream.pipe(output);
							    res.status(200).jsonp({"message": "success"});
							});
		                });
                    }
                    z++;
                });
	    	}
	    });
}

exports.saveschNote = function(req, res){
	var schid = req.body.scheduleId;
	var schNote = req.body.schNote;
	Schedule.findOneAndUpdate({_id:schid},{schnotes:schNote}).populate("event_account_id employee_id").exec(function(error, scheduleinfo){
		res.send({"status_code": "200", "data": scheduleinfo});
	});
}

exports.checkonTradestatus = function(req, res){
	var schId = req.body.scheduleId;
	if(schId){
		var condition = {scheduleId : schId, $or:[{status:{$exists:false}},{status:'Waiting'}]};
	}else{
		var condition = {shiftId : req.body.shiftId, eventId : req.body.eventId, timeslot:req.body.timeslot, $or:[{status:{$exists:false}},{status:'Waiting'}]};
	}
	Trade.findOne(condition).exec(function(error, tradeExistInWaiting){
		if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
		}else{
			res.status(200).jsonp({"data": tradeExistInWaiting});
		}
	});
}

exports.takeFromTrade = function(req, res){
	var tradeId = req.body.tradeId;
	Trade.remove({_id:tradeId}).exec(function(error,removeResponse){
		if (null!==removeResponse) {
			res.send({"status_code": "200", "data": removeResponse});
		}else{
			res.send({"status_code": "404"});
		}
	});
}

exports.getTradeNotifications = function(id, callback) {
    Trade.count({location:id.location, coWorkerUserId : id.user_id, status : null}).exec(function(err, total){
	    if (err) {
            callback({
                "message": "error",
                "data": err,
                "status_code": "500"
            });
        } else {
            callback({
                "message": "success",
                "data": total,
                "status_code": "200"
            });
        }
	});
}

exports.getAdminTradeNotifications = function(id, callback) {
   Trade.count({location:id.location, coWorkerUserId:{$ne:null}, status:'Waiting'}).exec(function(err, total) {
	    if (err) {
            callback({
                "message": "error",
                "data": err,
                "status_code": "500"
            });
        } else {
            callback({
                "message": "success",
                "data": total,
                "status_code": "200"
            });
        }
	});
}

exports.checkoverlappings = function(req, res){
    var query = {scheduleDate : new Date(req.body.schDte), employee_id:req.body.userId};
	Schedule.find(query).populate('event_account_id').exec(function(err, schedules){
		if (!err) {
          res.send({
            "message": "success",
            "data": schedules,
            "status_code": "200"
          });
        } else {
          res.send({
            "message": "Error",
            "data": err,
            "status_code": "400"
          });
        }
	});
}

exports.markOverlapReport = function(req, res){
    var query = {scheduleDate : new Date(req.body.scheduleDate), employee_id:req.body.employee_id};
    Schedule.update(query,{$set : {checkInOverlap:req.body.checkOverlapVal}}, {multi:true}).populate("event_account_id employee_id").exec(function(error, scheduleinfo){
		if (!error) {
          res.send({
            "message": "success",
            "data": scheduleinfo,
            "status_code": "200"
          });
        } else {
          res.send({
            "message": "Error",
            "data": error,
            "status_code": "400"
          });
        }
	});
}

exports.getAvailabilitylistEmpWise = function(req, res){
	  var to = new Date(req.body.to);
	  var searchEmployeeId = req.body.searchEmpId;
	  var from = new Date(req.body.from);
	  var page = req.body.page || 1,
      count = req.body.count || 49;
      var skipNo = (page - 1) * count;
	  var query = {};
	  if(searchEmployeeId){
          query.employee_id = mongoose.Types.ObjectId(searchEmployeeId);
      }
	  query.currDate = {
	    $gte: from,
	    $lte: to
	  }
	  Availability.aggregate([{$match: query },
	    {
	        $lookup: {
	          from: "users",
	          localField: "employee_id",
	          foreignField: "_id",
	          as: "userInfo"
	        }
	  	},
	  	{
		    $unwind : "$userInfo"
		},
	  	{$match:{"userInfo.location" : mongoose.Types.ObjectId(req.body.location), "userInfo.active" : true, "userInfo.is_deleted" : false}}]).exec(function(err, totalReport) {
			if (err) {
	            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
	        }else{
	        	  Availability.aggregate([
				  	{$match: query },
				    {
				        $lookup: {
				          from: "users",
				          localField: "employee_id",
				          foreignField: "_id",
				          as: "userInfo"
				        }
				  	},
				  	{
					    $unwind : "$userInfo"
					},
				  	{$match:{"userInfo.location" : mongoose.Types.ObjectId(req.body.location), "userInfo.active" : true, "userInfo.is_deleted" : false}},
				  	{$sort: {"userInfo.last_name" : 1}},
				  	//{$skip: skipNo},
	      			//{$limit: count},
				  	{$project:{
				        _id:1,
				        currDate:1,
				        employee_id:1,
				        is_morning_scheduled:1,
				        is_afternoon_scheduled:1,
				        is_night_scheduled:1,
				        is_late_night_scheduled:1,
				        is_morning_scheduled_type:1,
				        is_afternoon_scheduled_type:1,
				        is_night_scheduled_type:1,
				        is_late_night_scheduled_type:1,
				        is_morning_scheduled_force:1,
				        is_afternoon_scheduled_force:1,
				        is_night_scheduled_force:1,
				        is_late_night_scheduled_force:1,
				        empFirstName:"$userInfo.first_name",
				        empLastName:"$userInfo.last_name",
				        empZipcode:"$userInfo.address.zip",
				        empActive:"$userInfo.active",
				        empDeleted:"$userInfo.is_deleted",
				        employeeid:"$userInfo.employeeid",
				        zipcode:"$userInfo.address.zip"
				  	}}
				  ]).exec(function(err, schedules){
				  	var result = _.chain(schedules)
				            .groupBy("employee_id")
				            .pairs()
				            .map(function(currentItem) {
				                return _.object(_.zip(["empId", "result"], currentItem));
				            })
				            .value();
					if (err) {
				        res.status(500).jsonp({"message": "Something went wrong!", "err": err});
				    }else{
						res.status(200).jsonp({"Availabilitydata": result, "totalReports" : totalReport.length});
					}
				 });
			}
	  });
}

exports.getAvailabilityHaveNotMarked = function(req, res){
	var to = new Date(req.body.to);
	var searchEmployeeId = req.body.searchEmpId;
	var from = new Date(req.body.from);
	var query = {roles: {$in:["employee","manager","hr"]}, location : req.body.location, active:true, $or:[{is_deleted:{$exists:false}},{is_deleted:false}]};
    if(searchEmployeeId){
        query._id = mongoose.Types.ObjectId(searchEmployeeId);
    }
	User.find(query, {email:1, phone:1, notificationalert:1, location : 1}, function(err, users){
		if (err){
       	 	console.log("No user is found.");
	    }
	    else{
	    	var empids = [];
	    	var empNeedReminder = _.chain(users)
                .map(function(currentItem) {
                    empids.push(currentItem._id.toString());
                })
                .value();
            var empID = empids; 
            var firstand = {$and:[{'is_morning_scheduled_type': {$exists:true}}, {'is_morning_scheduled_type': {$ne:''}}]};
            var secondand = {$and:[{'is_afternoon_scheduled_type': {$exists:true}}, {'is_afternoon_scheduled_type': {$ne:''}}]};
            var thirdand = {$and:[{'is_night_scheduled_type': {$exists:true}}, {'is_night_scheduled_type': {$ne:''}}]};
            var fourthand = {$and:[{'is_late_night_scheduled_type': {$exists:true}}, {'is_late_night_scheduled_type': {$ne:''}}]};

		    Availability.aggregate([
		        {$match: {
		                 employee_id: {
		                    $in: empID.map(
		                        function(id) {
		                            return mongoose.Types.ObjectId(id);
		                        })
		                },
		                currDate: {
		                    $lte: to,
		                    $gte: from
		                },
		                $or : [firstand,secondand,thirdand,fourthand]
		            }
		        },
		        {$group: {
		                _id: "$employee_id",
		                count: {
		                    $sum: 1
		                }
		            }
		        }
		    ]).exec(function(err, response) {
	            if (response !== undefined && response.length > 0) {
	                var x = 0;
	                for (var i = 0; i < response.length; i++) {
	                    if (response[i].count >= 1) {
	                        var index = empID.indexOf(response[i]._id.toString());
	                        if (index > -1) {
	                            empID.splice(index, 1);
	                        }
	                    }
	                    if (x === response.length - 1) {
	                        User.find({
	                            _id: {
	                                $in: empID
	                            }
	                        }, function(err, users) {
	                        	var result = _.chain(users)
					            .groupBy("_id")
					            .pairs()
					            .map(function(currentItem) {
					                return _.object(_.zip(["empId", "result"], currentItem));
					            })
					            .value();
					            res.status(200).jsonp({"data": result, "totalReports" : users.length});
	                        });
	                    }
	                    x++;
	                }
	            } else {
	                User.find({
	                    _id: {
	                        $in: empID
	                    }
	                }, function(err, users) {
	                    var result = _.chain(users)
			            .groupBy("_id")
			            .pairs()
			            .map(function(currentItem) {
			                return _.object(_.zip(["empId", "result"], currentItem));
			            })
			            .value();
					    res.status(200).jsonp({"data": result, "totalReports" : users.length});
	                });
	            }
	        });
	    }
	});
}

exports.getSchedulelistEmpWise = function(req, res){
      var to = new Date(req.body.to);
      var from = new Date(req.body.from);
      var searchEmployeeId = req.body.searchEmpId;
      var query = {"userInfo.location" : mongoose.Types.ObjectId(req.body.location), "userInfo.active" : true, "userInfo.is_deleted" : false, "eventInfo.is_active" : true};
	  if(searchEmployeeId){
            query.employee_id = mongoose.Types.ObjectId(searchEmployeeId);
      }
      query.scheduleDate = {
        $gte: from,
        $lte: to
      }
      Schedule.aggregate([
        {
	        $lookup: {
	          from: "users",
	          localField: "employee_id",
	          foreignField: "_id",
	          as: "userInfo"
	        }
      	},
      	{
	        $lookup: {
	          from: "events",
	          localField: "event_account_id",
	          foreignField: "_id",
	          as: "eventInfo"
	        }
      	},
      	{ $match: query },
      	{$project:{
	        _id:1,
	        scheduleDate:1,
	        day_of_week:1,
	        timeslot:1,
	        employee_id:1,
	        shiftEndTime : 1,
	        shiftReminderTime : 1,
	        empFirstName:"$userInfo.first_name",
	        empLastName:"$userInfo.last_name",
	        employeeid:"$userInfo.employeeid",
	        event_account_id :"$eventInfo.name"
      	}}
	  ]).exec(function(err, schedules){
	  	var result = _.chain(schedules)
                .groupBy("employee_id")
                .pairs()
                .map(function(currentItem) {
                    return _.object(_.zip(["empId", "result"], currentItem));
                })
                .value();
		if (err) {
            res.status(500).jsonp({"message": "Something went wrong!", "err": err});
        }else{
			res.status(200).jsonp({"scheduledata": result});
		}
	  });
}

exports.checkDateShift = function(req, res){
	Schedule.findOne(req.body).exec(function(error, scheduleinfo){
		if(error){
			res.status(500).jsonp({"message": "Something went wrong!", "err": error});
		}else{
			res.status(200).jsonp({"data": scheduleinfo});
		}
	});
}

exports.checkScreenLock = function(req, res){
	var query = {};
	query.schedulelocked = true;
	query.location = req.body.location;
	User.findOne(query, {first_name : 1, last_name : 1, email:1, phone:1, location : 1}, function(err, userDetail){
		if(err){
			res.status(500).jsonp({"message": "Something went wrong!", "err": err});
		}else{
			res.status(200).jsonp({"data": userDetail});
		}
	});
}

exports.screenLock = function(req, res){
	var userid = req.body.userId;
	User.findOneAndUpdate({_id:userid},{schedulelocked:true}, {new:true}).exec(function(error, userinfo){
		res.send({"status_code": "200", "data": userinfo});
	});
}

exports.screenUnLock = function(req, res){
	var userid = req.body.userId;
	User.findOneAndUpdate({schedulelocked:true},{schedulelocked:false, scheduleunlockedBy:userid}, {new:true}).exec(function(error, userinfo){
		res.send({"status_code": "200", "data": userinfo});
	});
}