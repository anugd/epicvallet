var Whiteboard = require("mongoose").model("Whiteboard");
var Whiteboardtopic = require("mongoose").model("Whiteboardtopic");
var mongoose = require('mongoose');

exports.addPostInWhiteBoard = function(req, res, socket) {
    var postInfo = {};
    postInfo.topicname = req.body.topicname;
    postInfo.location = req.body.location;
    postInfo.created_date = req.body.created_date;
    postInfo.modified_date = req.body.created_date;
    
    Whiteboard.findOne({'topicname':postInfo.topicname}).exec(function(error, postdata){
      if(postdata){
        postdata.whiteboard_items.push({description:req.body.description, created_date : req.body.created_date,created_by:req.body.created_by});
        postdata.modified_date = req.body.created_date;
        postdata.save(function(err,response){
          if (err) {
            res.status("500").json({"message": "Something went wrong!", "err": err});
          } else {
            res.status("200").json({"message": "success"});
            socket.emit('sendWhiteBoardPostNotification', 'NewNotification');
          }
        });
      }else{
        postInfo.whiteboard_items = [{description:req.body.description, created_date : req.body.created_date,created_by:req.body.created_by}];
        Whiteboard(postInfo).save(function(err,response){
          if (err) {
            res.status("500").json({"message": "Something went wrong!", "err": err});
          } else {
            res.status("200").json({"message": "success"});
            socket.emit('sendWhiteBoardPostNotification', 'NewNotification');
          }
        });
      }
    });   
}
exports.whiteboardListing = function(req, res) {
    var page = req.body.page || 1,count = req.body.count || 10;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var query = {'is_deleted':false, 'location' : mongoose.Types.ObjectId(req.body.location), "whiteboard_items": { $not: { $size: 0 } }};
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    if (search) {
      query['$or'] = [];
      query['$or'].push({
        'topicinfo.topicname': new RegExp(search, 'i')
      })
    }
  Whiteboard.aggregate([{
      $lookup: {
        from: "whiteboardtopics",
        localField: "topicname",
        foreignField: "_id",
        as: "topicinfo"
      }
    }, 
    {
      $match: query
    }]).exec(function(err, totalPosts) {
    if (err) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      Whiteboard.aggregate([{
        $lookup: {
          from: "whiteboardtopics",
          localField: "topicname",
          foreignField: "_id",
          as: "topicinfo"
        }
      }, 
      {
        $match: query
      },
      {$unwind:"$whiteboard_items"},
      {
        $lookup: {
          from: "users",
          localField: "whiteboard_items.created_by",
          foreignField: "_id",
          as: "createdbyInfo"
        }
      },
      {$project:{
        _id:1,
        topicname:1,
        topicfullname:"$topicinfo.topicname",
        created_date:1,
        modified_date:1,
        is_deleted:1,
        location : 1,
        whiteboard_items:{
          description:1,
          created_date:1,
          _id:1,
          created_by_fname:"$createdbyInfo.first_name",
          created_by_lname:"$createdbyInfo.last_name",
        }
      }},
      {$group: {
          _id:"$_id",
          topicname:{$push:"$topicname"},
          created_date:{$push:"$created_date"},
          modified_date:{$push:"$modified_date"},
          is_deleted:{$push:"$is_deleted"},
          topicfullname:{$push:"$topicfullname"},
          whiteboard_items:{$push:"$whiteboard_items"}
      }},{
        $sort: sortObject
      },{
        $skip: skipNo
      }, {
        $limit: count
      }]).exec(function(err, posts) {
        if (!err) {
          res.send({
            "message": "success",
            "data": posts,
            "total": totalPosts.length,
            "status_code": "200"
          });
        } else {
          res.send({
            "message": "success",
            "data": err,
            "status_code": "200"
          });
        }
      });
    }
  });
}

exports.addTopic = function(req, res){
    var topicData = req.body;
    Whiteboardtopic.count({'topicname':topicData.topicname}).exec(function(error,count){
      if(count){
          res.send({"message": "This topic is already exists.", "data": count, "status_code": "400"});
      }else{
        Whiteboardtopic(topicData).save(function(err,response){
            if (!err)
                res.send({"message": "success", "data": response, "status_code": "200"});
            else
                res.send({"message": "error", "data": err, "status_code": "500"});
        });
      }
    });
};

exports.WhiteboardtopicList = function(req, res){
  Whiteboardtopic.find({'is_deleted':false, location : req.body.location}).sort({"created_date":-1}).exec(function(err,response){
      if (err) {
        res.status("500").json({"message": "Something went wrong!", "err": err});
      } else {
        res.status("200").json({"message": "success", "topiclist":response});
      }
  });
}

exports.clearWhiteboard = function(req, res){
  Whiteboard.remove({}).exec(function(err,response){
      if (err) {
        res.status("500").json({"message": "Something went wrong!", "err": err});
      } else {
        res.status("200").json({"message": "success"});
      }
  });
}

exports.clearWhiteboardByTopic = function(req, res){
  if(req.body.topictype == "withouttopic"){
    Whiteboard.remove({'topicname':req.body.topicId}).exec(function(err,response){
        if (err) {
          res.status("500").json({"message": "Something went wrong!", "err": err});
        } else {
          res.status("200").json({"message": "success"});
        }
    });
  }
  if(req.body.topictype == "withtopic"){
    Whiteboard.remove({'topicname':req.body.topicId}).exec(function(err,response){
        if (err) {
          res.status("500").json({"message": "Something went wrong!", "err": err});
        } else {
          Whiteboardtopic.remove({'_id':req.body.topicId}).exec(function(error, newresponse){
              if (err) {
                res.status("500").json({"message": "Something went wrong!", "err": error});
              } else {
                
                res.status("200").json({"message": "success"});
              }
          });
        }
    });
  }
}

exports.clearWhiteboardbyIds = function(req, res){
  Whiteboard.remove({'_id':{$in:req.body.removeIds}}).exec(function(err,response){
      if (err) {
        res.status("500").json({"message": "Something went wrong!", "err": err});
      } else {
        res.status("200").json({"message": "success"});
      }
  });
}

exports.clearWhiteboardItem = function(req, res){
  Whiteboard.update({_id: req.body.mainId},{$pull:{whiteboard_items:{_id:req.body.itemId}}}, {multi:true}).exec(function(err, response) {
      if (err) {
        res.status("500").json({"message": "Something went wrong!", "err": err});
      } else {
        res.status("200").json({"message": "success"});
      }
  });
}
