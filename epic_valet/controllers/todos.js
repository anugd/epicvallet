var Todo = require("mongoose").model("Todo");
var mongoose = require('mongoose');
var nodemailer = require("nodemailer");
var path = require('path');
var root = process.cwd();
var constantObj = require(path.resolve(root, 'constants.js'));
exports.saveTodoList = function(req,res){
    if(req.body.listId){
      var query = {_id:req.body.listId};
      Todo.findOneAndUpdate(query,req.body).populate('assign_to.user_id').exec(function(err,response){
       if (!err) {
        Todo.findOne(query).populate('assign_to.user_id').exec(function(error, resp) {
          var emailid = resp.assign_to[0].user_id.email;
          var mailOptions = {
              from: constantObj.emailSettings.fromWords, // sender address
              to: emailid, // list of receivers
              subject: "New To Do List Assignment", // Subject line
              html: "Hello <p style='padding-left:10px'> You are aligned for a new task list.<br> Please login to check with your tasks assignments.</p>"+ constantObj.emailSettings.signature
            };
            constantObj.transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                console.log("email error", error);
              } else {
                console.log('Message sent: ' + info.response);
              }
            });
          res.json({"message": "success", "status_code": "200"});
        });
       }else{
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
       }

      });
    }else{
      Todo(req.body).save(function(err,response){
       if (!err) {
        res.json({"message": "success", "status_code": "200"});
       }else{
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
       }
      });    
    }
};

exports.employeeToDoForThisDate = function(req, res){
  var empId = req.body.employeeid;
  var due_date = req.body.due_date;
  var query = {'is_deleted':false, 'assign_to.user_id': empId, 'todo_items.due_date':due_date};
  Todo.find(query).exec(function(err, todolists) {
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } else {
        res.send({"message": "success", "data": todolists,"status_code": "200"});
      }
  })  
};

exports.getTodoList = function(req, res){
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var role = req.body.role;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var query = {};
    if(role == "employee"){
      var query = {'assign_to.user_id': req.body.owner_id};  
    }
    query.is_deleted = false;
    query.location = req.body.location;
    if (search) {
      query['$or'] = [];
      query['$or'].push({listname: new RegExp(search,'i')});
    }
    Todo.count(query).exec(function(err, total) {
        if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        } 
        else {
          Todo.find(query).sort(sortObject).skip(skipNo).limit(count).populate('todo_items.assign_by created_by').exec(function(err, todolists) {
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
            res.send({"message": "success", "data": todolists,"total": total, "status_code": "200"});
            }
          })
        }
    });
};

exports.dashboardtodolist = function(req, res){
    var query = {};
    var query = {'assign_to.user_id': req.body.owner_id, 'todo_items.done' : false};  
    query.is_deleted = false;
    query.location = req.body.location;
    Todo.find(query).sort({"created_date" : -1}).skip(0).limit(10).populate('created_by').exec(function(err, todolists) {
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } else {
        res.send({"message": "success", "data": todolists,"total": todolists.length, "status_code": "200"});
      }
    })  
}

exports.addItemsInList = function(req, res, socket){
  Todo.findOne({_id: req.body.todolistId}).populate('assign_to.user_id').exec(function(err, response) {
      var itemData = req.body.todo_items;
      response.todo_items.push(itemData);
      response.save(function(error,newresponse){
        if(response.assign_to.length){
          var emailid = response.assign_to[0].user_id.email;
          var mailOptions = {
              from: constantObj.emailSettings.fromWords, // sender address
              to: emailid, // list of receivers
              subject: "To Do List ("+response.listname+") Assignment", // Subject line
              html: "Hello <p style='padding-left:10px'> "+ "To Do assigned : "+itemData.todoText+"<br>Due Date : "+new Date(itemData.due_date).toLocaleDateString()+"<br> <br>Please login to check with new items in your task list.</p>" + constantObj.emailSettings.signature
            };
            constantObj.transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                console.log("email error", error);
              } else {
                console.log('Message sent: ' + info.response);
              }
            });
        }
        res.send({"message": "success", "status_code": "200"});   
        if(response.assign_to[0]){
          socket.emit('todoNotification', response.assign_to[0].user_id._id);
        }
      }); 
  });
}

exports.removeAllItemsInList = function(req, res, socket){
  var update = {'todo_items':[]};
  Todo.findOneAndUpdate({_id: req.body.todolistId},update).exec(function(err,response){
      if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
      } else{
        if(response.assign_to.length){
          var emailid = response.assign_to[0].user_id.email;
          var mailOptions = {
              from: constantObj.emailSettings.fromWords, // sender address
              to: emailid, // list of receivers
              subject: "To Do List ("+response.listname+") Update", 
              html: "Hello <p style='padding-left:10px'> All the items from ("+response.listname+") has been removed. <br> Please login to check with new updates in your task list.</p>"+ constantObj.emailSettings.signature
            };
            constantObj.transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                console.log("email error", error);
              } else {
                console.log('Message sent: ' + info.response);
              }
            });
        }
        res.send({"message": "success", "status_code": "200"}); 
        if(response.assign_to.length){
          socket.emit('todoNotification', response.assign_to[0].user_id);
        }
      }
  });
}

exports.removeItemsInList = function(req, res, socket){
  Todo.findOneAndUpdate({_id: req.body.todolistId},{$pull:{todo_items:{_id:{$in:req.body.todoSelectedIds}}}}, {multi:true}).exec(function(err, response) {
    if (err) {
        res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
    } else{
        res.send({"message": "success", "status_code": "200"}); 
        if(response.assign_to.length){
          socket.emit('todoNotification', response.assign_to[0].user_id);
        }
    }
  });
}

//delete User
exports.deletetoDoList=function(req,res){
        var query = {};
        var fields = {};
        query._id = {
            $in: req.body.todoid
        };
        fields.is_deleted = true;
        Todo.update(query, fields, {
          multi: true
        }).exec(function(error, users) {
          if (error) {
            res.send({
              "message": "Something went wrong!",
              "err": err,
              "status_code": "500"
            });
          }
          Todo.find({'is_deleted' : false}, function(err, todolist) {
              if (err) {
                res.send({
                  "message": "Something went wrong!",
                  "err": err,
                  "status_code": "500"
                });
              } else {
                res.send({
                  "message": "success",
                  "data": todolist,
                  "status_code": "200"
                });
              }
            })
        })
}

exports.completeTaskInList = function(req, res){
    var taskListId = req.body.todolistId;
    var taskDetails = req.body.todo_item;
    var completion_date = req.body.completion_date;
    var query = {_id:taskListId, 'todo_items._id': taskDetails._id};
    if(taskDetails.done){
      var setvalues = { "todo_items.$.completion_date" : completion_date, "todo_items.$.done" : true};
    }else{
      var setvalues = { "todo_items.$.completion_date" : "", "todo_items.$.done" : false};
    }
    var querynested = {_id:taskListId};
    Todo.update(query,{ $set: setvalues}).exec(function(err,response){
     if (!err) {
      Todo.findOne(querynested).populate('created_by todo_items.assign_by').exec(function(error, resp) {
        var emailid = resp.created_by.email;
        var mailOptions = {
            from: constantObj.emailSettings.fromWords, // sender address
            to: emailid, // list of receivers
            subject: "To Do List ("+resp.listname+") Task Completion Notification", // Subject line
            html: "Hello <p style='padding-left:10px'>There is some updates in this list by "+req.body.completeBy+", please check.</p>" + constantObj.emailSettings.signature
          };
          constantObj.transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
              console.log("email error", error);
            } else {
              console.log('Message sent: ' + info.response);
            }
          });
        res.json({"message": "success", "status_code": "200", "data" : resp});
      });
     }else{
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
     }
    });
}

exports.todoListForCalendar = function(req, res){
  var role = req.body.role;
  var query = {};
  if(role == "manager" || role == "admin" || role == "hr"){
    var query = {'is_deleted':false, 'created_by': req.body.owner_id};  
  }
  if(role == "employee"){
    var query = {'is_deleted':false, 'assign_to.user_id': req.body.owner_id};   
  }
  var sortObject = {};
  var stype = 'created_date';
  var sdir = -1;
  sortObject[stype] = sdir;
  Todo.find(query).sort(sortObject).populate('assign_to.user_id').exec(function(err, todolists) {
      if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } else {
        res.send({"message": "success", "data": todolists,"status_code": "200"});
      }
  })
}

exports.getnotificationicon = function(data, callback) {
    var userId = data.user_id;
    var query = {'location' : mongoose.Types.ObjectId(data.location), 'is_deleted':false, 'assign_to.user_id': mongoose.Types.ObjectId(userId), 'todo_items.completion_date': { $exists: false }};  
    Todo.aggregate({$unwind:"$todo_items"}, {$match:query}).exec(function(err,response){
        if (err)
            callback({"message": "error", "data": err, "status_code": "500"});
        else
            callback({"message": "success", "data": response.length, "status_code": "200"});
    });  
};