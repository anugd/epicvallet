var Document = require("mongoose").model("Document");
var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var formidable = require('formidable');
var zip = require('just-zip')

exports.createFolder = function(req, res) {
    var folderData = req.body;
    Document(folderData).save(function(err,response){
        if (!err)
            res.send({"message": "success", "data": response, "status_code": "200"});
        else
            res.send({"message": "error", "data": err, "status_code": "500"});
    });
};
exports.createLink = function(req, res) {
    var linkData = req.body;
    if(linkData.folderId){
        Document.update({_id: req.body.folderId},{$push:{links:{"link":linkData.link,"linktitle":linkData.linktitle}}}, function(err, response) {
            if (!err)
                res.send({"message": "success", "data": response, "status_code": "200"});
            else
                res.send({"message": "error", "data": err, "status_code": "500"});
        });
    }
};

exports.checkOnTopDocument = function(req, res){
    var query = {_id: req.body.doc_id};
    Document.update(query,{$set : {checkOnTop:req.body.checkOnTopVal}}, {multi:true}).exec(function(error, docinfo){
        if (!error) {
          res.send({
            "message": "success",
            "data": docinfo,
            "status_code": "200"
          });
        } else {
          res.send({
            "message": "Error",
            "data": error,
            "status_code": "400"
          });
        }
    });
}

exports.getFolder = function(req, res){
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var query = {'is_deleted' : false};
    if (search) {
        query['$or'] = [{foldername: new RegExp(search,'i')}, {'my_file.filename': new RegExp(search,'i')}];
    }
    Document.count(query).exec(function(error, totalfolders) {
        if (error) {
            res.send({"message": "Something went wrong!", "err": error, "status_code": "500"});
        }else if(!totalfolders){
            res.send({"message": "success", "data": [],"total": totalfolders, "status_code": "200"});
        }else{
            Document.find(query).sort(sortObject).skip(skipNo).limit(count).exec(function(err, folders) {
                if (err) {
                    res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
                }
                var x = 0;
                folders.forEach(function(elem, key){
                    var my_file_copy = [];
                    var links_copy = [];
                    if(elem.my_file.length){
                        elem.my_file.forEach(function(e, k){
                            if(!e.is_deleted){
                                my_file_copy.push(e);
                            }
                        });
                        elem.my_file = my_file_copy;
                    }

                    if(elem.links.length){
                        elem.links.forEach(function(el, ke){
                            if(!el.is_deleted){
                                links_copy.push(el);
                            }
                        });
                        elem.links = links_copy;
                    }

                    if(x === folders.length-1){
                        res.send({"message": "success", "data": folders,"total": totalfolders, "status_code": "200"});
                    }
                   x++;
                });
            }); 
        }
    });    
};




exports.docUploadInDirectory = function(req, res) {
    var form = new formidable.IncomingForm();
    mkdirp(__dirname + './../uploads/docs_attachment/', function (err) {
      if (err) { throw err;}
    });
    form.uploadDir = path.join(__dirname + './../uploads/docs_attachment/')
    form.parse(req, function(err, fields, files) {
    var fileNames = []
    console.log("Fields", files);
    for (i in files) {
    if (i.indexOf('file') != -1) {
    var dateTime = new Date().toISOString().replace(/T/, '').replace(/\..+/, '').split(" ");
    try{
        fs.renameSync(files[i].path, __dirname + "/../uploads/docs_attachment/" + dateTime + "___" +files[i].name);
    }catch (err) {
        console.log("Error in file renaming", err)
    }
    fileNames.push(dateTime + "___" +files[i].name)
    }
    }
    res.json({'msg':'success',data:fileNames})
    });
};

exports.saveDocument = function(req, res){
    if(req.body.folderid){
        var docData = req.body;
        Document.findOne({_id: req.body.folderid}, function(err, response) {
            var fileData = req.body.my_file;
            var y = 0;
            for (var i = 0; i < fileData.length; i++) {
                (function(i) {
                    var fileTempName = fileData[i].substr(fileData[i].lastIndexOf("___")+3);
                    response.my_file.push({'filename' : fileTempName.slice(0,-4), 'filepath' : fileData[i]});
                    docData.my_file = response.my_file;
                    if(y == fileData.length-1){
                        response.save(function(err,response){
                            if (err) {
                             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
                            } else {
                             res.send({"message": "success", "status_code": "200"});
                            }
                        }); 
                    }
                    y++;
                })(i);
            }
        });
    }else{
        var x = 0;
        var outputJson = {};
        var docData = req.body;
        var fileData = req.body.my_file;
        for (var i = 0; i < fileData.length; i++) {
            (function(i) {
                var fileTempName = fileData[i].substr(fileData[i].lastIndexOf("___")+3);
                docData.my_file = [{'filename' : fileTempName.slice(0,-4), 'filepath' : fileData[i]}];
                Document(docData).save(function(err,response){
                    if(err){
                        outputJson= {"message": "error", "data": err, "status_code": "500"};
                        res.send(outputJson);
                    }
                }); 
                if (x == fileData.length-1) {
                    outputJson={"message": "success", "status_code": "200"};
                    res.send(outputJson);
                }   
                x++;
            })(i);
        } 
    }
}
exports.getFolderById = function(req, res){
    Document.findOne({_id: req.params.folderid}, function(err, response) {
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
            res.send({"message": "success", "data": response, "status_code": "200"});
            }
    });
}
exports.docDownload = function(req, res) {
    var download_file = req.body.file;
    var fileArr = [];
    
    mkdirp(__dirname + './../uploads/zipFile/', function (err) {
      if (err) { throw err;}
    });

    download_file.forEach(function(val, index) {
        var temp = path.resolve(__dirname + "/../uploads/docs_attachment/" + val);
        fileArr.push(temp)
    })

    var customZipFilePath = path.resolve(__dirname + "/../uploads/zipFile/");
    zip(fileArr, customZipFilePath, function(err, data) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            res.send({
                "message": "success",
                "zipFilePath": data,
                "status_code": "200"
            });
        }
    })
};

exports.viewDoc = function(req,response){
    var fileId = req.params.fileId;
    Document.findOne({'my_file._id': fileId},{ "my_file.$": 1}, function(err, res) {
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
                console.log(res.my_file[0].filepath);
                var tempFile = path.resolve(__dirname + "/../uploads/docs_attachment/"+res.my_file[0].filepath);
                fs.readFile(tempFile, function (err,data){
                     response.contentType("application/pdf");
                     response.send(data);
                });
            }
    });             
};     

exports.renameFile = function(req,res){
    var fileId = req.body._id;
    var filename = req.body.filename;
    var recordId = req.body.recordId;
    Document.update({'my_file._id': fileId},{'$set': {
    'my_file.$.filename': filename
    }}, function(err, response) { 
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
             res.send({"message": "success", "status_code": "200"});
            }
        }
    ); 
}   

exports.renameLink = function(req,res){
    var linkId = req.body._id;
    var name = req.body.link;
    var linktitle = req.body.linktitle;
    var recordId = req.body.recordId;
    Document.update({'links._id': linkId},{'$set': {
    'links.$.link': name, 'links.$.linktitle': linktitle
    }}, function(err, response) { 
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
             res.send({"message": "success", "status_code": "200"});
            }
        }
    ); 
}   

exports.renameFolder = function(req,res){
    var folderId = req.body._id;
    var foldername = req.body.foldername;
    Document.update({'_id': folderId},{'$set': {
    'foldername': foldername
    }}, function(err, response) { 
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
             res.send({"message": "success", "status_code": "200"});
            }
        }
    ); 
}  

exports.removeFile = function(req,res){
    var fileId = req.body._id;
    var filename = req.body.filename;
    var recordId = req.body.recordId;
    var location = req.body.fileloc;
    if(location == 'outFolder'){
        var setfields = {'my_file.$.is_deleted': true, 'is_deleted' : true};
    }
    if(location == 'inFolder'){
        var setfields = {'my_file.$.is_deleted': true};
    }
    Document.update({'my_file._id': fileId},{'$set': setfields}, function(err, response) { 
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
             res.send({"message": "success", "status_code": "200"});
            }
        }
    ); 
}  

exports.removeFolder = function(req,res){
    var folderId = req.body._id;
    var foldername = req.body.foldername;
    Document.update({'_id': folderId},{'$set': {
    'is_deleted': true
    }}, function(err, response) { 
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
             res.send({"message": "success", "status_code": "200"});
            }
        }
    ); 
}

exports.removeLink = function(req,res){
    var linkId = req.body._id;
    var recordId = req.body.recordId;
    var location = req.body.fileloc;
    if(location == 'inFolder'){
        var setfields = {'links.$.is_deleted': true};
    }
    Document.update({'links._id': linkId},{'$set': setfields}, function(err, response) { 
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
             res.send({"message": "success", "status_code": "200"});
            }
        }
    ); 
}




