var Memo = require("mongoose").model("Memo");
var Memogroup = require("mongoose").model("Memogroup");
var Scheduletext = require("mongoose").model("Scheduletext");
var fs = require('fs');
var mkdirp = require('mkdirp');
var mongoose = require('mongoose');
var path = require('path');
var formidable = require('formidable');
var zip = require('just-zip');
var root = process.cwd();
var constantObj = require(path.resolve(root, 'constants.js'));
exports.getMemos = function(req, res) {
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var search = req.body.search || "";
    if (req.body.field) {
        var sort = req.body.sortOrder;
        var sort_field =  req.body.field;
    }else{
        var sort = -1;
        var sort_field =  'created_date';
    }
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var skipNo = (page - 1) * count;
    var memoData  = req.body;
    if ( "adminInboxMsgs" === memoData.action )
        var query = {created_by: { $ne: memoData.userId}};
    else if ( "managerInboxMsgs" === memoData.action )
        var query = {sent_to: {$elemMatch:{user_id: memoData.userId}}};
    else if ( "empInboxMsgs" === memoData.action )
        var query = {sent_to: {$elemMatch:{user_id: memoData.userId}}};
    else if ( "getAllCreatedByMe" === memoData.action )
        var query = {created_by: memoData.userId};
    if (search) {
        query['$or'] = [];
        query['$or'].push({title: new RegExp(search,'i')})
    }
    Memo.count(query).exec(function(err, total) {
        if (err) {
            console.log(err);
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        } else {
            Memo.find(query).sort(sortObject).populate("sent_to.user_id").skip(skipNo).limit(count).exec(function(err, response) {
                if (!err &&  memoData.action == "getAllCreatedByMe" ){
                        for(i in response){
                            prev_time=response[i].created_date;
                            new_time = Date.now();
                            var diff = Math.abs(prev_time - new_time);
                            var minutes = Math.floor((diff/1000)/60);
                            response[i]['mins'] = minutes;
                            console.log('===========================');
                            console.log(response[i]);
                        }
                        setTimeout(function(){
                            console.log("=============================response sent back");
                            res.send({"message": "success", "data": response, "status_code": "200","total": total});
                        },10)
                }
                else if (!err)
                    res.send({"message": "success", "data": response, "status_code": "200","total": total});
                
                else
                    res.send({"message": "error", "data": err, "status_code": "500"});
            });
        }
    });  
};

exports.getSchTexts = function(req, res) {
    var page = req.body.page || 1,
    count = req.body.count || 50;
    var search = req.body.search || "";
    if (req.body.field) {
        var sort = req.body.sortOrder;
        var sort_field =  req.body.field;
    }else{
        var sort = -1;
        var sort_field =  'schDate';
    }
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var skipNo = (page - 1) * count;
    var memoData  = req.body;
    var query = {};
    if (search) {
        query['$or'] = [];
        query['$or'].push({title: new RegExp(search,'i')})
    }
    Scheduletext.count(query).exec(function(err, total) {
        if (err) {
            console.log(err);
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        } else {
            Scheduletext.find(query).sort(sortObject).skip(skipNo).limit(count).exec(function(err, response) {
                if (!err)
                    res.send({"message": "success", "data": response, "status_code": "200","total": total});
                else
                    res.send({"message": "error", "data": err, "status_code": "500"});
            });
        }
    });  
};

exports.createMemoGroup = function(req, res) {
    var memoData = req.body.groupdata;
    if(req.body._id){
        Memogroup.update({'_id': req.body._id},{'$set': memoData}, function(err, response) { 
                if (err) {
                 res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
                } else {
                 res.send({"message": "success", "status_code": "200"});
                }
        }); 
    }else{
        Memogroup(memoData).save(function(err,response){
            if (!err){            
                res.send({"message": "success", "data": response, "status_code": "200"});
            }
            else{
                res.send({"message": "error", "data": err, "status_code": "500"});
            }
        });   
    } 
};

exports.removeMemoGroup = function(req, res){
    var setfields = {'is_deleted': true};
    Memogroup.update({'_id': req.body.groupId},{'$set': setfields}, function(err, response) { 
            if (err) {
                res.status(500).json({"message": "Something went wrong!", "err": err});
            } else {
                res.status(200).json({"message": "success"});
            }
        }
    ); 
}

exports.createMemo = function(req,res){
    var memoData = req.body;
    Memo(memoData).save(function(err,response){
        if (!err){            
            exports.send_sms_reminder(req, res, 0, memoData.phoneNumberList, memoData.title,memoData.description);
            res.send({"message": "success", "data": response, "status_code": "200"});
        }
        else{
            res.send({"message": "error", "data": err, "status_code": "500"});
        }
    });  
}

exports.fetchGroups = function(req, res){
    var query = {'is_deleted':false};
    if(req.body.role == "manager"){
        query.created_by = req.body.owner_id;
    }
    Memogroup.find(query).exec(function(err,response){
        if (!err){            
            res.send({"message": "success", "data": response, "status_code": "200"});
        }
        else{
            res.send({"message": "error", "data": err, "status_code": "500"});
        }
    })
}

exports.send_sms_reminder = function(req, res, i, numberList,title, description) {
    if (numberList.length) {
        var client = require('twilio')(constantObj.twilio.accountSid, constantObj.twilio.authToken);
        client.messages.create({
            to: constantObj.twilio.countrycode+numberList[i],
            from: constantObj.twilio.twilio_number, //test number of client registered in twilio
            body: title+' : '+description+'\n\n'+ constantObj.twilio.twilio_signature
        }, function(err, message) {
            console.log(err);
            if (i + 1 == numberList.length) {
                console.log("Sent to all");
            } else {
                exports.send_sms_reminder(req, res, i + 1, numberList, title, description);
            }
        });
    } else {
        console.log("no data");
    }
}

exports.getMemo = function(req, res) {
    if (req.params.id) {
        Memo.findById(req.params.id, function(err, response){
            if (!err){ 
                res.send({"message": "success", "data": response, "status_code": "200"});
            }
            else{
                res.send({"message": "error", "data": err, "status_code": "500"});
            }
        }).populate("sent_to.user_id");
    }
    else{
        res.send({"message": "error", "data": "No Id requested", "status_code": "500"});
    }
};

exports.markMemo = function(req, res) {
    var memoData = req.body;
    if ( "admin" === memoData.user_role ) {
        Memo.findById(memoData.memoId, function(err, memo){
            if(!err){
                memo.admin_checked = true;
                memo.save(function(err, memoresult){
                    res.send({"message": "success", "data": memoresult, "status_code": "200"});
                });
            }
            else
                res.send({"message": "error", "data": err, "status_code": "500"});
        });
    }
    else {
        Memo.findById(memoData.memoId, function(err, memo){
            if (!err) {
                var continueloop = true;
                memo.sent_to.forEach(function(element, key){
                    if(continueloop){
                        if(JSON.stringify(element.user_id) === JSON.stringify(mongoose.Types.ObjectId(memoData.userId))) {
                            element.mark_read = true;
                            continueloop = false;
                            memo.save(function(err, memoresult){
                                    res.send({"message": "success", "data": memoresult, "status_code": "200"});
                            });
                        }
                    }
                });
            }
            else{
                res.send({"message": "error", "data": err, "status_code": "500"});
            }
        });
    }
};

//to delete memo
exports.delmemo=function(req, res,_id){
    var a= req.body.Id;
    Memo.findOne({_id:a},function(err,data){
        prev_time=data.created_date;
        new_time = Date.now();
        var diff = Math.abs(prev_time - new_time);
        var min = Math.floor((diff/1000)/60);
        if(min>15){
            res.json({"code":"500", "response":"time has exceeded 15 mins"})
        }else{
            Memo.findByIdAndRemove(a,function(err){
                if(err){
                    res.send('err');
                }
                else{
                    res.send('done');
                }
            });    
        }
    })        
};

//update Memo
exports.updmemo=function(req,res)
{
    var title1= req.body.title;
    var description1= req.body.description;
    Memo.update({_id: req.body._id},{$set:{title:title1,description:description1}},function(err,data){
        if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      }
        else {
        res.send({"message": "success", "data": data, "status_code": "200"});
      }
    });
};

exports.memoupload = function(req, res) {
    var form = new formidable.IncomingForm();
    mkdirp(__dirname + './../uploads/memos_attachment/', function (err) {
      if (err) { throw err;}
    });
    form.uploadDir = path.join(__dirname + './../uploads/memos_attachment/')
    form.parse(req, function(err, fields, files) {
    var fileNames = []
    console.log("Fields", files);
    for (i in files) {
    if (i.indexOf('file') != -1) {
    var dateTime = new Date().toISOString().replace(/T/, '').replace(/\..+/, '').split(" ");
    fs.renameSync(files[i].path, __dirname + "/../uploads/memos_attachment/" + dateTime + files[i].name)
    fileNames.push(dateTime + files[i].name)
    }
    }
    res.json({'msg':'success',data:fileNames})
    });
};

exports.memodownload = function(req, res) {
    var download_file = req.body.file;
    var fileArr = [];
    download_file.forEach(function(val, index) {
        var temp = path.resolve(__dirname + "/../uploads/memos_attachment/" + val);
        fileArr.push(temp)
    })
    var customZipFilePath = path.resolve(__dirname + "/../uploads/zipFile/");
    zip(fileArr, customZipFilePath, function(err, data) {
        if (err) {
            res.send({
                "message": "Something went wrong!",
                "err": err,
                "status_code": "500"
            });
        } else {
            res.send({
                "message": "success",
                "zipFilePath": data,
                "status_code": "200"
            });
        }
    })
};
