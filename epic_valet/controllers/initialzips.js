var Initalzip = require("mongoose").model("Initialzip");
var mongoose = require('mongoose');
exports.addInitialzip = function(req,res){
  Initalzip(req.body).save(function(err,response){
    if(!err) {
      res.json({"message": "success", "status_code": "200"});
    }else{
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    }
  });    
};

exports.getInitalziplist = function(req, res){
    var page = req.body.page || 1,
    count = req.body.count || 10;
    var role = req.body.role;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var query = {'is_deleted':false};
   /* if(role == "manager" || role == "admin"){
      var query = {'is_deleted':false, 'created_by': req.body.owner_id};  
    }*/
    if (search) {
    query['$or'] = [];
    query['$or'].push({initial: new RegExp(search,'i')});
    }

    Initalzip.count(query).exec(function(err, total) {
        if (err) {
            res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        } 
        else{
          Initalzip.find(query).sort(sortObject).skip(skipNo).limit(count).exec(function(err, Initalziplists) {
            if (err) {
             res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
            } else {
            res.send({"message": "success", "data": Initalziplists,"total": total, "status_code": "200"});
            }
          })
        }
    });
};

exports.getAllInitalzips = function(req, res){
  Initalzip.find({'is_deleted':false, 'active':true}, {is_deleted : 1, active : 1, initial : 1, zipcodes : 1 }).exec(function(err,Initalzips){
    if (err) {
      res.status(500).jsonp({"message": "Something went wrong!", "err": err});
    } else {
      res.status(200).jsonp({"message": "success", "data": Initalzips});
    }
  });
}

exports.update_Initalzip_status = function(req, res) {
  var update = {};
  var data = req.body.data
  update.active = (data.active) ? false : true;
  Initalzip.findOneAndUpdate({
    _id: data._id
  }, update, {new:true}).exec(function(error, Initalzips) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": Initalzips,
        "status_code": "200"
      });
    }
  })
}

exports.editInitalzip = function(req, res) {
  var update = {};
  var data = req.body;
  update.initial = data.initial;
  update.zipcodes = data.zipcodes;
  Initalzip.update({
    _id: data.zipId
  }, update).exec(function(error, Initalzips) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": Initalzips,
        "status_code": "200"
      });
    }
  })
}

/*enable/disable Initalzips*/
exports.enable_disable_Initalzips = function(req, res) {
  var enabled = req.body.enabled;
  var selAll = req.body.allChecked;
  var query = {};
  var fields = {};
  query._id = {
      $in: req.body.Initalzips
  };
  if (enabled == true) {
      fields.active = true;
  } else {
      fields.active = false;
  }
  Initalzip.update(query, fields, {
    multi: true
  }).exec(function(error, Initalzips) {
    if (error) {
      return res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    }
    Initalzip.find({is_deleted:false}, function(err, Initalzips) {
        if (error) {
          res.send({
            "message": "Something went wrong!",
            "err": err,
            "status_code": "500"
          });
        } else {
          res.send({
            "message": "success",
            "data": Initalzips,
            "status_code": "200"
          });
        }
      })
  })
}



exports.deleteZipList=function(req,res){
    var query = {};
    var fields = {};
    query._id = {
        $in: req.body.zipid
    };
    fields.is_deleted = true;
    Initalzip.update(query, fields, {
      multi: true
    }).exec(function(error, Initalzips) {
      if (error) {
        return res.send({
          "message": "Something went wrong!",
          "err": err,
          "status_code": "500"
        });
      }
      Initalzip.find({}, function(err, Initalzips) {
          if (err) {
            res.send({
              "message": "Something went wrong!",
              "err": err,
              "status_code": "500"
            });
          } else {
            res.send({
              "message": "success",
              "data": Initalzips,
              "status_code": "200"
            });
          }
        })
    })
}
exports.IdBasedInitalzip = function(req, res){
  Initalzip.find({'is_deleted':false, 'active':true, '_id':{$in:req.body.InitalzipIds}}).exec(function(err,Initalzips){
    if (err) {
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      res.send({"message": "success", "data": Initalzips,"status_code": "200"});
    }
  });
}


