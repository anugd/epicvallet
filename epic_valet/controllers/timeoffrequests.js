var Timeoffrequest = require("mongoose").model("Timeoffrequest");
var mongoose = require('mongoose');
var nodemailer = require("nodemailer");
var path = require('path');
var root = process.cwd();
var constantObj = require(path.resolve(root, 'constants.js'));
exports.savetimeoff = function(req,res, socket){
    Timeoffrequest(req.body).save(function(err,response){
       if (!err) {
        res.json({"message": "success", "status_code": "200"});
        socket.emit('updatetimeOffNotification');
       }
    });
};

exports.gettimeoff = function(req, res) {
    var empID = mongoose.Types.ObjectId(req.body.userId);
    var page = req.body.page || 1,count = req.body.count || 50;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var condition = {'employee.location' : mongoose.Types.ObjectId(req.body.location)};
    if(req.body.role == "employee" || req.body.myrole == "personaltimeoff") {
        var condition = {employee_id: empID, 'employee.location' : mongoose.Types.ObjectId(req.body.location)};
    }
    if(req.params.timeoffid){
        var condition = {_id: mongoose.Types.ObjectId(req.params.timeoffid)};
        req.body.field = 'applyDate';
        req.body.sortOrder = -1;
    }

    if(req.body.searchByRole == "Approve"){
      condition.status = "Approve";
    }
    else if(req.body.searchByRole == "Reject"){
      condition.status = "Reject";
    }
    else if(req.body.searchByRole == "Waiting"){
      condition.status = null;
    }
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
  if (search) {
    condition['$or'] = [];
    condition['$or'].push({
      'employee.last_name': new RegExp(search, 'i')
    })
    condition['$or'].push({
      'employee.first_name': new RegExp(search, 'i')
    })
  }


  Timeoffrequest.aggregate([{
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "employee"
        }
      }, {
        $match: condition
      }]).exec(function(err, total) {
    if (err) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      Timeoffrequest.aggregate([{
         $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "employee"
        }
      },
      {
         $lookup: {
          from: "users",
          localField: "manager_id",
          foreignField: "_id",
          as: "updatedBy"
        }
      },
      {
        $match: condition
      }, {
        $sort: sortObject
      },{
        $skip: skipNo
      }, {
        $limit: count
    
    }]).exec(function(err, timeoff) {
console.log('...........',timeoff);
        if (!err) {
          res.send({
            "message": "success",
            "data": timeoff,
            "total": total.length,
            "status_code": "200"
          });
        } else {
          res.send({
            "message": "success",
            "data": err,
            "total": total.length,
            "status_code": "200"
          });
        }
      });
    }
  });
};
exports.getEmployeesList = function(req, res) {
  var page = req.body.page || 1,
  count = req.body.count || 50;
  var skipNo = (page - 1) * count;
  var search = req.body.search || "";
  var query = {roles: {$in:["employee"]},is_deleted:false};
  if (search) {
    query['$or'] = [];
    query['$or'].push({first_name: new RegExp(search,'i')})
    query['$or'].push({last_name: new RegExp(search,'i')})
    query['$or'].push({phone: new RegExp(search,'i')})
    //query['$or'].push({employeeid: new RegExp(search,'i')})
    query['$or'].push({email: new RegExp(search,'i')}) 
  }

  console.log("qyery************",JSON.stringify(query));
  User.count(query).exec(function(err, total) {
    if (err) {
        res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      User.find(query).sort({
        'created_date': -1
      }).skip(skipNo).limit(count).exec(function(err, users) {
        // console.log("users",users);
        if (err) {
         res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
        } else {
        res.send({"message": "success", "data": users,"total": total, "status_code": "200"});
        }
      })
    }

  })
}

exports.updatetimeoffRequest = function(req,res){
  var responsestatus = '';
  Timeoffrequest.findOne({_id:req.body._id}).populate('employee_id').exec(function(err,response){
      if (null!==response) {
        response.status = req.body.status;
        if(req.body.status == 'Approve'){
          var responsestatus = 'approved';
        }
        if(req.body.status == 'Reject'){
          var responsestatus = 'rejected';
        }
        response.manager_id = req.body.manager_id;
        response.accept_reject_date = req.body.accept_reject_date;
        response.manager_comment = req.body.manager_comment;
        response.save(function(err, updateResponse){
          if (!err){
              res.json({"message":"success","status_code":"200","data":updateResponse});
              var message = new Date(response.startdate).toLocaleDateString()+' - '+new Date(response.enddate).toLocaleDateString()+' is '+responsestatus+'. Reason Is : '+req.body.manager_comment +'.';
              var client = require('twilio')(constantObj.twilio.accountSid, constantObj.twilio.authToken);
              client.messages.create({
                  to: constantObj.twilio.countrycode+response.employee_id.phone,
                  from: constantObj.twilio.twilio_number, //test number of client registered in twilio
                  body: "TimeoffResponse : "+message+'\n\n'+ constantObj.twilio.twilio_signature
              }, function(err, message) {
                  if(err){
                      console.log(err);
                  }else{
                      console.log('TimeoffRequest SMS Sent');
                  }
              });

              //Send Email To Respective Employee
              var mailOptions = {
                from: constantObj.emailSettings.fromWords, // sender address
                to: response.employee_id.email, // list of receivers
                subject: "TimeoffResponse", // Subject line                
                html: "Hello "+response.employee_id.first_name.toUpperCase() + ", <p style='padding-left:10px'>TimeoffResponse : "+message+"</p>"+ constantObj.emailSettings.signature
              };  
              constantObj.transporter.sendMail(mailOptions, function(error, info){
                  if(error){
                      console.log(error);
                  }else{
                      console.log('Message sent: ' + info.response);
                  }
              });

          }         
        });
      }
  });    
};

exports.gettimeOffnotificationicon = function(data, callback) {
    var userId = data.user_id;
    var role = data.role;
    var condition = {'status':{$exists : false}, 'employee.location' : mongoose.Types.ObjectId(data.location)};
    Timeoffrequest.aggregate([{
        $lookup: {
          from: "users",
          localField: "employee_id",
          foreignField: "_id",
          as: "employee"
        }
    }, {
        $match: condition
    }]).exec(function(err,response){
      console.log(err, response);
        response.map(function(item) {
          item.employee_id = item.employee[0];  
        })
        if (err)
            callback({"message": "error", "data": err, "status_code": "500"});
        else
            callback({"message": "success", "data": response.length, "status_code": "200"});
    });  
};