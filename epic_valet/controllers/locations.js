var Location = require("mongoose").model("Location");
var mongoose = require('mongoose');
exports.addlocation = function(req,res){
  Location(req.body).save(function(err,response){
    if(!err) {
      res.json({"message": "success", "status_code": "200"});
    }else{
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    }
  });    
};

exports.getLocationlist = function(req, res){
    var page = req.body.page || 1,
    count = req.body.count || 10;
    var role = req.body.role;
    var sort = req.body.sortOrder;
    var sort_field =  req.body.field;
    var skipNo = (page - 1) * count;
    var search = req.body.search || "";
    var sortObject = {};
    var stype = sort_field;
    var sdir = sort;
    sortObject[stype] = sdir;
    var query = {'is_deleted':false}; 
    if (search) {
      query['$or'] = [];
      query['$or'].push({title: new RegExp(search,'i')});
    }
    Location.count(query).exec(function(err, total) {
      if (err) {
          res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
      } 
      else{
        Location.find(query).sort(sortObject).skip(skipNo).limit(count).exec(function(err, locationlists) {
          if (err) {
           res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
          } else {
          res.send({"message": "success", "data": locationlists,"total": total, "status_code": "200"});
          }
        })
      }
    });
};

exports.getAllLocations = function(req, res){
  Location.find({'is_deleted':false, 'active':true}).exec(function(err,locations){
    if (err) {
      res.status(500).jsonp({"message": "Something went wrong!", "err": err});
    } else {
      res.status(200).jsonp({"message": "success", "data": locations});
    }
  });
}

exports.update_location_status = function(req, res) {
  var update = {};
  var data = req.body.data
  update.active = (data.active) ? false : true;
  Location.findOneAndUpdate({
    _id: data._id
  }, update, {new:true}).exec(function(error, locations) {
    if (error) {
      res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    } else {
      res.send({
        "message": "success",
        "data": locations,
        "status_code": "200"
      });
    }
  })
}

/*enable/disable locations*/
exports.enable_disable_Locations = function(req, res) {
  var enabled = req.body.enabled;
  var selAll = req.body.allChecked;
  var query = {};
  var fields = {};
  query._id = {
      $in: req.body.locations
  };
  if (enabled == true) {
      fields.active = true;
  } else {
      fields.active = false;
  }
  Location.update(query, fields, {
    multi: true
  }).exec(function(error, locations) {
    if (error) {
      return res.send({
        "message": "Something went wrong!",
        "err": err,
        "status_code": "500"
      });
    }
    Location.find({is_deleted:false}, function(err, locations) {
        if (error) {
          res.send({
            "message": "Something went wrong!",
            "err": err,
            "status_code": "500"
          });
        } else {
          res.send({
            "message": "success",
            "data": locations,
            "status_code": "200"
          });
        }
      })
  })
}



exports.deleteLocationList=function(req,res){
        var query = {};
        var fields = {};
        query._id = {
            $in: req.body.locationid
        };
        fields.is_deleted = true;
        Location.update(query, fields, {
          multi: true
        }).exec(function(error, locations) {
          if (error) {
            return res.send({
              "message": "Something went wrong!",
              "err": err,
              "status_code": "500"
            });
          }
          Location.find({}, function(err, locations) {
              if (error) {
                res.send({
                  "message": "Something went wrong!",
                  "err": err,
                  "status_code": "500"
                });
              } else {
                res.send({
                  "message": "success",
                  "data": locations,
                  "status_code": "200"
                });
              }
            })
        })
}
exports.IdBasedLocation = function(req, res){
  Location.find({'is_deleted':false, 'active':true, '_id':{$in:req.body.LocationIds}}).exec(function(err,locations){
    if (err) {
      res.send({"message": "Something went wrong!", "err": err, "status_code": "500"});
    } else {
      res.send({"message": "success", "data": locations,"status_code": "200"});
    }
  });
}


