var mongoose = require("mongoose");
var breakSchema = new mongoose.Schema({ startTime: Date, endTime: Date, timediff : Number});
var questionSchema = new mongoose.Schema({question:String, answer:String});
var timesheetSchema = mongoose.Schema({
        userId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        },
        eventId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Event'
        },
        revenueType : String,
        revenueRate : Number,
        revenueFixed : Number, // In case of flat_rate
        shift:String,
        location_coordinates:{type:[Number],index:'2dsphere'},
        created_by: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        },
        clockInDateTime: Date,
        clockOutDateTime: Date,
        alignScheduleTime : String,
        breaks: [breakSchema],
        totalBreakTime:Number, // Milliseconds
        questions:[questionSchema],
        ticketControl: {
            type: Boolean,
            default: false
        },
        totalTimeDiff:Number, // Milliseconds
        payrate : Number, // In Dollars
        payroll : Number, // In Dollars
        created_date : Date,
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location'
        }
});
var Timesheet = mongoose.model("Timesheet", timesheetSchema);