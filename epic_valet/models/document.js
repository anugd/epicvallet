var mongoose = require("mongoose");
var fileSchema = new mongoose.Schema({ filename: String, filepath : String, filetype : String, is_deleted: {type: Boolean, default: false}});
var linkSchema = new mongoose.Schema({ linktitle:String, link: String, is_deleted: {type: Boolean, default: false}});
var documentSchema = mongoose.Schema({
        foldername: String,
        links:[linkSchema],
        my_file: [fileSchema],
        creator_name: String,
        created_by: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        },
        created_date: Date,
        is_deleted: {
            type: Boolean,default: false
        },
        checkOnTop : {
         type: Boolean,default: false
        }
});

var Document = mongoose.model("Document", documentSchema);