var mongoose = require("mongoose");
var bufferSchema = new mongoose.Schema({ 
    day: String,
    timeslot: String,
    buffer: Number,
    last_modified_date : {
            type:Date,
            default: Date.now
    },
    last_modified_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
    }
});
var timeslotbufferSchema = mongoose.Schema({
    location: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location'
    },
    buffervalues: [bufferSchema],
});



var Timeslotbuffer = mongoose.model("Timeslotbuffer", timeslotbufferSchema);