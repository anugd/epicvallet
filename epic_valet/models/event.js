var mongoose = require("mongoose");
var shiftsSchema = new mongoose.Schema({ 
    startShiftTime: String,
    endshiftTime :String
});
//var shiftTemplateSchema = new mongoose.Schema({ day: String, timeslot: String, eventid: mongoose.Schema.Types.ObjectId, shiftid: String, shifts : [shiftsSchema]});
var shiftTemplateSchema = new mongoose.Schema({ 
    day: String,
    timeslot: String,
    shifts : [shiftsSchema]
});
var extrashiftTemplateSchema = new mongoose.Schema({ 
    shiftdate: Date,
    day: String,
    timeslot: String,
    shifts : [shiftsSchema]
});
var parkingPricesSchema = new mongoose.Schema({
    day: String,
    timeslot: String,
    price: String
});
var payRatesSchema = new mongoose.Schema({
    day: String,
    timeslot: String,
    price: String
});
var eventSchema = mongoose.Schema({
    shift_template_id: [shiftTemplateSchema],
    extrashifts:[extrashiftTemplateSchema],
    set_up_person: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    is_repeat: Boolean,
    accountId: {type: Number},
    showendtime: {type:Boolean, default: false},
    name: String,
    location: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location'
    },
    description: String,
    start_date: Date,
    end_date: Date,
    basic_pay_rate:Number, // used in case of payment
    radius:Number,
    is_active: Boolean,
    event_data: {
        created_at: Date,
        updated_at: Date,
        event_type: String,
        booked_by: String,
        equipment: String,
        equipment_needed: String,
        equip_assigned : {type:Boolean, default: false},
        invoice: String,
        contact_name: String,
        contact_phone: String,
        contact_email: String,
        on_call : {type:Boolean, default: false},
        notes: String,
    },
    location_address: {
        address: String,
        lat: String,
        lng: String
    },
    location_coordinates:{type:[Number],index:'2dsphere'},
    billing_address: {
        address: String,
        lat: String,
        lng: String
    },
    parking_prices : [parkingPricesSchema],
    pay_rates : [payRatesSchema],
    questions:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Question'
    }],
    revenue_type: {type: String, enum: ['per_car', 'flat_rate', 'rate_per_hour']},
    flat_rate : Number,
    rate_per_hour : Number,
    ticket_control: {type:Boolean, default: true},
    text_reminder: {type:Boolean, default: true}
});
var Event = mongoose.model("Event", eventSchema);