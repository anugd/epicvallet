var mongoose = require("mongoose");
var itemSchema = new mongoose.Schema({ description : String, created_date: Date, created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
}, is_deleted:false});
var whiteboardSchema = mongoose.Schema({
    topicname: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Whiteboardtopic'
    },
    whiteboard_items : [itemSchema],
    created_date: Date,
    modified_date: Date,
    created_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
    },
    location: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location'
    },
    is_deleted: {
        type: Boolean,default: false
    }
});
var Whiteboard = mongoose.model("Whiteboard", whiteboardSchema);