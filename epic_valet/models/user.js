var mongoose = require("mongoose"),
    encrypt = require("../utilities/encryption");

var userSchema = mongoose.Schema({
  first_name: {
    type: String,
    require: "{PATH} is required"
  },
  last_name: {
    type: String,
    require: "{PATH} is required"
  },
  email: {
    type: String,
    require: "{PATH} is required",
    unique: true
  },
  salt: {
    type: String,
    require: "{PATH} is required"
  },
  password: {
    type: String,
    require: "{PATH} is required"
  },
  roles: [String],
  employeeid: {
    type: Number,
      },
  address: {
    line1: String,
    line2: String,
    city: String,
    state: String,
    zip: String
  },
  active: {
    type: Boolean,
    default: false
  },
  is_deleted: {
    type: Boolean,default: false
  },
  phone: String,
  password_change: {
    type: Boolean,
    default: true
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  location: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Location'
  },
  locationtimezone : String,
  created_date: Date,
  date_of_birth: Date,
  date_of_hiring:Date,
  emergency_contact_name:String,
  emergencyphone:String,
  driver_license: String,
  last_login: Date,
  prof_image: String,
  logged_in: {
    type: Boolean,
    default: false
  },
  notificationalert: {
    type: Boolean,
    default: false
  },
  tradesubscribe: {
    type: Boolean,
    default: true
  },
  schedulelocked:{
    type: Boolean,
    default: false
  },
  scheduleunlockedBy : {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});
userSchema.methods = {
  authenticate: function(password) {
    return encrypt.hashPassword(this.salt, password) === this.password;
    //return true;
  },
  hasRole: function(role) {
    return this.roles.indexOf(role) > -1;
  }
};
var User = mongoose.model("User", userSchema);

