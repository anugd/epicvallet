var mongoose = require("mongoose");
var rejectedtradeSchema = mongoose.Schema({
        userId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        },
        tradeId : mongoose.Schema.Types.ObjectId,
        eventId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Event'
        },
        scheduleId: {
                type: mongoose.Schema.Types.ObjectId
        },
        shiftId: {
                type: mongoose.Schema.Types.ObjectId
        },
        shiftTime:String,
        endshiftTime : String, // If schedule is posted by admin on board.
        day_of_week : String, // If schedule is posted by admin on board.
        week_of : String, // If schedule is posted by admin on board.
        tradePosttype:String, // onboard || coworker
        coWorkerUserId : {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        },
        comment : String,
        trade_conditional : {
            type: Boolean,default: false
        },
        eventScheduleDate : Date,
        timeslot : String,
        tradeDateTime : {
                type:Date,
                default: Date.now
        },
        status : String, // 'Waiting' | 'Approve' | 'Reject' 
        manager_comment : String,
        updatedAt : Date,
        exchangeshift: String,
        exchangeScheduleDate : Date,
        exchangeSchId : {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Schedule'
        },
        exchangeEventAccId : {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Event'
        },
        approveRejectBy:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        },
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location'
        }
});
var Rejectedtrade = mongoose.model("Rejectedtrade", rejectedtradeSchema);