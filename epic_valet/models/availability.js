var mongoose = require("mongoose");
var availabilitySchema = mongoose.Schema({
        employee_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}, 
        currDate: Date,
        morning_schedule_time: Date,
        afternoon_schedule_time: Date,
        night_schedule_time: Date,
        late_night_schedule_time: Date,
        is_morning_scheduled: Boolean,
        is_afternoon_scheduled: Boolean,
        is_night_scheduled: Boolean,
        is_late_night_scheduled: Boolean,
        is_morning_scheduled_type: String,
        is_afternoon_scheduled_type: String,
        is_night_scheduled_type: String,
        is_late_night_scheduled_type: String,
        is_morning_scheduled_force: Boolean,
        is_afternoon_scheduled_force: Boolean,
        is_night_scheduled_force: Boolean,
        is_late_night_scheduled_force: Boolean,
        modified_date: Date,
        modified_by: {type:mongoose.Schema.Types.ObjectId,ref:'User'}
});

var Availability = mongoose.model("Availability", availabilitySchema);