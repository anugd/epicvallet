var mongoose = require("mongoose");
var geoSchema = mongoose.Schema({
        radius:Number,
        startTime:Number,
        endTime:Number,
        created_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        created_date: {
            type: Date,
            default: Date.now
        },
        modified_date: {
            type: Date,
            default: Date.now
        }
});
var Geolocation = mongoose.model("Geolocation", geoSchema);