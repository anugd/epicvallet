var mongoose = require("mongoose");
var notificationSchema = mongoose.Schema({
  memo_id: mongoose.Schema.Types.ObjectId,
  receiver_id: mongoose.Schema.Types.ObjectId,
  message: String,
  sender_id: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'User'
  },
  sender_name: String,
  sent_date: Date,
  marked_received_date: Date,
  is_marked: Boolean,
  notification_type: {type: String, enum: ['Availability']}
});

var Notification = mongoose.model("Notification", notificationSchema);

