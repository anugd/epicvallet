var mongoose = require("mongoose");
var missedpunchSchema = mongoose.Schema({
    missedPunchOut:Number,
    ClockInDate: Date,
    missedPunchTimesheets:[{
    	timesheetId:{
        	type: mongoose.Schema.Types.ObjectId,
        	ref: 'Timesheet'
    	},
    	clockInDateTime : Date,
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location'
        }
    }],
    created_date: {
            type: Date,
            default: Date.now
    }
});
var Missedpunch = mongoose.model("Missedpunch", missedpunchSchema);