var mongoose = require("mongoose");
var questionSchema = new mongoose.Schema({ 
    questionId : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Evalquestion'
    },
    evalvalue: String
});
var evalSchema = mongoose.Schema({
    employee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    manager: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    questions : [questionSchema],
    evaluation : Number,
    created_date: {
        type: Date,
        default: Date.now
    },
    currDate: {
        type: Date
    },
    active: {
        type: Boolean,
        default: true
    }
});
var Employeeeval = mongoose.model("Employeeeval", evalSchema);