var mongoose = require("mongoose");
var questionSchema = mongoose.Schema({
    title: {
        type:String,
        required: 'Please enter the title.'
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: 'Please enter created_by field.'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    active: {
        type: Boolean,
        default: true
    },
    is_deleted: {
        type: Boolean,default: false
    }
});
var Question = mongoose.model("Question", questionSchema);