var mongoose = require("mongoose");
var whiteboardtopicSchema = mongoose.Schema({
    topicname: {
    	type:String,
    	required: 'Please enter the topicname.'
    },
    created_date: {
	    type: Date,
	    default: Date.now
	},
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: 'Please enter created_by field.'
    },
    location: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Location'
    },
    is_deleted: {
        type: Boolean,default: false
    }
});
var Whiteboardtopic = mongoose.model("Whiteboardtopic", whiteboardtopicSchema);