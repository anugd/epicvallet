var mongoose = require("mongoose");
var reportSchema = mongoose.Schema({
        employee_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}, 
        startWeekDate: Date,
        endWeekDate: Date,
        comment : String,
        created_date : Date  
});


var Report = mongoose.model("Report", reportSchema);