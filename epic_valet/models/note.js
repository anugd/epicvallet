var mongoose = require("mongoose");
var noteSchema = new mongoose.Schema({ noteText : String, added_date: Date, added_by : {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }});
var notesSchema = mongoose.Schema({
        notes: [noteSchema],
        created_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        employee: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        created_date: Date,
        is_deleted: {
            type: Boolean,default: false
        }
});
var Note = mongoose.model("Note", notesSchema);