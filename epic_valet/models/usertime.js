var mongoose = require("mongoose");
var usertimeSchema = mongoose.Schema({
	employee_id : {
    	type: mongoose.Schema.Types.ObjectId,
    	ref: 'User'
  	},
  	InActiveFrom : Date,
  	ActiveFrom : Date,
  	InActiveDays : Number
});

var Usertime = mongoose.model("Usertime", usertimeSchema);