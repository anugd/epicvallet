var mongoose = require("mongoose");
var rankingSchema = mongoose.Schema({
        manager: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}, 
        event_account: {type: mongoose.Schema.Types.ObjectId, ref: 'Event'},
        employee: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},  
        currDate: {type:Date, default: Date.now},
        value : Number,
        writeup_type: String,       
        reason: String,
        writeupDate : {type:Date, default: Date.now},
        created_date: {
                type: Date,
                default: Date.now
        },
        location: {
	        type: mongoose.Schema.Types.ObjectId,
	        ref: 'Location'
	    }
});

var Ranking = mongoose.model("Ranking", rankingSchema);