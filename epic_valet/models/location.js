var mongoose = require("mongoose");
var locationSchema = mongoose.Schema({
    title: {
        type:String,
        required: 'Please enter the title.'
    },
    locationtimezone : {
        type : String
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: 'Please enter created_by field.'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    active: {
        type: Boolean,
        default: true
    },
    is_deleted: {
        type: Boolean,default: false
    }
});
var Location = mongoose.model("Location", locationSchema);