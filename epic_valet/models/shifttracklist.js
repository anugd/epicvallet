var mongoose = require("mongoose");
var shifttracksubSchema =  new mongoose.Schema({
        make:String,
        license:String,
        created_date: {
           type: Date,
           default: Date.now
        }
});
var shifttrackSchema = mongoose.Schema({
        trackList: [shifttracksubSchema],
        clockTimesheetId:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Timesheet'
        }
});
var ShiftTrackList = mongoose.model("ShiftTrackList", shifttrackSchema);