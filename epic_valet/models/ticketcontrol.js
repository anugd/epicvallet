var mongoose = require("mongoose");
var ticketcontrolSchema = mongoose.Schema({
    eventId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Event'
    },
    ticketadded: Number,
    prevCount: Number,
    endCount:Number,
    vips:Number,
    viplist:String,
    shift:String,
    equipNeeds:String,
    notes:String,
    totalChargedCars:Number,
    cashDeposit:Number, //In Dollars
    revenueType : String,
    revenueRate : Number,
    revenue:Number,
    ccc:Number, //In Dollars
    created_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
    },
    dateOfReport:Date,
    created_date : {
            type:Date,
            default: Date.now
    }
});
var Ticketcontrol = mongoose.model("Ticketcontrol", ticketcontrolSchema);