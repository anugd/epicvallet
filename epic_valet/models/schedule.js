var mongoose = require("mongoose");
var scheduleSchema = mongoose.Schema({
    employee_id : {type: mongoose.Schema.Types.ObjectId, ref: 'User'}, 
    current_date : Date,
    event_account_id :  {type: mongoose.Schema.Types.ObjectId, ref: 'Event'},
    aligned_by : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    week_of : Date,
    day_of_week :String,
    shift_id : {type: mongoose.Schema.Types.ObjectId},
    timeslot : String,
    scheduleDate : Date,
    shiftReminderTime : String, // shiftstarttime
    shiftEndTime : String, // shiftendtime
    is_approve : {
        type: Boolean, default: false
    },
    is_approve_date : Date,//Acknowledge Date
    created_date : {
           type: Date,
           default: Date.now
    },
    schnotes : String,
    checkInOverlap : {
         type: Boolean,default: false
    }
});
var Schedule = mongoose.model("Schedule", scheduleSchema);