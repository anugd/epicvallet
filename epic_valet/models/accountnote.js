var mongoose = require("mongoose");
var noteSchema = new mongoose.Schema({ noteText : String, added_date: Date});
var notesSchema = mongoose.Schema({
        notes: [noteSchema],
        created_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        event_account: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Event'
        },
        created_date: Date,
        is_deleted: {
            type: Boolean,default: false
        }
});
var Accountnote = mongoose.model("Accountnote", notesSchema);