var mongoose = require("mongoose");
var initialzipSchema = mongoose.Schema({
    initial: {
        type:String,
        required: 'Please enter the title.'
    },
    zipcodes: { 
        type:String,
        required: 'Please enter the zipcodes.'
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: 'Please enter created_by field.'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    active: {
        type: Boolean,
        default: true
    },
    is_deleted: {
        type: Boolean,default: false
    }
});
var Initialzip = mongoose.model("Initialzip", initialzipSchema);