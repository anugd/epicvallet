var mongoose = require("mongoose");
var schtextSchema = mongoose.Schema({
        title: String,
        description: String,
        created_by_name: String,
        created_date: Date,
        cronTimestr : String,
        cronExestr : String,
        texttype: String, // EventBased || EmployeeBased

        // Start-------- In case of EventBased -------------
        eventAccountId:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Event'
        }, 
        // End-------- In case of EventBased -------------

        // Start-------- In case of EmployeeBased -------------
        employeeId:[{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        }], 
        groupId:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Memogroup'
        },
        // End-------- In case of EmployeeBased -------------

        texttime: String, // now || later || repeat
        //In Case of now or later
        schDate : Date,
        //In Case of repeat
        weekdy : Number,
        schRepeattime : String,
        // -------- In case of EmployeeBased -------------

        CronStatus: { type: Boolean, default: false }, // true when it will be sent
        status: { type: Boolean, default: true }
});

var Scheduletext = mongoose.model("Scheduletext", schtextSchema);