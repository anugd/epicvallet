var mongoose = require("mongoose");
var todoSchema = new mongoose.Schema({ todoText : String, assign_date: Date, assign_by: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        }, due_date :Date, completion_date : Date, done: Boolean});
var todolistSchema = mongoose.Schema({
        listname: String,
        todo_items: [todoSchema],
        created_by: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        },
        created_date: Date,
        assign_to: [{user_id: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        }}],
        comment : String,
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location'
        },
        is_deleted: {
            type: Boolean,default: false
        }
});

var Todo = mongoose.model("Todo", todolistSchema);