var mongoose = require("mongoose");
var rateSchema = new mongoose.Schema({ 
    price : String,
    effective_date : Date,
    created_date: {
            type: Date,
            default: Date.now
    }
});
var exceptionSchema = mongoose.Schema({
        employee: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        event_account: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Event'
        },
        created_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        rates: [rateSchema]
});
var Exceptionrate = mongoose.model("Exceptionrate", exceptionSchema);