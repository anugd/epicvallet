var mongoose = require("mongoose");
var memogroupSchema = mongoose.Schema({
    groupname: {
    	type:String,
    	required: 'Please enter the topicname.'
    },
    employees: [{user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'},
        phone:String
    }], 
    created_date: {
	    type: Date,
	    default: Date.now
	},
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: 'Please enter created_by field.'
    },
    is_deleted: {
        type: Boolean,default: false
    }
});
var Memogroup = mongoose.model("Memogroup", memogroupSchema);