var mongoose = require("mongoose");
var memoSchema = mongoose.Schema({
        title: String,
        description: String,
        my_file: Array,
        creator_name: String,
        created_by: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
        },
        created_date: Date,
        mins:Number,
        sent_to: [{user_id: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
                }, 
                mark_read: {type: Boolean, default: false}, read_date: Date
        }],
        admin_checked: { type: Boolean, default: false }
});

var Memo = mongoose.model("Memo", memoSchema);