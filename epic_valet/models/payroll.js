var mongoose = require("mongoose");
var payrollSchema = mongoose.Schema({
    owner_id: mongoose.Schema.Types.ObjectId,
    emp_id: {type:mongoose.Schema.Types.ObjectId,ref: 'User'},
    accounttype: {type:mongoose.Schema.Types.ObjectId,ref: 'Client'},
    pay_rate: String,
    total_hours: String
});
var Payroll = mongoose.model("Payroll", payrollSchema);