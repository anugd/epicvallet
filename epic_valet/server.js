var express = require("express");
var path = require("path");
var env = process.env.NODE_ENV = process.env.NODE_ENV || "development";
var app = express();
var config = require("./config/config")[env];
/** For Socket.io **/
var server = require('http').Server(app);
var io = require('socket.io')(server);
require("./config/express")(app, config);
require("./config/mongoose")(config);
require("./config/passport")();
require("./config/cron")();

require("./config/routes")(app, config, io);
app.use('/public', express.static(path.join(__dirname, '/uploads')));
server.listen(4011);
console.log("Listening on port " + config.port + "...");